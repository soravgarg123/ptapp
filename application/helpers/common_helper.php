<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


if ( ! function_exists('get_categoryTitleById'))
{
  function get_trainerTitleById($Id){ 
        $ci=& get_instance();
        $ci->load->database(); 
    
    $query = $ci->db->where('id',$Id);
    $query = $ci->db->get('trainner');
    $category = $query->result_array();
    if(!empty($category)){
      return $category['0']['name'].' '.$category['0']['surname'] ;
    }else{
      return false;
    }
  }
}

function getcomment($Id){ 
        $ci=& get_instance();
        $ci->load->database();
    $filed ='comment.comment,comment.date_added,trainner.name';   
    $query = $ci->db->select($filed);     
    $query = $ci->db->from('comment');
    $query = $ci->db->join('trainner',"trainner.user_id = comment.user_id",'left');
    $query = $ci->db->where('article_id',$Id);
    $query = $ci->db->get();
    $comment = $query->result_array();
    if(!empty($comment)){
      return $comment;
    }else{
      return '';
    }
  }

  function getlike($Id){ 
    $ci=& get_instance();
    $ci->load->database();
    $query = $ci->db->where('article_id',$Id);
    $query = $ci->db->get('like');
    $like = $query->result_array();
    if(!empty($like)){
      return $like;
    }else{
      return '';
    }
  }

  function product_detail($section,$pid)
  {
       $ci=& get_instance();
       $query = $ci->db->where('product_id',$pid);
       $query = $ci->db->get('products');
       $product_detail=$query->result_array();
       if($section=='image')
       {
          return $product_detail[0]['image'];
       }
       if($section=='name')
       {
          return $product_detail[0]['title'];
       }
       
  }

  function product_price($pid)
  {
       $ci=& get_instance();
       $query = $ci->db->where('product_id',$pid);
       $query = $ci->db->get('products');
       $product_detail=$query->result_array();
       return $product_detail[0]['price']+$product_detail[0]['adon_price'];
       
  }

  function sendPushNotifications($title,$content,$playerids)
  {
    $headings = array("en" => $title);
    $content  = array("en" => $content);
    $fields = array(
      'app_id' => "8ead9473-f4de-4493-b123-cd8b30a8d773", // One Signal App Id
      'data' => array("foo" => "bar"),
      'headings' => $headings,
      'contents' => $content
    );
    if($playerids == 'All'){
      $fields['included_segments']  = array('All');
    }else{
      $fields['include_player_ids'] = $playerids;
    }
    $fields = json_encode($fields);
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, "https://onesignal.com/api/v1/notifications");
    curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json',
                           'Authorization: Basic ZWJmMmE4M2ItOWJkNS00Y2ZjLWI2NzgtYTFkMDIwODZkNzE2')); // REST API Key
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
    curl_setopt($ch, CURLOPT_HEADER, FALSE);
    curl_setopt($ch, CURLOPT_POST, TRUE);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
    $response = curl_exec($ch);
    curl_close($ch);
    return $response;
  }


if(!function_exists('imgUpload'))
{
  function imgUpload($name,$hidden_img)
  {
        $image = $_POST[$hidden_img];
        $filename = $_FILES[$name]['name'];
        if(!empty($filename))
          {
            $path = "images/" . $image;
            unlink($path);
            $f_name = $_FILES[$name]['name'];
            $f_tmp = $_FILES[$name]['tmp_name'];
            $f_size = $_FILES[$name]['size'];
            $f_extension = explode('.',$f_name); 
            $f_extension = strtolower(end($f_extension));
            $f_newfile="";
            if($f_name){
            $f_newfile = uniqid().'.'.$f_extension; 
            $store = "images/" . $f_newfile;
            $image =  move_uploaded_file($f_tmp,$store);
            }
          }
        else
          {
            $f_newfile = $image;
          }
         return $f_newfile;
  }

  function buyer_mail()
 {
  $ci=& get_instance();

  $message = '<html>
              <head>
              </head>
              <body>
                  <table cellspacing="0" cellpadding="0" border="0" align="center" style="width:100%;cellpadding:5px;"style="border-radius:4px;border:1px #dceaf5 solid">
                      <tbody>
                         
                          <tr style="line-height:0px">
                              <td colspan="5" width="100%" height="1" align="center" style="font-size:0px">
                                  <!-- <img class="CToWUd" width="40px" src="https://ci6.googleusercontent.com/proxy/qWDXNAP_TOSBYiPU0LLlaL20XyvHYpR5Wjt7ZrwC7sFGXFV9xwWcU6EZz4ADjZGpDlKQpKcYslWST3yhyOTp_Qja9pmMsiVEEC_kBkYk2erqGyElr0-6YQ=s0-d-e1-ft#https://www.dropbox.com/static/images/emails/glyph/glyph_34@2x.png" alt="" style="max-height:73px;width:40px"> -->
                              </td>
                          </tr>
                          <tr>
                              <td colspan="5"> <h3> Order Detail</ </td>
                          </tr> <tr>
                              <td > Product Image </td><td > Product Name </td><td > Price </td><td > Qty </td><td > Total </td>
                          </tr>';
                          $cart_detail=$ci->cart->contents();
         if(count($cart_detail)>0){
                           
                            $total=0;
                            $cartind=1;
                            foreach ($cart_detail as $key => $cart) {
                             $total+=$cart['subtotal'];
                               
                         
                          $message .= '<tr>  <td><img src="'.product_detail('image',$cart['id']).'" style="width:80px;height:80px"> </td>
                                <td><div class="thumb_details">
                                        <a href="#">'.product_detail('name',$cart['id']).'</a>
                                    </div></td>
                                <td>'.$cart['price'].'USD </td>
                                <td>'.$cart['qty'].'</td>
                                <td>'.$cart['subtotal'].'USD</td </tr>';
                           $cartind++; } } 




                    $message .='  </tbody>
                  </table>
              </body>
              </html>';

        $from="omar@afn.uk.com";
        $headers = "MIME-Version: 1.0" . "\r\n";
        $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
        $headers .= 'From: '.$from."\r\n".'Reply-To: '.$from."\r\n" .'X-Mailer: PHP/' . phpversion();
        $ship_id=$ci->session->userdata['shiping']['ship_id'];
        $shiping_detail=$ci->product_model->get_recordByid('shiping_master',array('ship_id'=>$ship_id));
        $to=$shiping_detail[0]['email'];
            mail($to,"Ptapp Order.",$message,$headers);

 }


 function admin_mail()
 {
  $ci=& get_instance();

  $message = '<html>
              <head>
              </head>
              <body>
                  <table cellspacing="0" cellpadding="0" border="0" align="center" style="width:100%;cellpadding:5px;"style="border-radius:4px;border:1px #dceaf5 solid">
                      <tbody>
                         
                          <tr style="line-height:0px">
                              <td colspan="5" width="100%" height="1" align="center" style="font-size:0px">
                                 <h2> You Recieve New Order</h2>
                              </td>
                          </tr>
                          <tr>
                              <td colspan="5"> <h3> Order Detail</ </td>
                          </tr> <tr>
                              <td > Product Image </td><td > Product Name </td><td > Price </td><td > Qty </td><td > Total </td>
                          </tr>';
                          $cart_detail=$ci->cart->contents();
         if(count($cart_detail)>0){
                           
                            $total=0;
                            $cartind=1;
                            foreach ($cart_detail as $key => $cart) {
                             $total+=$cart['subtotal'];
                               
                         
                          $message .= '<tr>  <td><img src="'.product_detail('image',$cart['id']).'" style="width:80px;height:80px"> </td>
                                <td><div class="thumb_details">
                                        <a href="#">'.product_detail('name',$cart['id']).'</a>
                                    </div></td>
                                <td>'.$cart['price'].'USD </td>
                                <td>'.$cart['qty'].'</td>
                                <td>'.$cart['subtotal'].'USD</td </tr>';
                           $cartind++; } } 




                    $message .='  </tbody>
                  </table>
              </body>
              </html>';

        $from="omar@afn.uk.com";
        $headers = "MIME-Version: 1.0" . "\r\n";
        $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
        $headers .= 'From: '.$from."\r\n".'Reply-To: '.$from."\r\n" .'X-Mailer: PHP/' . phpversion();
        $to="omar@afn.uk.com";
        
            mail($to,"Ptapp Order.",$message,$headers);

 }




}
