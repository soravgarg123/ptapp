<?php defined('BASEPATH') OR exit('No direct script access allowed');
class Admin extends CI_Controller
{
    
    function __construct()
    {
        
        parent::__construct();
        $this->load->model('login_model');
    }
    
        function index()
    {
        
        $data['title'] = array(
            "Login page"
        );
        $this->load->view('admin/login', $data);
        
    }
  
   
    
    function login()
    {
        $data['title'] = array(
            "Login page"
        );
        $this->load->view('admin/login', $data);
    }
  



     // product 

    function products()
    {
        $offset=0;
        $limit=1000000;
        $where = array(
			'status'=>1
			);
        $data['product_list']=$this->product_model->get_all_product('products',$where,'products.product_id,products.adon_price,products.product_category_path,products.flavor,products.title,products.brand,products.image,products.price','products.product_id',$limit,$offset);
        
         $data['title'] = array(
            'Products manage page'
        );
        
      
     
       $this->template->load('default', 'products', $data);
      
    }

   //order section 
    function orders()
    {
    	$offset=0;
        $limit=1000000;
        $where = array('status'=>1);
        $data['order_list']=$this->product_model->get_orders('product_order_master','order_id','desc');
        $data['title'] = array('order page');
       
        $this->template->load('default', 'orders', $data);
    }


    function order_items()
	{
		 $order_id=$_POST['orderId'];
         $product_item_list=$this->product_model->get_recordByid('product_order_items',array('item_order_id'=>$order_id));
         if(count($product_item_list)>0)
         {
         	$responce="";
         	foreach($product_item_list as $item)
         	{
         		$qty=$item['product_qty'];
         		 $product_detail=$this->product_model->get_recordByid('products',array('product_id'=>$item['product_id']));
                $responce.='<div class="row"> <div class="col-md-2"><img src="'.$product_detail[0]['image'].'" style="width:80px;height:60px;"> </div>
                    <div class="col-md-3" style="padding-top:15px"><b>'.$qty.' X $'.$product_detail[0]['price'].'</b></div><div class="col-md-4" style="padding-top:15px">'.$product_detail[0]['title'].'</div><div class="col-md-3" style="padding-top:15px"><b> $'.$qty * $product_detail[0]['price'].'</b> </div></div>';
                    
         	}

         }
         echo $responce;
        
	}

  function payment_order()
  {
  	 $paymentId=$_POST['payment_id'];
         $product_item_list=$this->product_model->get_recordByid('product_payment_master',array('payment_id'=>$paymentId));
         $responce="";
         if(count($product_item_list)>0)
         {
         	$responce="";
         	foreach($product_item_list[0] as $key => $item)
         	{
         		
                $responce.='<div class="row"> <div class="col-md-4"><b>'.strtoupper(str_replace('_',' ',$key)).'</b></div>
                            <div class="col-md-6" >'.$product_item_list[0][$key].'</div></div>';
                    
         	}

         }
         else{
             $responce="Payment Detail Not Found";
         } 
         echo $responce;
  }

  //Payment
    
    function payments()
    {
    	
        $data['payment_list']=$this->product_model->get_all_records('product_payment_master');
        $data['title'] = array('Payment page');
        $this->template->load('default', 'payments', $data);
    }






     
     function  update_product_price()
     {
        
        $this->product_model->updates('products',array('adon_price'=>$this->input->post('product_adon_price')),array('product_id'=>$this->input->post('product_id')));
        $this->session->set_flashdata('success','Product Adon Price Updated Successfully.');
        redirect('admin/admin/products');
     }



    function checklogin()
    {
        
        $this->form_validation->set_rules('username', 'username', 'required');
        $this->form_validation->set_rules('password', 'password', 'required|callback_verifyUser');
        if ($this->form_validation->run() == false) {
            $this->load->view('admin/login');
        } else {
			redirect('admin/admin/trainers','refresh');
        }
        
        
    }
    
    
    function dashboard()
    {
        $data['title']  = array(
            'Dashboard'
        );
        $data['contro'] = array(
            'dashboard'
        );
        $this->load->view('admin/index', $data);
    }



    
    function verifyUser()
    {
        
        $username = $this->input->post('username');
        $password = md5($this->input->post('password'));
        
        if ($this->login_model->login($username, $password)) {
            return true;
        } else {
			
            $this->form_validation->set_message('verifyUser', "<p style='color:white;font-size:20px;' align='center'>Incorrect usernsme and password</p>");
            return false;
        }
    }
    
    function logout()
    {
        
        $this->session->unset_userdata('username');
        session_destroy();
        redirect('admin/admin/login');
    }
    
    
    function register()
    {
        $data['title'] = array(
            "register"
        );
        $this->load->view('admin/register', $data);
        
        if ($this->input->post('insert')) {
            $this->load->model('admin/login_model');
            $this->login_model->registration();
            
        }
    }
    
    
    
    function forget_password()
    {
        $data['title'] = array(
            "forget page"
        );
        $this->load->view('forget_email', $data);
        if ($this->input->post('login')) {
            $this->login_model->forget_password();
        }
    }
    
    function reset()
    {
        $data['title'] = array(
            "Reset page"
        );
        $this->load->view('reset_password_page', $data);
    }
    
    function reset_password()
    {
        $this->load->view('reset_password_page');
    }
    
    function update_password()
    {
        
        $data['title'] = array(
            'Reset password page'
        );
        $this->load->view('admin_panel/reset_password_page', $data);
        if ($this->input->post('submit')) {
            $this->login_model->update_password();
        }
        
    }
    function profile()
    {		
		 $id = $this->session->userdata('user_id');
		
        $this->form_validation->set_rules('fullname', 'Full Name', 'required');
        $this->form_validation->set_rules('email', 'Email', 'required');
		$this->form_validation->set_rules('mobile', 'Mobile', 'required');  
        $data['title'] = array(
                'add User'
            );
       if(!empty($id)){
		$user = $this->login_model->get_profile($id); 
		$data  = $user[0];
		//print_r($data); die;
		}     
       if(isset($_POST['submit']))
       {
		   
        if ($this->form_validation->run() == FALSE) { 
		
			$this->template->load('default', 'profile', $data);
       
        }else{ 
				$id = $_REQUEST['id'];
				
			if(!empty($id)){ //update data 
						
						$data              = array(
						'id'=>$id,
						'fullname' => $this->input->post('fullname'),
						'email' => $this->input->post('email'),
						'mobile' => $this->input->post('mobile'),
						'password' => md5($this->input->post('password'))
						
						);
						
						$this->session->set_flashdata('success','Profile Has Been Updated Successfully.');
						$this->login_model->profile($data);
						//$this->template->load('default', 'profile', $data);
						redirect('admin/admin/profile');
				
			}else{
			
			
			
				$data              = array(
				
						'fullname' => $this->input->post('fullname'),
						'email' => $this->input->post('email'),
						'mobile' => $this->input->post('mobile'),
						'password' => md5($this->input->post('password'))
				);
				
				$this->session->set_flashdata('success','Profile Has Been Updated Successfully.');
				$id = $this->login_model->profile($data);
				redirect('admin/admin/profile');
			}
			//upload channel_image  END  
			
					
		}
        
        }else{  
			
		 $this->template->load('default', 'profile', $data);	
		
		}
    }
      function trainers()
    {
        $data['title'] = array(
            'Trainers manage page'
        );
        
      $data['users']= $this->admin_model->get_trainers();   
     
       $this->template->load('default', 'trainers', $data);
    }

    function trainers_delete($id){
		  $this->admin_model->trainers_delete($id);
      $this->admin_model->users_delete($id);   
		$this->session->set_flashdata('success','Trainer Has Been Deleted Successfully.');
		redirect('admin/admin/trainers');
	}
    
     function clients()
    {
        $data['title'] = array(
            'clients manage page'
        );
       $data['users']= $this->admin_model->get_clients();   
       // echo "<pre/>"; print_r($data['users']); die;
       $this->template->load('default', 'clients', $data);
    }

    function users()
    {
        $data['title'] = array(
            'Users manage page'
        );
       $data['users_access']= $this->common_model->getAllData('users_access','id','ASC');   
       $this->template->load('default', 'users_access', $data);
    }
    
    public function add_user(){
    	if($this->session->userdata('user_id'))
    	{
    		$id = $this->uri->segment(4);
    		$data['users_access']= $this->common_model->getSingleData('users_access','id',$id);
    		$data['title'] = array(
	                'add User'
	            );
	        if($_POST)
	        {

	        	$this->form_validation->set_rules('email', 'Email', 'required|valid_email|is_unique[users.email]');
	        	$this->form_validation->set_rules('date_range', 'Date Range', 'required');

	        	if($this->form_validation->run() == false)
	        	{
	        		$this->template->load('default', 'add_user_access', $data);
	        	}
	        	else
	        	{
	        		$date = explode('/',$this->input->post('date_range'));
	        		$start_date =$date[0];
	        		$end_date =$date[1];

	        	if(empty($data['users_access'])){	
	        		$form_data = array(
	        				'email'	 =>	$this->input->post('email'),
	        				'start_date' =>	$start_date,
	        				'end_date'		=>$end_date,
	        				'status'		=>1,
	        				'date_added'	=>date('Y-m-d H:i:s') 
	        			);

	        		$out = $this->common_model->add('users_access',$form_data);
	        		if(!empty($out))
	        		{
	        			$this->session->set_flashdata('success', 'User Add Successfully');
	        		}
	        		else
	        		{
	        			$this->session->set_flashdata('error', 'User Could Not Added');
	        		}
	        	}else{
	        		$form_data = array(
	        				'email'	 =>	$this->input->post('email'),
	        				'start_date' =>	$start_date,
	        				'end_date'		=> $end_date,
	        				'status'		=>1,
	        				'modify_date'	=>date('Y-m-d H:i:s') 
	        			);
	        		$data['users']= $this->common_model->getSingleData('users','email',$this->input->post('email'));
	        		if(!empty($data['users'])){
	        			$this->common_model->edit('users','user_id',array('start_date' => $start_date,'expiry_date' => $end_date),$data['users'][0]['user_id']);
	        		}
	        		$out = $this->common_model->edit('users_access','id',$form_data,$id);
	        		if($out == true)
	        		{
	        			$this->session->set_flashdata('success', 'User data Updated Successfully');
	        		}
	        		else
	        		{
	        			$this->session->set_flashdata('error', 'User data Could Not Updated');
	        		}
	        	}
	        		redirect(base_url('admin/admin/add_user'));
	        	}
	        }
	        else
	        {
	    		$this->template->load('default', 'add_user_access', $data);
	        }
    	}
    	else
    	{
    		redirect(base_url());
    	}
    }

    function user_delete($id)
    {
		$responce = $this->common_model->deleteData('users_access','id',$id);
		if($responce == true){
			$this->session->set_flashdata('success','User Has Been Deleted Successfully.');
		}else{
			$this->session->set_flashdata('error', 'User Could Not Deleted');
		}   
		redirect('admin/admin/users');
		
	}

    function clients_delete($id)
    { 
		$this->admin_model->clients_delete($id);   
		$this->session->set_flashdata('success','Clients Has Been Deleted Successfully.');
		redirect('admin/admin/clients');
	}
	function services()
    {
        $data['title'] = array(
            'services manage page'
        );
        
      $data['services']= $this->admin_model->get_services();   
     
       $this->template->load('default', 'services', $data);
    }
    
    function services_delete($id)
    {
		$this->admin_model->services_delete($id);   
		$this->session->set_flashdata('success','Service Has Been Deleted Successfully.');
		redirect('admin/admin/services');
	}
     function add_services($id='')
    {
        $this->form_validation->set_rules('service_title', 'Title', 'required');
        $data['title'] = array(
                'add service'
            );
       if(!empty($id)){
		$service = $this->admin_model->get_service($id); 
		$data  = $service[0];
		}     
       if(isset($_POST['submit']))
       {
		   
        if ($this->form_validation->run() == FALSE) { 
		
			$this->template->load('default', 'add_service', $data);
       
        }else{ 
				$id = $_REQUEST['id'];
				
			if(!empty($id)){ //update data 
				
					
					//upload channel_image  start
					$config['upload_path'] = './upload/';
					$config['allowed_types'] = 'jpg|png';
					$config['max_size']	= '20000';
					$config['encrypt_name']	= true; //'encrypt_name'  => true,
					//$config['max_width']  = '1024';
					//$config['max_height']  = '768';

					$this->load->library('upload', $config);
					$this->upload->initialize($config);
					if ( ! $this->upload->do_upload('image'))
					{ 
						
						$data              = array(
						'id'=>$id,
						'service_title' => $this->input->post('service_title'),									
						'description' => $this->input->post('description'),									
						);
						
						$this->session->set_flashdata('success','Service Has been Updated Successfully.');
						$this->admin_model->add_service($data);
						//$this->template->load('default', 'add_category', $data);
						redirect('admin/admin/add_services/'.$id);
						
						
						
					}
					else
					{
						$uploadedImage = $this->upload->data(); 
						$imageName = $uploadedImage['file_name'];
						$data              = array(
						'id'=>$id,
						'service_title' => $this->input->post('service_title'),
						'description' => $this->input->post('description'),					
						'image' => $imageName
						);
						
						$this->session->set_flashdata('success','Service Has Seen Added Successfully.');
						$this->admin_model->add_service($data);
						//$this->template->load('default', 'add_category', $data);
						redirect('admin/admin/add_services/'.$id);
					}
					//upload channel_image  END  
				
				
				
				
			}else{
			
			
			//upload channel_image  start
			$config['upload_path'] = './upload/';
			$config['allowed_types'] = 'jpg|png';
			$config['max_size']	= '20000';
			$config['encrypt_name']	= true; //'encrypt_name'  => true,
		
			$this->load->library('upload', $config);
			$this->upload->initialize($config);
			if ( ! $this->upload->do_upload('image'))
			{ 
				
				$this->session->set_flashdata('error',$this->upload->display_errors());
				$this->template->load('default', 'add_service', $data);
			}
			else
			{ 
				$uploadedImage = $this->upload->data(); 
				$imageName = $uploadedImage['file_name'];
				$data              = array(
				'service_title' => $this->input->post('service_title'),
				'description' => $this->input->post('description'),	
				'image' => $imageName
				);
				
				$this->session->set_flashdata('success','Service Has Been Added Successfully.');
				$id = $this->admin_model->add_service($data);
				redirect('admin/admin/add_service');
			}
			//upload channel_image  END  
			
			} //insert
		
		}
        
        }else{  
			
		 $this->template->load('default', 'add_service', $data);	
		
		}
    }
   /****************Plans**************/
   function plans()
    {
        $data['title'] = array(
            'plans manage page'
        );
        
      $data['plans']= $this->admin_model->get_plans();   
     
       $this->template->load('default', 'plans', $data);
    }
    
    function plans_delete($id)
    {
		$this->admin_model->plans_delete($id);   
		$this->session->set_flashdata('success','Plans Has Been Deleted Successfully.');
		redirect('admin/admin/plans');
	}
     function add_plans($id='')
    {
        $this->form_validation->set_rules('plan_title', 'Title', 'required');
        $data['title'] = array(
                'add plans'
            );
       if(!empty($id)){
		$service = $this->admin_model->get_plan($id); 
		$data  = $service[0];
		}     
       if(isset($_POST['submit']))
       {
		   
        if ($this->form_validation->run() == FALSE) { 
		
			$this->template->load('default', 'add_plan', $data);
       
        }else{ 
				$id = $_REQUEST['id'];
				
			if(!empty($id)){ //update data 
				
					
					//upload channel_image  start
					$config['upload_path'] = './upload/';
					$config['allowed_types'] = 'jpg|png';
					$config['max_size']	= '20000';
					$config['encrypt_name']	= true; //'encrypt_name'  => true,
					//$config['max_width']  = '1024';
					//$config['max_height']  = '768';

					$this->load->library('upload', $config);
					$this->upload->initialize($config);
					if ( ! $this->upload->do_upload('image'))
					{ 
						
						$data              = array(
						'id'=>$id,
						'plan_title' => $this->input->post('plan_title'),									
						'description' => $this->input->post('description'),									
						'price' => $this->input->post('price'),									
						'time' => $this->input->post('time'),									
						);
						
						$this->session->set_flashdata('success','Plan Has been Updated Successfully.');
						$this->admin_model->add_plan($data);
						//$this->template->load('default', 'add_category', $data);
						redirect('admin/admin/add_plans/'.$id);
						
						
						
					}
					else
					{
						$uploadedImage = $this->upload->data(); 
						$imageName = $uploadedImage['file_name'];
						$data              = array(
						'id'=>$id,
						'plan_title' => $this->input->post('service_title'),
						'description' => $this->input->post('description'),	
						'price' => $this->input->post('price'),									
						'time' => $this->input->post('time'),					
						'image' => $imageName
						);
						
						$this->session->set_flashdata('success','Plan Has Seen Added Successfully.');
						$this->admin_model->add_plan($data);
						//$this->template->load('default', 'add_category', $data);
						redirect('admin/admin/add_plans/'.$id);
					}
					//upload channel_image  END  
				
				
				
				
			}else{
			
			
			//upload channel_image  start
			$config['upload_path'] = './upload/';
			$config['allowed_types'] = 'jpg|png';
			$config['max_size']	= '20000';
			$config['encrypt_name']	= true; //'encrypt_name'  => true,
		
			$this->load->library('upload', $config);
			$this->upload->initialize($config);
			if ( ! $this->upload->do_upload('image'))
			{ 
				
				$this->session->set_flashdata('error',$this->upload->display_errors());
				$this->template->load('default', 'add_plan', $data);
			}
			else
			{ 
				$uploadedImage = $this->upload->data(); 
				$imageName = $uploadedImage['file_name'];
				$data              = array(
				'plan_title' => $this->input->post('plan_title'),
				'description' => $this->input->post('description'),
				'price' => $this->input->post('price'),									
				'time' => $this->input->post('time'),		
				'image' => $imageName
				);
				
				$this->session->set_flashdata('success','Plan Has Been Added Successfully.');
				$id = $this->admin_model->add_service($data);
				redirect('admin/admin/add_plan');
			}
			//upload channel_image  END  
			
			} //insert
		
		}
        
        }else{  
			
		 $this->template->load('default', 'add_plan', $data);	
		
		}
    }

    /*
-----------------------Code Written by Aditya ----------------------
    */
	function people_say()
    { 
    	if($this->session->userdata('user_id'))
	    {
	    	$data['title'] = array(
            'What People Say'
        	);
        
	      	$data['people_say']= $this->admin_model->get_people_say();        
	       $this->template->load('default', 'people_say', $data);
	    }
	    else
	    {
	    	redirect(base_url());
	    }
        
    }
    public function add_people_say(){
    	if($this->session->userdata('user_id'))
    	{
    		$data['title'] = array(
	                'add People Say'
	            );
	        if($_POST)
	        {
	        	$this->form_validation->set_rules('people_type', 'People', 'required');
	        	$this->form_validation->set_rules('name', 'People Name', 'required');
	        	$this->form_validation->set_rules('what_say', 'What Say', 'required');

	        	if($this->form_validation->run() == false)
	        	{
	        		$this->template->load('default', 'add_people_say', $data);
	        	}
	        	else
	        	{
	        		$form_data = array(
	        				'people_type'	=>	$this->input->post('people_type'),
	        				'name'			=>	$this->input->post('name'),
	        				'what_say'		=>	$this->input->post('what_say'),
	        				'active'		=>	1
	        			);
	        		$out = $this->admin_model->add_people_say($form_data);
	        		if($out == true)
	        		{
	        			$this->session->set_flashdata('success', 'Service Add Successfully');
	        		}
	        		else
	        		{
	        			$this->session->set_flashdata('error', 'Service Could Not Added');
	        		}
	        		redirect(base_url('admin/admin/add_people_say'));
	        	}
	        }
	        else
	        {
	    		$this->template->load('default', 'add_people_say', $data);
	        }
    	}
    	else
    	{
    		redirect(base_url());
    	}
    }

    public function edit_people_say($id){
    	if($this->session->userdata('user_id'))
    	{
    		$data['title'] = array(
	                'Edit People Say'
	            );
    		$data['get_people_say'] = $this->admin_model->people_say_by_id($id);
    		if($_POST)
    		{
    			$this->form_validation->set_rules('people_type', 'People','required');
	        	$this->form_validation->set_rules('name', 'People Name','required');
	        	$this->form_validation->set_rules('what_say', 'What Say','required');

	        	if($this->form_validation->run() == false)
	        	{
	        		$this->template->load('default', 'add_people_say', $data);
	        	}
	        	else
	        	{
	        		$form_data = array(
	        				'people_type'	=>	$this->input->post('people_type'),
	        				'name'			=>	$this->input->post('name'),
	        				'what_say'		=>	$this->input->post('what_say'),
	        				'active'		=>	1
	        			);
	        		$out = $this->admin_model->edit_people_say($form_data, $id);
	        		if($out == true)
	        		{
	        			$this->session->set_flashdata('success', 'Service Update Successfully');
	        		}
	        		else
	        		{
	        			$this->session->set_flashdata('error', 'Service Could Not Updated');
	        		}
	        		redirect(base_url('admin/admin/edit_people_say')."/$id");
	        	}
    		}
    		else
    		{
    			$this->template->load('default', 'add_people_say', $data);
    		}
    	}
    	else
    	{
    		redirect(base_url());
    	}
    }

    public function delete_people_say($id){
    	if($this->session->userdata('user_id'))
    	{
    		$out = $this->admin_model->delete_people_say($id);
    		redirect(base_url('admin/admin/people_say'));
    	}
    	else
    	{
    		redirect(base_url());
    	}
    }

    function article(){
    	if($this->session->userdata('user_id'))
		{
			$data['title'] = array('Article/blog');
			$data['article']= $this->common_model->getAllData('article','id','desc');
			$this->template->load('default', 'article', $data);
		}
		else
		{
			redirect(base_url());
		}
    }
    public function delete_article($id){
    	$this->common_model->deleteData('article','id',$id);   
		$this->session->set_flashdata('success','Article Has Been Deleted Successfully.');
		redirect('admin/admin/article');
    }

    function add_article(){
    	if($this->session->userdata('user_id'))
		{
			 $data['id'] =$this->uri->segment(4); 
			$data['title'] = array('Article/blog');
			$data['article']= $this->common_model->getSingleData('article','id',$data['id']);
				$this->form_validation->set_rules('title', 'Title', 'required');
	      $this->form_validation->set_rules('article', 'Article', 'required');
			if($this->form_validation->run() == True){ 
         $sess_id= $_POST['id'];
			if($sess_id=='add'){
				$inserdata=array(
							'title'=>$this->input->post('title'),
							'article'=>$this->input->post('article'),
							'date_added'=>date('Y-m-d h-i-s'),
							);
        //print_r($inserdata); die;
				$this->common_model->add('article',$inserdata);
				$this->session->set_flashdata('success', 'Article Added Successfully');
				redirect(base_url('admin/admin/article'));
			}else{
				$updatedata=array(
							'title'=>$this->input->post('title'),
							'article'=>$this->input->post('article'),
							'modify_date'=>date('Y-m-d h-i-s'),
							);
        $sess_id= $_POST['id'];
				$out=$this->common_model->edit('article','id',$updatedata,$sess_id);
				$out =true;
				if($out == true)
	        		{
	        			$this->session->set_flashdata('success', 'Article Update Successfully');
	        		}
	        		else
	        		{
	        			$this->session->set_flashdata('error', 'Article Could Not Updated');
	        		}
	        		redirect(base_url('admin/admin/article'));
			}
		}
			$this->template->load('default', 'add_article', $data);
		}
		else
		{
			redirect(base_url());
		}
    }

    public function comment(){
    	if($this->session->userdata('user_id'))
		{
			$id =$this->uri->segment(4);
			$data['title'] = array('Comment');
			$data['comment']= $this->common_model->commentJoin(array('comment.article_id'=>$id));
			$this->template->load('default', 'comment', $data);
		}
		else
		{
			redirect(base_url());
		}
    }

     public function like(){
    	if($this->session->userdata('user_id'))
		{
			$id =$this->uri->segment(4);
			$data['title'] = array('Like');
			$data['like']= $this->common_model->likeJoin(array('like.article_id'=>$id));
			$this->template->load('default', 'like', $data);
		}
		else
		{
			redirect(base_url());
		}
    }

    /*
---------date : 24/11/2015----------------------------------
-----------------------code written by Aditya --------------------
    */

	public function why_us(){
		if($this->session->userdata('user_id'))
		{
			$data['title'] = array(
            'Why Choose Us'
        	);

			$data['why'] = $this->admin_model->get_why_us();
			//echo "<pre>";var_dump($data['why']); exit();
			$this->template->load('default', 'why_us', $data);
		}
		else
		{
			redirect(base_url());
		}
	}

	public function add_why_us(){$data = array();
			$data['title'] = array(
            "Add Why Choose Us"
        );

		if($this->session->userdata('user_id'))
		{
			$data = array();
			$data['title'] = array(
            "Add Why Choose Us"
        );
			if($_POST)
			{
				if($_FILES['image']['tmp_name'])
				{
					
					$name = date('i_s').'why_us';
					$new_file_name = $name. '.'. str_replace('image/', '', $_FILES["image"]['type']);

					$config['upload_path'] = './images/';
					$config['allowed_types'] = 'gif|jpg|png|jpeg';
					$config['max_size']	= '10000';
					$config['max_width']  = '1024';
					$config['max_height']  = '1024';
					$config['file_name'] = $new_file_name;
					$file=  'image';
					$this->load->library('upload', $config);
					if(!$this->upload->do_upload($file))
					{
						$this->session->set_flashdata('file_error', $this->upload->display_errors());
						redirect(base_url('admin/admin/why_us'));
					}
					else
					{
						$image_name = $new_file_name;
					}
				}
				else
				{
					$image_name = null;
				}
				$form_data = array(
						'image'		=>	$image_name,
						'heading'	=>	$this->input->post('heading'),
						'text'		=>	$this->input->post('text'),
						'status'	=>	1,
					);
				$out = $this->admin_model->insert_why_us($form_data);
				if($out == true)
				{
					$this->session->set_flashdata('success', 'Why Choose Details Successfully Added');
				}
				else
				{
					$this->session->set_flashdata('failure', 'Details Could Not Added');

				}
				redirect(base_url('admin/admin/why_us'));
			}
			else
			{
				$this->template->load('default', 'add_why_us', $data);
			}
			
		}
		else
		{
			redirect(base_url());
		}
	}
/*
---------Code Added Date : 25/11/2015 start here --------------------------
*/
	public function edit_why_us($id){
			if($this->session->userdata('user_id'))
			{
				$data = array();
				$data['title'] = array(
	            "Edit Why Choose Us"
	        );
				$data['edit_why_us'] = $this->admin_model->why_us_by_id($id); 
				if($_POST)
				{
					if($_FILES['image']['tmp_name'])
					{
						$name = date('i_s').'why_us';
						$new_file_name = $name. '.'. str_replace('image/', '', $_FILES["image"]['type']);

						$config['upload_path'] = './images/';
						$config['allowed_types'] = 'gif|jpg|png|jpeg';
						$config['max_size']	= '10000';
						$config['max_width']  = '1024';
						$config['max_height']  = '1024';
						$config['file_name'] = $new_file_name;
						$file=  'image';
						$this->load->library('upload', $config);
						if(!$this->upload->do_upload($file))
						{
							$this->session->set_flashdata('file_error', $this->upload->display_errors());
							redirect(base_url('admin/admin/why_us'));
						}
						else
						{
							$image_name = $new_file_name;
						}
					}
					else
					{
						$image_name = $this->input->post('image_name');
					}
					$header =$this->input->post('heading');
					$form_data = array(
						'image'		=>	$image_name,
						'heading'	=>	$header,
						'text'		=>	$this->input->post('text'),
						'status'	=>	1,
					);
					$out = $this->admin_model->edit_why_us($id, $form_data);
					if($out == true)
					{
						$this->session->set_flashdata('success', 'Why Choose Us Detail Successfully Updated');
					}
					else
					{
						$this->session->set_flashdata('failure', 'Why Choose Us Detail Could Not Updated');
					}
					redirect(base_url('admin/admin/why_us'));
				}
				else
				{
					$this->template->load('default', 'add_why_us', $data);
				}
			}
			else
			{
				redirect(base_url());
			}
		}

		public function delete_why_us($id){
			if($this->session->userdata('user_id') && $id > 0)
			{
				$out = $this->admin_model->delete_why_us($id);
				if($out == true)
				{
					$this->session->set_flashdata('success', 'Why Choose Us Detail Deleted Successfully');
				}
				else
				{
					$this->session->set_flashdata('failure', 'Detail Could Not Deleted');
				}
				redirect(base_url('admin/admin/why_us'));
			}
			else
			{
				redirect(base_url());
			}
		}

/*
---------Code Added Date : 25/11/2015 end here --------------------------
*/

/*
----------Code Added Date : 1/12/2015 start here ---------------------------
*/
	public function latest_update(){
		if($this->session->userdata('user_id'))
		{
			$data = array();
			$data['title'] = array(
	            "Get Latest Update"
	        );
			$data['latest_update'] = $this->admin_model->get_latest_update();
			$this->template->load('default', 'latest_update', $data);
		}
		else
		{
			redirect(base_url());
		}
	}

	public function add_latest_update(){
		if($this->session->userdata('user_id'))
		{
			$data = array();
			$data['title'] = array(
	            "Add Latest Update"
	        );
			if($_POST)
			{	
				$form_data = array(
						'heading'		=>	$this->input->post('heading'),
						'text'			=>	$this->input->post('text'),
						'created_date'	=>	date('Y-m-d H:i:s'),
						'active'		=>	1,
					);
				$out = $this->admin_model->add_latest_update($form_data);
				if($out == true)
				{
					$this->session->set_flashdata('success', 'Latest Update Added Successfully');
				}
				else
				{
					$this->session->set_flashdata('failure', 'Latest Update Could Not Added');
				}
				redirect(base_url('admin/admin/latest_update'));
			}	
			else
			{
				$this->template->load('default', 'add_latest_update', $data);	
			}
		}
		else
		{
			redurect(base_url());
		}
	}

	public function edit_latest_update($id){
		if($this->session->userdata('user_id'))
		{
			$data = array();
			$data['title'] = array(
	            "Add Latest Update"
	        );
	        $data['edit_latest_update'] = $this->admin_model->latest_update_id($id);
	        if($_POST)
	        {
	        	$form_data = array(
	        			'heading'		=>	$this->input->post('heading'),
	        			'text'			=>	$this->input->post('text'),
	        			'modify_date'	=>	date('Y-m-d H:i:s'),
	        		);
	        	$out = $this->admin_model->edit_latest_update($id, $form_data);
	        	if($out == true)
	        	{
	        		$this->session->set_flashdata('success', 'Latest Update Updated Successfully');
	        	} 
	        	else
	        	{
	        		$this->session->set_flashdata('failure', 'Latest Update Could NOt Updated');
	        	}
	        	redirect(base_url('admin/admin/latest_update'));
	        }
	        else
	        {
	        	$this->template->load('default', 'add_latest_update', $data);
	        }
		}
		else
		{
			redirect(base_url());
		}
	}

	public function delete_latest_update($id){
		if($this->session->userdata('user_id'))
		{
			$out = $this->admin_model->delete_latest_update($id);
			if($out == true)
			{
				$this->session->set_flashdata('success', 'Latest Update Deleted Successfully');
			}
			else
			{
				$this->session->set_flashdata('failure', 'Latest Update Could Not Deleted');
			}
			redirect(base_url('admin/admin/latest_update'));
		}
		else
		{
			redirect(base_url());
		}
	}

	public function manage_banner(){
		if($this->session->userdata('user_id'))
		{
			$data = array();
			$data['title'] = array('Manage Banner Image');
			$data['banner'] = $this->admin_model->get_banner_image();
			$this->template->load('default', 'banner_image', $data);
		}
		else
		{
			redirect(base_url());
		}
	}

	public function manage_product_banner(){
		if($this->session->userdata('user_id'))
		{
			$data = array();
			$data['title'] = array('Manage Banner Image');
			$data['banner'] = $this->admin_model->get_product_banner_image();
			$this->template->load('default', 'product_banner_image', $data);
		}
		else
		{
			redirect(base_url());
		}
	}
  


	public function manage_story()
	{
		if($this->session->userdata('user_id'))
		{
			$data = array();
			$data['title'] = array('Manage My Story');
			$data['images'] = $this->admin_model->get_story_image();
			$this->template->load('default', 'get_story_image', $data);
		}
		else
		{
			redirect(base_url());
		}
	}   

	public function manage_profile_images()
	{
		if($this->session->userdata('user_id'))
		{
			$data = array();
			$data['title'] = array('Manage Profile Images');
			$data['images'] = $this->admin_model->get_story_image();
			$this->template->load('default', 'get_profile_image', $data);
		}
		else
		{
			redirect(base_url());
		}
	}

	public function editCommonImages($id)
	{
		if($this->session->userdata('user_id'))
		{
			$data = array();
			$data['title'] = array('Edit Images');
			$data['images'] = $this->admin_model->get_story_imageById($id);
			$this->template->load('default', 'edit_common_image', $data);
		}
		else
		{
			redirect(base_url());
		}
	}

	
	public function updatecommonImage(){
		$type = $this->input->post('type');
		$id = $this->input->post('hidden_id');
		$filename = imgUpload('file','hidden_image');
		$this->admin_model->updatecommonImage($id,$filename);
		$this->session->set_flashdata('success', 'Banner Was Successfully Added');
		if($type == "story"){
			redirect('admin/admin/manage_story');
		}else{
			redirect('admin/admin/manage_profile_images');
		}	
	}

	public function updateclientLogin(){
		$id = $this->input->post('hidden_id');
		$quotes = $this->input->post('quotes');
		$filename = imgUpload('file','hidden_image');
		$this->admin_model->updateclientLogin($id,$filename,$quotes);
		$this->session->set_flashdata('success', 'Client Login Page Successfully Added');
		redirect('admin/admin/manage_clientlogin');
	}

	public function add_banner_manage(){
		if($this->session->userdata('user_id'))
		{
			$data = array();
			$data['title'] = array('Add Banner Image');
			if($_POST)
			{
				if($_FILES['file']['tmp_name'])
				{
					$name = date('i_s').'banner';
					$new_file_name = $name. '.'. str_replace('image/', '', $_FILES["file"]['type']);

					$config['upload_path'] = './images/';
					$config['allowed_types'] = 'gif|jpg|png|jpeg';
					$config['max_size']	= '1000000';
					$config['min_width']  = '1300';
					$config['min_height']  = '537';
					$config['file_name'] = $new_file_name;
					$file=  'file';
					$this->load->library('upload', $config);
					//print_r($config); exit();
					if(!$this->upload->do_upload($file))
					{
						$this->session->set_flashdata('failure', $this->upload->display_errors());
						redirect(base_url('admin/admin/manage_banner'));
					}
					else
					{
						$form_data = array(
								'image'			=>	$new_file_name,
								'active'		=>	1,
								'created_date'	=>	date('Y-m-d H:i:s'),
							);
						$out = $this->admin_model->add_banner_image($form_data);
						if($out == true)
						{
							$this->session->set_flashdata('success', 'Banner Was Successfully Added');
						}
						else
						{
							$this->session->set_flashdata('failure', 'Banner Could Not Added');
						}
						redirect(base_url('admin/admin/manage_banner'));
					}
				}
				else
				{
					$this->session->set_flashdata('failure', 'Please Select file to Upload');
					redirect(base_url('admin/admin/manage_banner'));
				}
			}
			else
			{
				$this->template->load('default', 'add_banner_image', $data);
			}
		}
		else
		{
			redirect(base_url());
		}
	}
     
     public function add_product_banner_manage(){

     
		if($this->session->userdata('user_id'))
		{ 
			$data = array();
			$data['title'] = array('Add Banner Image');
			if($_POST)
			{
				if($_FILES['file']['tmp_name'])
				{
					$name = date('i_s').'banner';
					$new_file_name = time(). '.'. str_replace('image/', '', $_FILES["file"]['type']);

					$config['upload_path'] = './images/';
					$config['allowed_types'] = 'gif|jpg|png|jpeg';
					$config['max_size']	= '1000000';
					$config['min_width']  = '1200';
					$config['min_height']  = '415';
					$config['file_name'] = $new_file_name;
					$file=  'file';
					$this->load->library('upload', $config);
					//print_r($config); exit();
					if(!$this->upload->do_upload($file))
					{
						$this->session->set_flashdata('failure', $this->upload->display_errors());
						redirect(base_url('admin/admin/manage_banner'));
					}
					else
					{
						$form_data = array(
								'image'			=>	$new_file_name,
								'status'		=>	1,
								'added_date'	=>	date('Y-m-d H:i:s'),
							);
						$out = $this->admin_model->add_product_banner_image($form_data);
						if($out == true)
						{
							$this->session->set_flashdata('success', 'Banner Was Successfully Added');
						}
						else
						{
							$this->session->set_flashdata('failure', 'Banner Could Not Added');
						}
						redirect(base_url('admin/admin/manage_product_banner'));
					}
				}
				else
				{
					$this->session->set_flashdata('failure', 'Please Select file to Upload');
					redirect(base_url('admin/admin/manage_banner'));
				}
			}
			else
			{
        //$this->load->view('admin/add_product_banner_image', $data);
				$this->template->load('default', 'add_product_banner_image', $data);
			}
		}
		else
		{
			redirect(base_url());
		}
	}


	public function delete_banner_image($id){
		if($this->session->userdata('user_id'))
		{
			$out = $this->admin_model->delete_banner_image($id);
			if($out == true)
			{
				$this->session->set_flashdata('success', 'Banner Was Successfully Deleted');
			}
			else
			{
				$this->session->set_flashdata('failure', 'Banner Could Not Deleted');
			}
			redirect(base_url('admin/admin/manage_banner'));
		}
		else
		{
			redirect(base_url());
		}
	}


  public function delete_product_banner_image($id){
    if($this->session->userdata('user_id'))
    {
      $out = $this->admin_model->delete_product_banner_image($id);
      if($out == true)
      {
        $this->session->set_flashdata('success', 'Product Banner Was Successfully Deleted');
      }
      else
      {
        $this->session->set_flashdata('failure', ' Product Banner Could Not Deleted');
      }
      redirect(base_url('admin/admin/manage_product_banner'));
    }
    else
    {
      redirect(base_url());
    }
  }



/*
----------Code Added Date : 1/12/2015 end here ---------------------------
*/

/*
-------- Date : 07-12-2015 create function for manage about us page start here----------
*/
	public function manage_about_us(){
		if($this->session->userdata('user_id'))
		{
			$data['title'] = array('Manage About Us');
			$data['about'] = $this->admin_model->get_about_us();
			$this->template->load('default', 'about_us', $data);
		}
		else
		{
			redirect(base_url());
		}
	}

	public function manage_terms(){
		if($this->session->userdata('user_id'))
		{
			$data['title'] = array('Manage Terms & Conditions');
			$data['terms'] = $this->common_model->getAllData('terms','id','desc');
			$this->template->load('default', 'terms', $data);
		}
		else
		{
			redirect(base_url());
		}
	}

	public function manage_clientlogin(){
		if($this->session->userdata('user_id'))
		{
			$data['title'] = array('Manage Client Login Page');
			$data['images'] = $this->common_model->getSingleData('common_images','type','client_login');
			$this->template->load('default', 'manage_clientlogin', $data);
		}
		else
		{
			redirect(base_url());
		}
	}

	public function editClientLogin($id){
		if($this->session->userdata('user_id'))
		{
			$data['title'] = array('Manage Client Login Page');
			$data['images'] = $this->common_model->getSingleData('common_images','id',$id);
			$this->template->load('default', 'editClientLogin', $data);
		}
		else
		{
			redirect(base_url());
		}
	}



	public function add_about_us(){
		if($this->session->userdata('user_id'))
		{
			$data['title'] = array('Add About Us');
			if($_POST)
			{
				$form_data = array(
						'text'			=>	$this->input->post('text'),
						'created_date'	=>	date('Y-m-d H:i:s'),	
						'active'		=>	1	
					);
				$out = $this->admin_model->add_about_us($form_data);
				if($out == true)
				{
					$this->session->set_flashdata('success', 'Text Successfully Added');
					redirect(base_url('admin/admin/manage_about_us'));
				}
				else
				{
					$this->session->set_flashdata('failure', 'Text Could Not Added');
					redirect(base_url('admin/admin/manage_about_us'));
				}
			}
			else
			{
				$this->template->load('default', 'add_about_us',$data);
			}
		}
		else
		{
			redirect(base_url());
		}
	}

	public function add_terms(){
		if($this->session->userdata('user_id'))
		{
			$data['title'] = array('Add ATerms & Conditions');
			if($_POST)
			{
				$form_data = array(
						'text'			=>	$this->input->post('text'),
						'created_date'	=>	date('Y-m-d H:i:s'),	
						'active'		=>	1,	
						'status'		=>	$this->input->post('status')	
					);
				$out = $this->common_model->add('terms',$form_data);
				if(!empty($out))
				{
					$this->session->set_flashdata('success', 'Text Successfully Added');
					redirect(base_url('admin/admin/manage_terms'));
				}
				else
				{
					$this->session->set_flashdata('failure', 'Text Could Not Added');
					redirect(base_url('admin/admin/manage_terms'));
				}
			}
			else
			{
				$this->template->load('default', 'add_terms',$data);
			}
		}
		else
		{
			redirect(base_url());
		}
	}

	public function edit_about_us($id = null){
		if($this->session->userdata('user_id'))
		{
			$data['about'] = $this->admin_model->get_about_by_id($id);
			if($_POST)
			{
				$form_data = array(
						'text'			=>	$this->input->post('text'),
						'modify_date'	=>	date('Y-m-d H:i:s'),	
					);
				$out = $this->admin_model->edit_about_us($id, $form_data);
				if($out == true)
				{
					$this->session->set_flashdata('success', 'Your Text Successfully Updated');
					redirect(base_url('admin/admin/manage_about_us'));
				}	
				else
				{
					$this->session->set_flashdata('failure', 'Your Text Could Not Updated');
					redirect(base_url('admin/admin/manage_about_us'));
				}

			}
			else
			{
				$this->template->load('default', 'add_about_us', $data);	
			}
			
		}
		else
		{
			redirect(base_url());
		}
	}

	public function edit_terms($id = null){
		if($this->session->userdata('user_id'))
		{
			$data['about'] = $this->common_model->getSingleData('terms','id',$id);
			if($_POST)
			{
				$form_data = array(
						'text'			=>	$this->input->post('text'),
						'modify_date'	=>	date('Y-m-d H:i:s'),	
					);
				$out = $this->common_model->edit('terms','id',$form_data,$id);
				if($out == true)
				{
					$this->session->set_flashdata('success', 'Your Text Successfully Updated');
					redirect(base_url('admin/admin/manage_terms'));
				}	
				else
				{
					$this->session->set_flashdata('failure', 'Your Text Could Not Updated');
					redirect(base_url('admin/admin/manage_terms'));
				}

			}
			else
			{
				$this->template->load('default', 'add_terms', $data);	
			}
			
		}
		else
		{
			redirect(base_url());
		}
	}

/*
-------- Date : 07-12-2015 create function for manage about us page end here----------
*/

/*
-------Date : 08-12-2015 create function for manage social links start here----------
*/

	public function delete_about_us($id)
	{
		if($this->common_model->deleteData('about_us','id',$id))
		{
			$this->session->set_flashdata('success', 'Data Deleted Successfully');
			redirect(base_url('admin/admin/manage_about_us'));
		}	
		else
		{
			$this->session->set_flashdata('failure', 'Failed please try again !');
			redirect(base_url('admin/admin/manage_about_us'));
		}
	}

	public function delete_terms($id)
	{
		if($this->common_model->deleteData('terms','id',$id))
		{
			$this->session->set_flashdata('success', 'Data Deleted Successfully');
			redirect(base_url('admin/admin/manage_terms'));
		}	
		else
		{
			$this->session->set_flashdata('failure', 'Failed please try again !');
			redirect(base_url('admin/admin/manage_terms'));
		}
	}

	public function manage_social_link(){
		if($this->session->userdata('user_id'))
		{
			$data['title'] = array('Manage Social Link'); 
			$data['link'] = $this->admin_model->get_links();
			$this->template->load('default', 'social_link', $data);
		}
		else
		{
			redirect(base_url());
		}
	}

	public function add_social_link(){
		if($this->session->userdata('user_id'))
		{
			$data['title'] = array('Add Social Link');
			if($_POST)
			{
				$form_data = array(
						'name'			=>	$this->input->post('category_name'),
						'url'			=>	$this->input->post('url'),
						'created_date'	=>	date('Y-m-d H:i:s'),
						'active'		=>	1
					);

				$out = $this->admin_model->add_social_link($form_data);
				if($out == true)
				{
					$this->session->set_flashdata('success', 'Your Social Link Successfully Added');
				}
				else
				{
					$this->session->set_flashdata('failure', 'Your Social Link Could Not Added');
				}
				redirect(base_url('admin/admin/manage_social_link'));
			}
			else
			{
				$this->template->load('default', 'add_social_link', $data);
			}
		}
		else
		{
			redirect(base_url());
		}
	}

	public function edit_social_link($id){
		if($this->session->userdata('user_id'))
		{
			$data['title'] = array('Edit Social Link');
			$data['edit_social'] = $this->admin_model->get_social_by_id($id);
			if($_POST)
			{
				$form_data = array(
						'name'			=>	$this->input->post('category_name'),
						'url'			=>	$this->input->post('url'),
						'modify_date'	=>	date('Y-m-d H:i:s'),
						'active'		=>	1,
					);
				$out = $this->admin_model->edit_social_link($id, $form_data);
				if($out == true)
				{
					$this->session->set_flashdata('success', 'Your Social Link Successfully Updated');
				}
				else
				{
					$this->session->set_flashdata('failure', 'Your Social Link Could Not Updated');
				}
				redirect(base_url('admin/admin/manage_social_link'));
			}
			else
			{
				$this->template->load('default', 'add_social_link', $data);
			}
		}
		else
		{
			redirect(base_url());
		}
	}

	public function delete_social_link($id){
		if($this->session->userdata('user_id'))
		{
			$out = $this->admin_model->delete_social_link($id);
			if($out == true)
			{
				$this->session->set_flashdata('success', 'Your Social Link Successfully Deleted');
			}
			else
			{
				$this->session->set_flashdata('failure', 'Your Social Link Could Not Deleted');
			}
			redirect(base_url('admin/admin/manage_social_link'));
		}
		else
		{
			redirect(base_url());
		}
	}
/*
-------Date : 08-12-2015 create function for manage social links end here----------
*/

/*
---------Create function for manage subscribers start here-----------------------
*/
	public function manage_subscribers(){
		if($this->session->userdata('user_id'))
		{
			$data['title'] = array('Manage Subscribers');
			$data['subscribe'] = $this->admin_model->get_subscribers();
			$this->template->load('default', 'subscribers', $data);
		}
		else
		{
			redirect(base_url());
		}
	}

/*
----------------------------End of Code ---------------------------------------- 
*/
	
 public function change_payment_status($id)
 {
 	$this->common_model->edit('users','user_id',array('payment_status' => 0),$id);
	$this->common_model->edit('trainner','user_id',array('trainer_payment_status' => 0),$id);
		redirect(base_url('admin/admin/trainers'));
 }

}
?>