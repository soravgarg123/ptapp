<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class User extends CI_Controller {

	public function __construct(){
            parent::__construct();
            $this->load->model('user_model');
            $this->load->library('email');
    }
    public function send_notification_mail($from, $to, $message){
    	$config['protocol'] = 'sendmail';
		$config['mailpath'] = '/usr/sbin/sendmail';
		$config['charset'] = 'iso-8859-1';
		$config['wordwrap'] = TRUE;
		$this->email->initialize($config);

		$this->email->from($from);
		$this->email->to($to); 
		$this->email->subject('Notification');
		$this->email->message($message);

		if($this->email->send())
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	public function index(){ 
			$data['appointmentsArr'] = array();					 
			$data["country"] = $this->user_model->get_country(); 
			$data["services"] = $this->user_model->get_services(); 
			$data["abc"] = $this->user_model->get_service();
			$data["plans"] = $this->user_model->get_plans(); 
			$data["people_say"] = $this->user_model->get_people_say();
			$data["my_story"] = $this->common_model->getSingleData('common_images','type','story');  
			$data['why_us'] = $this->user_model->get_why_us();
			$data['banner'] = $this->user_model->get_banner();
			$footer['latest_update'] = $this->user_model->get_latest_update();
			$footer['social_link'] = $this->user_model->get_social_link();
			$this->load->view('header');   
			$this->load->view('home',$data);
			$this->load->view('footer', $footer);  
	}

	public function clientlogin(){
			$data["plans"] = $this->user_model->get_plans();
			$footer['latest_update'] = $this->user_model->get_latest_update();
			$footer['social_link'] = $this->user_model->get_social_link();
			$data['banner_details'] = $this->common_model->getSingleData('common_images','type','client_login');
			$this->load->view('header');
			$this->load->view('clientlogin',$data);
			$this->load->view('footer', $footer);
	}


	public function service(){
            $id=$_GET['id'];
            $data["service"] = $this->user_model->service($id); 
            $data["plans"] = $this->user_model->get_plans();
            $footer['latest_update'] = $this->user_model->get_latest_update();
			$footer['social_link'] = $this->user_model->get_social_link();
		    $this->load->view('header');   
			$this->load->view('service',$data);
			$this->load->view('footer', $footer);
			 
	}
	public function about(){
			$data["country"] = $this->user_model->get_country(); 
			$this->load->model('admin/admin_model', 'admin_model');
			$data['about']	= $this->admin_model->get_about_us();
			$data["plans"] = $this->user_model->get_plans();
			$footer['latest_update'] = $this->user_model->get_latest_update();
			$footer['social_link'] = $this->user_model->get_social_link();
			$this->load->view('header');
			$this->load->view('about',$data);
			$this->load->view('footer', $footer);
	}

	public function terms(){
			$data["country"] = $this->user_model->get_country(); 
			$this->load->model('admin/admin_model', 'admin_model');
			$user_type =$this->session->userdata('user_type');
			if($user_type =='trainee'){
				$where= array('status'=>1);
			}elseif($user_type =='trainer'){
				$where= array('status'=>0);
			}else{
				$where= array('status'=>2);
			}
			$data['about']	= $this->common_model->getAllDatawhere('terms','id','desc',$where);
			$footer['latest_update'] = $this->user_model->get_latest_update();
			$data["plans"] = $this->user_model->get_plans();
			$footer['social_link'] = $this->user_model->get_social_link();
			$this->load->view('header');
			if($user_type =='trainee'){
				$this->load->view('terms_client',$data);	
			}else{
				$this->load->view('terms',$data);
			}
			
			$this->load->view('footer', $footer);
	}

	
    public function register(){
    		$email = $this->input->post('email');
			$this->db->select('*');
            $this->db->from('users');
			$this->db->where('email',$email);				 
			$query = $this->db->get();	
			$email_data = $query->result_array();
			$user_access = $this->common_model->getSingleData('users_access','email',$email);
			if(!empty($email_data)){
				echo json_encode(array('lid' => 'exist')); 
			}
			else{
				if(!empty($user_access)){
					$lid = $this->user_model->signup();
					$this->common_model->edit('users','user_id',array('payment_status' => 0,'start_date' => $user_access[0]['start_date'],'expiry_date' => $user_access[0]['end_date']),$lid);
					$this->common_model->edit('trainner','user_id',array('trainer_payment_status' => 0,'expiry_date' => $user_access[0]['end_date']),$lid);   
				    echo json_encode(array('lid' => 'success'));
			
			}else{
				$lid = $this->user_model->signup();   
			    echo json_encode(array('lid' => $lid));
			}
				
			}
	}
	public function check_user(){ 
			$email=$this->input->post('email');					
			$result=$this->user_model->check_user_exist($email);
			if($result)
				{
					echo "false";
			
				}else{
			
					echo "true";
					}
				}
	public function login(){
		$email = $this->input->post('email');
		$password = md5($this->input->post('password'));

		$result = $this->user_model->login($email,$password);
		$msg = "";
		$date='';
		if(empty($result)){
			$msg = "invalid";
		}
		else{
			$user_type = $result[0]['user_type'];
			$payment_status = $result[0]['payment_status'];
			$expiry_date = $result[0]['expiry_date'];
			$start_date = $result[0]['start_date'];
			$current_date = date('Y-m-d');
			if($user_type == "trainee"){
				$msg = "trainee";
			}
			else{
				if($payment_status == 1){
					$msg = "pending payment";
				}
				if($payment_status == 2){
					$msg = "failed payment";
				}
				if($payment_status == 0){
					if(strtotime($start_date) <= strtotime($current_date)){
					if(strtotime($expiry_date) < strtotime($current_date) && $current_date != $expiry_date){
						$msg = "expiry";
					}else{
						$newdata = array(
							    'user_id' 	  => $result[0]['user_id'],
	                            'email'       => $result[0]['email'],
			                    'user_type'   => $result[0]['user_type'],
		                        'logged_in'   => TRUE,
	                   		);
						$this->session->set_userdata($newdata);
						$msg = 'success';
					}
				}else{
					$msg = "not_start";
					$date = $start_date;
				}
				}
			}
		}
		echo json_encode(array('msg' => $msg,'start_date'=>$date));
	}

	public function profile(){
 			$data["user_details"] = $this->user_model->add_profile();
 			$data["country"] = $this->user_model->get_country();
 			$footer['latest_update'] = $this->user_model->get_latest_update();
			$footer['social_link'] = $this->user_model->get_social_link();
			$data["plans"] = $this->user_model->get_plans();
			$this->load->view('header');
			$this->load->view('profile',$data);
			$this->load->view('footer', $footer);			       
	}
	public function profiles(){
			$user_id =$_GET['id'];
 			$data["country"] = $this->user_model->get_country();
			$user_type = $this->session->userdata('user_type');
			$data["plans"] = $this->user_model->get_plans();
			// $user_id = $this->session->userdata('user_id');
			//echo $user_type; die;
			if($user_type == "trainner"){
				
			        $this->db->select('*');
					$this->db->from('trainner');
					$this->db->where('user_id',$user_id);				
					$query=$this->db->get();
                    $data['user'] = $query->result_array();
                    $footer['latest_update'] = $this->user_model->get_latest_update();
					$footer['social_link'] = $this->user_model->get_social_link();
					$this->load->view('header');
			        $this->load->view('trainner_profile',$data);
			        $this->load->view('footer', $footer);

				    }else if($user_type == "trainee"){

			        $this->db->select('*');
					$this->db->from('trainee');
					$this->db->where('user_id',$user_id);				
					$query=$this->db->get();
                    $data['user'] = $query->result_array();
                    $trainee_id = $this->session->userdata('user_id');
                    redirect('user/client_profile/'.$trainee_id);

			        }else{
                            $this->index();
                    }
	}
	public function logout(){
					$this->session->sess_destroy();
					redirect('/');
	}
	public function add_profile_trainner(){
					$user_id = $this->session->userdata('user_id');    
			    	$data['user_details']=$this->user_model->add_profile_trainner();
			    	$footer['latest_update'] = $this->user_model->get_latest_update();
					$footer['social_link'] = $this->user_model->get_social_link();
					$data["plans"] = $this->user_model->get_plans();
					$file =$_FILES;
					if(!empty($file)){
		               $data1 = array('sender_id' => $user_id,
		               				  'receiver_id' => $user_id,
		               				  'parent_id'=>$user_id,
		               				  'message' => 'updated profile details',
		               				  'type' => "Edit Profile",
		               				  'trainee_pic' => $_FILES['user_pic']['name'],
		               				  'status' => '0',
		               				  'added_date' =>date("Y-m-d H:i:s")
		               				  );
		               $query=$this->db->insert('notification', $data1);
	           		}
			    	$this->load->view('header');
					$this->load->view('add_profile_trainner',$data);
					$this->load->view('footer', $footer);
	}
	public function user_information_trainner(){
                    $data = $_POST;
			    	$this->user_model->user_information_trainner($data);
			    	$file =$_FILES;
			    	if(!empty($file)){

			    	}
	}
	public function check_password_trainner(){
                    $data = $_POST;                    
			    	$result = $this->user_model->check_password_trainner($data);
			    	if($result){
                       echo "false";
			    	}else{
                       echo "true";
			    	}
	}
	public function user_information_trainee(){ 
                    $data = $_POST;                    
			    	$this->user_model->user_information_trainee($data);
	}
	public function check_password_trainee(){ 
                    $data = $_POST;                    
			    	$result = $this->user_model->check_password_trainee($data);
			    	if($result){
                       echo "false";
			    	}else{
                       echo "true";
			    	}
	}
	public function search_trainner(){ 
                    $data["country"] = $this->user_model->get_country();
                    $data["trainner_details"] = $this->user_model->trainner_details();
                    $footer['latest_update'] = $this->user_model->get_latest_update();
					$footer['social_link'] = $this->user_model->get_social_link();
					$data["plans"] = $this->user_model->get_plans();
			        $this->load->view('header');   
					$this->load->view('search_trainner',$data);
					$this->load->view('footer', $footer); 
	}
	public function search_trainner_details(){
                    $search = $_POST['search'];
                    $data["trainner_search"] =  $this->user_model->search_trainner_details($search);
                    foreach ($data as $trainner_search) {
                    if($trainner_search){
                    $trainner_id = $data['trainner_search'][0]['id'];
                    ?>
                     <li class="col-sm-4 col-md-3">
                        <div class="trainer_sec">
                          <div class="trainer_img"><a href="<?php echo base_url(); ?>user/trainner_profile?id=<?php echo $trainner_id;?>"> <img class="img-responsive" alt="trainer_img" src="<?php echo base_url(); ?>images/strainer1.jpg"></a>
                          </div>
                          <div class="trainer_info">
                            <p><?php echo $data['trainner_search'][0]['name'].' '.$data['trainner_search'][0]['surname'];?></p>
                            <p><strong>Location : </strong> <?php echo $data['trainner_search'][0]['country'];?></p>
                            <p><strong>Rank :</strong> <?php echo $data['trainner_search'][0]['rank_level']?></p>
                          </div>
                        </div>
                      </li>
                      <?php
                               }else{
                                      echo "<i style='color:red;'>Result not found</i>";
                                    }
                  }				
	}
	public function search_trainee(){ 
                    $data["country"] = $this->user_model->get_country();
                    $data["trainee_details"] =  $this->user_model->trainee_details();
                    $footer['latest_update'] = $this->user_model->get_latest_update();
					$footer['social_link'] = $this->user_model->get_social_link();
					$data["plans"] = $this->user_model->get_plans();
			        $this->load->view('header');   
					$this->load->view('search_trainee',$data);
					$this->load->view('footer',$footer); 
	}
	public function search_trainee_details(){ 
                    $search = $_POST['search'];
                    $data["trainee_search"] =  $this->user_model->search_trainee_details($search);
                    foreach ($data as $trainee_search) {
                    if($trainee_search){
                    ?>
                    <li class="col-sm-4 col-md-3">
                        <div class="trainer_sec">
                          <div class="trainer_img"> <img class="img-responsive" alt="trainer_img" src="<?php echo base_url(); ?>images/strainer1.jpg">
                          </div>
                          <div class="trainer_info">
                            <p><?php echo $data['trainee_search'][0]['trainee_name'].' '.$data['trainee_search'][0]['trainee_surname'];?></p>
                            <p><strong>Location : </strong> <?php echo $data['trainee_search'][0]['trainee_country'];?></p>
                            <p><strong>Rank :</strong> <?php echo $data['trainee_search'][0]['rank_level']?></p>
                          </div>
                        </div>
                      </li>
                      <?php
                                            }else{
                                                   echo "<i style='color:red;'>Result not found</i>";
                                                 }
                        }				
	}
	public function trainner_profile(){
			        $trainner_id = $_GET['id'];    
			    	$data["trainner_details"] = $this->user_model->trainner_profile($trainner_id);
			    	$footer['latest_update'] = $this->user_model->get_latest_update();
					$footer['social_link'] = $this->user_model->get_social_link();
					$data["plans"] = $this->user_model->get_plans();
			    	$this->load->view('header');
					$this->load->view('hire_trainner',$data);
					$this->load->view('footer', $footer);
	}
	public function hire_trainner(){
			    	$status = 0;
			    	$data = array('trainner_id'=>$_POST['trainner_id'],'trainee_id'=>$_POST['user_id'],'status'=>$status,'added_date'=>$_POST['added_date'],'modify_date'=>$_POST['modify_date'],);
			    	$this->user_model->hire_trainner($data);
			    }
	public function trainee_upload_image(){ 
			       $data = $_POST;
				   $data["trainee_image"] = $this->user_model->trainee_upload_image($data);
	}
	public function trainee_delete_image(){   
			       $photo_id = $_POST['photo_id'];  
			       $this->user_model->trainee_delete_image($photo_id);
	}
	public function add_profile_trainee(){  
	               $user_id = $this->session->userdata('user_id');
			       $data['user_details']=$this->user_model->add_profile_trainee();
			       $footer['latest_update'] = $this->user_model->get_latest_update();
					$footer['social_link'] = $this->user_model->get_social_link();
					$data["plans"] = $this->user_model->get_plans();
			       $this->load->view('header');
				   $this->load->view('add_profile_trainee',$data);
				   $this->load->view('footer', $footer);
	}
public function calculator(){  
	               $user_id = $this->session->userdata('user_id');
			  //      $data['user_details']=$this->user_model->add_profile_trainee();
			       $footer['latest_update'] = $this->user_model->get_latest_update();
					$footer['social_link'] = $this->user_model->get_social_link();
					// $data["plans"] = $this->user_model->get_plans();
			       $this->load->view('header');
				   $this->load->view('bmr_calculator');
				   $this->load->view('footer', $footer);
	}

	public function edit_profile_trainee($id = null){  
					if($this->session->userdata('user_type') == 'trainner')
					{
						$user_id = $id;
					}
					else
					{
						$user_id = $this->session->userdata('user_id');
					}
					$this->session->set_userdata('last_trainee_id',$user_id);
					$file =$_FILES;
					if(!empty($file)){
		               $data1 = array('sender_id' => $id,
		               				  'receiver_id' => $user_id,
		               				  'parent_id'=>$user_id,
		               				  'message' => 'updated profile details',
		               				  'type' => "Edit Profile",
		               				  'trainee_pic' => $_FILES['user_pic']['name'],
		               				  'status' => '0',
		               				  'added_date' =>date("Y-m-d H:i:s")
		               				  );
		               $query=$this->db->insert('notification', $data1);
	           		}
			       $data['user_details']=$this->user_model->edit_profile_trainee($user_id);
			     	$data["plans"] = $this->user_model->get_plans();
			       $footer['latest_update'] = $this->user_model->get_latest_update();
					$footer['social_link'] = $this->user_model->get_social_link();
			       $this->load->view('header');
				   $this->load->view('edit_profile_trainee',$data);
				   $this->load->view('footer', $footer);
	}
    public function add_trainee1(){ 
			       $data = $_POST;
			       $data["res"] = $this->user_model->add_trainee1($data);
			       $data["plans"] = $this->user_model->get_plans();
			       $this->load->view('add_profile_trainee2',$data);
	}
	public function edit_trainee1(){   
			       $data = $_POST;
			       $data["res"] = $this->user_model->edit_trainee1($data);
			       $data["plans"] = $this->user_model->get_plans();
			       $this->load->view('edit_profile_trainee2',$data);
	}
	public function add_trainee2(){   
			       $data = $_POST;
			       $this->user_model->add_trainee2($data);
			       $data["plans"] = $this->user_model->get_plans();
			       $this->load->view('add_profile_trainee3',$data);
	}
	public function edit_trainee2(){   
			       $data = $_POST;
			       $data["res"]=$this->user_model->edit_trainee2($data);
			       $data["plans"] = $this->user_model->get_plans();
			       $this->load->view('edit_profile_trainee3',$data);
	}
	public function add_trainee3(){   
			       $data = $_POST;
			       $this->user_model->add_trainee3($data);
			       //redirect('user/client_list');
	}
	public function edit_trainee3(){   
			       $data = $_POST;
			       $data["res"]= $this->user_model->edit_trainee3($data);
			       $data["plans"] = $this->user_model->get_plans();
			       $this->load->view('edit_profile_trainee4',$data);
	}
	public function edit_trainee4(){   
			       $data = $_POST;
			       $trainee_id = $data['trainee_id'];
			       unset($data['trainee_id']);
			       $this->user_model->edit_trainee4($data,$trainee_id);
			   
	}
	
	public function trainee_profile_back(){ 
			      $user_details=$this->session->userdata('last_trainee_id');
                  $user_id=$user_details['user_id'];
                  $data['res'] = $this->user_model->trainee_profile_back($user_id);                  
                  $data["plans"] = $this->user_model->get_plans();
                  $data['back'] = 'back';
                  $this->load->view('add_profile_trainee_back',$data);
	}
	public function trainee_profile_back1(){ 
			      $user_details=$this->session->userdata('last_trainee_id');
                  $user_id= $user_details['user_id'];

                  $data['res'] = $this->user_model->trainee_profile_back($user_id);
                  $data["plans"] = $this->user_model->get_plans();
                  $this->load->view('add_profile_trainee2_back',$data);
	}
	public function edit_trainee_profile_back(){
				$user_details=$this->session->userdata('last_trainee_id');			     
                  $data['res'] = $this->user_model->edit_trainee_profile_back($user_details);
                  $data['back'] = 'back';
                  $data["plans"] = $this->user_model->get_plans();
                  $this->load->view('edit_profile_trainee_back',$data);
	}
	public function edit_trainee_profile_back1(){ 
				$user_details=$this->session->userdata('last_trainee_id');
			      $data["plans"] = $this->user_model->get_plans();
                  $data['res'] = $this->user_model->edit_trainee_profile_back($user_details);
                  $this->load->view('edit_profile_trainee2_back',$data);
	}
	public function edit_trainee_profile_back2(){ 
		$user_details=$this->session->userdata('last_trainee_id');
                  $data['res'] = $this->user_model->edit_trainee_profile_back($user_details);
                  $data["plans"] = $this->user_model->get_plans();
                  $this->load->view('edit_profile_trainee3_back',$data);
	}
	
	public function client_list(){ 
			      $data['client_list'] = $this->user_model->client_list();
			      $footer['latest_update'] = $this->user_model->get_latest_update();
				$footer['social_link'] = $this->user_model->get_social_link();
				$data["plans"] = $this->user_model->get_plans();
			      $this->load->view('header');
			      $this->load->view('search_trainee',$data);
			      $this->load->view('footer', $footer);
	}

	function delete_profile_trainee($id)
    {
		$this->admin_model->clients_delete($id);
		$this->session->set_flashdata('success','Client Profile Has Been Deleted.');
		redirect('user/client_list');
	}

	public function search_clients_details(){ 
                    $search = $_POST['search'];
                    $data["client_search"] =  $this->user_model->search_client_details($search);
                    foreach ($data as $client_search) {
                    if($client_search){
                    ?>
                    <li class="col-sm-4 col-md-3">
                        <div class="trainer_sec">
                          <div class="trainer_img"> <img class="img-responsive" alt="trainer_img" src="<?php echo base_url(); ?>images/<?php echo $client_search[0]['user_pic'];?>">
                          </div>
                          <div class="trainer_info">
                            <p>
                            	<?php echo $client_search[0]['name'];?>
                            </p>
                          </div>
                        </div>
                      </li>
                      <?php
                                            }else{
                                                   echo "<i style='color:red;'>Result not found</i>";
                                                 }
                        }				
	}
	public function client_profile($client_id){
		if($this->session->userdata('user_type'))
		{
			 // $client_id = $_GET['id']; 
			 $user_id=$this->session->userdata('user_id');

					$data['fms'] =$this->common_model->getAllDatawhere('fms','id','desc',array('trainee_id'=>$user_id));   
			    	$data["client_details"] = $this->user_model->client_profile($client_id);
			    	$data["client_workout"] = $this->user_model->client_workout($client_id);
			    	$data["search_detail"] = $this->user_model->search_nutrition_details($user_id);
			    	$data["plans"] = $this->user_model->get_plans();
			    	$data['trainee'] = $this->user_model->get_trainee();
			    	$data['trainee_new'] = $this->user_model->get_trainee_new();
					$data['category'] = $this->user_model->get_category();
					$data['meal'] = $this->user_model->get_meal_plans($client_id);
					$data['client_id'] = $client_id;
					$data['pt_details'] = $this->user_model->getPTDetails($this->session->userdata('user_id'));
					
					if($_POST)
					{ 
						$form_data = array(
								'name'			=>	$this->input->post('name'),
								'trainee_id'	=>	$this->input->post('trainee'),
								'category_id'	=>	$this->input->post('category'),
								'start_date'	=>	date('Y-m-d', strtotime($this->input->post('start_date'))),
								'end_date'		=>	date('Y-m-d', strtotime($this->input->post('end_date'))),
								'row'			=>	$this->input->post('row'),
								'color'			=>	$this->input->post('color'),
								'active'		=>	1	
							);
						
						$out = $this->user_model->insert_meal_plan($form_data);
						if($out == true)
						{
							$this->session->set_flashdata('success', 'Meal Plan Add Successfully');
							redirect('user/client_profile/'.$client_id);
						}   
						else
						{
							$this->session->set_flashdata('failure', 'Meal Plan Could Not Added');
							redirect('user/client_profile/'.$client_id);
						}
					}

					$footer['latest_update'] = $this->user_model->get_latest_update();
					$footer['social_link'] = $this->user_model->get_social_link();
					$data['client_id'] = $client_id;
			    	$this->load->view('header');
					$this->load->view('client_profile',$data);
					$this->load->view('footer', $footer);
		}
		else
		{
			redirect(base_url());
		}
			       
	}
	public function nutrition_dairy(){
			    	$footer['latest_update'] = $this->user_model->get_latest_update();
					$footer['social_link'] = $this->user_model->get_social_link();
					$data["plans"] = $this->user_model->get_plans();
			    	$this->load->view('header');
					$this->load->view('nutrition_dairy',$data);
					$this->load->view('footer', $footer);
	}
	public function add_nutrition(){
			        $data = $_POST;
			        
			        $selected_date = $data["datepicker"];
	                        $newDate = date("Y-m-d", strtotime($selected_date));
                                $this->db->select('*');
				$this->db->from('nutrition_details');
				$this->db->where('selected_date',$newDate);	
				$query=$this->db->get();
				$getdate_result = $query->result_array();
				
				/*if($getdate_result){ echo json_encode(array('id' => $getdate_result[0]['id'])); }else{
				$arr = array();
				echo json_encode($arr);*/
			        $data["nutrition_detail"] = $this->user_model->add_nutrition($data); 
			        /*}*/
			    	
	}
    public function show_nutrition_details(){
			   		$footer['latest_update'] = $this->user_model->get_latest_update();
					$footer['social_link'] = $this->user_model->get_social_link();
					$data["plans"] = $this->user_model->get_plans();
			        $this->load->view('header');
					$this->load->view('nutrition_details',$data);
					$this->load->view('footer', $footer);  	
	}
	public function search_nutrition_details(){
		$user_id=$_REQUEST['user_id'];
		$name=$_REQUEST['name'];
		$data["search_detail"] = $this->user_model->search_nutrition_details($user_id);
        ?>
		      <tr style="border-top:1px solid #000;">
	                
	                 <th><h3 class="panel-title">Date</h3></th>
	                 <th><h3 class="panel-title">Meal </h3></th>
	                 <th><h3 class="panel-title">Food</h3></th>
	                 <th><h3 class="panel-title">Calories</h3></th>
	                 <th><h3 class="panel-title">Notes</h3></th>
	                 <th colspan="2" style="text-align:center;"><h3 class="panel-title">Action</h3></th>
	         </tr>
		<?php
		foreach ($data["search_detail"] as $nutrition_details) {
			
			if($nutrition_details != ""){?>
			<tr>
			    <td><?php echo ucfirst($nutrition_details["selected_date"]);?></td>
				<td><?php echo ucfirst($nutrition_details["category"]);?></td>
				<td><?php echo ucfirst($nutrition_details["title"]);?></td>
				<td><?php echo ucfirst($nutrition_details["calory"]);?></td>
				<td><?php echo ucfirst($nutrition_details["description"]);?></td>				
				<td> <a class='btn btn-primary btn-sm' href="<?php echo base_url();?>user/nutrition_edit?id=<?php echo $nutrition_details["id"];?>"><i class='icon-edit icon-white'></i>Edit &nbsp&nbsp&nbsp</a>
                </td>
				<td>  <a class='btn btn-danger btn-sm' href='<?php echo base_url();?>user/nutrition_delete?id=<?php echo $nutrition_details["id"];?>'onClick='javascript:return confirm("Are you sure to Delete this Record?")'><i class='icon-edit icon-white'></i>Delete </a>
				 </td>
		    </tr>
          <?php
		}
		
				
	 
	}
}
 	public function search_date_nutrition_details(){
			$date = $_GET['date'];
			$name = $_GET['name'];
			$data["search_detail"] = $this->user_model->search_date_nutrition_details($date);
			?>
			 <tbody>
	         <tr>
	               <th><h3 class="panel-title">Date</h3></th>
	                 <th><h3 class="panel-title">Meal </h3></th>
	                 <th><h3 class="panel-title">Food</h3></th>
	                 <th><h3 class="panel-title">Calories</h3></th>
	                 <th><h3 class="panel-title">Notes</h3></th>
	                 <th colspan="2" style="text-align:center;"><h3 class="panel-title">Action</h3></th>
	         </tr>
		<?php
			foreach ($data["search_detail"] as $search_details) {
			
			if($search_details != ""){?>
			<tr style="border-top:1px solid #000;">
				
				<td><?php echo ucfirst($search_details["date"]);?></td>
				<td><?php echo ucfirst($search_details["category"]);?></td>
				<td><?php echo ucfirst($search_details["title"]);?></td>
				<td><?php echo ucfirst($search_details["calory"]);?></td>
				<td><?php echo ucfirst($search_details["description"]);?></td>
				<td> <a class='btn btn-primary btn-sm' href="<?php echo base_url();?>user/nutrition_edit?id=<?php echo $nutrition_details["id"];?>"><i class='icon-edit icon-white'></i>Edit &nbsp&nbsp&nbsp</a>
                </td>
				<td>  <a class='btn btn-danger btn-sm' href='<?php echo base_url();?>user/nutrition_delete?id=<?php echo $nutrition_details["id"];?>'onClick='javascript:return confirm("Are you sure to Delete this Record?")'><i class='icon-edit icon-white'></i>Delete </a>
				 </td>
		    </tr>

          <?php
		}
		
	}?>
    </tbody>
	<?php }

	public function article(){
		$footer['latest_update'] = $this->user_model->get_latest_update();
		$footer['social_link'] = $this->user_model->get_social_link();
		$data['article']= $this->common_model->getAllData('article','id','desc');
		$this->load->view('header');
		$this->load->view('article',$data);
		$this->load->view('footer', $footer);  	
	}
	public function comment_article(){
		$comment =$this->input->post('comment_text');
		$article_id =$this->input->post('comment_ar_id');
		$user_id =$this->session->userdata('user_id');
		$insert_data = array(
				'article_id'=>$article_id,
				'user_id'=>$user_id,
				'comment'=>$comment,
				'date_added'=>date('Y-m-d h-i-s'),
				);
		$response = $this->common_model->add('comment',$insert_data);
		if(!empty($response)){
			echo '1'; 
		}else{
			echo '2';
		}
	}

	public function like_article(){
		$article_id =$this->input->post('article_id');
		$user_id =$this->session->userdata('user_id');
		$insert_data = array(
				'article_id'=>$article_id,
				'user_id'=>$user_id,
				'like'=>1,
				'date_added'=>date('Y-m-d h-i-s'),
				);
		$response = $this->common_model->add('like',$insert_data);
		if(!empty($response)){
			echo '1'; 
		}else{
			echo '2';
		}
	}
	public function add_workout(){
			        $data = $_POST;
			        $data["workout_detail"] = $this->user_model->add_workout($data); 	
			    	
	}
	public function client_plan(){
					$user_id =$this->session->userdata('user_id');
					$data['client_id']=$this->uri->segment(3);
		            $data["select_client"] = $this->user_model->select_client($user_id);
		            $footer['latest_update'] = $this->user_model->get_latest_update();
					$footer['social_link'] = $this->user_model->get_social_link();
					$footer["country"] = $this->user_model->get_country(); 
					$data["plans"] = $this->user_model->get_plans();
			        $this->load->view('header');
					$this->load->view('add_client_plan',$data);
					$this->load->view('footer', $footer); 
					
	}
	public function add_client_plan(){
        $data = $_POST;
        $data["client_plan"] = $this->user_model->add_client_plan($data);
        if($this->session->userdata('user_type') == 'trainner')
        { 
        	$trainner = $this->user_model->get_trainner_name();
        	$trainee = $this->user_model->get_trainee_name($data['user_id']);
        	$message = "Trainner Name :".$trainner['trainner_name'].
						" <br/> Added Training Plan For You ".
						" <br/> Date : ".date('Y-m-d');
        	$out = $this->send_notification_mail($trainner['email'], $trainee['email'], $message);
        	if($out == true)
        	{
        		echo "Notification Successfully Send";
        	}
        	else
        	{
        		echo "Notification Sending Failed";
        	}
        }
			        
	}
	public function search_client_plan(){
		$user_id=$_REQUEST['user_id'];
		$name=$_REQUEST['name'];
		$data["search_client_plan"] = $this->user_model->search_client_plan($user_id);
		?>
	         <tr style="border-top:1px solid #000;">
	                 <th><h3 class="panel-title">Date </h3></th>
	                 <th><h3 class="panel-title">Exercise </h3></th>
	                 <th><h3 class="panel-title">Sets</h3></th>
	                 <th><h3 class="panel-title">Reps</h3></th>
	                 <th><h3 class="panel-title">Time</h3></th>	                
	                 <th><h3 class="panel-title">Weight</h3></th>
	                 <th><h3 class="panel-title">Notes</h3></th>
	                 <th colspan="2" style="text-align:center;"><h3 class="panel-title">Action</h3></th>
	         </tr>
		<?php
		foreach ($data["search_client_plan"] as $client_plan) {
			
			if($client_plan != ""){?>
			<tr>
			    <td><?php echo ucfirst($client_plan["selected_date"]);?></td>
				<td><?php echo ucfirst($client_plan["exercise"]);?></td>
				<td><?php echo ucfirst($client_plan["sets"]);?></td>
				<td><?php echo ucfirst($client_plan["reps"]);?></td>
				<td><?php echo ucfirst($client_plan["time"]);?></td>
				<td><?php echo ucfirst($client_plan["weight"]);?></td>
				<td><?php echo ucfirst($client_plan["notes"]);?></td>
				<td> <a class='btn btn-primary btn-sm' href="<?php echo base_url();?>user/training_plan_edit?id=<?php echo $client_plan["id"];?>"><i class='icon-edit icon-white'></i>Edit &nbsp&nbsp&nbsp</a>
                </td>
				<td>  <a class='btn btn-danger btn-sm' href='<?php echo base_url();?>user/training_plan_delete?id=<?php echo $client_plan["id"];?>'onClick='javascript:return confirm("Are you sure to Delete this Record?")'><i class='icon-edit icon-white'></i>Delete </a>
				 </td>
		    </tr>
          <?php
		}
	}
	}
	public function add_image(){
                    $data = $_POST;                    
			    	$data['image'] = $this->user_model->add_image($data);
			    	
			    	foreach ($data['image'] as $images) {
                   ?>
                   
            	   <div style="text-align:center;">
                   <img src="<?php echo base_url();?>add-after-before-image/<?php echo $images['image'];?>" height="200" width="200" style="boder:5px solid #000;">
                   </div>
                  <?php
                   }
                  
	}
	public function show_image(){
                   
                    $data = $this->user_model->show_image();
                    ?> 
                    <div class="row">
                    <?php
                    foreach ($data as $images) { ?>
            	       <div class="col-sm-3" id="show-image"><a class="example-image-link" href="<?php echo base_url();?>add-after-before-image/<?php echo $images['image'];?>" data-lightbox="example-set" data-title="Click the right half of the image to move forward."><img src="<?php echo base_url();?>add-after-before-image/<?php echo $images['image'];?>" width="200" height="200" style="border:5px solid #000;"></a><div class="update" ></div></div>
                    <?php } ?>
                    </div>
                    <?php
                           
			    	
	}
	public function edit_image(){
		 $trainee_id = $_REQUEST['trainee_id'];
         $data = $this->user_model->edit_image($trainee_id);

	}
	public function notification(){
		if($this->session->userdata('user_type') == 'trainner')
		{
			$user_id = $this->session->userdata('user_id');
			$this->db->select('*');
			$this->db->from('trainner');
			$this->db->where('user_id',$user_id);				
			$query=$this->db->get();
	        $data['user'] = $query->result_array();

	        $data["country"] = $this->user_model->get_country();
 			$footer['latest_update'] = $this->user_model->get_latest_update();
			$footer['social_link'] = $this->user_model->get_social_link();
	        
	        $this->db->select('*');
			$this->db->from('trainee');
			$this->db->where('trainner_id',$user_id);				
			$query=$this->db->get();
	        $data['clients'] = $query->result_array();
            //$data["country"] = $this->user_model->get_country();  
	        $this->db->select('*');
			$this->db->from('notification');
			$this->db->where('receiver_id',$user_id);
			$this->db->order_by('id', 'desc');
			$this->db->limit(7);		
			$query=$this->db->get();
	        $data['notification'] = $query->result_array();

            
	        $query = $this->db->query("SELECT * from  chating  where  chat_sender_id ='$user_id' or chat_reciever_id = '$user_id' ORDER BY chat_id DESC");
	        $data['chat'] = $query->result_array();
	        $data["plans"] = $this->user_model->get_plans();
			$this->load->view('header');
			$this->load->view('notification',$data);
			$this->load->view('footer', $footer);
		}
		else
		{
			redirect(base_url());
		}
		

	}
	public function notification_time(){
	
	      $data = $_POST; 
	      $this->user_model->notification_time($data);
	
	
	
	}
	public function nutrition_edit(){
	        $id = $_GET['id'];
	        $data["nutrition_details"] = $this->user_model->nutrition_edit($id);
	        $footer['latest_update'] = $this->user_model->get_latest_update();
			$footer['social_link'] = $this->user_model->get_social_link();
			$data["plans"] = $this->user_model->get_plans();
	        $this->load->view('header');
			$this->load->view('edit_nutrition_details',$data);
			$this->load->view('footer', $footer);
	}
	public function training_plan_edit(){
	        $id = $_GET['id'];
	        $user_id =$this->session->userdata('user_id');
	        $data["training_plan_details"] = $this->user_model->training_plan_edit($id);
	        $data["select_client"] = $this->user_model->select_client($user_id);
	        $footer['latest_update'] = $this->user_model->get_latest_update();
			$footer['social_link'] = $this->user_model->get_social_link();
			$data["plans"] = $this->user_model->get_plans();
	        $this->load->view('header');
			$this->load->view('client_edit_plan',$data);
			$this->load->view('footer', $footer);
	}
	public function update_nutrition_details(){
	       $data = $_POST; 
	       $data["update_nutrition_details"] = $this->user_model->update_nutrition_details($data);
	       echo $data["update_nutrition_details"][0]['id'];


	}
	public function update_client_plan(){
	       $data = $_POST; 
	       $data["update_client_plan"] = $this->user_model->update_client_plan($data);
	       echo $data["update_client_plan"][0]['id'];


	}
	public function nutrition_delete(){
	        $id = $_GET['id'];

	        $this->db->select('*');
		    $this->db->from('nutrition_details');
		    $this->db->where('id',$id);
		    $query = $this->db->get();
		    $data['nutrition_details'] = $query->result_array();
	        $user_id = $data ['nutrition_details'][0]['user_id']; 

	        $this->user_model->nutrition_delete($id);

	        
	        if($user_id){
	        redirect('user/client_profile/'.$user_id);
	      }
	}
	public function training_plan_delete(){
	        $id = $_GET['id'];

	        $this->db->select('*');
		    $this->db->from('client_plan');
		    $this->db->where('id',$id);
		    $query = $this->db->get();
		    $data['training_plan_details'] = $query->result_array();
	        $user_id = $data ['training_plan_details'][0]['user_id']; 

	        $this->user_model->training_plan_delete($id);

	        
	        if($user_id){
	        redirect('user/client_profile/'.$user_id);
	      }
	}
	public function nutrition_details_by_week(){
	     
	      $start_date = $_REQUEST['start_date'];
	      $user_id = $_REQUEST['user_id'];
	      $end_date = $_REQUEST['end_date'];
	      $star_newdate = date('Y-m-d', strtotime('-7 days', strtotime($start_date))); 
	      $end_newdate = date('Y-m-d', strtotime('-7 days', strtotime($end_date)));
	   
	      $data = array('start_date'=>$star_newdate, 'end_date'=>$end_newdate, 'user_id'=>$user_id);
	      $data["week_date"] = $this->user_model->nutrition_details_by_week($data);?>
	      <tbody>
	      <tr>
	                <th><h3 class="panel-title">Date & Time</h3></th>
	                <th><h3 class="panel-title">category </h3></th>
	                <th><h3 class="panel-title">Title</h3></th>
	                <th><h3 class="panel-title">Calory</h3></th>
	                <th><h3 class="panel-title">Description</h3></th>
	                <th colspan="2" style="text-align:center;"><h3 class="panel-title">Action</h3></th>
	         </tr>
		<?php
			foreach ($data["week_date"] as $search_details) {
			
			if($search_details != ""){?>
			<tr style="border-top:1px solid #000;">
				ucfirst("hello world!");
				<td><?php echo ucfirst($search_details["selected_date"]);?></td>
				<td><?php echo ucfirst($search_details["category"]);?></td>
				<td><?php echo ucfirst($search_details["title"]);?></td>
				<td><?php echo ucfirst($search_details["calory"]);?></td>
				<td><?php echo ucfirst($search_details["description"]);?></td>
				<td> <a class='btn btn-primary btn-sm' href="<?php echo base_url();?>user/nutrition_edit?id=<?php echo $search_details["id"];?>"><i class='icon-edit icon-white'></i>Edit &nbsp&nbsp&nbsp</a>
                </td>
				<td>  <a class='btn btn-danger btn-sm' href='<?php echo base_url();?>user/nutrition_delete?id=<?php echo $search_details["id"];?>'onClick='javascript:return confirm("Are you sure to Delete this Record?")'><i class='icon-edit icon-white'></i>Delete </a>
				 </td>
				<input type="hidden" name="start_date" id="start_date" value="<?php echo $star_newdate;?>">
				<input type="hidden" name="end_date" id="end_date" value="<?php echo $end_newdate;?>">
		         </tr>
		    <?php 
	     
	}}?>
	
	 </tbody>
	<?php }
	public function trainning_plan_by_week(){
	     

	      $plan_start_date = $_REQUEST['plan_start_date'];
	      $plan_user_id = $_REQUEST['plan_user_id'];
	      $plan_end_date = $_REQUEST['plan_end_date'];
	      $plan_start_newdate = date('Y-m-d', strtotime('-7 days', strtotime($plan_start_date))); 

	      $plan_end_newdate = date('Y-m-d', strtotime('-7 days', strtotime($plan_end_date)));
	   
	      $data = array('plan_start_date'=>$plan_start_newdate, 'plan_end_date'=>$plan_end_newdate, 'plan_user_id'=>$plan_user_id);
	      $data["plan"] = $this->user_model->trainning_plan_by_week($data);
         
	      ?>
	      <tbody>
	      <tr>
	                <th><h3 class="panel-title">Selected Date</h3></th>
	                <th><h3 class="panel-title">Exercise </h3></th>
	                <th><h3 class="panel-title">Sets</h3></th>
	                <th><h3 class="panel-title">Reps</h3></th>
	                <th><h3 class="panel-title">Time</h3></th>
	                <th><h3 class="panel-title">Notes</h3></th>
	                <th colspan="2" style="text-align:center;"> <h3 class="panel-title">Action</h3></th>
	         </tr>
		<?php
			foreach ($data["plan"] as $search_details) {
			
			if($search_details != ""){?>
			<tr style="border-top:1px solid #000;">
				
				<td><?php echo ucfirst($search_details["selected_date"]);?></td>
				<td><?php echo ucfirst($search_details["exercise"]);?></td>
				<td><?php echo ucfirst($search_details["sets"]);?></td>
				<td><?php echo ucfirst($search_details["reps"]);?></td>
				<td><?php echo ucfirst($search_details["time"]);?></td>
				<td><?php echo ucfirst($search_details["notes"]);?></td>
				<td> <a class='btn btn-primary btn-sm' href="<?php echo base_url();?>user/training_plan_edit?id=<?php echo $search_details["id"];?>"><i class='icon-edit icon-white'></i>Edit &nbsp&nbsp&nbsp</a>
                </td>
				<td>  <a class='btn btn-danger btn-sm' href='<?php echo base_url();?>user/training_plan_delete?id=<?php echo $search_details["id"];?>'onClick='javascript:return confirm("Are you sure to Delete this Record?")'><i class='icon-edit icon-white'></i>Delete </a>
				 </td>
				<input type="hidden" name="plan_start_date" id="plan_start_date" value="<?php echo $plan_start_newdate;?>">
				<input type="hidden" name="plan_end_date" id="plan_end_date" value="<?php echo $plan_end_newdate;?>">
		         </tr>
		    <?php 
	     
	}}?>
	
	 </tbody>
	<?php }
	public function nutrition_details_by_week_next(){
	
	      $start_date = $_REQUEST['start_date'];	      
	      $end_date = $_REQUEST['end_date'];
	      $user_id = $_REQUEST['user_id'];
	      $star_newdate = date('Y-m-d', strtotime('+7 days', strtotime($start_date))); 
	      $end_newdate = date('Y-m-d', strtotime('+7 days', strtotime($end_date)));
	   
	      $data = array('start_date'=>$star_newdate, 'end_date'=>$end_newdate , 'user_id'=>$user_id);
	      $data["week_date"] = $this->user_model->nutrition_details_by_week($data);?>
	      <tbody>
	      <tr>
	                <th><h3 class="panel-title">Date & Time</h3></th>
	                <th><h3 class="panel-title">category </h3></th>
	                <th><h3 class="panel-title">Title</h3></th>
	                <th><h3 class="panel-title">Calory</h3></th>
	                <th><h3 class="panel-title">Description</h3></th>
	                <th colspan="2" style="text-align:center;"><h3 class="panel-title">Action</h3></th>
	         </tr>
		<?php
			foreach ($data["week_date"] as $search_details) {
			
			if($search_details != ""){?>
			<tr style="border-top:1px solid #000;">
				
				<td><?php echo ucfirst($search_details["selected_date"]);?></td>
				<td><?php echo ucfirst($search_details["category"]);?></td>
				<td><?php echo ucfirst($search_details["title"]);?></td>
				<td><?php echo ucfirst($search_details["calory"]);?></td>
				<td><?php echo ucfirst($search_details["description"]);?></td>
				<td> <a class='btn btn-primary btn-sm' href="<?php echo base_url();?>user/nutrition_edit?id=<?php echo $search_details["id"];?>"><i class='icon-edit icon-white'></i>Edit &nbsp&nbsp&nbsp</a>
                </td>
				<td>  <a class='btn btn-danger btn-sm' href='<?php echo base_url();?>user/nutrition_delete?id=<?php echo $search_details["id"];?>'onClick='javascript:return confirm("Are you sure to Delete this Record?")'><i class='icon-edit icon-white'></i>Delete </a>
				 </td>
				
				<input type="hidden" name="plan_next_start_date" id="start_date" value="<?php echo $star_newdate;?>">
				<input type="hidden" name="end_date" id="end_date" value="<?php echo $end_newdate;?>">
		         </tr>
		    <?php 
	     
	}}?>
	
	 </tbody>
	<?php }
	public function client_plan_by_week_next(){
	
	      $plan_start_date = $_REQUEST['plan_start_date'];
	      $plan_end_date = $_REQUEST['plan_end_date'];
	      $plan_user_id = $_REQUEST['plan_user_id'];
	      $plan_star_newdate = date('Y-m-d', strtotime('+7 days', strtotime($plan_start_date))); 
	      $plan_end_newdate = date('Y-m-d', strtotime('+7 days', strtotime($plan_end_date)));
	   
	      $data = array('plan_start_date'=>$plan_star_newdate, 'plan_end_date'=>$plan_end_newdate , 'plan_user_id'=>$plan_user_id);
	      $data["plan_next"] = $this->user_model->trainning_plan_by_week($data);?>
	      <tbody>
	      <tr>
	                <th><h3 class="panel-title">Selected Date</h3></th>
	                <th><h3 class="panel-title">Exercise </h3></th>
	                <th><h3 class="panel-title">Sets</h3></th>
	                <th><h3 class="panel-title">Reps</h3></th>
	                <th><h3 class="panel-title">Time</h3></th>
	                <th><h3 class="panel-title">Notes</h3></th>
	                <th colspan="2" style="text-align:center;"> <h3 class="panel-title">Action</h3></th>
	         </tr>
		<?php
			foreach ($data["plan_next"] as $search_details) {
			
			if($search_details != ""){?>
			<tr style="border-top:1px solid #000;">
				
				<td><?php echo ucfirst($search_details["selected_date"]);?></td>
				<td><?php echo ucfirst($search_details["exercise"]);?></td>
				<td><?php echo ucfirst($search_details["sets"]);?></td>
				<td><?php echo ucfirst($search_details["reps"]);?></td>
				<td><?php echo ucfirst($search_details["time"]);?></td>
				<td><?php echo ucfirst($search_details["notes"]);?></td>
				<td> <a class='btn btn-primary btn-sm' href="<?php echo base_url();?>user/training_plan_edit?id=<?php echo $search_details["id"];?>"><i class='icon-edit icon-white'></i>Edit &nbsp&nbsp&nbsp</a>
                </td>
				<td>  <a class='btn btn-danger btn-sm' href='<?php echo base_url();?>user/training_plan_delete?id=<?php echo $search_details["id"];?>'onClick='javascript:return confirm("Are you sure to Delete this Record?")'><i class='icon-edit icon-white'></i>Delete </a>
				 </td>
				<input type="hidden" name="plan_start_date" id="plan_start_date" value="<?php echo $plan_star_newdate;?>">
				<input type="hidden" name="plan_end_date" id="plan_end_date" value="<?php echo $plan_end_newdate;?>">
		         </tr>
		    <?php 
	     
	}}?>
	
	 </tbody>
	<?php }
	public function client_trainner(){
	
			$user_id=$this->session->userdata('user_id');	
			$data["client_trainner"] = $this->user_model->get_client_trainner($user_id);
			$footer['latest_update'] = $this->user_model->get_latest_update();
			$footer['social_link'] = $this->user_model->get_social_link();
			$data["plans"] = $this->user_model->get_plans();
			$this->load->view('header');
			$this->load->view('trainee_trainner_profile',$data);
			$this->load->view('footer', $footer);
	
	
	}

	public function tools(){
		if($this->session->userdata('user_id')){
			$id = $this->session->userdata('user_id');

			$data['fms'] =$this->common_model->getAllDatawhere('fms','id','desc',array('trainner_id'=>$id));
			$footer['latest_update'] = $this->user_model->get_latest_update();
			$footer['social_link'] = $this->user_model->get_social_link();
			$this->load->view('header');
			$this->load->view('tools',$data);
			$this->load->view('footer', $footer);
		}else{
			redirect(base_url());
		}
	}

	public function resources(){
		if($this->session->userdata('user_id')){
			$id = $this->session->userdata('user_id');
			$data[]='';
			$footer['latest_update'] = $this->user_model->get_latest_update();
			$footer['social_link'] = $this->user_model->get_social_link();
			$this->load->view('header');
			$this->load->view('resources',$data);
			$this->load->view('footer', $footer);
		}else{
			redirect(base_url());
		}
	}

	public function fms_edit(){
		if($this->session->userdata('user_id')){
			$id = $this->uri->segment(3);
			$user_id =$this->session->userdata('user_id');
			$data["select_client"] = $this->user_model->select_client($user_id);
			$data['fms'] = $this->common_model->getSingleData('fms','id',$id);
		if($this->input->post()){
			$name =$this->common_model->getSingleData('trainee','user_id',$this->input->post('user_id'));
			$total =$this->input->post('deep_squat')+$this->input->post('hurdle_step_l')+$this->input->post('hurdle_step_r')+$this->input->post('inline_lunge_l')+
					$this->input->post('inline_lunge_r')+$this->input->post('hurdle_step_l')+$this->input->post('hurdle_step_r')+$this->input->post('shoulder_mobility_l')+
					$this->input->post('shoulder_mobility_r')+$this->input->post('impingement_test_l')+$this->input->post('impingement_test_r')+$this->input->post('active_straight_r')+
					$this->input->post('active_straight_l')+$this->input->post('trunk_stablity')+$this->input->post('press_up')+$this->input->post('rotary_stability_l')+$this->input->post('rotary_stability_r')+$this->input->post('posterior');
			$fms = array(
            		   'deep_squat'=>$this->input->post('deep_squat'),       
     				   'deep_squat_comment'=>$this->input->post('deep_squat_comment'),       
                      'hurdle_step_l'=>$this->input->post('hurdle_step_l'),       
         			  'hurdle_step_l_comment'=>$this->input->post('hurdle_step_l_comment'),
         			  'hurdle_step_r'=>$this->input->post('hurdle_step_r'),
         			  'hurdle_step_r_comment'=>$this->input->post('hurdle_step_r_comment'),       
                      'inline_lunge_l'=>$this->input->post('inline_lunge_l'),       
					  'inline_lunge_l_comment'=>$this->input->post('inline_lunge_l_comment'),      
                      'inline_lunge_r'=>$this->input->post('inline_lunge_r'),       
                      'inline_lunge_r_comment'=>$this->input->post('inline_lunge_r_comment'),       
                      'hurdle_step_l'=>$this->input->post('hurdle_step_l'),       
                      'hurdle_step_l_comment'=>$this->input->post('hurdle_step_l_comment'),       
                      'hurdle_step_r'=>$this->input->post('hurdle_step_r'),       
                      'hurdle_step_r_comment'=>$this->input->post('hurdle_step_r_comment'),       
                      'shoulder_mobility_l'=>$this->input->post('shoulder_mobility_l'),       
                      'shoulder_mobility_l_comment'=>$this->input->post('shoulder_mobility_l_comment'), 
                      'shoulder_mobility_r'=>$this->input->post('shoulder_mobility_r'),       
                      'shoulder_mobility_r_comment'=>$this->input->post('shoulder_mobility_r_comment'),       
                      'impingement_test_l'=>$this->input->post('impingement_test_l'),       
                      'impingement_test_l_comment'=>$this->input->post('impingement_test_l_comment'),       
                      'impingement_test_r'=>$this->input->post('impingement_test_r'),       
                      'impingement_test_r_comment'=>$this->input->post('impingement_test_r_comment'),      
                      'active_straight_l'=>$this->input->post('active_straight_l'),
                      'active_straight_l_comment'=>$this->input->post('active_straight_l_comment'),
                      'active_straight_r'=>$this->input->post('active_straight_r'),
                      'active_straight_r_comment'=>$this->input->post('active_straight_r_comment'),
                      'trunk_stablity'=>$this->input->post('trunk_stablity'),
                      'trunk_stablity_comment'=>$this->input->post('trunk_stablity_comment'),
                      'press_up'=>$this->input->post('press_up'),
                      'press_up_comment'=>$this->input->post('press_up_comment'),
                      'rotary_stability_l'=>$this->input->post('rotary_stability_l'),
                      'rotary_stability_l_comment'=>$this->input->post('rotary_stability_l_comment'),
                      'rotary_stability_r'=>$this->input->post('rotary_stability_r'),
                      'rotary_stability_r_comment'=>$this->input->post('rotary_stability_r_comment'),
                      'posterior'=>$this->input->post('posterior'),
                      'posterior_comment'=>$this->input->post('posterior_comment'),
                   );
			$fms_val = json_encode($fms);
				if(empty($id)){
			$insertdata=array(
						'trainner_id'=>$user_id,
						'trainee_id'=>$this->input->post('user_id'),
						'full_name'=>$name[0]['name'],
						'age'=>$this->input->post('age'),
						'gender'=>$this->input->post('gender'),
						'height'=>$this->input->post('height'),
						'weight'=>$this->input->post('weight'),
						'fms'=>$fms_val,
						'total'=>$total,
						'date_added'=>date('Y-m-d h:i:s'),
						);
					$response = $this->common_model->add('fms',$insertdata,$id);
					if(!empty($response)){
				     	$this->session->set_flashdata('success', 'Record Added Successfully');
				        redirect('user/tools/');
				      }else{
				      	$this->session->set_flashdata('failure',"Record Could Not Deleted");
				      }
				}else{
					$updtdata=array(
						'full_name'=>$name[0]['name'],
						'age'=>$this->input->post('age'),
						'gender'=>$this->input->post('gender'),
						'height'=>$this->input->post('height'),
						'weight'=>$this->input->post('weight'),
						'fms'=>$fms_val,
						'total'=>$total,
						'modify_date'=>date('Y-m-d h:i:s'),
						);
					$response = $this->common_model->edit('fms','id',$updtdata,$id);
					if($response === true){
				     	$this->session->set_flashdata('success', 'Record Successfully Deleted');
				        redirect('user/tools/');
				      }else{
				      	$this->session->set_flashdata('failure',"Record Could Not Deleted");
				      }
				}
				}
			$footer['latest_update'] = $this->user_model->get_latest_update();
			$footer['social_link'] = $this->user_model->get_social_link();
			$this->load->view('header');
			$this->load->view('fms_edit',$data);
			$this->load->view('footer', $footer);
		}else{
			redirect(base_url());
		}
		
	}

	public function fms_delete(){
		if($this->session->userdata('user_id')){
			 $id = $this->uri->segment(3);
		     $response =$this->common_model->deleteData('fms','id',$id);
		     if($response === true){
		     	$this->session->set_flashdata('success', 'Record Successfully Deleted');
		        redirect('user/tools/');
		      }else{
		      	$this->session->set_flashdata('failure',"Record Could Not Deleted");
		      }
		}else{
			redirect(base_url());
		}
		
	}
	
	public function appointment(){   
		$this->load->helper('form');

		if($this->session->userdata('user_id'))
		{
			if($this->session->userdata('user_type') == "trainner")
			{
				$data["trainee"] = $this->user_model->trainee_details();
				$appointments = $this->user_model->view_appointment();

				if($_POST)
				{
				
					$form_data = array(
							'app_date' 			=> date('Y-m-d', strtotime($this->input->post('app_date'))),
							'created_date'		=> date('Y-m-d'),
							'app_start_time' 	=> date('H:i:s', strtotime($this->input->post('app_start_time'))),
							'app_end_time' 		=> date('H:i:s', strtotime($this->input->post('app_end_time'))),
							'trainer_id'		=> $this->session->userdata('user_id'),
							'app_color'			=> $this->input->post('app_color'),
							'trainee_id'		=> $this->input->post('trainee'),
							'app_msg'  			=> $this->input->post('app_message'),
							'active'			=> 1,	
						);
					$data = $this->user_model->insert_appointment($form_data);
					if(isset($data))
					{
		 			if($this->session->userdata('user_type') == 'trainner')
			        { 
			        	$trainner = $this->user_model->get_trainner_name();
			        	$trainee = $this->user_model->get_trainee_name($this->input->post('trainee'));
			        	$message = "Trainner Name :".$trainner['trainner_name'].
									" <br/> Added Dairy Plan For You ".
									" <br/> Date : ".date('Y-m-d');
			        }
						$this->load->library('email');
						$config['protocol'] = 'sendmail';
						$config['mailpath'] = '/usr/sbin/sendmail';
						$config['charset'] = 'iso-8859-1';
						$config['wordwrap'] = TRUE;

						$this->email->initialize($config);
						$this->email->from($data['trainner_email'], 'Your Name');
						$this->email->to($data['trainee_email']); 
   					    $message = "Trainer Name :".$data['trainner_name'].
						" <br/> Trainee Name :".$data['trainee_name'].
						" <br/> Appointment Date : ".date('Y-m-d', strtotime($this->input->post('app_date'))).
						" <br/> Appointment Time : ".date('H:i:s', strtotime($this->input->post('app_start_time'))).
						" To ".date('H:i:s', strtotime($this->input->post('app_end_time')));
						$this->email->subject('Appointment');
						$this->email->message($message);	
						$this->email->send();
/*
---------------------------end code for send email to trainee--------------------------
						*/
						
						$this->session->set_flashdata('success', 'Appointment Add Successfully');
					}
					else
					{
						$this->session->set_flashdata('failure', 'Appointment Could Not Added');
					}
					redirect('user/appointment');
				}

				
			}
			else if($this->session->userdata('user_type') == "trainee")
			{
				$appointments = $this->user_model->view_appointment();
			}
			/*----------start array for full calender data-----------------
			*/
			//echo $this->session->userdata('user_id'); exit();
			if(!empty($appointments))
				{
					foreach($appointments as $va)
					{
						$data['appointmentsArr'][] = array(
		                        'title' => $va['trainee_name'],
		                        'id' => $va['id'],
		                        'start' => $va['app_date'] ." ".$va['app_start_time'],
		                        'end' => $va['app_date'] ." ".$va['app_end_time'],
		                        'backgroundColor' => $va['app_color']
		                      );
					}
				}
				else
				{
					$data['appointmentsArr'] = array();
				}
				// echo "<pre>";
				// print_r($data['appointmentsArr']);die;
				$footer['latest_update'] = $this->user_model->get_latest_update();
				$footer['social_link'] = $this->user_model->get_social_link();
				$data["plans"] = $this->user_model->get_plans();
		    	$this->load->view('header');
				$this->load->view('appointment',$data);
				$this->load->view('footer', $footer);
			/*----------end array for full calender data-----------------
			*/
		}
		else
		{
			$this->index();
		}
		/*echo "<pre>"; trainner
		var_dump($appointments); exit();*/
		
		
		
	}

	public function getBookSlotsData(){
		$app_id = $this->input->post('app_id');
		$data = $this->user_model->getBookSlotsData($app_id);
		/*echo "<pre>";
		var_dump($data); exit();*/
		if(!empty($data))
		{
			echo json_encode($data);
		}
	}
	public function other(){
		$trainer_id =$this->session->userdata('user_id');
		$user_id=$_REQUEST['user_id'];
		$name=$_REQUEST['name'];
		$data["client_quention"] = $this->user_model->client_quetion($user_id);
		?>
	         <tr style="border-top:1px solid #000;">
	                 <th><h3 class="panel-title">Date </h3></th>
	                 <th><h3 class="panel-title">Forename</h3></th>
	                 <th><h3 class="panel-title">Date of Birth</h3></th>
	                 <th><h3 class="panel-title">Email</h3></th>
	                 <th><h3 class="panel-title">Next of Kin</h3></th>
	                 <th><h3 class="panel-title">Job Title</h3></th>
	                 <th><h3 class="panel-title">goals</h3></th>
	                 <?php if($this->session->userdata('user_type') === 'trainner'){ ?>	                
	                 <th colspan="2" style="text-align:center;"><h3 class="panel-title">Action</h3></th>
	                 <?php } ?>
	         </tr>
		<?php
		foreach ($data["client_quention"] as $client_quetion) {
			if($client_quetion != ""){ ?>
			<tr>
			    <td><?php echo ucfirst($client_quetion["date_added"]);?></td>
			    <td><?php echo ucfirst($client_quetion["forename"]);?></td>
				<td><?php echo ucfirst($client_quetion["date_of_birth"]);?></td>
				<td><?php echo ucfirst($client_quetion["email_id"]);?></td>
				<td><?php echo ucfirst($client_quetion["kin"]);?></td>
				<td><?php echo ucfirst($client_quetion["job_title"]);?></td>
				<td><?php echo ucfirst($client_quetion["goal"]);?></td>
				<?php if($this->session->userdata('user_type') === 'trainner'){ ?>
				<td> <a class='btn btn-primary btn-sm' href="<?php echo base_url();?>user/client_quetion_add/<?php echo $client_quetion["trainee_id"];?>/<?php echo $client_quetion["id"];?>"><i class='icon-edit icon-white'></i>Edit &nbsp&nbsp&nbsp</a>
                </td>
				<td>  <a class='btn btn-danger btn-sm' href='<?php echo base_url();?>user/client_quetion_delete?id=<?php echo $client_quetion["id"];?>'onClick='javascript:return confirm("Are you sure to Delete this Record?")'><i class='icon-edit icon-white'></i>Delete </a>
				 </td>
				<?php } ?>
		    </tr>
          <?php
		}
	}
	}

	public function client_quetion_delete(){
		 $id = $_GET['id'];
	     $response =$this->user_model->client_quetion_delete($id);
	     if($response === true){
	     	$this->session->set_flashdata('success', 'Record Successfully Deleted');
	        redirect('user/client_profile/'.$user_id);
	      }else{
	      	$this->session->set_flashdata('failure',"Record Could Not Deleted");
	      }
	}

	public function client_quetion_add(){
			$trainee_id = $this->uri->segment(3);
			$id = $this->uri->segment(4);

			$data["client_quetion"] = $this->user_model->client_quetion_edit($id);
		  	$footer['latest_update'] = $this->user_model->get_latest_update();
			$footer['social_link'] = $this->user_model->get_social_link();
			if($this->input->post()){
				$insertdata=array(
						'trainee_id'=>$trainee_id,
						'user_id'=>$this->session->userdata('user_id'),
						'forename'=>$this->input->post('forename'),
						'surname'=>$this->input->post('surname'),
						'date_of_birth'=>$this->input->post('dob'),
						'email_id'=>$this->input->post('email'),
						'address'=>$this->input->post('address'),
						'kin'=>$this->input->post('kin'),
						'emergency'=>$this->input->post('emergency'),
						'job_title'=>$this->input->post('job_title'),
						'msg'=>$this->input->post('msg'),
						'goal'=>$this->input->post('goal'),
						'date_added'=>date('Y-m-d h:i:s'),
						);
				if(empty($data["client_quetion"])){
					$response_id=$this->common_model->add('quetion',$insertdata);
					redirect('user/client_quetion_add2/'.$trainee_id.'/'.$response_id);
				}else{
					$response_id = $data["client_quetion"][0]['id'];
					$response= $this->common_model->edit('quetion','id',$insertdata,$id);
					redirect('user/client_quetion_add2/'.$trainee_id.'/'.$response_id);
				}
			}
	        $this->load->view('header');
			$this->load->view('client_quetion_add',$data);
			$this->load->view('footer', $footer);
	}

	public function client_quetion_add2(){

			$trainee_id = $this->uri->segment(3);
			$id = $this->uri->segment(4);
			$data["client_quetion"] = $this->user_model->client_quetion_edit($id);
		  	$footer['latest_update'] = $this->user_model->get_latest_update();
			$footer['social_link'] = $this->user_model->get_social_link();
			if($this->input->post()){
				$insertdata=array(
						'heart_condition'=>$this->input->post('heart_condition'),
						'heart_attack'=>$this->input->post('heart_attack'),
						'chest_pain'=>$this->input->post('chest_pain'),
						'consciousness'=>$this->input->post('consciousness'),
						'drugs'=>$this->input->post('drugs'),
						'bone'=>$this->input->post('bone'),
						'smoke'=>$this->input->post('smoke'),
						'medical_conditions'=>$this->input->post('medical_conditions'),
						'exercise'=>$this->input->post('exercise'),
						'agree'=>$this->input->post('agree'),
						'injuries'=>$this->input->post('injuries'),
						'full_name'=>$this->input->post('full_name'),
						'injuries'=>$this->input->post('injuries'),
						'date'=>date('Y-m-d h:i:s'),
						'date_added'=>date('Y-m-d h:i:s'),
						);
					$this->common_model->edit('quetion','id',$insertdata,$id);
					$this->session->set_flashdata('success', 'Record Add Successfully');
					redirect('user/client_profile/'.$trainee_id);
			}
			$this->load->view('header');
			$this->load->view('client_quetion_add2',$data);
			$this->load->view('footer', $footer);
	}

	public function delete_book_app(){
		$app_id = $this->input->post('id');
		$data = $this->user_model->delete_book_app($app_id);
		if($data == true)
		{
			echo "Record Successfully Deleted";
		}
		else
		{
			echo "Record Could Not Deleted";
		}
	}

	public function payment_success(){
		
		$form_data = array(
				'trainner_id'	=>	$this->session->userdata('user_id'),
				'trans_id'		=>	$_REQUEST['tx'],
				'status'		=>	$_REQUEST['st'],
				'amount'		=>	$_REQUEST['amt'],
				'cc'			=>	$_REQUEST['cc'],
				'plan_id'		=>	$_REQUEST['cm'],
				'item_number'	=>	$_REQUEST['item_number']
				);
		$data = $this->user_model->insert_payment($form_data);
		if(isset($data))
		{
			$this->load->library('email');
			$config['protocol'] = 'sendmail';
			$config['mailpath'] = '/usr/sbin/sendmail';
			$config['charset'] = 'iso-8859-1';
			$config['wordwrap'] = TRUE;

			$this->email->initialize($config);
			$this->email->from($data['email'], 'Your Name');
			$this->email->to($data['email']); 
				
			$message = "Trainer Name :".$data['name'].
			" <br/> Payment Date : ".date('Y-m-d').
			" <br/> Plan : ".$data['plan_title'].
			" <br/> Price : ".$data['price'].
			" <br/> Time Duration : ".$data['time']." Days".
			" <br/> Plan Description : ".$data['description'];
			$this->email->subject('Purchased  Plan');
			$this->email->message($message);	

			$this->email->send();
			redirect(base_url());
		}
		else
		{
			redirect(base_url());
		}
		
	}
	/*
---------------------Code written by Aditya Dubey -----------------------------
date : 21/11/2015
----------------------------- start here --------------------------------------- 
	*/
	public function setting(){
		if($this->session->userdata('user_id'))
		{
			$data['setting'] = $this->user_model->get_setting();
			if(empty($data['setting']))
			{
				$q = "insert";
			}
			else
			{
				$q = "update";
			}
			if($_POST)
			{
				if($this->session->userdata('user_type') == 'trainner')
				{
					$col_name = "trainner_id";
					$table_name = "trainner_notification_setting";
					$form_data = array(
						'trainner_id'	=>	$this->session->userdata('user_id'),
						'value'			=>	$this->input->post('setting'),
						'added_date'	=>	date('Y-m-d H:i:s'),
						'status'		=>	1
					);
				}
				else
				{
					$col_name = "trainee_id";
					$table_name = "trainee_notification_setting";
					$form_data = array(
						'trainee_id'	=>	$this->session->userdata('user_id'),
						'value'			=>	$this->input->post('setting'),
						'added_date'	=>	date('Y-m-d H:i:s'),
						'status'		=>	1
					);
				}
				$out = $this->user_model->insert_setting($form_data, $table_name, $col_name, $q);
				if($out == true)
				{
					$this->session->set_flashdata('success', 'Notification Setting Inserted Successfully');
				}
				else
				{
					$this->session->set_flashdata('failure', 'Notification Setting Could Not Inserted');
				}
				redirect(base_url('user/setting'));
			}
			else
			{
				$footer['latest_update'] = $this->user_model->get_latest_update();
				$footer['social_link'] = $this->user_model->get_social_link();
				$data["plans"] = $this->user_model->get_plans();
				$this->load->view('header');
				$this->load->view('setting',$data);
				$this->load->view('footer', $footer);
			}
			
		}
		else
		{
			redirect(base_url());
		}
	}


	/*
----------------Code Added Date : 03/12/2015 --------------------------
----------------- start create function for edit meal plan ----------------------
	*/
	public function get_meal_by_id(){
		if($this->session->userdata('user_id'))
		{
			$meal_plan = $this->user_model->get_meal_by_id($this->input->post('client_id'), $this->input->post('plan_id'));
			if(!empty($meal_plan))
			{
				echo json_encode($meal_plan);
			}
		}
		else
		{
			redirect(base_url());
		}
	}
	public function edit_meal_plan(){
		if($this->session->userdata('user_id'))
		{
			if($_POST)
			{
				$form_data = array(
						'name'			=>	$this->input->post('edit_name'),
						'trainee_id'	=>	$this->input->post('edit_trainee'),
						'category_id'	=>	$this->input->post('edit_category'),
						'start_date'	=>	date('Y-m-d', strtotime($this->input->post('edit_start_date'))),
						'end_date'		=>	date('Y-m-d', strtotime($this->input->post('edit_end_date'))),
						'row'			=>	$this->input->post('edit_row'),
					);
				$plan_id = $this->input->post('plan_id');
				$out = $this->user_model->edit_meal_plan($plan_id, $form_data);
				if($out == true)
				{
					$this->session->set_flashdata('success', 'Meal Plan Successfully Updated');
				}
				else
				{
					$this->session->set_flashdata('failure', 'Meal Plan Could Not Updated');
				}
				redirect(base_url('user/client_profile/').$this->input->post('edit_trainee'));
			}
		}
		else
		{
			redirect(base_url());
		}
	}

	public function delete_meal_plans(){
		$out = $this->user_model->delete_meal_plan($this->input->post('plan_id'));
		if($out == true)
		{
			echo "Meal Plan Successfully Deleted";
		}
		else
		{
			echo "Meal Plan Could Not Deleted";
		}
	}
/*
----------------Code Added Date : 03/12/2015 --------------------------
----------------- end of function for edit meal plan ----------------------
	*/

/* Start Text Chat Code Written By Sorav Garg */

	public function getChatBox()
	{
		$id = base64_decode($this->input->get('user_id'));
		$user_type = $this->session->userdata('user_type');
		$own_id = $this->session->userdata('user_id');
		if($user_type == "trainner"){
			$data['profile_data'] = $this->user_model->getUserProfile($id);
			$data['own_profile'] = $this->user_model->getPTProfile($own_id);
		}
		if($user_type == "trainee"){
			$data['profile_data'] = $this->user_model->getPTProfile($id);
			$data['own_profile'] = $this->user_model->getUserProfile($own_id);
		}
		$data['chating_msg'] = $this->user_model->getChatingMsgs($own_id,$id);
		$data["plans"] = $this->user_model->get_plans();
		$this->load->view('chatbox',$data);
	}

	public function insertChatMsg()
	{
		$msg = $this->input->post('msg');
		$reciever_id = $this->input->post('reciever_id');
		$sender_id = $this->input->post('sender_id');
		$data = array(
			'chat_msg' => $this->input->post('msg'),
			'chat_sender_id' => $this->input->post('sender_id'),
			'chat_reciever_id' => $this->input->post('reciever_id'),
			'added_date' => date('Y-m-d H:i:s')
			);
		$lid = $this->user_model->insertChatMsg($data);
		echo $lid;
	}

	/* End Text Chat Code Written By Sorav Garg */

	/*
----------code written by Aditya Dubey ----------------------------
------------create function for profit and loss start here---------------
	*/
	public function profit_loss(){
		if($this->session->userdata('user_id'))
		{
			$where = array('trainer_id'=> $this->session->userdata('user_id'));
			$data['pl'] = $this->common_model->getAllDatawhere('profit_loss_detail','id','asc',$where);
			$footer['latest_update'] = $this->user_model->get_latest_update();
			$footer['social_link'] = $this->user_model->get_social_link();
			$this->load->view('header');
			$this->load->view('profit_loss',$data);
			$this->load->view('footer', $footer);
		}
		else
		{	
			redirect(base_url());
		}
	}
	public function add_profit_loss(){
		if($this->session->userdata('user_id'))
		{
			$id =$this->uri->segment(3);
			if($_REQUEST)
			{
				if(empty($id)){
				$total =$this->input->post('jan') + $this->input->post('fab') +$this->input->post('march') + $this->input->post('april') +$this->input->post('may') + $this->input->post('june') +$this->input->post('july')+ $this->input->post('aug') + $this->input->post('sep') + $this->input->post('oct') + $this->input->post('nov') + $this->input->post('dec');
				$insertdata =array(
						'trainer_id'=>$this->session->userdata('user_id'),
						'type'=>$this->input->post('type'),
						'name'=>$this->input->post('name'),
						'jan'=>$this->input->post('jan'),
						'feb'=>$this->input->post('feb'),
						'march'=>$this->input->post('march'),
						'april'=>$this->input->post('april'),
						'may'=>$this->input->post('may'),
						'june'=>$this->input->post('june'),
						'july'=>$this->input->post('july'),
						'aug'=>$this->input->post('aug'),
						'sep'=>$this->input->post('sep'),
						'oct'=>$this->input->post('oct'),
						'nov'=>$this->input->post('nov'),
						'dec'=>$this->input->post('dec'),
						'total'=>$total,
						'created_date'=>date('Y-m-d h-i-s'),
					);
				$out = $this->common_model->add('profit_loss_detail',$insertdata);
				if(!empty($out))
				{
					$this->session->set_flashdata('success', 'Profit Loss Added Successfully');
				}
				else
				{
					$this->session->set_flashdata('failure', 'Profit Loss Could Not Added');
				}
				redirect(base_url('user/profit_loss'));
			}else{
				$total =$this->input->post('jan') + $this->input->post('fab') +$this->input->post('march') + $this->input->post('april') +$this->input->post('may') + $this->input->post('june') +$this->input->post('july')+ $this->input->post('aug') + $this->input->post('sep') + $this->input->post('oct') + $this->input->post('nov') + $this->input->post('dec');
				$updatedata =array(
						'trainer_id'=>$this->session->userdata('user_id'),
						'type'=>$this->input->post('type'),
						'name'=>$this->input->post('name'),
						'jan'=>$this->input->post('jan'),
						'feb'=>$this->input->post('feb'),
						'march'=>$this->input->post('march'),
						'april'=>$this->input->post('april'),
						'may'=>$this->input->post('may'),
						'june'=>$this->input->post('june'),
						'july'=>$this->input->post('july'),
						'aug'=>$this->input->post('aug'),
						'sep'=>$this->input->post('sep'),
						'oct'=>$this->input->post('oct'),
						'nov'=>$this->input->post('nov'),
						'dec'=>$this->input->post('dec'),
						'total'=>$total,
						'modify_date'=>date('Y-m-d h-i-s'),
					);
				$out = $this->common_model->edit('profit_loss_detail','id',$updatedata,$id);
				if($out == true)
				{
					$this->session->set_flashdata('success', 'Profit Loss Updated Successfully');
				}
				else
				{
					$this->session->set_flashdata('failure', 'Profit Loss Could Not Updated');
				}
				redirect(base_url('user/profit_loss'));
			}
		}
			
				$footer['latest_update'] = $this->user_model->get_latest_update();
				$footer['social_link'] = $this->user_model->get_social_link();
				$data["profit"] = $this->common_model->getAllDatawhere('profit_loss_detail','id','asc',array('id'=>$id),'1');
				$this->load->view('header');
				$this->load->view('add_profit_loss',$data);
				$this->load->view('footer', $footer);
		}
		else
		{
			redirect(base_url());
		}
	}

	

	public function delete_profit_loss(){
		if($this->session->userdata('user_id'))
		{
			$id =$this->uri->segment(3);
			$response =$this->common_model->deleteData('profit_loss_detail','id',$id);
			if($response == true)
				{
					$this->session->set_flashdata('success', 'Profit Loss Deleted Successfully');
				}
				else
				{
					$this->session->set_flashdata('failure', 'Profit Loss Could Not Deleted');
				}
			redirect(base_url('user/profit_loss'));
		}
		else
		{
			redirect(base_url());
		}
	}
public function subscribe(){
		if($this->input->post('email'))
		{
			$form_data = array(
					'email'			=>	$this->input->post('email'),
					'name'			=>	$this->input->post('name'),
					'created_date'	=>	date('Y-m-d H:i:s'),
					'active'		=>	1,	
				);
			$out = $this->user_model->add_subscribe($form_data);
			if($out == true)
			{
				$this->load->library('email');
				$config['protocol'] = 'sendmail';
				$config['mailpath'] = '/usr/sbin/sendmail';
				$config['charset'] = 'iso-8859-1';
				$config['wordwrap'] = TRUE;

				$this->email->initialize($config);
				$this->email->from($this->input->post('email'));
				$this->email->to($this->input->post('email')); 
		
				$message = " Congratulation ! You Have Successfully Subscribe Ous Services <br/>".
				" We will in touch with you".
				" <br/> Subscribtion Date : ".date('Y-m-d H:i:s');
				$this->email->subject('Appointment');
				$this->email->message($message);	

				$this->email->send();
				$this->session->set_flashdata('success', 'You Have Successfully Subscribed Our Services');
			}
			else
			{
				$this->session->set_flashdata('failure', 'Your Subscribtion is Failed');
			}
			redirect(base_url());
		}
		else
		{
			redirect(base_url());
		}
	}
	
	/* ----------- Code Written by Aditya Dubey ---------------------
--------- function for subscribe end here----------------------------------
	*/

	public function fblogin()
	{
		$data   = $this->input->post();
		$email  = $data['response']['email'];
		$result = $this->common_model->getSingleData('users','email',$email);
		if(!empty($result)){
			if($result[0]['user_type'] == "trainee"){
				echo json_encode(array('msg' => 'trainee'));
			}else{
				$sessionData = array( 'user_id'     => $result[0]['user_id'],
	                                  'email'       => $email,
	                                  'user_type'   => 'trainer',
	                                  'logged_in'   => TRUE
	                            );
				$this->session->set_userdata($sessionData);
				echo json_encode(array('msg' => 'success'));
			}
		}else {
			$loginArr = array(
					'user_type' => 'trainer',
					'email'     => $email,
					'publish_date' => date('Y-m-d')
				);
			$lid = $this->common_model->add('users',$loginArr);
			$trainerArr = array(
				  'user_id'   => $lid,
				  'user_type' => 'trainer',
				  'name'	  => $data['response']['first_name'],
				  'surname'	  => $data['response']['last_name'],
				  'email'     => $email,
				  'publish_date' => date('Y-m-d')
				);
			$tlid = $this->common_model->add('trainner',$trainerArr);
			$sessionData = array( 'user_id'     => $lid,
                                  'email'       => $email,
                                  'user_type'   => 'trainer',
                                  'logged_in'   => TRUE
                            );
			$this->session->set_userdata($sessionData);
			echo json_encode(array('msg' => 'success'));
		}
	}

	public function gplogin()
	{
		$email   = $this->input->post('email');
		$name   = $this->input->post('name');
		$result = $this->common_model->getSingleData('users','email',$email);
		if(!empty($result)){
			if($result[0]['user_type'] == "trainee"){
				echo json_encode(array('msg' => 'trainee'));
			}else{
				$sessionData = array( 'user_id'     => $result[0]['user_id'],
	                                  'email'       => $email,
	                                  'user_type'   => 'trainer',
	                                  'logged_in'   => TRUE
	                            );
				$this->session->set_userdata($sessionData);
				echo json_encode(array('msg' => 'success'));
			}
		}else {
			$loginArr = array(
					'user_type' => 'trainer',
					'email'     => $email,
					'publish_date' => date('Y-m-d')
				);
			$lid = $this->common_model->add('users',$loginArr);
			$trainerArr = array(
				  'user_id'   => $lid,
				  'user_type' => 'trainer',
				  'name'	  => $name,
				  'email'     => $email,
				  'publish_date' => date('Y-m-d')
				);
			$tlid = $this->common_model->add('trainner',$trainerArr);
			$sessionData = array( 'user_id'     => $lid,
                                  'email'       => $email,
                                  'user_type'   => 'trainer',
                                  'logged_in'   => TRUE
                            );
			$this->session->set_userdata($sessionData);
			echo json_encode(array('msg' => 'success'));
		}
	}

	public function cancelPayment()
	{
		$user_id = $this->input->post('user_id');
		$this->common_model->deleteData('users','user_id',$user_id);
		$this->common_model->deleteData('trainner','user_id',$user_id);
	}

	public function getPlanData()
	{
		$plan_id = $this->input->post('plan_id');
		$result  = $this->common_model->getSingleData('plans','id',$plan_id);
		echo json_encode($result);
	}

	public function cancelPaypalPayemnt()
	{
		$user_id = $_GET['id'];
		$this->common_model->deleteData('users','user_id',$user_id);
		$this->common_model->deleteData('trainner','user_id',$user_id);
		redirect('/');
	}

	public function paypalPayemnt()
	{
		$response = $_REQUEST;
		$plan_user_id = $response['cm'];
		$plan_user_id_arr = explode("-", $plan_user_id);
		$plan_id = $plan_user_id_arr[0];
		$trainer_id = $plan_user_id_arr[1];
		$plan_data = $this->common_model->getSingleData('plans','id',$plan_id); 
		$plan_time = $plan_data[0]['time'];
		$current_date = date('Y-m-d');
		$expiry_date = date('Y-m-d', strtotime('+'.$plan_time.' days', strtotime($current_date)));
		if($response['st'] == "Completed"){
			$status = 0;
			$message = 'Payment successfully done';
		}
		if($response['st'] == "Pending"){
			$status = 1;
			$message = 'Your payment status is pending please wait for while admin approval';
		}
		if($response['st'] == "Failed"){
			$status = 2;
			$message = 'Payment failed';
		}
		$paymentArr = array(
			'trainner_id' => $trainer_id,
			'trans_id'    => $response['tx'],
			'status'      => $response['st'],
			'amount'      => $response['amt'],
			'cc'          => $response['cc'],
			'plan_id'     => $plan_id
			);
		$this->common_model->add('payment',$paymentArr);
		$this->common_model->edit('users','user_id',array('payment_status' => $status,'expiry_date' => $expiry_date),$trainer_id);
		$this->common_model->edit('trainner','user_id',array('trainer_payment_status' => $status,'expiry_date' => $expiry_date),$trainer_id);
		$this->session->set_userdata(array('payment_msg' => $message));
		redirect('/');
	}

	public function login1(){
		$email = $this->input->post('email');
		$password = md5($this->input->post('password'));
		$result = $this->user_model->login($email,$password);
		$msg = "";
		if(empty($result)){
			$msg = "invalid";
		}
		else{
			$user_type = $result[0]['user_type'];
			if($user_type == "trainner"){
				$msg = "trainer";
			}
			else{
				$newdata = array(
					    'user_id' 	  => $result[0]['user_id'],
                        'email'       => $result[0]['email'],
	                    'user_type'   => $result[0]['user_type'],
                        'logged_in'   => TRUE,
               		);
				$this->session->set_userdata($newdata);
				$msg = 'success';
			}
		}
		echo json_encode(array('msg' => $msg));
	}

	public function some_service(){
		$data["services"] = $this->user_model->get_services();
		$data["abc"] = $this->user_model->get_service();
		$footer['latest_update'] = $this->user_model->get_latest_update();
		$footer['social_link'] = $this->user_model->get_social_link();
		$this->load->view('header');
		$this->load->view('some_service',$data);
		$this->load->view('footer', $footer);

	}

}
?>
