<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Profile extends CI_Controller {
	public function profiles(){
			$user_id =$_GET['id'];
 			$data["country"] = $this->user_model->get_country();
			$user_type = "trainner";
			$data["plans"] = $this->user_model->get_plans();
			// $user_id = $this->session->userdata('user_id');
			if($user_type == "trainner"){
				
			        $this->db->select('*');
					$this->db->from('trainner');
					$this->db->where('user_id',$user_id);				
					$query=$this->db->get();
                    $data['user'] = $query->result_array();
                    $footer['latest_update'] = $this->user_model->get_latest_update();
					$footer['social_link'] = $this->user_model->get_social_link();
					$this->load->view('header');
			        $this->load->view('trainner_profile',$data);
			        $this->load->view('footer', $footer);

				    }else if($user_type == "trainee"){

			        $this->db->select('*');
					$this->db->from('trainee');
					$this->db->where('user_id',$user_id);				
					$query=$this->db->get();
                    $data['user'] = $query->result_array();
                    $trainee_id = $this->session->userdata('user_id');
                    redirect('user/client_profile/'.$trainee_id);

			        }
	}
}