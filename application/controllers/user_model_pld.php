<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class User_model extends CI_Model {

	function __construct(){
			    parent::__construct();
			    $this->load->library('session');
			     $this->load->library('email');
	}
	function signup(){
				$user_type = $this->input->post('user_type');
				$current_date = date("Y-m-d H:i:s");
				$status = 0;
			    $data=array('user_type' => $this->input->post('user_type'),
			                'email' => $this->input->post('email'),	 
						    'password' => md5($this->input->post('password')),
						    'status' => $status,
						    'publish_date' => $current_date,
						    'modify_date' => $current_date,
						  );
	            $this->db->insert('users',$data);
	            $last_insert_id = $this->db->insert_id();
                 if($user_type == "trainee"){
		        $data1=array('user_id' => $last_insert_id,
			  	              'name' => $this->input->post('name'),
			  			      'surname' => $this->input->post('surname'),
						      'email' => $this->input->post('email'),
						      'country' => $this->input->post('country'),
						      'password' => md5($this->input->post('password')),
						      'status' => $status,				  
						      'publish_date' => $current_date,
						      'modify_date' => $current_date,
						   );
		        $this->db->insert('trainee',$data1);

	          }else if($user_type == "trainner"){
                $data2=array('user_id' => $last_insert_id,
                   	            'user_type' => 'trainner',
			  	                'name' => $this->input->post('name'),
			  			        'surname' => $this->input->post('surname'),
						        'email' => $this->input->post('email'),
						        'country' => $this->input->post('country'),
						        'password' => md5($this->input->post('password')),
						        'status' => $status,				  
						        'publish_date' => $current_date,
						        'modify_date' => $current_date,
						      );
		        $this->db->insert('trainner',$data2);
	          }
	}
	function check_user_exist($email){
		        $email=$this->input->post('email');		                  
		        $this->db->select('*');
                $this->db->from('users');
				$this->db->where('email',$email);				 
				$query=$this->db->get();		
				if($query->num_rows()>0)
				{				
				    return true;
				}
				else
				{
				    return false;
				}
	}
	function get_country(){
				$this->db->order_by('country_name', 'asc'); 
				$query = $this->db->get('countries'); 
				return $query->result_array();
	}
	function get_services(){
				$this->db->order_by('id', 'desc'); 
				$query = $this->db->get('services'); 
				return $query->result_array();
	}
	function get_service(){
				$this->db->order_by('id', 'desc'); 
				$query = $this->db->get('services'); 
				return $query->result_array();
	}
	function service($id){
				$this->db->select('*');
                $this->db->from('services');
				$this->db->where('id',$id);				    
				$query=$this->db->get();
				return $query->result_array();
	}
	function get_plans(){
				$this->db->order_by('id', 'asc'); 
				$query = $this->db->get('plans'); 
				return $query->result_array();
	}
	function login($email,$password){
	            $this->db->select('*');
                $this->db->from('users');
				$this->db->where('email',$email);
				$this->db->where('password',$password);
				$query=$this->db->get();
	            if($query->num_rows()>0){
	         	  foreach($query->result() as $rows){
	            	//add all data to session
	            $newdata = array('user_id' 	   => $rows->user_id,
	                             'email'       => $rows->email,
			                     'user_type'   => $rows->user_type,
		                         'logged_in'   => TRUE,
	                   );
				}
	            $this->session->set_userdata($newdata);
	            return true;            
			}			
			return false;
	}
	function add_profile_trainner(){
		        $user_id = $this->session->userdata('user_id');
				$this->db->select('*');
                $this->db->from('trainner');
				$this->db->where('user_id',$user_id);				    
				$query=$this->db->get();
				return $query->result_array();
	}
	function add_profile_trainee(){
		        $user_id = $this->session->userdata('user_id');
				$this->db->select('*');
                $this->db->from('trainee');
				$this->db->where('user_id',$user_id);				    
				$query=$this->db->get();
				return $query->result_array();
	}
	function edit_profile_trainee(){
		        $user_id = $this->session->userdata('user_id');
				$this->db->select('*');
                $this->db->from('trainee');
				$this->db->where('user_id',$user_id);				    
				$query=$this->db->get();
				return $query->result_array();
	}
	function user_information_trainner($data){
  
		        if($_FILES['user_pic']['name'] != ""){
		               $img=$_FILES['user_pic']['name'];
		        }else{

		        	   $img=$_POST['user_pic'];
		        }		             
                $up=$_FILES['user_pic']['tmp_name'];  
                if(is_uploaded_file($up))
                  {
                    move_uploaded_file($up, "trainner/" .$img);
                  } 
                     
                $newdata = array('password' =>md5($data['new_password']),'user_pic' =>$img ,'rank_level' =>$data['rank_level'],'skill' =>$data['skill'] ,'fb_url' =>$data['fb_url'],'twit_url' =>$data['twit_url'] ,'link_url' =>$data['link_url'],'bio' =>$data['bio'] ,'certi' =>$data['certi'],'award' =>$data['award'] ,'accomplish' =>$data['accomplish'],'loc' =>$data['loc'] ,'credential' =>$data['credential'],'hob' =>$data['hob'] ,'intrest' =>$data['intrest']);
		        $user_id = $this->input->post('user_id');                
	            $this->db->where('user_id', $user_id);
                $this->db->update('trainner', $newdata);     
	}
	function check_password_trainner($data){
	            $user_id = $this->session->userdata('user_id');
		        $current_password = $this->input->post('current_password');
		        $this->db->select('*');
                $this->db->from('trainner');
				$this->db->where('user_id',$user_id);
				$this->db->where('password',$current_password);	 
				$query=$this->db->get();
				if($query->num_rows()>0)
				  {
					return true;
				  }else{	
					return false;
						}
	}
	function user_information_trainee($data){
		        $img=$_FILES['user_pic']['name'];		             
                $up=$_FILES['user_pic']['tmp_name'];  
                if(is_uploaded_file($up))
                 {
                   move_uploaded_file($up, "trainee/" .$img);
                 }   
                $newdata = array('password' =>md5($data['new_password']),'user_pic' =>$img ,'rank_level' =>$data['rank_level'],'skill' =>$data['skill'] ,'fb_url' =>$data['fb_url'],'twit_url' =>$data['twit_url'] ,'link_url' =>$data['link_url']);
		        $user_id = $this->input->post('user_id');                
	            $this->db->where('user_id', $user_id);
                $this->db->update('trainee', $newdata);     
	}
	function check_password_trainee($data){
	            $user_id = $this->session->userdata('user_id');
		        $current_password = md5($this->input->post('current_password'));
		        $this->db->select('*');
                $this->db->from('trainee');
				$this->db->where('user_id',$user_id);
				$this->db->where('password',$current_password);	 
				$query=$this->db->get();
				if($query->num_rows()>0)
					{
					   return true;
					}else{
					   return false;
						 }
	}
	function trainner_details(){
		        $user_id = $this->session->userdata('user_id');
				$this->db->select('*');
                $this->db->from('trainner');		    				    
				$query=$this->db->get();
				return $query->result_array();
	}
	function search_trainner_details($search){
		        $user_id = $this->session->userdata('user_id');			    
                $query = $this->db->query("SELECT * from  trainner  where name Like '$search%'");				  
				return $query->result_array();
	}
	function trainee_details(){
		        $user_id = $this->session->userdata('user_id');
				$this->db->select('*');
                $this->db->from('trainee');		    				    
				$query=$this->db->get();
				return $query->result_array();
	}
	function search_trainee_details($search){
		        $user_id = $this->session->userdata('user_id');			    
                $query = $this->db->query("SELECT * from  trainee  where  name Like '$search%'");				  
				return $query->result_array();
	}
	function trainner_profile($trainner_id){
                $this->db->select('*');
                $this->db->from('trainner');
                $this->db->where('id',$trainner_id);	
				$query=$this->db->get();
				return $query->result_array();				
	}
	function hire_trainner($data){
                $trainner_id = $data["trainner_id"];
                $trainee_id = $data["trainee_id"];                   
                $this->db->select('*');
                $this->db->from('hire_trainers');
                $this->db->where('id',$trainner_id);
                $this->db->where('id',$trainee_id);	
				$query=$this->db->get();
				if($query->num_rows()>0)
				{	   
				  echo "true";
				}else{	    
				  $this->db->insert('hire_trainers',$data);
					 }
	}
	function trainee_upload_image(){
			    $trainee_id = $this->session->userdata('user_id');
                $image=$_FILES['photoimg']['name'];
                $current_date = date("Y-m-d H:i:s");
                $up=$_FILES['photoimg']['tmp_name']; 
                if(is_uploaded_file($up))
                     {
                        move_uploaded_file($up, "trainee_gallery/" .$image);
                     }
                $newdata = array('image_name' =>$image,'image_type' =>'0' ,'id' =>'0','id' =>$trainee_id ,'status' =>'0','added_date' =>$current_date ,'modify_date' =>$current_date);
                     $result = $this->db->insert('photos',$newdata);
                if($result){ 
                         echo "true";
                            }
	}
	function add_trainee1($data){ 
                $back = $this->input->post('back');
			    if($back == ''){ 
	            $current_date = date("Y-m-d H:i:s");
	            $status = 0;
	            $data1 = array('user_type'=>'trainee','email'=>$this->input->post('email'),'password'=>md5($this->input->post('password')),'status'=>$status,'publish_date'=>$current_date,'modify_date'=>$current_date,
	            	);
				$query=$this->db->insert('users', $data1);
				$last_insert_id = $this->db->insert_id();
	            $this->session->set_userdata('last_trainee_id',array('user_id'=>$last_insert_id));
	            $user_type = 'trainee';
	            if($_FILES['user_pic']['name'] != ""){
		               $img=$_FILES['user_pic']['name'];
		        }else{

		        	   $img=$_POST['user_pic'];
		        }		             
                $up=$_FILES['user_pic']['tmp_name'];  
                if(is_uploaded_file($up))
                  {
                    move_uploaded_file($up, "trainee/" .$img);
                  }
	            $data2=array('trainner_id' => $this->input->post('trainner_id'),
	            	                'user_id' => $last_insert_id,
	            	                'user_pic' => $img,
	            	                'user_type' => $user_type,
				  	                'name' => $this->input->post('name'),
				  			        'email' => $this->input->post('email'),
							        'password' => md5($this->input->post('password')),
							        'age' => $this->input->post('age'),
							        'height' => $this->input->post('height'),
							        'weight' => $this->input->post('weight'),
							        'bmr' => $this->input->post('bmr'),
							        'skinfolds' => $this->input->post('skinfolds'),
							        'bicep' => $this->input->post('bicep'),
							        'tricep' => $this->input->post('tricep'),
							        'status' => $status,		  
							        'publish_date' => $current_date,
							        'modify_date' => $current_date,
							      );
	            $query=$this->db->insert('trainee', $data2);
	   //          $this->email->from('your@example.com', 'Your Name');
				// $this->email->to('ajay.coderevolts@gmail.com');
				// $this->email->cc('ajay.coderevolts@gmail.com');
				// $this->email->bcc('ajay.coderevolts@gmail.com');
				// $this->email->subject('Email Test');
				// $this->email->message('Testing the email class.');
				// $this->email->send();  

                }else{
	            $current_date = date("Y-m-d H:i:s");
	            $status = 0;
	            $user_details=$this->session->userdata('last_trainee_id');
	            $user_id=$user_details['user_id'];
	            $data1 = array('user_type'=>'trainee','email'=>$this->input->post('email'),'password'=>$this->input->post('password'),'status'=>$status,'publish_date'=>$current_date,'modify_date'=>$current_date,
	            	);
	            $this->db->where('user_id', $user_id);
				$query=$this->db->update('users', $data1);
	            $user_type = 'trainee';
				$data2=array('trainner_id' => $this->input->post('trainner_id'),
					                'user_id' => $user_id,
	            	                'user_type' => $user_type,
				  	                'name' => $this->input->post('name'),
				  			        'email' => $this->input->post('email'),
							        'password' => $this->input->post('password'),
							        'age' => $this->input->post('age'),
							        'height' => $this->input->post('height'),
							        'weight' => $this->input->post('weight'),
							        'bmr' => $this->input->post('bmr'),
							        'skinfolds' => $this->input->post('skinfolds'),
							        'bicep' => $this->input->post('bicep'),
							        'tricep' => $this->input->post('tricep'),
							        'abs' => $data['abs'],
							        'subscapular' => $data['subscapular'],
							        'chin' => $data['chin'],
							        'chin' => $data['chin'],
							        'pectoral' => $data['pectoral'],
							        'Suprailliac' => $data['Suprailliac'],
							        'mid-axillary' => $data['mid-axillary'],
							        'umbilical' => $data['umbilical'],
							        'knee' => $data['knee'],
							        'medial-calf' => $data['medial-calf'],
							        'quadriceps' => $data['quadriceps'],
							        'hamstrings' => $data['hamstrings'],
							        'goals' => $data['goals'],
							        'notes' => $data['notes'],
							        'status' => $status,		  
							        'publish_date' => $current_date,
							        'modify_date' => $current_date,
							      );
				$this->db->where('user_id', $user_id);
				$query=$this->db->update('trainee', $data2);
				$user_details=$this->session->userdata('last_trainee_id');
	            $user_id=$user_details['user_id'];
	            $this->db->select('*');
		        $this->db->from('trainee');
		        $this->db->where('user_id',$user_id);
		        $query = $this->db->get();
		        return $query->row_array();
               }       
	}
	function edit_trainee1($data){ 
                $back = $this->input->post('back');
			    if($back == ''){ 
	            $current_date = date("Y-m-d H:i:s");
	            $status = 0;
	            $user_id=$this->session->userdata('user_id');
	            $data1 = array('user_type'=>'trainee','email'=>$this->input->post('email'),'password'=>$this->input->post('password'),'status'=>$status,'publish_date'=>$current_date,'modify_date'=>$current_date,
	            	);
				$this->db->where('user_id', $user_id);
				$query=$this->db->update('users', $data1);
	            $user_type = 'trainee';
	            if($_FILES['user_pic']['name'] != ""){
		               $img=$_FILES['user_pic']['name'];
		        }else{

		        	   $img=$_POST['user_pic'];
		        }		             
                $up=$_FILES['user_pic']['tmp_name'];  
                if(is_uploaded_file($up))
                  {
                    move_uploaded_file($up, "trainee/" .$img);
                  }
	            $data2=array('trainner_id' => $this->input->post('trainner_id'),
	            	                'user_id' => $user_id,
	            	                'user_pic' => $img,
	            	                'user_type' => $user_type,
				  	                'name' => $this->input->post('name'),
				  			        'email' => $this->input->post('email'),
							        'password' => $this->input->post('password'),
							        'age' => $this->input->post('age'),
							        'height' => $this->input->post('height'),
							        'weight' => $this->input->post('weight'),
							        'bmr' => $this->input->post('bmr'),
							        'skinfolds' => $this->input->post('skinfolds'),
							        'bicep' => $this->input->post('bicep'),
							        'tricep' => $this->input->post('tricep'),
							        'status' => $status,		  
							        'publish_date' => $current_date,
							        'modify_date' => $current_date,
							      );

	            $this->db->where('user_id', $user_id);
				$query=$this->db->update('trainee', $data2);
				$this->db->select('*');
		        $this->db->from('trainee');
		        $this->db->where('user_id',$user_id);
		        $query = $this->db->get();
		        return $query->row_array();

                }else{
	            $current_date = date("Y-m-d H:i:s");
	            $status = 0;
	            $user_id=$this->session->userdata('user_id');
	            $data1 = array('user_type'=>'trainee','email'=>$this->input->post('email'),'password'=>$this->input->post('password'),'status'=>$status,'publish_date'=>$current_date,'modify_date'=>$current_date,
	            	);
	            $this->db->where('user_id', $user_id);
				$query=$this->db->update('users', $data1);
	            $user_type = 'trainee';
				$data2=array('trainner_id' => $this->input->post('trainner_id'),
					                'user_id' => $user_id,
	            	                'user_type' => $user_type,
				  	                'name' => $this->input->post('name'),
				  			        'email' => $this->input->post('email'),
							        'password' => $this->input->post('password'),
							        'age' => $this->input->post('age'),
							        'height' => $this->input->post('height'),
							        'weight' => $this->input->post('weight'),
							        'bmr' => $this->input->post('bmr'),
							        'skinfolds' => $this->input->post('skinfolds'),
							        'bicep' => $this->input->post('bicep'),
							        'tricep' => $this->input->post('tricep'),
							        'abs' => $data['abs'],
							        'subscapular' => $data['subscapular'],
							        'chin' => $data['chin'],
							        'chin' => $data['chin'],
							        'pectoral' => $data['pectoral'],
							        'Suprailliac' => $data['Suprailliac'],
							        'mid-axillary' => $data['mid-axillary'],
							        'umbilical' => $data['umbilical'],
							        'knee' => $data['knee'],
							        'medial-calf' => $data['medial-calf'],
							        'quadriceps' => $data['quadriceps'],
							        'hamstrings' => $data['hamstrings'],
							        'goals' => $data['goals'],
							        'notes' => $data['notes'],
							        'status' => $status,		  
							        'publish_date' => $current_date,
							        'modify_date' => $current_date,
							      );
				$this->db->where('user_id', $user_id);
				$query=$this->db->update('trainee', $data2);
				$user_id=$this->session->userdata('user_id');
	            $this->db->select('*');
		        $this->db->from('trainee');
		        $this->db->where('user_id',$user_id);
		        $query = $this->db->get();
		        return $query->row_array();
               }       
	}
	function add_trainee2($data){
	           $user_details=$this->session->userdata('last_trainee_id');
	           $user_id=$user_details['user_id'];
	           $this->db->where('user_id', $user_id);
	           $query=$this->db->update('trainee', $data);   
	}
	function edit_trainee2($data){
	           $user_id=$this->session->userdata('user_id');
	           $this->db->where('user_id', $user_id);
	           $query=$this->db->update('trainee', $data);

	           $this->db->select('*');
		       $this->db->from('trainee');
		       $this->db->where('user_id',$user_id);
		       $query = $this->db->get();
		       return $query->row_array();   
	}
	function add_trainee3($data){
	           $user_details=$this->session->userdata('last_trainee_id');
	           $user_id=$user_details['user_id'];
	           $this->db->where('user_id', $user_id);
	           $query=$this->db->update('trainee', $data);   
	}
	function edit_trainee3($data){
	           $user_id=$this->session->userdata('user_id');
	           $this->db->where('user_id', $user_id);
	           $query=$this->db->update('trainee', $data);   
	}
	function trainee_profile_back(){ 
		    $user_details=$this->session->userdata('last_trainee_id');
            $user_id=$user_details['user_id'];
            $this->db->select('*');
	        $this->db->from('trainee');
	        $this->db->where('user_id',$user_id);
	        $query = $this->db->get();
	        return $query->row_array();       
	}
	function edit_trainee_profile_back(){ 
		    $user_id=$this->session->userdata('user_id');
            $this->db->select('*');
	        $this->db->from('trainee');
	        $this->db->where('user_id',$user_id);
	        $query = $this->db->get();
	        return $query->row_array();       
	}
	function client_list(){ 

		    $this->db->select('*');
	        $this->db->from('trainee');
	        $query = $this->db->get();
	        return $query->result_array();    	      
	}
	function search_client_details($search){
		        $user_id = $this->session->userdata('user_id');			    
                $query = $this->db->query("SELECT * from  trainee  where  name Like '$search%'");				  
				return $query->result_array();
	}
	function client_profile($client_id){
                $this->db->select('*');
                $this->db->from('trainee');
                $this->db->where('user_id',$client_id);	
				$query=$this->db->get();
				return $query->result_array();
	}
	function add_nutrition($data){

			    $query=$this->db->insert('nutrition_details', $data);	
			    	
	}
	function search_nutrition_details($user_id){



                $this->db->select('*');
                $this->db->from('nutrition_details');
                $this->db->where('user_id',$user_id);	
				$query=$this->db->get();
				return $query->result_array();
	}
	function search_date_nutrition_details($date){
			   $query = $this->db->query("SELECT * FROM `nutrition_details` WHERE `date` ='$date'");
			   return $query->result_array();
	}
	function add_workout($data){

			    $query=$this->db->insert('workout_details', $data);	
			    	
	}
	function add_client_plan($data){

			    $query=$this->db->insert('client_plan', $data);	
			    	
	}
	function select_client(){

			    $this->db->select('*');
                $this->db->from('trainee');
				$query=$this->db->get();
				return $query->result_array();	
			    	
	}
	function search_client_plan($user_id){
		       
                $this->db->select('*');
                $this->db->from('client_plan');
                $this->db->where('user_id',$user_id);	
				$query=$this->db->get();
				return $query->result_array();
	}
	function add_image($data){

                $user_id = $this->session->userdata('user_id');
                $trainee_id = $data['user_id'];
	            if($_FILES['user_pic']['name'] != ""){
		               $img=$_FILES['user_pic']['name'];
		        }else{

		        	   $img=$_POST['user_pic'];
		        }		             
                $up=$_FILES['user_pic']['tmp_name'];  
                if(is_uploaded_file($up))
                  {
                    move_uploaded_file($up, "add-after-before-image/" .$img);
                  } 
                     
                $newdata = array('trainner_id' =>$user_id,'trainee_id' =>$trainee_id,'image'=>$img);
	            $query=$this->db->insert('add-after-before-image', $newdata); 

	            $last_insert_id = $this->db->insert_id();

	            $this->db->select('*');
                $this->db->from('add-after-before-image');
                $this->db->where('id',$last_insert_id);
				$query=$this->db->get();
				return $query->result_array();
      }
      function show_image(){

      	        $data = $_REQUEST['user_id'];
                $this->db->select('*');
                $this->db->from('add-after-before-image');
                $this->db->where('trainee_id',$data);
				$query=$this->db->get();
				return $query->result_array();  
	   }
	   



	/*
-----------------------------------------------------------------------------
code written by Aditya Dubey start here
Date : 3 Nov 2015 
*/
	public function insert_appointment($form_data){
		if($this->db->insert('appointment', $form_data))
		{
			$this->db->select('tab1.email as trainee_email, tab1.name as trainee_name,
				tab2.email as trainner_email, tab2.name as trainner_name');
			$this->db->from('trainee as tab1, trainner as tab2');
			$this->db->where('tab1.user_id', $this->input->post('trainee'));
			$this->db->where('tab2.user_id', $this->session->userdata('user_id'));
			$data = $this->db->get();
			//echo $this->db->last_query(); exit();
			return $data->row_array();
		}
		else
		{
			return false;
		}
	}

	public function view_appointment(){
		$this->db->select('tab1.*, tab2.name as trainee_name');
		$this->db->from('appointment as tab1');
		$this->db->join('trainee as tab2', 'tab1.trainee_id = tab2.user_id');
		$this->db->where('tab1.active', 1);
		if($this->session->userdata('user_type') == "trainner")
		{
			$this->db->where('tab1.trainer_id', $this->session->userdata('user_id'));
		}
		else
		{
			$this->db->where('tab1.trainee_id', $this->session->userdata('user_id'));
		}
		
		$data = $this->db->get();
		//echo $this->db->last_query(); exit();
		return $data->result_array();
	}

	public function getBookSlotsData($app_id){
		$this->db->select('tab1.*, tab2.name as trainee_name');
		$this->db->from('appointment as tab1');
		$this->db->join('trainee as tab2', 'tab1.trainee_id = tab2.user_id');
		$this->db->where('tab1.id', $app_id);

		$data = $this->db->get();
		//echo $app_id; exit();
		return $data->row_array();
	}

	public function delete_book_app($app_id){
		$this->db->where('id', $app_id);
		if($this->db->delete('appointment'))
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	
	public function insert_payment($form_data){
		if($this->db->insert('payment', $form_data))
		{
			$this->db->select('trainner.email, trainner.name, plans.*');
			$this->db->from('trainner, plans');
			$this->db->where('trainner.user_id', $this->session->userdata('user_id'));
			$this->db->where('plans.id', $form_data['plan_id']);
			$data = $this->db->get();
			//echo $this->db->last_query(); exit();
			return $data->row_array();
		}
		else
		{
			return false;
		}
	}

	public function get_trainee(){
		$this->db->select('user_id, name');
		$this->db->from('trainee');
		$this->db->where('trainner_id', $this->session->userdata('user_id'));
		$data = $this->db->get();
		return $data->result_array();
	}

	public function get_category(){
		$this->db->select('id, name');
		$this->db->from('category');
		$data = $this->db->get();
		return $data->result_array();
	}
	public function insert_meal_plan($form_data){
		if($this->db->insert('meal_plans', $form_data))
		{
			return true;
		}
		else
		{
			return false;
		}
	}

	public function get_meal_plans($client_id){
		$this->db->select('meal_plans.*, category.name as cname');
		$this->db->from('meal_plans');
		$this->db->join('category', 'meal_plans.category_id = category.id');
		$this->db->where('trainee_id', $client_id);
		$data = $this->db->get();
		return $data->result_array();
	}
	/*
-----------------------------------------------------------------------------
code end here
Date : 3 Nov 2015 
*/

}