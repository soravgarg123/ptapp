<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
error_reporting(0);
class Product extends CI_Controller {
	//===============show product list view=========================
	public function product_list_view(){

		//echo ":jo";die;
		$data['client_id'] = '';

		// get all products start
		$where = array(
			'status'=>1
			);
		$limit = 30;
		$offset = 0;

		$data['product_list']=$this->product_model->get_all_product('products',$where,'products.product_id,products.title,products.brand,products.image,products.price','products.product_id',$limit,$offset);
		$data['category_list']=$this->product_model->make_query('select product_category_path,count(product_id) as category_count from products group by product_category_path');
		
		$footer['latest_update'] = $this->user_model->get_latest_update();
		$footer['social_link'] = $this->user_model->get_social_link();

        $data['banner'] = $this->user_model->get_product_banner();
        $data['total_product']=$this->product_model->get_all_product_key_total('products',$where,'products.product_id,products.title,products.brand,products.image,products.price','products.product_id',$limit,$offset,$key);
		$this->load->view('header');
		$this->load->view('product/product_list',$data);
		$this->load->view('footer', $footer);
	}

	function productDetail($product_id)
	{
		$data['product_detail']=$this->product_model->get_recordByid('products',array('product_id'=>$product_id));

		$footer['latest_update'] = $this->user_model->get_latest_update();
		$footer['social_link'] = $this->user_model->get_social_link();
		$this->load->view('header');
		$this->load->view('product/product_detail',$data);
		$this->load->view('footer', $footer);
	}

	function search_product()
	{
       $key="";
		if(isset($_POST['key']))
		{
            $key=$_POST['key'];
		}
		
        $offset = 0;
		if(isset($_POST['offset']))
		{
		  $offset=$_POST['offset'];	
		}
		
        $limit = 30;
		$where = array('status'=>1);
        $data['product_list']=$this->product_model->get_all_product_key('products',$where,'products.product_id,products.title,products.brand,products.image,products.price','products.product_id',$limit,$offset,$key);
        $data['total_product']=$this->product_model->get_all_product_key_total('products',$where,'products.product_id,products.title,products.brand,products.image,products.price','products.product_id',$limit,$offset,$key);
        $this->load->view('product/product_search',$data);
           
	}


	function get_total_product()
	{
       $key="";
		if(isset($_POST['key']))
		{
            $key=$_POST['key'];
		}
		 $offset = 0;
		if(isset($_POST['offset']))
		{
		  $offset=$_POST['offset'];	
		}
		
        $limit = 30;
		$where = array('status'=>1);
	 echo  $total_product=$this->product_model->get_all_product_key_total('products',$where,'products.product_id,products.title,products.brand,products.image,products.price','products.product_id',$limit,$offset,$key);
       
           
	}



	function add_cart($product,$qty,$price)
	{
		$data = array(
               'id'      => $product,
               'qty'     => $qty,
               'price'   => $price,
               'name'    => 'aaa',
               'options' => array()
            );
       $status=$this->cart->insert($data);
       echo count($this->cart->contents());
      
	}
	function checkout()
	{
        $data['cart_detail']=$this->cart->contents();
        $this->load->view('header');
		$this->load->view('product/checkout',$data);
		$this->load->view('footer', $footer);
	}
	
	function place_order()
	{
      $this->form_validation->set_rules('fullname','Full Name','required');
      $this->form_validation->set_rules('phone','Mobile No','required|min_length[10]|max_length[12]');
      $this->form_validation->set_rules('pincode','PinCode','required|min_length[5]|max_length[10]');
      $this->form_validation->set_rules('address','Address','required');
      $this->form_validation->set_rules('city','City','required');
      $this->form_validation->set_rules('email','Email','required|valid_email');
      $this->form_validation->set_rules('state','State','required');


      if($this->form_validation->run()==false)
      {

            $this->load->view('header');
		    $this->load->view('product/placeorder',$data);
		    $this->load->view('footer', $footer);
      }
      else
      {
            $shipid=$this->product_model->add('shiping_master',array('full_name'=>$_POST['fullname'],'mobile_no'=>$_POST['phone'],'pin_no'=>$_POST['pincode'],'address'=>$_POST['address'],'landmark'=>'','email'=>$_POST['email'],'town_city'=>$_POST['city'],'state'=>$_POST['state'],'added_date'=>date('Y-m-d H:i:s'),'client_id'=>$this->session->userdata('user_id')));
      	    $this->session->set_userdata('shiping',array('ship_id'=>$shipid));

      	    redirect('product/showshippingdetail/');
      }
        
	}

	function showshippingdetail()
	{

		   $ship_id=$this->session->userdata['shiping']['ship_id'];
           $data['shiping_detail']=$this->product_model->get_recordByid('shiping_master',array('ship_id'=>$ship_id));
            $data['cart_detail']=$this->cart->contents();
            $this->load->view('header');
		    $this->load->view('product/showpaymentdetail',$data);
		    $this->load->view('footer', $footer);
	}



	function remove_cart_item($rowid)
	{

		$data = array();
		  foreach($this->cart->contents() as $items){
          if($items['rowid'] != $rowid){
           $data[] =$items;
		       }
		   }

		   $this->cart->destroy();
		   $this->cart->insert($data);


	}

	function show_cart_total()
	{ 
        $total=0.00;
		 foreach($this->cart->contents() as $items){
              $total+=($items['price']*$items['qty']);
		   }
        echo $total;
	}

	function show_cart_item($item,$qty)
	{
         $product_detail=$this->product_model->get_recordByid('products',array('product_id'=>$item));

         $responce='<div class="col-md-2"><img src="'.$product_detail[0]['image'].'" style="width:80px;height:60px;"> </div>
                    <div class="col-md-3" style="padding-top:15px"><b>'.$qty.' X '.$product_detail[0]['price'].'£</b></div><div class="col-md-4" style="padding-top:15px">'.$product_detail[0]['title'].'</div><div class="col-md-3" style="padding-top:15px"><b> '.$qty * $product_detail[0]['price'].'£</b> </div>';
                    echo $responce;
	}




	function payment_success()
	{

		// payment process
		$payment_data = array(
					'transaction_id'		=>	$_GET['tx'],
					'amount'				=>	$_GET['amt'],
					'cc'					=>	$_GET['cc'],
					'client_id'				=>	$this->session->userdata('user_id'),
					'payment_date'			=>	date('Y-m-d H:i:s'),
				);

        $paymentid=$this->product_model->add('product_payment_master',$payment_data);

        //order master
        $ship_id=$this->session->userdata['shiping']['ship_id'];
         $cart_detail=$this->cart->contents();
         foreach ($cart_detail as $key => $cart) {
            $total+=$cart['subtotal'];
             }
        $order_data = array(
					'order_user_id'=>$this->session->userdata('user_id'),
					'shiping_id'=>$this->session->userdata['shiping']['ship_id'],
					'order_status'=>1,
					'order_payment_id'=>$paymentid,
					'total_amount'=>$total,
					'order_date'=>date('Y-m-d H:i:s')
				);

        $orderid=$this->product_model->add('product_order_master',$order_data);

       // add order items
    
	    foreach ($cart_detail as $key => $cart) {

	         $order_items_data = array('item_order_id'=>$orderid,
						'product_id'=>$cart['id'],
						'product_price'=>$cart['price'],
						'product_qty'=>$cart['qty'],
						'product_sum'=>$cart['subtotal']);
	        $orderitemid=$this->product_model->add('product_order_items',$order_items_data);
	     }

        if($order_items_data!="" and $orderid!="" and  $paymentid!="")
        {
        	buyer_mail();
           $this->session->set_userdata('payment_sucess',array('msg'=>"Your Order Is Placed Successfully"));	
           $this->cart->destroy();
           redirect('product/product_list_view','refresh');
        }
       

	}
 //upload product script

	function upload()
	{

		ini_set('max_execution_time', '24000'); 
		ini_set('upload_max_filesize', '2048M'); 
		ini_set('post_max_size', '2048M'); 

		$get = file_get_contents('http://dropshipping-stores.com/dropship/GB/Dropship01.XML');
		$arr = (array) simplexml_load_string($get);
		$product_detail=array();
		$productar=array();

		            $dbhost = 'localhost';
		            $dbuser = 'akmwdemo_atul';
		            $dbpass = 'mobiweb@123';
		            $conn = mysql_connect($dbhost, $dbuser, $dbpass);
		             mysql_select_db('akmwdemo_ptapp');
		            
		            if(! $conn ) {
		               die('Could not connect: ' . mysql_error());
		            }


		$count=1;
		for($i=3901;$i<4524;$i++){

		  $product_detail[$i]['groupID']=$arr['product'][$i]->productVariationGroup->groupID;
		  $product_detail[$i]['SKU']=$arr['product'][$i]->SKU;
		  $product_detail[$i]['title']=$arr['product'][$i]->title;
		  $product_detail[$i]['seourl']=$arr['product'][$i]->seourl;
		  $product_detail[$i]['brand']=$arr['product'][$i]->Brand;
          $product_detail[$i]['productID']=$arr['product'][$i]->productID;
		  $product_detail[$i]['flavor']=$arr['product'][$i]->Flavor;
		  $product_detail[$i]['weight']=$arr['product'][$i]->Weight;
		  $product_detail[$i]['image']=$arr['product'][$i]->image1;
		  $product_detail[$i]['product_category_path']=$arr['product'][$i]->product_category_path;
		  $product_detail[$i]['product_price_partner']=$arr['product'][$i]->product_price_partner;
		  $product_detail[$i]['product_price_partner_without_tax']=$arr['product'][$i]->product_price_partner_without_tax;
		  $product_detail[$i]['price']=$arr['product'][$i]->conditionInfo->price;
		  $product_detail[$i]['price_without_tax']=$arr['product'][$i]->conditionInfo->price_without_tax;
		  $product_detail[$i]['quantity']=$arr['product'][$i]->conditionInfo->Quantity;
		  $product_detail[$i]['featured']=$arr['product'][$i]->conditionInfo->Featured;
		  $product_detail[$i]['popularidade']=$arr['product'][$i]->conditionInfo->Popularidade;
		  $product_detail[$i]['IVA_TAX']=$arr['product'][$i]->conditionInfo->IVA_TAX;
		  $product_detail[$i]['productDescription_Long']=$arr['product'][$i]->description->productDescription_Long;
		  $product_detail[$i]['status']=1;
		  $product_detail[$i]['create_date']=date('Y-m-d H:i:s');
		 

		$check_exist = mysql_query("select SKU, groupID from products where groupID = '".$product_detail[$i]['groupID']."' "); 
		$num_rows = mysql_num_rows($check_exist);
		if ($num_rows > 0) {
			
		}
		else{
		    $sql = "INSERT INTO products (groupID, SKU, title, seourl, brand, productID, flavor, weight, image, product_category_path, product_price_partner, product_price_partner_without_tax, price, price_without_tax, quantity, featured, popularidade, IVA_TAX, status, create_date,productDescription_Long,adon_price,update_date)
		 VALUES ('".$product_detail[$i]['groupID']."', '".$product_detail[$i]['SKU']."','". str_replace("'", "' '",$product_detail[$i]['title'])."','". str_replace("'", "' '",$product_detail[$i]['seourl'])."','". str_replace("'", "' '",$product_detail[$i]['brand'])."','". $product_detail[$i]['productID']."','". $product_detail[$i]['flavor']."','". $product_detail[$i]['weight']."','". $product_detail[$i]['image']."','". $product_detail[$i]['product_category_path']."','". $product_detail[$i]['product_price_partner']."','". $product_detail[$i]['product_price_partner_without_tax']."','". $product_detail[$i]['price']."','". $product_detail[$i]['price_without_tax']."','". $product_detail[$i]['quantity']."','". $product_detail[$i]['featured']."','". $product_detail[$i]['popularidade']."','". $product_detail[$i]['IVA_TAX']."','". $product_detail[$i]['status']."','". $product_detail[$i]['create_date']."','". mysql_real_escape_string($product_detail[$i]['productDescription_Long'])."','0.00','0000-00-00')";
		  
		   // echo "<br>";
		   // echo $sql;

		     $rs = mysql_query($sql) or die(mysql_error());
		}
		echo "<br>";
		echo $i;  


		}



	}


	

}//end class here

?>