<main class="animsition">
    <!--Header sec start-->
    <header class="header_sec" id="header">

    </header>
      
    <div class="main_container">

       <div class="slider_wrapper product_slider_wrapper">
            <div id="home_slider" class="owl-carusel  product_slider" >
            <?php if(!empty($banner)) {
                foreach($banner as $b){
                    ?>
                      <div class="item"><img class="img-responsive" src="<?php echo base_url().'images/'.$b['image']; ?>" alt=""/></div>
                    <?php
                }
                }?>
            </div>
        
        </div>




    <div class="container">
     <div class="product_list">

           <div class="row brand_list">
                <div class="col-md-12 col-sm-3">
                <img class="img-responsive" src="<?php echo base_url('images/footericon/brands-over-footer_1874x92_1728_23382.jpg'); ?>" alt=""/>
                </div>
           </div>



         <div class="row">
            <div class="col-md-3 col-sm-3">
              <div class="category_box">
                <div class="pagination_info">
                  <strong> <b class="product_count"><?php echo $total_product;?></b> Products</strong>
                </div>
                <form class="search_filter">
                  <span>search in this list</span>
                  <div class="input-group">
                    <input type="text" class="form-control search_product" placeholder="">
                    <span class="input-group-btn">
                      <button class="btn btn-default" type="button"><i class="fa fa-search" aria-hidden="true"></i>
</button>
                    </span>
                  </div>
                  <!-- /input-group -->
                </form>
                <span class="filter">Filter</span>
                <div class="filter_box">
                  <div class="head_filter">
                    By Category
                  </div>
                  <div class="cotent_filter">
                    <ul>
                    <?php $category_array=array();
                          $subcategory_array=array();
                          $cartind=0;
                          $subcatind=0;

                        foreach($category_list as $list){ 
                          $path=$list['product_category_path'];
                          $path=trim($path);
                        $catar=explode('>',$path);
                        if($catar[0]=="")
                        {
                          $catar[0]="Gear";
                        }
                        if($catar[0]=="")
                        {
                          if($catar[1]!="")
                          {
                            $category_array[$cartind]=$catar[1];
                            $cartind++;
                          }
                          
                        }
                        else
                        {

                          if($catar[0]!="")
                          {
                            $category_array[$cartind]=$catar[0];
                            $cartind++;
                          }

                           
                        }

                        if($catar[1]!="")
                        {
                          if($catar[0]!="")
                          {
                              $rootcat=$catar[0];
                               $subcategory_array[$rootcat][$subcatind]=array('sub'=>$catar[1]);
                               $subcatind++;
                          }
                         
                        }


                          }
                         $category_array = array_unique($category_array);
                           // echo "<pre>";
                          $Newcat=array();
                          $newind=0;
                          foreach($category_array as $index => $category)
                          {
                            $cat=$category_array[$index];
                            $Newcat[$newind]=$cat;
                            $newind++;
                          }
                          $category_array=array();
                          for($i=0;$i<count($Newcat);$i++)
                          {
                               $cat=$Newcat[$i];
                               $category_array[$i]['cat']=$cat  ;
                               $category_array[$i]['sub']=$subcategory_array[$cat];
                          }

                          
                           
                            
                        foreach($category_array as $list)
                        {
                           if(count($list['sub'])>0)
                           {
                                 $subcat_satus=1;
                           }  
                           else{
                                $subcat_satus=0;
                           }   
                        
                        
                     ?>
                      <li>
                        <a>
                            <div class="checkbox <?php if($subcat_satus==0){ echo "subCategorydiv"; } else{ echo "Categorydiv"; } ?>">
                              <label>
                              <input type="checkbox" <?php if($subcat_satus==0){?> class="subCategory"  <?php } else{?>class="Category" <?php } ?> value="<?php if($subcat_satus==0){ echo " >".$list['cat'];}?> " subcat="<?php echo $subcat_satus;?>"> <strong><?php echo $list['cat']; ?> </strong> 
                               
                               

                              <!--   <span class="categoty_count"><?php echo $list['category_count']; ?></span> -->
                              </label>
                              <div style="float:right">
                              <?php if($subcat_satus==0){?><i class="fa fa-minus pull-right "></i><?php } else{?><i class="fa fa-plus pull-right"></i> <?php } ?></div>
                              
                                 <?php  foreach($list['sub'] as $sub)
                                   { ?>
                                 <div class="row sub_cat_div" >
                                  
                                  <div class="col-md-12">
                                    <label>
                                      <input type="checkbox" class="subCategory" value="<?php echo trim($list['cat'])." > ".trim($sub['sub']);?> "> <?php echo $sub['sub']; ?> 
                                      
                                    </label>

                                   
                                  </div>
                                   </div>
                                  <?php } ?>

                              
                            </div>
                        </a>
                      </li>
                    <?php   } ?>
                      
                    
                      <!-- <li>
                        <a href="#">
                           Show More <i class="fa fa-plus"></i>
                        </a>
                      </li> -->

                    </ul>

                  </div>
                </div>
               
              </div>
            </div>
            <div class="col-md-9 col-sm-9">
              <div class="row Product_list">
              <?php if(!empty($product_list)) { //echo "<pre>";print_r($product_list);
                foreach ($product_list as $products) {
                  
               ?>
                <div class="col-md-4 col-sm-5 product">
                  <div class="product_box">
                    <div class="product_box_hover">
                      <div class="product_image">
                        <a target="_blank" href="<?php echo base_url('product/productDetail/'.$products->product_id); ?>">
                          <img src="<?php echo str_replace('http://sstaticc-a.akamaihd.net/', 'https://sscontent.secure.footprint.net/', $products->image); ?>">
                        </a>
                      </div>
                      <div class="product_content">
                        <div class="product_brand">
                          <?php echo $products->brand; ?>
                        </div>
                        <div class="product_name">
                          <?php echo $products->title; ?>
                        </div>
                      </div>
                      <div class="product_action">
                        <div class="product_price_info">
                          <strong>£<?php echo product_price($products->product_id); ?></strong>
                        </div>
                        <a target="_blank" href="<?php echo base_url('product/productDetail/'.$products->product_id); ?>">
                        <button type="button" class="btn_buy">Buy Now</button></a>
                      </div>
                    </div>
                  </div>

                </div>
               <?php } } else {?>

               <?php echo "No Record Found"; } ?>
              <input type="hidden" id="total_product" value="<?php echo $total_product;?>">
              </div><!-- class ="row" ends here-->
               <div class="row">
                  <div class="col-md-5" > </div><div class="col-md-2"><img class="load_more" style="display:none" src="<?php echo base_url('images/LoaderIcon.gif');?>"> </div><div class="col-md-5" > </div>

               </div>

            </div>
          </div>
        </div>
      </div>
    </div>

    </div>
    <!--Main container sec end-->
    <!--Footer sec start-->
    <input type="hidden" id="offset" value="30">
    <footer id="footer" class="footer_sec">

    </footer>
    <!--Footer sec end-->
  </main>

