
    <!--Main container sec start-->
    <div class="main_container">
      <div class="container">
        <div class="product_list product_list1">
          <div class="row">
            <div class="col-md-3 col-sm-3">
              <!-- <div id="product_details"> -->

                <!-- <div class="item"><img src="<?php echo $product_detail[0]['image']; ?>" alt="Owl Image"></div> -->
                <!-- <div class="item"><img src="images/2.jpg" alt="Owl Image"></div> -->

                <div class="product_detail_image">
                  <a href="javascript:">
                    <img class="product_detail_image" src="<?php echo str_replace('http://sstaticc-a.akamaihd.net/', 'https://sscontent.secure.footprint.net/', $product_detail[0]['image']); ?>">
                  </a>
                </div>


              <!-- </div> -->
            </div>
            <div class="col-md-6 col-sm-6">
              <div class="product_details">
                <div class="product_brand">
                  <?php echo $product_detail[0]['brand']; ?>
                </div>
                <div class="product_name">
                  <b> <?php echo $product_detail[0]['title']; ?></b>
                </div>
                <div class="product_describe">
                  <!-- Xcore BCAA 8:1:1 – The most anabolic BCAA in the market with an astonishing 8:1:1 ratio! -->
                  <?php $detail = $product_detail[0]['productDescription_Long'];
                  $striphtml=strip_tags($detail,'<br/>');
                 // echo substr($striphtml, 0,100)."<br/>";
                  echo $striphtml; 
                ?>
                  <!-- <a href="#" class="full_describe">View Full Description</a> -->
                </div>
              </div>
            </div>
            <div class="col-md-3 col-sm-3">
              <div class="product_price_sec">
                <div class="common_price_box">
                  <span class="our_price">Our Price</span>
                  <div class="product_price">£<?php echo product_price($product_detail[0]['product_id']); ?></div>
                </div>
                <?php if(!empty($product_detail[0]['flavor'])) {  ?>
                 <div class="flavour">
                    flavour <span> <?php echo $product_detail[0]['flavor']; ?></span>
                 </div>
                <?php } ?>
                <div class="common_price_box top_none">
                  <span class="our_price">Quantity</span>
                  <div class="product_price quantity bootstrap-select">
                    <form id="bootstrapSelectForm" method="post" class="form-horizontal">
                      <div class="form-group">
                       
                        <div class="col-xs-12 selectContainer">
                          <select name="colors" class="form-control product_qty">
                            <option value="1">1</option>
                            <option value="2">2</option>
                            <option value="3">3</option>
                            <option value="4">4</option>
                            <option value="5">5</option>
                            <option value="6">6</option>
                            <option value="7">7</option>
                            <option value="8">8</option>
                            <option value="9">9</option>
                            <option value="10">10</option>
                        </select>
                        </div>
                      </div>
                    </form>
                    <button class="ad_cart" price="<?php echo product_price($product_detail[0]['product_id']);?>" pid="<?php echo $product_detail[0]['product_id'];?>" >Add To Cart</button>
                   
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

    </div>
    <!--Main container sec end-->
    