<div class="main_container">
            <div class="container">
                <div class="check_out_wrap">
                    <h1 class="page_title"><i class="fa fa-ship" aria-hidden="true"></i>Shipping Detail</h1>

                   <form method="post" action="">
                   <div class="row">
                     <div class="col-md-7">
                     <label>Full Name</label>
                     <span class="form-error" ><?php echo form_error('fullname');?></span>
                     <div class="input-group">
                      
                         <input type="text" name="fullname" class="form-control-ship" >
                   
                     </div>
                     </div>
                  </div>
                 

                    <label>Mobile No </label>
                  <div class="input-group">
                   <span class="error"><?php echo form_error('phone');?></span>
                    <input name="phone" type="text" class="form-control-ship" >
                    
                  </div>



                  <label>Email </label>
                  <div class="input-group">
                   <span class="error"><?php echo form_error('email');?></span>
                    <input name="email" type="text" class="form-control-ship" >
                    
                  </div>


               

                   <label>House Address</label>
                  <div class="input-group">
                   <span class="error"><?php echo form_error('address');?></span>
                    <textarea  name="address" class="form-control-ship" ></textarea>
                   
                  </div>

                
                 <label>Town/City</label>
                   <div class="input-group">
                    <span class="error"><?php echo form_error('city');?></span>
                    <input type="text" name="city" class="form-control-ship" >
                    
                   </div>





                    <label>Post Code/Zip Code</label>
                  <div class="input-group">
                   <span class="error"><?php echo form_error('pincode');?></span>
                    <input name="pincode" type="text" class="form-control-ship" >
                    
                  </div>


                    

                  


                  

                    <label>State</label>
                   <div class="input-group">
                    <span class="error"><?php echo form_error('state');?></span>
                    <input type="text" name="state" class="form-control-ship" >
                    
                   </div>




                  <br><br>

                    
                    <div class="bottom_wrap">
                    <a href="javascript:">
                    <button type="submit" class="pull-left">  Submit <i class="fa fa-caret-right" aria-hidden="true"></i></button></a>
                     
                     </form>   

                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
        </div>