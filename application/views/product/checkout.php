<div class="main_container">
            <div class="container">
                <div class="check_out_wrap">
                    <h1 class="page_title"><i class="fa fa-shopping-cart"></i> Shopping Cart</h1>
                    <table class="table">
                        <thead>
                            <tr>
                                <th>Products</th>
                                <th>Price</th>
                                <th>Quantity</th>
                                <th>Sub-Total</th>
                                <th>Remove</th>
                            </tr>
                        </thead>
                        <tbody>
                        <?php if(count($cart_detail)>0){
                           
                            $total=0;
                            $cartind=1;
                            foreach ($cart_detail as $key => $cart) {
                             $total+=$cart['subtotal'];
                               
                           ?>
                            <tr>
                                <td>
                                    <div class="thumb_img">
                                        <a href="#"><img src="<?php echo product_detail('image',$cart['id']);?>"></a>
                                    </div>
                                    <div class="thumb_details">
                                        <a href="#"><?php echo product_detail('name',$cart['id']);?></a>
                                        <!-- <p>Flavor: Vanilla</p> -->
                                        <!-- <span class="pro_info">Best before end: 2016-07-30</span> -->
                                    </div>
                                </td>
                                <td><span  name="price[]" ind="<?php echo $cartind;?>" class="cart_product_price<?php echo $cartind;?>"><?php echo $cart['price'];?></span> £ </td>
                                <td><select class="cart_qty" ind="<?php echo $cartind;?>" name="cart_qty[1]">
                                <option <?php if($cart['qty']==1){ echo "selected";}?> value="1">1</option>
                                <option <?php if($cart['qty']==2){ echo "selected";}?> value="2">2</option>
                                <option <?php if($cart['qty']==3){ echo "selected";}?> value="3">3</option>
                                <option <?php if($cart['qty']==4){ echo "selected";}?> value="4">4</option>
                                <option <?php if($cart['qty']==5){ echo "selected";}?> value="5">5</option>
                                <option <?php if($cart['qty']==6){ echo "selected";}?> value="6">6</option>
                                <option <?php if($cart['qty']==7){ echo "selected";}?> value="7">7</option>
                                <option <?php if($cart['qty']==8){ echo "selected";}?> value="8">8</option>
                                <option <?php if($cart['qty']==9){ echo "selected";}?> value="9">9</option>
                                <option <?php if($cart['qty']==10){ echo "selected";}?> value="10">10</option>
                                </select></td>
                                <td>£<span  name="price[]" ind="<?php echo $cartind;?>" class="cart_row_sum cart_row_product_price<?php echo $cartind;?>"><?php echo $cart['subtotal'];?></span> </td>
                                <td><a href="javascript:" class="remove_cart_item" productkey="<?php echo $cart['rowid'];?>"><i class="fa fa-trash"></i></a></td>
                            </tr>
                            <?php $cartind++; } } ?>
                            
                            <tr>
                                <td colspan="5" class="tatal">Total:  £<r class="grand_total"><?php echo $total;?></r></td>
                            </tr>
                        </tbody>
                    </table>
                    <div class="bottom_wrap">
                    <a href="<?php echo base_url('product/product_list_view');?>">
                        <button type="button" class="pull-left"> <i class="fa fa-caret-left" aria-hidden="true"></i>
 Continew shopping </button></a>
                       <a href="<?php echo base_url('product/place_order');?>"> <button type="button" class="pull-right">
  Place Order <i class="fa fa-caret-right" aria-hidden="true"></i> </button></a>

                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
        </div>