<div class="main_container">
            <div class="container">
                <div class="check_out_wrap">
                    

                     <div class="row">
                       <div class="col-md-3"> 
                       <h1 class="page_title"><i class="fa fa-shopping-cart"></i> Shipping</h1>
                       <p>
                          <?php echo $shiping_detail[0]['full_name'].", ".$shiping_detail[0]['address'].", ".$shiping_detail[0]['pin_no'].", ".$shiping_detail[0]['town_city'].", ".$shiping_detail[0]['state'];?>
                       </p>

                       </div>

                       <div class="col-md-7"> 
                        <h1 class="page_title"><i class="fa fa-shopping-cart"></i>Cart</h1>
                        <table class="table">
                        <thead>
                            <tr>
                                <th>Products</th>
                                <th>Name</th>
                                <th>Price</th>
                                <th>Quantity</th>
                                <th>Sub-Total</th>
                                
                            </tr>
                        </thead>
                        <tbody>
                        <?php if(count($cart_detail)>0){
                           
                            $total=0;
                            $cartind=1;
                            foreach ($cart_detail as $key => $cart) {
                             $total+=$cart['subtotal'];
                               
                           ?>
                            <tr>
                                <td>
                                    <div class="thumb_img">
                                        <a href="#"><img src="<?php echo product_detail('image',$cart['id']);?>"></a>
                                    </div>
                                    
                                </td>
                                <td><div class="thumb_details">
                                        <a href="#"><?php echo product_detail('name',$cart['id']);?></a>
                                        <!-- <p>Flavor: Vanilla</p> -->
                                        <!-- <span class="pro_info">Best before end: 2016-07-30</span> -->
                                    </div></td>
                                <td>£<span  name="price[]" ind="<?php echo $cartind;?>" class="cart_product_price<?php echo $cartind;?>"><?php echo $cart['price'];?></span> </td>
                                <td><?php echo $cart['qty'];?></td>
                                <td>£<span  name="price[]" ind="<?php echo $cartind;?>" class="cart_row_sum cart_row_product_price<?php echo $cartind;?>"><?php echo $cart['subtotal'];?></span></td>
                               
                            </tr>
                            <?php $cartind++; } } ?>
                            
                            <tr>
                                <td colspan="5" class="tatal">Total: £<r class="grand_total"><?php echo $total;?></r></td>
                            </tr>
                        </tbody>
                    </table>

                       </div>


                       <div class="col-md-2"> 
                           <div class="bottom_wrap" style="margin-top:40px;">
              
                         

          <form action="https://www.sandbox.paypal.com/cgi-bin/webscr" method="post">
          
              <input type="hidden" name="cmd" value="_xclick" />
              <input type="hidden" name="charset" value="utf-8" />
              <input type="hidden" name="business" value="atul-facilitator@mobiwebtech.com" />
              <input type="hidden" name="item_name" value="<?php echo $total;  ?> " />
              <input type="hidden" name="item_number" value="1" />
              <input type="hidden" name="amount" value="<?php echo $total; ?>" />
              <input type="hidden" name="currency_code" value="USD" />
              <input type="hidden" name="custom" value="1"/>
             
              <input type="hidden" name="return" value="<?php echo base_url('product/payment_success/');?>" />
              <input type="hidden" name="bn" value="Business_BuyNow_WPS_SE" />
           
              <a href="<?php echo base_url('product/place_order');?>"> <button type="submit" class="pull-right">
  Make Payment <i class="fa fa-caret-right" aria-hidden="true"></i> </button></a>

             <div class="col-md-8"> 
             
               </div>
          </form>


                           </div>
                       </div>




                      </div>


                    
                </div>
            </div>
        </div>