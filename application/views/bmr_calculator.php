<?php $this->load->view('baneer_section'); ?>
            <div class="container">
                    
                
            </div>
        </div>
        
    </header>
    <!--Header sec end-->
  <!--Main container sec start-->
    <div class="main_container">
    
        <div class="container">
            
            <div class="row">
                <div class="col-sm-3">
                    <?php $user_id = $this->session->userdata('user_id');?>
                    <div class="trainee_tabs_sect">
                          <h3>CALCULATOR</h3>
                          <!-- Nav tabs -->
                          <ul class="nav_tabs">
                            <li onclick="check_tab('BMR');" id="one" class="active" ><a href="#informaiton" aria-controls="informaiton"data-toggle="tab1">BMR</a></li>
                            <li onclick="check_tab('TDEE');" id="two" ><a href="#informaiton" aria-controls="" role="" data-toggle="tab2">TDEE</a></li>
                            <!-- <li onclick="check_tab('IIFYM');" id="three"><a href="#informaiton" aria-controls="" role="" data-toggle="tab3">IIFYM</a></li> -->
                            <li onclick="check_tab('RM');" id="four"><a href="#informaiton" aria-controls="" role="" data-toggle="tab4">1RM</a></li>
                            <li onclick="check_tab('4-site');" id="five"><a href="#informaiton" aria-controls="" role="" data-toggle="tab5">4 Site Caliper Test</a></li>
                            <li onclick="check_tab('7-site');" id="six"><a href="#informaiton" aria-controls="" role="" data-toggle="tab6">7 Site Caliper Test</a></li>
                          </ul>
                    </div>
                    
                </div>
                <div class="col-sm-9">
                    <div class="trainee_tab_content">
                          <form method="post" id="claculate">
                          <input type="hidden" id="tab_name" name="tad_name" value="tab1">
                          <!-- Tab panes -->
                          <div class="tab-content">
                            <div role="tabpanel" class="tab-pane active" id="informaiton">
                                <h3 class="trai_title_sect">Calculate</h3>                              
                                <div class="form_wrapper">
                                    <div class="form-group">
                                        <label>AGE</label>
                                        <input type="text" autofocus placeholder="AGE" class="form-control"  id="age" name="age" autocomplete="off">
                                    </div> 
                                    <div class="form-group">
                                        <label>SEX</label>
                                        <br>
                                        <label class="radio-inline">
                                         <input type="radio" value="male"  id="sex" name="sex">Male</label><label class="radio-inline"><input type="radio"  value="female" id="sex" name="sex">Female</label>
                                       
                                    </div>
                                    <div class="form-group" id="height_box">
                                        <label>Height(CM)</label>
                                        <input type="text" autofocus  placeholder="Height" class="form-control"  id="height" name="height" autocomplete="off">
                                        
                                    </div> 
                                    <div class="form-group" id="weight_box">
                                        <label>Weight(KG)</label>
                                        <input type="text" autofocus  placeholder="Weight" class="form-control" id="weight" name="weight" autocomplete="off">
                                        
                                    </div> 
                                    <div id ='rep_drop'class="form-group">
                                    </div>  
                                    <div id ='bmr_input'class="form-group">
                                    </div>
                                    <div id ='7_site_input'>
                                    </div>                                  
                                </div>
                            </div>
                            <button id="cal" class="btn submit_btn" type="button">Calculate</button>
                          </form>
                          </div>                          
                          <div class="clearfix"></div> 
                    </div>
                    
                </div>
            </div>
            
        </div>
        
    </div>
    
    <!--Main container sec end-->
    
    <div class="clearfix"></div>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script type="text/javascript">
         $("#cal").on('click',function() {
         var age = $('#age').val();
         var sex = $('#sex').val();
         var height = $('#height').val();
         var rep = $('#rep').val();
         var lif = $('#lif').val();
         var weight = $('#weight').val();
         var activity = $('#activity').val();
         var id =$('#tab_name').val();
         if(age !== '' && sex !=='' && height !=='' && weight !==''){
             if(id == 'tab1' ){
                if(sex == 'female'){
                 var  BMR = 655.1 + ( 9.563 * parseInt(weight)) + ( 1.850 * parseInt(height) ) - ( 4.676 * parseInt(age));
                 notify('success','<i>  BMR </i>',BMR);
                 return false;
                }else{
                   var BMR  = 66.5 + (13.75 * parseInt(weight)) + (5.003 * parseInt(height)) - (6.755 * parseInt(age) );
                   notify('success','<i>  BMR </i>',Math.round(BMR));
                    return false;
                }
             }else if(id == 'tab2'){
                    if(sex == 'female'){
                        var  bmr_iifym = 655.1 + ( 9.563 * parseInt(weight)) + ( 1.850 * parseInt(height) ) - ( 4.676 * parseInt(age));
                      }else{
                        var bmr_iifym  = 66.5 + (13.75 * parseInt(weight)) + (5.003 * parseInt(height)) - (6.755 * parseInt(age) );
                    }
                    var IIFYM = parseInt(bmr_iifym) * activity;
                    notify('success','<i>  TDEE </i>',Math.round(IIFYM));
                     return false;
             }else if(id == 'tab3'){
                   var TDEE =10 * parseInt(weight) + 6.25 * parseInt(height) - 5 * parseInt(age) + 5;
                   notify('success','<i>  TDEE </i>',Math.round(TDEE));
                    return false;
             }else if(id == 'tab4'){
                   var RM = parseInt(lif) * ( 1.0278 + ( 0.0278 * parseInt(rep) ) );
                   notify('success','<i> 1RM </i>',Math.round(RM));
                    return false;
             }else if(id == 'tab5'){
                var one = $('#triceps').val();
                var two = $('#suprailiac').val();
                var three = $('#umbilicus').val();
                var four = $('#thigh').val();
                var sum_4site = parseInt(one) + parseInt(two)+ parseInt(three)+ parseInt(four);
                var squre_site = 2 * sum_4site;
                 if(sex == 'female'){
                        var  Body_Fat = (0.29669 * sum_4site ) - (0.00043 * squre_site ) + (0.02963 * parseInt(age)) + 1.4072;
                      }else{
                       var Body_Fat  = (0.29288 * sum_4site ) - (0.0005 * squre_site ) + (0.15845 * parseInt(age)) - 5.76377;
                    }
                   notify('success','<i> 4 Site Body Fat Percentage</i>',Math.round(Body_Fat)+ ' %');
                    return false;
             }else if(id == 'tab6'){
                var one = $('#axilla').val();
                var two = $('#chest').val();
                var three = $('#abdominal').val();
                var four = $('#subscapular').val();
                var five = $('#suprailiac').val();
                var six = $('#tricep').val();
                var seven = $('#thigh').val();
                var sum_7site = parseInt(one) + parseInt(two) + parseInt(three) + parseInt(four) + parseInt(five) + parseInt(six) + parseInt(seven);
                var squre_site = 2 * sum_7site;
                 if(sex == 'female'){
                        var  Body_Fat = 1.097 - (0.00046971 * sum_7site) + (0.00000056 * squre_site) - (0.00012828* parseInt(age));
                      }else{
                        var Body_Fat  =  1.112 - (0.00043499 * sum_7site) + (0.00000055 * squre_site) - (0.00028826 * parseInt(age));
                    }
                   notify('success','<i> 7 Site Body Fat Percentage</i>',Math.round(Body_Fat)+ ' %');
                    return false;
             }
         }else{
            notify('error','<i class="fa fa-times"> Error ! </i>','Please fill all filed !');
            return false
         }
      });

    function check_tab(val){
        var hight ='<div class="form-group" id="height_box"><label>Height(CM)</label><input type="text" autofocus  placeholder="Height" class="form-control"  id="height" name="height" autocomplete="off"></div>';
        var weight ='<div class="form-group" id="weight_box"><label>Weight(KG)</label><input type="text" autofocus  placeholder="Weight" class="form-control" id="weight" name="weight" autocomplete="off"></div>';                            
        if(val =='BMR'){

            $('#rep_drop').html('');
            $('#height_box').html(hight);
            $('#weight_box').html(weight);
            $('#two').removeClass('active');
            $('#tab_name').val('tab1');
            $('#three').removeClass('active');
            $('#four').removeClass('active');
            $('#five').removeClass('active');
            $('#six').removeClass('active');
            $('#one').addClass('active');
             $('#rep_drop').html('');
             $('#bmr_input').html('');
             $('#7_site_input').html('');
        }else if(val =='TDEE'){
            $('#one').removeClass('active');
            $('#tab_name').val('tab2');
            $('#three').removeClass('active');
            $('#four').removeClass('active');
            $('#five').removeClass('active');
            $('#six').removeClass('active');
            $('#two').addClass('active');
            $('#height_box').html(hight);
            $('#weight_box').html(weight);
            var drop_type ='<label>Activity</label><select name="activity" id="activity" autocomplete="off" class="form-control"><option value="">Select activity</option><option value="1.15">Sedentary</option><option value="1.25">Lightly Activity</option><option value="1.35">Active</option><option value="1.4">Very Active</option></select>';
            $('#rep_drop').html('');
            $('#bmr_input').html('');
            $('#rep_drop').html(drop_type);
            $('#7_site_input').html('');
        }else if(val =='IIFYM'){
            $('#tab_name').val('tab3');
            $('#two').removeClass('active');
            $('#one').removeClass('active');
            $('#four').removeClass('active');
            $('#five').removeClass('active');
            $('#six').removeClass('active');
            $('#three').addClass('active');
            $('#height_box').html(hight);
            $('#weight_box').html(weight);
             $('#rep_drop').html('');
             $('#bmr_input').html('');
             $('#7_site_input').html('');
        }else if(val =='RM'){
            $('#tab_name').val('tab4');
            $('#two').removeClass('active');
            $('#one').removeClass('active');
            $('#three').removeClass('active');
            $('#five').removeClass('active');
            $('#six').removeClass('active');
            $('#four').addClass('active');
            $('#height_box').html(hight);
            $('#weight_box').html(weight);
            var input_type ='<label>Repetitions</label><input type="text" autofocus  placeholder="Repetitions" class="form-control" id="rep" name="rep" autocomplete="off">';
            var drop_type ='<label>Weight Lifted</label><input type="text" autofocus  placeholder="Weight Lifted" class="form-control" id="lif" name="lif" autocomplete="off">';
            $('#rep_drop').html('');
            $('#bmr_input').html(drop_type);
            $('#rep_drop').html(input_type);
            $('#7_site_input').html('');
        }else if(val =='4-site'){
            $('#tab_name').val('tab5');
            $('#two').removeClass('active');
            $('#one').removeClass('active');
            $('#four').removeClass('active');
            $('#five').addClass('active');
            $('#six').removeClass('active');
            $('#three').removeClass('active');
             $('#height_box').html('');
             $('#weight_box').html('');
             $('#rep_drop').html('');
             $('#bmr_input').html('');
             var input_type ='<div class="form-group"><label>Triceps(mm)</label><input type="text" autofocus  placeholder="Triceps" class="form-control" id="triceps" name="triceps" autocomplete="off"></div><div class="form-group"><label>Suprailiac(mm)</label><input type="text" autofocus  placeholder="Suprailiac" class="form-control" id="suprailiac" name="suprailiac" autocomplete="off"></div><div class="form-group"><label>Umbilicus(mm)</label><input type="text" autofocus  placeholder="Umbilicus" class="form-control" id="umbilicus" name="umbilicus" autocomplete="off"></div><div class="form-group"><label>Thigh(mm)</label><input type="text" autofocus  placeholder="Thigh" class="form-control" id="thigh" name="thigh" autocomplete="off"></div>';
             $('#7_site_input').html(input_type);
        }else if(val =='7-site'){
            $('#tab_name').val('tab6');
            $('#two').removeClass('active');
            $('#one').removeClass('active');
            $('#four').removeClass('active');
            $('#five').removeClass('active');
            $('#six').addClass('active');
            $('#three').removeClass('active');
            $('#rep_drop').html('');
            $('#height_box').html('');
             $('#weight_box').html('');
             $('#bmr_input').html('');
             var input_type ='<div class="form-group"><label>Axilla(mm)</label><input type="text" autofocus  placeholder="Axilla" class="form-control" id="axilla" name="axilla" autocomplete="off"></div><div class="form-group"><label>Chest(mm)</label><input type="text" autofocus  placeholder="Chest" class="form-control" id="chest" name="Chest" autocomplete="off"></div><div class="form-group"><label>Abdominal(mm)</label><input type="text" autofocus  placeholder="Abdominal" class="form-control" id="abdominal" name="abdominal" autocomplete="off"></div><div class="form-group"><label>Subscapular(mm)</label><input type="text" autofocus  placeholder="Subscapular" class="form-control" id="subscapular" name="subscapular" autocomplete="off"></div><div class="form-group"><label>Suprailiac(mm)</label><input type="text" autofocus  placeholder="Suprailiac" class="form-control" id="suprailiac" name="suprailiac" autocomplete="off"></div><div class="form-group"><label>Tricep(mm)</label><input type="text" autofocus  placeholder="Tricep" class="form-control" id="tricep" name="tricep" autocomplete="off"></div><div class="form-group"><label>Thigh(mm)</label><input type="text" autofocus  placeholder="Thigh" class="form-control" id="thigh" name="thigh" autocomplete="off"></div>';
             $('#7_site_input').html(input_type);
        }

    }
</script>
    
