<?php $this->load->view('baneer_section'); ?>
          <div class="container">
                  <div class="row">
                      <div class="col-sm-12">
                          <h3 class="page_title">Article/blog</h3>
                        </div>
                    </div>
            </div>
        </div>
        
    </header>
    <!--Header sec end-->
    
    <!--Main container sec start-->
    <div class="main_container">
    
      <div class="container">
          <div class="row">
              <div class="col-sm-12">
              <?php if(!empty($article)){
                foreach ($article as $value) {
                
                ?>
                  <div class="blog_main">
                     <div class="blog_data">
                      <h3><?php echo $value['article']; ?></h3>
                       <div class="blog_data_in">
                        <span class="blog_image">
                         <!-- <img src=""> -->
                        </span>
                       <?php echo $value['article']; ?>
                       </p>
                        
                     </div>
                     <div class="blog_button_box">
                         <a href="javascript:void(0)" class="like_btn" main='<?php echo $value['id'];?>' ><i class="fa fa-thumbs-up" aria-hidden="true"></i><?php $responce_like = getlike($value['id']);
                          if(!empty($responce_like)){
                            foreach ($responce_like as $rows) {
                                if($rows['user_id'] == $this->session->userdata('user_id')){
                                 echo  'Liked';
                                }else{
                                  echo 'Like';
                                }
                            }
                          }else{
                            echo 'Like';
                            } ?></a>
                          <a href="javascript:void(0)" class="comment_btn"  id="comment"> <i class="fa fa-comment-o" aria-hidden="true"></i> Comment</a>
                          <div class="comment_box">
                          <form method="post" id="comment_form"> 
                          <input type="hidden" id="comment_ar_id" name="comment_ar_id" value="<?php echo $value['id'];?>" >
                            <textarea class="form-control" name="comment_text" id="comment_text"></textarea>
                            <button class="btn btn_submit" id="comment_btn">submit</button>
                          </form>
                          </div>
                          <?php
                            $responce = getcomment($value['id']);
                           if(!empty($responce)){
                          foreach ($responce as $key ) {
                              ?>
                          <div class="reply_comment">
                            <h4><span class="name_box"><?php echo $key['name']; ?></span><span class="comment_wrap"><?php echo $key['comment']; ?> </span><span class="date_div"><?php echo $key['date_added']; ?></span></h4>
                          </div>
                          <?
                            }
                          }?>
                     </div>
                  </div>
                </div>
                 <?php }
                  }else{?>
                  <div class="no_article">no article</div>
                  <?php }?>
            </div>
            
        </div>
        
    </div>
    
    <!--Main container sec end-->
    
    <div class="clearfix"></div>
