      <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7/jquery.min.js">
</script>
<?php $this->load->view('baneer_section'); ?>
          <div class="container">
                  
                
            </div>
        </div>
        
    </header>
    <!--Header sec end-->
    <!--Main container sec start-->
    <div class="main_container"><!--main_container-->
    
    	<div class="container"><!--container-->
        	
            <div class="row">
            	<div class="col-sm-3"><!--col-sm-3-->
                    <div class="trainee_tabs_sect">
                    	  <h3>Settings</h3>
                    	  <!-- Nav tabs -->
                          <ul class="nav_tabs">
                            <li class="active"><a href="#informaiton" aria-controls="informaiton" role="tab" data-toggle="tab">Notification Settings</a></li>
                            <li><a href="#biography" aria-controls="informaiton" role="tab" data-toggle="tab">Notification Time</a></li>
                          </ul>
                    </div>
                </div><!--col-sm-3-->
                <div class="col-sm-9"><!--col-sm-9-->
                	<div class="trainee_tab_content">
                    	  <!-- Tab panes -->
                          <div class="tab-content">
                            <div role="tabpanel" class="tab-pane active" id="informaiton">
                            	<h3 class="trai_title_sect">Notification Settings</h3>
                            	<?php if(empty($setting)){
                               ?>
                              <h5 class="text-danger">Turn email notifications on/off and choose what time they are sent to you everyday. </h5>
                              <?php } ?>
		         <form method="post" action="">
		           <div class="panel panel_app">
		              <div class="panel-body">
		              On 
		              <?php if(empty($setting) || (isset($setting) && $setting['value'] == 'on') ){ ?>
		              <input type="radio" name="setting" value="on" checked="checked">
		              <?php }else{ ?>
		              <input type="radio" name="setting" value="on">
		                <?php } ?>                   
		              </div>
		              <br/>

		              <div class="panel-body">
		              Off
		              <?php if(!empty($setting) && $setting['value'] == 'off' ){ ?>
		              <input type="radio" name="setting" value="off" checked="checked">
		              <?php }else { ?>
		              <input type="radio" name="setting" value="off">
		              <?php } ?>
		              
		              </div>
		              <br/>

		              <div class="panel-body">
		              <input type="submit" class="btn submit_btn" name="submit" value="Submit" >
		              </div>

		            </div>
		            </form>
                            </div>
                             <div role="tabpanel" class="tab-pane" id="biography">
                            	<h3 class="trai_title_sect">Notification Time</h3>
                            	<form id="time" mathod="post">
                            	<div id="time_mess">Your data submitted successfully</div>
                            	<div class="form-group">
                                     <label>Select Hour</label>
                                     <select class="form-control" name="nutrition_time">
                                     <option value="">Select Time</option>
                                     <?php for($i=1;$i<=24;$i++){?>
                                     <option value="<?php echo $i;?>"><?php echo $i;?></option>  
                                     <?php }$current_date = date("Y-m-d H:i:s"); $user_id=$this->session->userdata('user_id');?>                                 
                                     
                                     </select> 
                                     <input type="hidden" name="created_date" value="<?php echo $current_date;?>"> 
                                     <input type="hidden" name="trainner_id" value="<?php echo $user_id;?>">            
                                 </div>
                            	
                            	<button id="notification_time" class="btn submit_btn" type="button">Submit</button>
                            	</form>
                            </div>
                          </div>
                          <div class="clearfix"></div>
                        </div>
                    
                </div><!--col-sm-9-->
            </div><!--row-->
            
        </div><!--container-->
        
    </div><!--main_container-->
    
    <!--Main container sec end-->
    <div class="clearfix"></div>
    <?php if($this->session->flashdata('success')) { ?>
        <script type="text/javascript">
            var msg = "<?php echo $this->session->flashdata('success'); ?>";
            notify('success','<i class="fa fa-check"> Success </i>',msg);
        </script>
    <?php } ?>

    <?php if($this->session->flashdata('failure')) { ?>
        <script type="text/javascript">
            var msg1 = "<?php echo $this->session->flashdata('failure'); ?>";
            notify('error','<i class="fa fa-times"> Error ! </i>',msg1);                      
         </script>
    <?php } ?>
    
    <script type="text/javascript">
    $(document).ready(function(){
      
         $(".tab-pane").on('click','#notification_time',function() {
            var data1 = new FormData($('#time')[0]);
           $.ajax({
             url:"<?php echo base_url();?>user/notification_time",
             type:"post",
             data: data1,
             contentType: false,
             processData: false,
             success: function(data)
             {
                notify('success','<i class="fa fa-check"> Success </i>','Your data submitted successfully');
                $('#time')[0].reset();
                setTimeout(function() { $("#time_mess").fadeOut(1500); }, 5000)
             }
         });
     
         });
      
    });
</script>
   
