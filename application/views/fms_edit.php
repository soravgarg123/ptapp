<?php $this->load->view('baneer_section'); ?>
          <div class="container">
                  
                
            </div>
        </div>
        
    </header>
    <!--Header sec end-->
  <!--Main container sec start-->
    <div class="main_container">
    
      <div class="container">
          
            <div class="row">
              <div class="col-sm-3">
                  <?php $user_id = $this->session->userdata('user_id');?>
                    <div class="trainee_tabs_sect">
                        <h3>THE FUNCTIONAL MOVEMENT SCREEN</h3>
                        <!-- Nav tabs -->
                          <ul class="nav_tabs">
                            <li class="active" ><a href="javascript:void(0);" aria-controls="informaiton" role="tab" data-toggle="tab">THE FUNCTIONAL MOVEMENT SCREEN</a></li>
                          </ul>
                    </div>                    
                </div>
                <div class="col-sm-9">
                  <div class="trainee_tab_content">
                          <form method="post" id="fms_form">
                        <!-- Tab panes -->
                          <div class="tab-content">
                            <div role="tabpanel" class="tab-pane active" id="informaiton">
                              <h3 class="trai_title_sect">Client Details</h3>
                                <table class="table table-bordered prifile-input-field client fms_main">
                                <thead><tr><th>Date</th><th></th></tr></thead>
                                       <tr>
                                           <th><h3 class="panel-title">Name</h3></th>
                                           <td><select class="form-control" autocomplete="off" name="user_id" required="">
                                             <option value="">Select Client</option>
                                             <?php foreach ($select_client as $clients) {
                                               $val ='';
                                              if(isset($fms[0]['trainee_id'])){

                                                    if($fms[0]['trainee_id'] == $clients['user_id']){
                                                        $val ='selected="selected"';
                                                    }else{
                                                      $val ='';
                                                    }
                                                  }
                                                ?>
                                               <option <?php echo $val;?> value="<?php echo $clients['user_id'];?>"><?php echo $clients['name'];?></option>
                                              
                                             <?php } ?>
                                         </select></td>
                                       </tr>
                                       <tr>
                                           <th><h3 class="panel-title">Age</h3></th>
                                           <td><input type="text" autofocus placeholder="Age" value="<?php if(isset($fms[0]['age'])){ echo $fms[0]['age']; }else{ } ?>" class="form-control" id="age" name="age" autocomplete="off" ></td>
                                       </tr>
                                       <tr>
                                           <th><h3 class="panel-title">Gender</h3></th>
                                           <td><select class="form-control" id="gender" name="gender">
                                            <option value="">Select Gender</option>
                                            <option <?php if(isset($fms[0]['gender'])){if($fms[0]['gender'] =='1'){ echo 'selected="selected"';} }else{ }?> value="1">Male</option>
                                            <option <?php if(isset($fms[0]['gender'])){if($fms[0]['gender'] =='2'){ echo 'selected="selected"'; } }else{ }?> value="2">Female</option>
                                            </select></td>
                                       </tr>
                                       <tr>
                                           <th><h3 class="panel-title">Height(cm)</h3></th>
                                           <td><input type="text" value="<?php if(isset($fms[0]['height'])){ echo $fms[0]['height']; }else{ } ?>" autofocus placeholder="Height(cm)" class="form-control" id="height" name="height" autocomplete="off" ></td>
                                       </tr>
                                       <tr>
                                           <th><h3 class="panel-title">Weight(Kg)</h3></th>
                                           <td><input type="text" autofocus placeholder="Weight(Kg)" value="<?php if(isset($fms[0]['weight'])){ echo $fms[0]['weight']; }else{ } ?>" class="form-control" id="weight" name="weight" autocomplete="off" ></td>
                                       </tr>
                               </table>
                               <h3 class="trai_title_sect">THE FUNCTIONAL MOVEMENT SCREEN</h3>
                               <table class="table table-bordered prifile-input-field client fms_table fms_main">
                               <thead><tr><th>TEST</th><th>SCORE</th><th>COMMENTS</th></tr></thead>
                               <?php
                               if(isset($fms[0]['fms'])){
                                $fms_val =json_decode($fms[0]['fms'],true);
                               }
                               ?>                                                                                                                                                               

                                       <tr>
                                           <th><h3 class="panel-title">DEEP SQUAT</h3></th>
                                           <td><input type="text" autofocus placeholder="DEEP SQUAT" value="<?php if(isset($fms_val['deep_squat'])){ echo $fms_val['deep_squat']; }else{ } ?>" class="form-control" id="deep_squat" name="deep_squat" autocomplete="off" ></td>
                                           <td><input type="text" autofocus placeholder="Comment" value="<?php if(isset($fms_val['deep_squat_comment'])){ echo $fms_val['deep_squat_comment']; }else{ } ?>" class="form-control" id="deep_squat_comment" name="deep_squat_comment" autocomplete="off" ></td>
                                       </tr>
                                       <tr>
                                           <th><h3 class="panel-title"> HURDLE STEP(Left)</h3></th>
                                           <td><input type="text" autofocus placeholder="HURDLE STEP(Left)" value="<?php if(isset($fms_val['hurdle_step_l'])){ echo $fms_val['hurdle_step_l']; }else{ } ?>" class="form-control" id="hurdle_step_l" name="hurdle_step_l" autocomplete="off" ></td>
                                           <td><input type="text" autofocus placeholder="Comment" value="<?php if(isset($fms_val['hurdle_step_l_comment'])){ echo $fms_val['hurdle_step_l_comment']; }else{ } ?>" class="form-control" id="hurdle_step_l_comment" name="hurdle_step_l_comment" autocomplete="off" ></td>
                                       </tr> 
                                       <tr>
                                           <th><h3 class="panel-title">(Right)</h3></th>
                                           <td><input type="text" autofocus placeholder="(Right)" value="<?php if(isset($fms_val['hurdle_step_r'])){ echo $fms_val['hurdle_step_r']; }else{ } ?>" class="form-control" id="hurdle_step_r" name="hurdle_step_r" autocomplete="off" ></td>
                                           <td><input type="text" autofocus placeholder="Comment" value="<?php if(isset($fms_val['hurdle_step_r_comment'])){ echo $fms_val['hurdle_step_r_comment']; }else{ } ?>" class="form-control" id="hurdle_step_r_comment" name="hurdle_step_r_comment" autocomplete="off" ></td>
                                       </tr>
                                       <tr>
                                           <th><h3 class="panel-title">INLINE LUNGE(Left)</h3></th>
                                           <td><input type="text" autofocus placeholder="INLINE LUNGE(Left)" value="<?php if(isset($fms_val['inline_lunge_l'])){ echo $fms_val['inline_lunge_l']; }else{ } ?>" class="form-control" id="inline_lunge_l" name="inline_lunge_l" autocomplete="off" ></td>
                                           <td><input type="text" autofocus placeholder="Comment" value="<?php if(isset($fms_val['inline_lunge_l_comment'])){ echo $fms_val['inline_lunge_l_comment']; }else{ } ?>" class="form-control" id="inline_lunge_l_comment" name="inline_lunge_l_comment" autocomplete="off" ></td>
                                       </tr>
                                       <tr>
                                           <th><h3 class="panel-title">(Right)</h3></th>
                                           <td><input type="text" autofocus placeholder="(Right)" value="<?php if(isset($fms_val['inline_lunge_r'])){ echo $fms_val['inline_lunge_r']; }else{ } ?>" class="form-control" id="inline_lunge_r" name="inline_lunge_r" autocomplete="off" ></td>
                                           <td><input type="text" autofocus placeholder="Comment" value="<?php if(isset($fms_val['inline_lunge_r_comment'])){ echo $fms_val['inline_lunge_r_comment']; }else{ } ?>" class="form-control" id="inline_lunge_r_comment" name="inline_lunge_r_comment" autocomplete="off" ></td>
                                       </tr>
                                       <tr>
                                           <th><h3 class="panel-title">SHOULDER MOBILITY(Left)</h3></th>
                                            <td><input type="text" autofocus placeholder="SHOULDER MOBILITY(Left)" value="<?php if(isset($fms_val['shoulder_mobility_l'])){ echo $fms_val['shoulder_mobility_l']; }else{ } ?>" class="form-control" id="shoulder_mobility_l" name="shoulder_mobility_l" autocomplete="off" ></td>
                                           <td><input type="text" autofocus placeholder="Comment" value="<?php if(isset($fms_val['shoulder_mobility_l_comment'])){ echo $fms_val['shoulder_mobility_l_comment']; }else{ } ?>" class="form-control" id="shoulder_mobility_l_comment" name="shoulder_mobility_l_comment" autocomplete="off" ></td>
                                       </tr>
                                       <tr>
                                           <th><h3 class="panel-title">(Right)</h3></th>
                                           <td><input type="text" autofocus placeholder="(Right)" value="<?php if(isset($fms_val['shoulder_mobility_r'])){ echo $fms_val['shoulder_mobility_r']; }else{ } ?>" class="form-control" id="shoulder_mobility_r" name="shoulder_mobility_r" autocomplete="off" ></td>
                                           <td><input type="text" autofocus placeholder="Comment" value="<?php if(isset($fms_val['shoulder_mobility_r_comment'])){ echo $fms_val['shoulder_mobility_r_comment']; }else{ } ?>" class="form-control" id="shoulder_mobility_r_comment" name="shoulder_mobility_r_comment" autocomplete="off" ></td>
                                       </tr>
                                       <tr>
                                           <th><h3 class="panel-title">IMPINGEMENT CLEARING TEST (Left)</h3></th>
                                          <td><input type="text" autofocus placeholder="IMPINGEMENT CLEARING TEST (Left)" value="<?php if(isset($fms_val['impingement_test_l'])){ echo $fms_val['impingement_test_l']; }else{ } ?>" class="form-control" id="impingement_test_l" name="impingement_test_l" autocomplete="off" ></td>
                                           <td><input type="text" autofocus placeholder="Comment" value="<?php if(isset($fms_val['impingement_test_l_comment'])){ echo $fms_val['impingement_test_l_comment']; }else{ } ?>" class="form-control" id="impingement_test_l_comment" name="impingement_test_l_comment" autocomplete="off" ></td>
                                       </tr>
                                       <tr>
                                           <th><h3 class="panel-title">(Right)</h3></th>
                                           <td><input type="text" autofocus placeholder="(Right)" value="<?php if(isset($fms_val['impingement_test_r'])){ echo $fms_val['impingement_test_r']; }else{ } ?>" class="form-control" id="impingement_test_r" name="impingement_test_r" autocomplete="off" ></td>
                                           <td><input type="text" autofocus placeholder="Comment" value="<?php if(isset($fms_val['impingement_test_r_comment'])){ echo $fms_val['impingement_test_r_comment']; }else{ } ?>" class="form-control" id="impingement_test_r_comment" name="impingement_test_r_comment" autocomplete="off" ></td>
                                       </tr>
                                       <tr>
                                           <th><h3 class="panel-title">ACTIVE STRAIGHT-LEG RAISE (Left)</h3></th>
                                           <td><input type="text" autofocus placeholder="ACTIVE STRAIGHT-LEG RAISE (Left)" value="<?php if(isset($fms_val['active_straight_l'])){ echo $fms_val['active_straight_l']; }else{ } ?>" class="form-control" id="active_straight_l" name="active_straight_l" autocomplete="off" ></td>
                                           <td><input type="text" autofocus placeholder="Comment" value="<?php if(isset($fms_val['active_straight_l_comment'])){ echo $fms_val['active_straight_l_comment']; }else{ } ?>" class="form-control" id="active_straight_l_comment" name="active_straight_l_comment" autocomplete="off" ></td>
                                       </tr>
                                       <tr>
                                           <th><h3 class="panel-title">(Right)</h3></th>
                                            <td><input type="text" autofocus placeholder="(Right)" value="<?php if(isset($fms_val['active_straight_r'])){ echo $fms_val['active_straight_r']; }else{ } ?>" class="form-control" id="active_straight_r" name="active_straight_r" autocomplete="off" ></td>
                                           <td><input type="text" autofocus placeholder="Comment" value="<?php if(isset($fms_val['active_straight_r_comment'])){ echo $fms_val['active_straight_r_comment']; }else{ } ?>" class="form-control" id="active_straight_r_comment" name="active_straight_r_comment" autocomplete="off" ></td>
                                       </tr>
                                       <tr>
                                           <th><h3 class="panel-title"> TRUNK STABILITY PUSH-UP</h3></th>
                                           <td><input type="text" autofocus placeholder="TRUNK STABILITY PUSH-UP" value="<?php if(isset($fms_val['trunk_stablity'])){ echo $fms_val['trunk_stablity']; }else{ } ?>" class="form-control" id="trunk_stablity" name="trunk_stablity" autocomplete="off" ></td>
                                           <td><input type="text" autofocus placeholder="Comment" value="<?php if(isset($fms_val['trunk_stablity_comment'])){ echo $fms_val['trunk_stablity_comment']; }else{ } ?>" class="form-control" id="trunk_stablity_comment" name="trunk_stablity_comment" autocomplete="off" ></td>
                                       </tr>
                                       <tr>
                                           <th><h3 class="panel-title"> PRESS-UP CLEARING TEST</h3></th>
                                           <td><input type="text" autofocus placeholder="PRESS-UP CLEARING TEST" value="<?php if(isset($fms_val['press_up'])){ echo $fms_val['press_up']; }else{ } ?>" class="form-control" id="press_up" name="press_up" autocomplete="off" ></td>
                                           <td><input type="text" autofocus placeholder="Comment" value="<?php if(isset($fms_val['press_up_comment'])){ echo $fms_val['press_up_comment']; }else{ } ?>" class="form-control" id="press_up_comment" name="press_up_comment" autocomplete="off" ></td>
                                       </tr>
                                       <tr>
                                           <th><h3 class="panel-title">ROTARY STABILITY(Left)</h3></th>
                                           <td><input type="text" autofocus placeholder="ROTARY STABILITY(Left)" value="<?php if(isset($fms_val['rotary_stability_l'])){ echo $fms_val['rotary_stability_l']; }else{ } ?>" class="form-control" id="rotary_stability_l" name="rotary_stability_l" autocomplete="off" ></td>
                                           <td><input type="text" autofocus placeholder="Comment" value="<?php if(isset($fms_val['rotary_stability_l_comment'])){ echo $fms_val['rotary_stability_l_comment']; }else{ } ?>" class="form-control" id="rotary_stability_l_comment" name="rotary_stability_l_comment" autocomplete="off" ></td>
                                       </tr>
                                       <tr>
                                           <th><h3 class="panel-title">(Right)</h3></th>
                                           <td><input type="text" autofocus placeholder="(Right)" value="<?php if(isset($fms_val['rotary_stability_r'])){ echo $fms_val['rotary_stability_r']; }else{ } ?>" class="form-control" id="rotary_stability_r" name="rotary_stability_r" autocomplete="off" ></td>
                                           <td><input type="text" autofocus placeholder="Comment" value="<?php if(isset($fms_val['rotary_stability_r_comment'])){ echo $fms_val['rotary_stability_r_comment']; }else{ } ?>" class="form-control" id="rotary_stability_r_comment" name="rotary_stability_r_comment" autocomplete="off" ></td>
                                       </tr>
                                       <tr>
                                           <th><h3 class="panel-title">POSTERIOR ROCKING CLEARING TEST</h3></th>
                                            <td><input type="text" autofocus placeholder="POSTERIOR ROCKING CLEARING TEST" value="<?php if(isset($fms_val['posterior'])){ echo $fms_val['posterior']; }else{ } ?>" class="form-control" id="posterior" name="posterior" autocomplete="off" ></td>
                                           <td><input type="text" autofocus placeholder="Comment" value="<?php if(isset($fms_val['posterior_comment'])){ echo $fms_val['posterior_comment']; }else{ } ?>" class="form-control" id="posterior_comment" name="posterior_comment" autocomplete="off" ></td>
                                       </tr>
                          </table>
                    
                            <input type="submit" id="submit_fms" class="btn submit_btn" value="Submit">
                          </form>
                          </div>
                          </div>                          
                          <div class="clearfix"></div> 
                    </div>
                    
                </div>
            </div>
            
        </div>
        
    </div>
    
    <!--Main container sec end-->
    
    <div class="clearfix"></div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<script type="text/javascript">
          $(document).ready(function(){
          $('#datepicker1').datepicker({
              autoclose:true,
              format: 'yyyy-mm-dd',
              startDate:'1970-01-01' ,
              endDate : '2050-12-31'
          });
        });
</script>
    
