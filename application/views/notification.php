<?php $this->load->view('baneer_section'); ?>
          <div class="container">
                  <div class="profile_header">
                        <div class="col-sm-2">
                          <span class="user_profile_pic"><img class=" img-responsive" src="<?php echo base_url(); ?>trainner/<?php echo $user[0]['user_pic'];?>"/>
                          </span>
                        </div>
                        <div class="col-sm-8">
                          <div class="user_profile_info">
                              <h3><?php echo $user[0]['name'].' '.$user[0]['surname']; ?></h3>
                                <span class="user_degi"><?php echo $user[0]['type'];?></span>
                                <p><?php echo $user[0]['intrest'];?></p> 
                            </div>
                        </div>
                        <div class="col-sm-2">
                          <ul class="social_icon_user">
                                  <li><a href="http://www.facebook.com/sharer.php?u=<?php echo base_url();?>profile/profiles?id=<?php echo $this->session->userdata('user_id');?>"><span class="fa fa-facebook"></span></a></li>
                                    <li><a href="http://twitter.com/home?status=<?php echo base_url();?>profile/profiles?id=<?php echo $this->session->userdata('user_id');?>"><span class="fa fa-twitter"></span></a></li>
                                    <li><a href="https://www.linkedin.com/cws/share?url='<?php echo base_url();?>profile/profiles?id=<?php echo $this->session->userdata('user_id');?>"><span class="fa fa-linkedin"></span></a></li>
                                    <!-- <li><a href="http://twitter.com/home?status=<?php echo base_url();?>profile/profiles?id=<?php echo $this->session->userdata('user_id');?>" target="_blank"><span class="fa fa-youtube"></span></a></li> -->
                            </ul>
                        </div>
                      
                    </div>
                
            </div>
        </div>
        
    </header>
    <!--Header sec end-->
    
    <!--Main container sec start-->
    <div class="main_container">
    
      <div class="container">
            <div class="row message_popup_main">
              <div class="col-md-12">
        <?php if(!empty($chat)){?>
              <div class="modal fade bs-example-modal-sm" id="msg_modal" tabindex="-1" role="dialog" aria-labelledby="login_modal">
          <div class="modal-dialog modal-sm" role="document">
            <div class="modal-content custom_modal">
            <button type="button" class="close mar_close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
              <div class="panel_block my_trainees">
                                <div class="panel_block_heading">

                                  <h4>Messages</h4>
                                </div>
                                <div class="panel_block_body">
                                  <ul>
                                    <?php foreach ($chat as $chats) { ?>
                                                <li>
                                                    <div class="pbb_left">
                                                        <div class="pbbl_img">
                                                            <img src="<?php echo base_url();?>trainner/<?php echo $user[0]['user_pic'];?>" alt="img" class="img-circle">
                                                        </div>
                                                        <div class="pbbl_txt">
                                                         <p><?php echo $chats['chat_msg'];?></p>
                                                        </div>
                                                    </div>
                                                </li>
                                    <?php   }  ?> 
                                  </ul>
                                </div>
                          </div>
              
            </div>
          </div>
        </div>
               <?php } ?>

              </div>
            </div>
            <div class="row client_box">
              <div class="col-sm-8">
                  <div class="row">

                      <div class="col-sm-6">
                          <div class="panel_block my_trainees">
                                <div class="panel_block_heading">
                                  <h4>Clients</h4>
                                </div>
                                <div class="panel_block_body">
                                  <ul>
                                  <?php foreach ($clients as $get_clients) { ?>
                                      <li>
                                      <a href="<?php echo base_url(); ?>user/client_profile/<?php echo $get_clients['user_id'];?>">
                                          <div class="pbb_left">
                                              <div class="pbbl_img">
                                              <img src="<?php echo base_url();?>trainee/<?php echo $get_clients['user_pic'];?>" alt="img" class="img-circle">
                                                <!-- <a href="<?php echo base_url(); ?>user/client_profile/<?php echo $get_clients['user_id'];?>"><img src="<?php echo base_url();?>trainee/<?php echo $get_clients['user_pic'];?>" alt="img" class="img-circle"></a> -->
                                              </div>
                                              <div class="pbbl_txt" style="color:black;">
                                                <?php echo $get_clients['name'];?>
                                              </div>
                                          </div>
                                          <div class="clearfix"></div>
                                          </a>
                                      </li>
                                     <?php } ?>
                                  </ul>
                                </div>
                          </div>
                      </div>
                      <div class="col-sm-6">
                          <div class="panel_block my_trainees">
                                <div class="panel_block_heading">
                                  <h4>Messages</h4>
                                </div>
                                <div class="panel_block_body">
                                  <ul>
                                    <?php foreach ($chat as $chats) { ?>
                                                <li>
                                                    <div class="pbb_left">
                                                        <div class="pbbl_img">
                                                            <img src="<?php echo base_url();?>trainner/<?php echo $user[0]['user_pic'];?>" alt="img" class="img-circle">
                                                        </div>
                                                        <div class="pbbl_txt">
                                                         <p><?php echo $chats['chat_msg'];?></p>
                                                        </div>
                                                    </div>
                                                </li>
                                    <?php   }  ?> 
                                  </ul>
                                </div>
                          </div>
                      </div>
                    </div>
                   
                </div>
                <div class="col-sm-4">
                  <aside class="innerp_right_bar">
                  <div class="panel_block notification">
                                <div class="panel_block_heading">
                                  <h4>Notification</h4>
                                </div>
                                <div class="panel_block_body">
                                  <ul>
                                   <?php  foreach ($notification as $get_notification) { ?>
                                      <li>
                                          <a href="<?php echo base_url(); ?>user/client_profile/<?php echo $get_notification['sender_id']; ?>">
                                          <div class="pbb_left">
                                              <div class="pbbl_img">
                                              <img src="<?php echo base_url(); ?>trainee/<?php echo $get_notification['trainee_pic'];?>" height="70" width="70" class="img-circle">
                                                </div>
                                                <div class="pbbl_txt">
                                                  <h5 style="color:black;"><?php echo $get_notification['message'];?></h5>
                                                </div>
                                            </div>
                                            <div class="pbb_right">
                                              <div class="mesg_time" style="color:black;">
                                                  <?php echo time_elapsed_string($get_notification['added_date']); ?>
                                                </div>
                                            </div>
                                            <div class="clearfix"></div>
                                            </a>
                                        </li>
                                        
                                        <?php } ?>
                                    </ul>
                                </div>
                            </div>
                     </aside>
                </div>
            </div>
            
            
        </div>
        
    </div>
    
    <!--Main container sec end-->
    
    <div class="clearfix"></div>
  
     <?php
      function time_elapsed_string($datetime, $full = false)
      {
          $now     = new DateTime;
          $ago     = new DateTime($datetime);
          $diff    = $now->diff($ago);
          $diff->w = floor($diff->d / 7);
          $diff->d -= $diff->w * 7;
          
          $string = array(
              'y' => 'year',
              'm' => 'month',
              'w' => 'week',
              'd' => 'day',
              'h' => 'hour',
              'i' => 'minute',
              's' => 'second'
          );
          foreach ($string as $k => &$v) {
              if ($diff->$k) {
                  $v = $diff->$k . ' ' . $v . ($diff->$k > 1 ? 's' : '');
              } else {
                  unset($string[$k]);
              }
          }
          
          if (!$full)
              $string = array_slice($string, 0, 1);
          return $string ? implode(', ', $string) . ' ago' : 'just now';
      }
  ?>

  <script type="text/javascript">
    //  $(window).load(function(){
    //     $('#msg_modal').modal('show');
    // });
    
  </script>

   
