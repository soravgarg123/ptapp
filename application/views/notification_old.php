<?php $this->load->view('baneer_section'); ?>
        	<div class="container">
                	<div class="profile_header">
                    	<div class="col-sm-2">
                        	<span class="user_profile_pic"><img class=" img-responsive" src="<?php echo base_url(); ?>trainner/<?php echo $user[0]['user_pic'];?>" alt=""/></span>
                        </div>
                        <div class="col-sm-8">
                        	<div class="user_profile_info">
                            	<h3><?php echo $user[0]['name'].' '.$user[0]['surname']; ?></h3>
                                <span class="user_degi">Student</span>
                                <p><?php echo $user[0]['intrest'];?></p> 
                            </div>
                            <!-- <div class="add_profile" style="padding-left:10px;"><a class="btn submit_profile" id="update" href="<?php echo base_url();?>user/add_profile_trainner">Edit Profile</a></div>
                            <div class="add_profile" ><a class="btn submit_profile" target="_blank" href="<?php echo $user[0]['youtube'];?>">Youtube</a></div> -->
                        </div>
                        <div class="col-sm-2">
                        	<ul class="social_icon_user">
                                	<li><a href="<?php echo $user[0]['fb_url'];?>" target="_blank"><span class="fa fa-facebook"></span></a></li>
                                    <li><a href="<?php echo $user[0]['twit_url'];?>" target="_blank"><span class="fa fa-twitter"></span></a></li>
                                    <li><a href="<?php echo $user[0]['link_url'];?>" target="_blank"><span class="fa fa-linkedin"></span></a></li>
                            </ul>

                        </div>
                    	 
                    </div>
                
            </div>
        </div>
        
    </header>
    <!--Header sec end-->
    
    <!--Main container sec start-->
    <div class="main_container">
    
    	<div class="container">
        	<div class="row">
            	<div class="col-sm-12">
                	<div class="panel panel_app">
                      <div class="panel-heading">
                       
                          
                           <div class="row">
                                <div class="col-sm-4"> <h3 class="panel-title">Client image </h3></div>
                                <div class="col-sm-4"> <h3 class="panel-title"> Message</h3></div>
                                <div class="col-sm-4"> <h3 class="panel-title"> Date & Time</h3></div>

                           </div>

                       
                      </div>
                      <?php foreach ($notification as $get_notification) { ?>
                      <div class="panel-body">
                           <div class="row">
                                <div class="col-sm-4"><p><img src="<?php echo base_url(); ?>trainee/<?php echo $get_notification['trainee_pic'];?>" height="70" width="70"></p></div>
                                <div class="col-sm-4"><p><?php echo $get_notification['message'];?></p></div>
                                <div class="col-sm-4"><p><?php echo time_elapsed_string($get_notification['added_date']); ?></p></div>

                           </div>
                       
                      </div>
                      <?php } ?>
                    </div>
                </div>
            </div>
            
        </div>
        
    </div>
    
    <!--Main container sec end-->
    
    <div class="clearfix"></div>

    <?php
      function time_elapsed_string($datetime, $full = false)
      {
          $now     = new DateTime;
          $ago     = new DateTime($datetime);
          $diff    = $now->diff($ago);
          $diff->w = floor($diff->d / 7);
          $diff->d -= $diff->w * 7;
          
          $string = array(
              'y' => 'year',
              'm' => 'month',
              'w' => 'week',
              'd' => 'day',
              'h' => 'hour',
              'i' => 'minute',
              's' => 'second'
          );
          foreach ($string as $k => &$v) {
              if ($diff->$k) {
                  $v = $diff->$k . ' ' . $v . ($diff->$k > 1 ? 's' : '');
              } else {
                  unset($string[$k]);
              }
          }
          
          if (!$full)
              $string = array_slice($string, 0, 1);
          return $string ? implode(', ', $string) . ' ago' : 'just now';
      }
  ?>

