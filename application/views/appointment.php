<?php $this->load->view('baneer_section'); ?>
          <div class="container">
                  <div class="row">
                      <div class="col-sm-12">
                          <h3 class="page_title">Diary</h3>
                        </div>
                        
                    </div>
            </div>
        </div>
        
    </header>
    <!--Header sec end-->

<?php if($this->session->userdata('user_type') == "trainner"){  ?>
      <!-- Book Appoinment Modal Start -->
        <div class="modal fade" id="book_appo_Modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
          <div class="modal-dialog" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Book Slots</h4>
              </div>
              <div class="modal-body">
                <form method="post" action="<?php echo base_url('user/appointment'); ?>" >
                  <div class="col-md-12">                        
                      <div class="col-md-4"> Date</div>
                      <div class="col-md-8">
                        <input type="text" readonly id="app_date" name="app_date" class="form-control" />
                      </div>
                  </div></br></br>
                  <div class="col-md-12">                        
                      <div class="col-md-4">Start Time</div>
                      <div class="col-md-8">
                        <input type="text"   id="app_start_time" required="required" name="app_start_time" class="form-control" />
                      </div> 
                  </div></br></br>
                  <div class="col-md-12">                        
                      <div class="col-md-4">End Time</div>
                      <div class="col-md-8">
                        <input type="text"  id="app_end_time" required="required" name="app_end_time" class="form-control" />
                      </div>
                  </div></br></br>
                  <div class="col-md-12">                        
                      <div class="col-md-4">Color</div>
                      <div class="col-md-8">
                        <input type="color"  id="app_color" name="app_color" class="form-control" />
                      </div>
                  </div></br></br>

                    <div class="col-md-12">                        
                      <div class="col-md-4">Trainee</div>
                      <div class="col-md-8">
                        <!-- <input type="color"  id="app_color"  name="app_color" class="form-control" /> -->
                        <?php
                        $data = array(null =>  'Select Trainee');
                        if(isset($trainee))
                        {
                          foreach($trainee as $a)
                          {
                            $data[$a['user_id']] = $a['name'];
                          }
                          echo form_dropdown('trainee', $data, '', 'id="trainee" class="form-control" required="required"');
                        }
                         ?>
                        <span class="error"></span>
                      </div>
                  </div></br></br>

                  <div class="col-md-12">                        
                    <div class="col-md-4"> Message</div>
                    <div class="col-md-8">
                      <textarea name="app_message" id="app_message" class="form-control" rows="2"></textarea>
                    </div>
                </div></br></br>
               </div></br>
              <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="submit" id="submit-btn" class="btn btn-primary">Submit</button>
              </div>
              </form>
            </div>
          </div>
        </div>
        <!--  Book Appoinment Modal End -->


          <!-- ///////////////View Appoinment View Modal Start/////////////// -->
        <div class="modal fade" id="book_appo_view" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
          <div class="modal-dialog" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Appoinment</h4>
              </div>
              <div class="modal-body">
                  <div class="col-md-12">                        
                      <div class="col-md-4"> Date</div>
                      <div class="col-md-8">
                        <input type="text" readonly id="tainee_app_date_view"  class="form-control" />
                      </div>
                  </div></br></br>
                  <div class="col-md-12">                        
                      <div class="col-md-4">Start Time</div>
                      <div class="col-md-8">
                        <input type="text" readonly id="tainee_app_start_time_view"  class="form-control" />
                      </div>
                  </div></br></br>
                  <div class="col-md-12">                        
                      <div class="col-md-4">End Time</div>
                      <div class="col-md-8">
                        <input type="text" readonly id="tainee_app_end_time_view"  class="form-control" />
                      </div>
                  </div></br></br>
                  <div class="col-md-12">                        
                    <div class="col-md-4">
                        Trainer 
                    </div>
                    <div class="col-md-8">
                      <input type="text" readonly id="tainee_app_reciever_id_view"  class="form-control" />
                    </div>
                </div></br></br>
                  <div class="col-md-12">                        
                    <div class="col-md-4"> Message</div>
                    <div class="col-md-8">
                      <textarea readonly id="tainee_app_message_view" class="form-control" rows="2"></textarea>
                    </div>
                </div></br></br>
               </div></br>
              <div class="modal-footer">
               <button type="button" id="delete-btn" class="btn btn-danger">Delete</button>
              </div>
             
             
            </div>
          </div>
        </div>
        <!--  /////////////////View Appoinment View Modal End ///////////////// -->

<?php  } ?>
 <div class="main_container">
    
      <div class="container">
          <div class="row">
              <div class="col-sm-12">
                <?php if($this->session->flashdata('success'))
                { ?>
                  <script type="text/javascript">
                      var msg = "<?php echo $this->session->flashdata('success'); ?>";
                      notify('success','<i class="fa fa-check"> Success </i>',msg);
                  </script>
               <?php } ?>

               <?php if($this->session->flashdata('failure'))
                  { ?>
                    <script type="text/javascript">
                      var msg1 = "<?php echo $this->session->flashdata('failure'); ?>";
                      notify('error','<i class="fa fa-times"> Error ! </i>',msg1);                      
                  </script>
                 <?php } ?>
                
            <div id='view_calendar'></div>                    
                </div>
            </div>
            
        </div>
        
    </div>
<script src="<?php echo base_url(); ?>js/jquery.min.js"></script>
<script>
$(document).ready(function(){
   var cal = '<?php echo json_encode($appointmentsArr); ?>';
  var array1 = $.parseJSON(cal);
  $('#view_calendar').fullCalendar({
    defaultView: 'agendaWeek',
    slotMinutes: 15,
    events: array1,
    dayClick: function(date) {
     var today = date.format();
      var todayArr = today.split("T");
      var cur_date = new Date();
      var sel_date = new Date(date.format());
      if(cur_date < sel_date || todayArr[0] == '<?php echo date("Y-m-d"); ?>')
      {
        $('#app_date').val(todayArr[0]);
        $('#book_appo_Modal').modal('show');
       
      }
    }
  });

  
/*script start for time clock
*/
  $(document).on("focusin", "#app_start_time,#app_end_time", function(event) {
    $(this).prop('readonly', true);
    });

    $('#app_start_time,#app_end_time').clockpicker({
        autoclose:true
      }); 
  /*script end for time clock
  */


    setTimeout(function(){
      $('#success').hide();
    }, 3000);

      setTimeout(function(){
      $('#failure').hide();
    }, 3000);

    /*
  ------------------  script for show appointment start here -------------------
    */

     $('a.book_slots').click(function(){
      var app_id = atob($(this).attr('main'));
      $('#book_appo_view').modal('show');
      $.ajax({
          url:"<?php echo base_url(); ?>user/getBookSlotsData",
          type:"post",
          data:{app_id : app_id},            
          success: function(data){ 
            var obj = JSON.parse(data); 
              $('#tainee_app_date_view').val(obj.app_date);
              $('#tainee_app_start_time_view').val(obj.app_start_time);
              $('#tainee_app_end_time_view').val(obj.app_end_time);
              $('#tainee_app_reciever_id_view').val(obj.trainee_name);
              $('#tainee_app_message_view').val(obj.app_msg);
              $('button#delete-btn,button#edit-btn').attr('main',obj.id);

            }
        });
      });

     $('body').on('click', '#delete-btn', function(){
        if(confirm('Are You Sure ! You want to Delete this Record'))
        {
          var id = $(this).attr('main');  
          $.ajax({
            type : "POST",
            url  : "<?php echo base_url('user/delete_book_app'); ?>",
            data  : {id:id},
            success : function(data){
              alert(data);
              location.reload();
            }  
          });
        }
        else
        {

          return false;
        }
     });
    /*
    -------------------script for show appointment end here ------------------
    */
});
</script>
