<?php $this->load->view('baneer_section'); ?>
          <div class="container">
                  
                
            </div>
        </div>
        
    </header>
    <!--Header sec end-->
  <!--Main container sec start-->
    <div class="main_container">
    
      <div class="container">
          
            <div class="row">
              <div class="col-sm-3">
                  <?php $user_id = $this->session->userdata('user_id');?>
                    <div class="trainee_tabs_sect">
                        <h3>THE FUNCTIONAL MOVEMENT SCREEN</h3>
                        <!-- Nav tabs -->
                          <ul class="nav_tabs">
                            <li class="active" ><a href="javascript:void(0);" aria-controls="informaiton" role="tab" data-toggle="tab">THE FUNCTIONAL MOVEMENT SCREEN</a></li>
                          </ul>
                    </div>                    
                </div>
                <div class="col-sm-9">
                  <div class="trainee_tab_content">
                          <form method="post" id="fms_form">
                        <!-- Tab panes -->
                          <div class="tab-content">
                            <div role="tabpanel" class="tab-pane active" id="informaiton">
                              <h3 class="trai_title_sect">FMS</h3>                           
                                <div class="form_wrapper">
                                     <input type="hidden" value="" name="trainner_id" id="trainner_id">
                                     <input type="hidden" value="" name="trainee_id" id="trainee_id">
                                     <div class="form-group">
                                        <label>Date</label>
                                        <input type="text"  placeholder="Select Date" class="form-control datepicker1"   name="date">
                                    </div>
                                    <div class="form-group">
                                        <label>Full Name</label>
                                        <input type="text" autofocus placeholder="Full Name" class="form-control"  id="name" name="name" autocomplete="off" value="">
                                    </div> 
                                    <div class="form-group">
                                        <label>DOB</label>
                                        <input type="text" autofocus  placeholder="Date of Birth" class="form-control datepicker1"   name="dob" autocomplete="off" value="">
                                    </div> 
                                    <div class="form-group">
                                        <label>Email</label>
                                         <input type="email" autofocus  placeholder="Email" class="form-control"  id="email" name="email" autocomplete="off" value="">
                                    </div>
                                    <div class="form-group">
                                        <label>Phone</label>
                                        <input type="text" autofocus placeholder="Phone" class="form-control" id="phone" name="phone" autocomplete="off" value="">
                                        
                                    </div>
                                    <div class="form-group">
                                        <label>Age</label>
                                        <input type="text" autofocus placeholder="Age" class="form-control" id="age" name="age" autocomplete="off" value="">
                                    </div>
                                    <div class="form-group">
                                        <label>Gender</label>
                                        <select class="form-control" id="gender" name="gender">
                                        <option value="">Select Gender</option>
                                        <option value="0">Male</option>
                                        <option value="1">Female</option>
                                        </select>
                                        
                                    </div>

                                    <div class="form-group">
                                        <label>Height(cm)</label>
                                        <input type="text" autofocus placeholder="Height(cm)" class="form-control" id="height" name="height" autocomplete="off" value="">
                                       
                                    </div>
                                    <div class="form-group">
                                        <label>Weight(Kg)</label>
                                        <input type="text" autofocus placeholder="Weight(Kg)" class="form-control" id="weight" name="weight" autocomplete="off" value="">
                                       
                                    </div>
                                </div>
                            </div>
                            <button id="submit_fms" class="btn submit_btn" type="button">Submit</button>
                          </form>
                          </div>                          
                          <div class="clearfix"></div> 
                    </div>
                    
                </div>
            </div>
            
        </div>
        
    </div>
    
    <!--Main container sec end-->
    
    <div class="clearfix"></div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<script type="text/javascript">
    var d = new Date();
    var n = d.getUTCDay(); 
          console.log(n); // get index of current date
          var prev = n;
          console.log(prev); // get total prev dates
          var next = 6-n;
          console.log(next); // get total next date
        var startdate = new Date();
          startdate.setDate(startdate.getDate());
          var enddate = new Date();
          enddate.setDate(enddate.getDate());
          $(document).ready(function(){
          $('.datepicker1').datepicker({
              autoclose:true,
              format: 'yyyy-mm-dd',
              startDate: startdate,
              endDate : enddate
          });
        });
</script>
    
