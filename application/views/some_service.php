<?php $this->load->view('baneer_section'); ?>
        	<div class="container">
                	<div class="row">
                    	<div class="col-sm-12">
                        	<h3 class="page_title">Some of our services</h3>
                        </div>
                        
                    </div>
            </div>
        </div>
        
    </header>
    <!--Header sec end-->
    
    <!--Main container sec start-->
   <div class="service_section2">
            <div class="container">
                <div class="row" id="services">
                    <!-- <div class="title_sect2">
                        <h3>some of our <span>services</span></h3>
                        <span class="title1_strip2"></span>
                    </div> -->
                    <div class="clearfix"></div>
                     <div class="col-sm-6">
                    <?php 
                    $i = 1;
                    foreach ($services as $service) {
                        

                        if($i == '1'){
                        ?>
                        <div class="services_block1">
                            <div class="services_block_img"><a href="<?php echo base_url();?>user/service?id=<?php echo $service['id'];?>"><img class="img-responsive" src="<?php echo base_url(); ?>upload/<?php echo $service['image'];?>" alt=""/></a></div>
                            <div class="services_block_cont">
                                <h3><a href="<?php echo base_url();?>user/service?id=<?php echo $service['id'];?>"><?php echo $service['service_title'];?></a></h3>
                                <p><?php 
                                $description = $service['description'];
                                echo substr($description,0,200);?></p>
                                <a class="readmore_btn" href="<?php echo base_url();?>user/service?id=<?php echo $service['id'];?>">Read more <span class="flaticon-arrow487"></span></a>
                            </div>
                        </div>
                        
                        <div class="clearfix"></div>
                        <?php } 
                        if($i == '2'){
                        ?>
                        <div class="services_block2 margin_right1">
                            <div class="services_block_img"><a href="<?php echo base_url();?>user/service?id=<?php echo $service['id'];?>"><img class="img-responsive" src="<?php echo base_url(); ?>upload/<?php echo $service['image'];?>" alt=""/></a></div>
                            <div class="services_block_cont">
                                <h3><a href="<?php echo base_url();?>user/service?id=<?php echo $service['id'];?>"><?php echo $service['service_title'];?></a></h3>
                                <p><?php 
                                $description = $service['description'];
                                echo substr($description,0,200);?></p>
                                <a class="readmore_btn" href="<?php echo base_url();?>user/service?id=<?php echo $service['id'];?>">Read more <span class="flaticon-arrow487"></span></a>
                            </div>
                        </div>
                        <?php  } 
                        
                        if($i == '3'){
                        ?>
                        <div class="services_block2 margin_left1">
                            <div class="services_block_img"><a href="<?php echo base_url();?>user/service?id=<?php echo $service['id'];?>"><img class="img-responsive" src="<?php echo base_url(); ?>upload/<?php echo $service['image'];?>" alt=""/></a></div>
                            <div class="services_block_cont">
                                <h3><a href="<?php echo base_url();?>user/service?id=<?php echo $service['id'];?>"><?php echo $service['service_title'];?></a></h3>
                                <p><?php 
                                $description = $service['description'];
                                echo substr($description,0,200);?></p>
                                <a class="readmore_btn" href="<?php echo base_url();?>user/service?id=<?php echo $service['id'];?>">Read more <span class="flaticon-arrow487"></span></a>
                            </div>
                        </div>
                     <?php  } ?>
                   
                    <?php  $i++; } ?>
                     </div>
                    <div class="col-sm-6">
                        <?php 
                         $j=1;
                        foreach ($abc as $xyz) {

                           
                           if($j == '4'){
                      ?>
                        <div class="services_block2">
                            <div class="services_block_img"><a href="<?php echo base_url();?>user/service?id=<?php echo $xyz['id'];?>"><img class="img-responsive" src="<?php echo base_url(); ?>upload/<?php echo $xyz['image'];?>" alt=""/></a></div>
                            <div class="services_block_cont">
                                <h3><a href="<?php echo base_url();?>user/service?id=<?php echo $xyz['id'];?>"><?php echo $xyz['service_title'];?></a></h3>
                                <p><?php
                                $description = $xyz['description'];
                                echo substr($description,0,200);?></p>
                                <a class="readmore_btn" href="<?php echo base_url();?>user/service?id=<?php echo $xyz['id'];?>">Read more <span class="flaticon-arrow487"></span></a>
                            </div>
                        </div>
                        <?php }
                           if($j == '5'){

                        ?>
                        <div class="services_block2 margin_left1">
                            <div class="services_block_img"><a href="<?php echo base_url();?>user/service?id=<?php echo $xyz['id'];?>"><img class="img-responsive" src="<?php echo base_url(); ?>upload/<?php echo $xyz['image'];?>" alt=""/></a></div>
                            <div class="services_block_cont">
                                <h3><a href="<?php echo base_url();?>user/service?id=<?php echo $xyz['id'];?>"><?php echo $xyz['service_title'];?></a></h3>
                                <p><?php
                                $description = $xyz['description'];
                                echo substr($description,0,200);?></p>
                                <a class="readmore_btn" href="<?php echo base_url();?>user/service?id=<?php echo $xyz['id'];?>">Read more <span class="flaticon-arrow487"></span></a>
                            </div>
                        </div>
                      <?php } ?>
                       <?php
                           if($j == '6'){

                        ?>
                        <div class="services_block1">
                            <div class="services_block_img"><a href="<?php echo base_url();?>user/service?id=<?php echo $xyz['id'];?>"><img class="img-responsive" src="<?php echo base_url(); ?>upload/<?php echo $xyz['image'];?>" alt=""/></a></div>
                            <div class="services_block_cont">
                                <h3><a href="<?php echo base_url();?>user/service?id=<?php echo $xyz['id'];?>"><?php echo $xyz['service_title'];?></a></h3>
                                <p><?php
                                $description = $xyz['description'];
                                echo substr($description,0,230);?></p>
                                <a class="readmore_btn" href="<?php echo base_url();?>user/service?id=<?php echo $xyz['id'];?>">Read more <span class="flaticon-arrow487"></span></a>
                            </div>
                        </div>
                         <?php } ?>
                        <?php  $j++; } ?>
                        
                    
                    </div>


                   
                   
                </div>
            </div>
    </div> 
    
    <!--Main container sec end-->
    
    <div class="clearfix"></div>
