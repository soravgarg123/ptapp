<?php $this->load->view('baneer_section'); ?>
          <div class="container">
                  
                    <div class="trainer_search">
                     <!--  <form> -->
                        <div class="form-group">
                          <div class="input-group">
                            <input type="text" placeholder="Search Clients" class="form-control" id="search">
                            <div class="input-group-addon"> <span class="fa fa-search"></span> </div>
                          </div>
                        </div>
                       
                      <!-- </form> -->
                    </div>
                    <?php if($this->session->userdata('user_type') =='trainner'){ ?> 
                    <!-- <div class="text-center"><a href="<?php echo base_url();?>user/client_plan" class="btn submit_profile">Add Training Plan</a>
                    </div>   -->
                    <?php } ?>
            </div>
        </div>
        
    </header>
   
    <!--Header sec end-->
    <!--Main container sec start-->
    <div class="main_container">
    
      <div class="container">
          
            <div class="row">
              
                <div class="col-sm-12">
                  <div class="ourtrainer_wrap">
                    <!-- <h3>Our Trainer</h3> -->
                    <!-- <h3>Our Clients</h3> -->
                    <a href="<?php echo base_url(); ?>user/add_profile_trainee" title="Add Clients" id="add-client-btn"><i class="fa fa-plus-circle"></i></a>
                    <ul id="search_result"></ul>
                    <ul class="row" id="row_search">
                    <?php 
                    foreach ($client_list as $client_lists) { 

                    ?>
                      <li class="col-sm-4 col-md-3">
                        <div class="trainer_sec">
                         
                          <div class="trainer_img"> <a href ="<?php echo base_url(); ?>user/client_profile/<?php echo $client_lists['user_id'];?>">
                          <?php 
                          $user_pic = $client_lists['user_pic'];
                          if($user_pic != ""){?>
                          <img class="img-responsive" alt="trainee_img" src="<?php echo base_url(); ?>trainee/<?php echo $client_lists['user_pic'];?>" alt="">
                          <?php }else{?>
                         <img class="img-responsive" alt="trainee_img" src="<?php echo base_url(); ?>trainee/strainer1.jpg" alt="">
                          <?php } ?>
                          </a>
                            <input type="hidden" class="hidden_val<?php echo $client_lists['user_id'];?>" value="0" />
                          </div>
                          <div class="trainer_info">
                            <p><?php echo $client_lists['name'];?><span class="pull-right"><a href="javascript:void(0);" class="chat-btn" cid="<?php echo $client_lists['user_id'];?>" style="color:white;" title="Text Chat"><i class="fa fa-weixin"></i></a></span></p>
                            <!-- <p><strong>Location : </strong> <?php echo $trainee_detail['trainee_country'];?></p>
                            <p><strong>Rank :</strong> <?php echo $trainee_detail['rank_level']?></p> -->
                          </div>
                        </div>
                      </li>
                      <?php } ?>                     
                    </ul>
                  </div>
                </div>

                
            </div>
            
        </div>
        
    </div>
    
    <!--Main container sec end-->
    
    <div class="clearfix"></div>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
     <script type="text/javascript">
     $(document).ready(function(){
       $(".trainer_search").on('keyup','#search',function() {
        
            var search = $('#search').val();
           
                $.ajax({
                  type: "post",
                  url: "<?php echo base_url(); ?>user/search_clients_details",
                  data: "search="+search,
                  
                  success: function(data){

                            $('#row_search').hide();
                            $('#search_result').html(data);
                            if($('#search').val() == ""){

                                       location.reload();

                            }
                            
                            
                  }
                });
            
        });
    });
</script>
<!-- Text Chat Script Start Written By Sorav Garg -->

<script type="text/javascript">
  $(document).ready(function(){
    $('body').on('click','.chat-btn',function(){
      var count = $(this).attr('cid');
      var user_id = btoa(count);
      var user_string = $('#hidden_array').val();
      var user_array = user_string.split(",");
      if($.inArray(count,user_array) == -1){
          user_array.push(count); // insert id in id array
          var usertostring = user_array.toString();
          $('#hidden_array').val(usertostring); // set array string in hidden value 
          $(".chatting_main").append('<div class="chating_wind_wrapper chat_window_'+count+'"></div>');
          $(".chat_window_"+count).load('<?php echo base_url(); ?>user/getChatBox?user_id='+user_id);
      }else{
        return false;
      }
      
    });
  });
</script>

<!-- Text Chat Script End Written By Sorav Garg -->
