      <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7/jquery.min.js">
</script>
<?php $this->load->view('baneer_section'); ?>
          <div class="container">
            </div>
        </div>
        
    </header>
    <!--Header sec end-->
    <!--Main container sec start-->
    <div class="main_container"><!--main_container-->
    
      <div class="container"><!--container-->
          
      <div class="row">
      <?php if($this->session->flashdata('success')) { ?>
          <script type="text/javascript">
              var msg = "<?php echo $this->session->flashdata('success'); ?>";
              notify('success','<i class="fa fa-check"> Success </i>',msg);
          </script>
      <?php } ?>

      <?php if($this->session->flashdata('failure')) { ?>
          <script type="text/javascript">
              var msg1 = "<?php echo $this->session->flashdata('failure'); ?>";
              notify('error','<i class="fa fa-times"> Error ! </i>',msg1);                      
           </script>
      <?php } ?>
                <div class="col-sm-12"><!--col-sm-9-->
                  <div class="trainee_tab_content">
                  <h3 class="trai_title_sect">Resources</h3>
                        <!-- Tab panes -->
                          <div class="tab-content">
 <section>
     <h3>Overhead Squat Solutions table</h3>
    <hr/>
    <div class="resourses_list">
     <ul>
      <li>
      <a class="example-image-link" href="<?php echo base_url();?>resources/Blood pressure chart for adults/Page_00001.jpg" data-lightbox="example-set" data-title="Click the right half of the image to move forward."><img class="example-image" src="<?php echo base_url();?>resources/Blood pressure chart for adults/Page_00001.jpg" alt=""/></a>
      </li> 
      <li> 
      <a class="example-image-link" href="<?php echo base_url();?>resources/Blood pressure chart for adults/Page_00002.jpg" data-lightbox="example-set" data-title="Click the right half of the image to move forward."><img class="example-image" src="<?php echo base_url();?>resources/Blood pressure chart for adults/Page_00002.jpg" alt=""/></a>
      </li>
      </ul>
    </div>
  </section>
    <section>
     <h3>Overhead Squat Solutions table</h3>
    <hr/>
    <div class="resourses_list">
     <ul>
      <li>
      <a class="example-image-link" href="<?php echo base_url();?>resources/Overhead Squat Solutions table.jpg" data-lightbox="example-set" data-title="Click the right half of the image to move forward."><img class="example-image" src="<?php echo base_url();?>resources/Overhead Squat Solutions table.jpg" alt=""/></a>
      </li>
      </ul>
    </div>
  </section>
  <section>
     <h3>One Rep Max Conversion</h3>
    <hr/>
    <div class="resourses_list">
     <ul>
      <li>
      <a class="example-image-link" href="<?php echo base_url();?>resources/One Rep Max Conversion/Page_00001.jpg" data-lightbox="example-set" data-title="Click the right half of the image to move forward."><img class="example-image" src="<?php echo base_url();?>resources/One Rep Max Conversion/Page_00001.jpg" alt=""/></a>
      </li> 
      <li> 
      <a class="example-image-link" href="<?php echo base_url();?>resources/One Rep Max Conversion/Page_00002.jpg" data-lightbox="example-set" data-title="Click the right half of the image to move forward."><img class="example-image" src="<?php echo base_url();?>resources/One Rep Max Conversion/Page_00002.jpg" alt=""/></a>
      </li> 
      <li> 
      <a class="example-image-link" href="<?php echo base_url();?>resources/One Rep Max Conversion/Page_00003.jpg" data-lightbox="example-set" data-title="Click the right half of the image to move forward."><img class="example-image" src="<?php echo base_url();?>resources/One Rep Max Conversion/Page_00003.jpg" alt=""/></a>
      </li> 
      <li> 
      <a class="example-image-link" href="<?php echo base_url();?>resources/One Rep Max Conversion/Page_00004.jpg" data-lightbox="example-set" data-title="Click the right half of the image to move forward."><img class="example-image" src="<?php echo base_url();?>resources/One Rep Max Conversion/Page_00004.jpg" alt=""/></a>
      </li>
      </ul>
    </div>
  </section>
  <section>
     <h3>One Rep Max Conversion %</h3>
    <hr/>
    <div class="resourses_list">
     <ul>
      <li>
      <a class="example-image-link" href="<?php echo base_url();?>resources/One Rep Max Conversion %/Page_00001.jpg" data-lightbox="example-set" data-title="Click the right half of the image to move forward."><img class="example-image" src="<?php echo base_url();?>resources/One Rep Max Conversion %/Page_00001.jpg" alt=""/></a>
      </li> 
      <li>
      <a class="example-image-link" href="<?php echo base_url();?>resources/One Rep Max Conversion %/Page_00002.jpg" data-lightbox="example-set" data-title="Click the right half of the image to move forward."><img class="example-image" src="<?php echo base_url();?>resources/One Rep Max Conversion/Page_00002.jpg" alt=""/></a>
      </li> 
      <li>
      <a class="example-image-link" href="<?php echo base_url();?>resources/One Rep Max Conversion %/Page_00003.jpg" data-lightbox="example-set" data-title="Click the right half of the image to move forward."><img class="example-image" src="<?php echo base_url();?>resources/One Rep Max Conversion %/Page_00003.jpg" alt=""/></a>
      </li> 
      <li>
      <a class="example-image-link" href="<?php echo base_url();?>resources/One Rep Max Conversion %/Page_00004.jpg" data-lightbox="example-set" data-title="Click the right half of the image to move forward."><img class="example-image" src="<?php echo base_url();?>resources/One Rep Max Conversion %/Page_00004.jpg" alt=""/></a>
      </li>
      </ul>
    </div>
  </section>
  <section>
     <h3>Testimonial and Photo release form</h3>
    <hr/>
    <div class="resourses_list">
     <ul>
      <li>
      <a class="example-image-link" href="<?php echo base_url();?>resources/Testimonial and Photo release form.jpg" data-lightbox="example-set" data-title="Click the right half of the image to move forward."><img class="example-image" src="<?php echo base_url();?>resources/Testimonial and Photo release form.jpg" alt=""/></a>
      </li>
      </ul>
    </div>
  </section>
  <section>
     <h3>Alcohol and Athletic Performance</h3>
    <hr/>
    <div class="resourses_list">
     <ul>
      <li>
      <a class="example-image-link" href="<?php echo base_url();?>resources/Alcohol and Athletic Performance/00001.jpg" data-lightbox="example-set" data-title="Click the right half of the image to move forward."><img class="example-image" src="<?php echo base_url();?>resources/Alcohol and Athletic Performance/00001.jpg" alt=""/></a>
      </li> 
      <li>
      <a class="example-image-link" href="<?php echo base_url();?>resources/Alcohol and Athletic Performance/00002.jpg" data-lightbox="example-set" data-title="Click the right half of the image to move forward."><img class="example-image" src="<?php echo base_url();?>resources/Alcohol and Athletic Performance/00002.jpg" alt=""/></a>
      </li> 
      <li>
      <a class="example-image-link" href="<?php echo base_url();?>resources/Alcohol and Athletic Performance/00003.jpg" data-lightbox="example-set" data-title="Click the right half of the image to move forward."><img class="example-image" src="<?php echo base_url();?>resources/Alcohol and Athletic Performance/00003.jpg" alt=""/></a>
      </li>
      </ul>
    </div>
  </section>
  <section>
     <h3>Allergies</h3>
    <hr/>
    <div class="resourses_list">
     <ul>
      <li>
      <a class="example-image-link" href="<?php echo base_url();?>resources/Allergies and/00001.jpg" data-lightbox="example-set" data-title="Click the right half of the image to move forward."><img class="example-image" src="<?php echo base_url();?>resources/Allergies and/00001.jpg" alt=""/></a>
      </li> 
      <li>
      <a class="example-image-link" href="<?php echo base_url();?>resources/Allergies and/00002.jpg" data-lightbox="example-set" data-title="Click the right half of the image to move forward."><img class="example-image" src="<?php echo base_url();?>resources/Allergies and/00002.jpg" alt=""/></a>
      </li> 
      <li>
      <a class="example-image-link" href="<?php echo base_url();?>resources/Allergies and/00003.jpg" data-lightbox="example-set" data-title="Click the right half of the image to move forward."><img class="example-image" src="<?php echo base_url();?>resources/Allergies and/00003.jpg" alt=""/></a>
      </li>
      </ul>
    </div>
  </section>
  <section>
     <h3>Anabolic Steroids</h3>
    <hr/>
    <div class="resourses_list">
     <ul>
      <li>
      <a class="example-image-link" href="<?php echo base_url();?>resources/Anabolic Steroids/00001.jpg" data-lightbox="example-set" data-title="Click the right half of the image to move forward."><img class="example-image" src="<?php echo base_url();?>resources/Anabolic Steroids/00001.jpg" alt=""/></a>
      </li> 
      <li>
      <a class="example-image-link" href="<?php echo base_url();?>resources/Anabolic Steroids/00002.jpg" data-lightbox="example-set" data-title="Click the right half of the image to move forward."><img class="example-image" src="<?php echo base_url();?>resources/Anabolic Steroids/00002.jpg" alt=""/></a>
      </li> 
      <li>
      <a class="example-image-link" href="<?php echo base_url();?>resources/Anabolic Steroids/00003.jpg" data-lightbox="example-set" data-title="Click the right half of the image to move forward."><img class="example-image" src="<?php echo base_url();?>resources/Anabolic Steroids/00003.jpg" alt=""/></a>
      </li>
      </ul>
    </div>
  </section>
  <section>
     <h3>Athletes and Pesticides</h3>
    <hr/>
    <div class="resourses_list">
     <ul>
      <li>
      <a class="example-image-link" href="<?php echo base_url();?>resources/00001.jpg" data-lightbox="example-set" data-title="Click the right half of the image to move forward."><img class="example-image" src="<?php echo base_url();?>resources/00001.jpg" alt=""/></a>
      </li>
      </ul>
    </div>
  </section>
  <section>
     <h3>Caffeine and Exercise</h3>
    <hr/>
    <div class="resourses_list">
     <ul>
      <li>
      <a class="example-image-link" href="<?php echo base_url();?>resources/Caffeine and Exercise/00001.jpg" data-lightbox="example-set" data-title="Click the right half of the image to move forward."><img class="example-image" src="<?php echo base_url();?>resources/Caffeine and Exercise/00001.jpg" alt=""/></a>
      </li> 
      <li>
      <a class="example-image-link" href="<?php echo base_url();?>resources/Caffeine and Exercise/00002.jpg" data-lightbox="example-set" data-title="Click the right half of the image to move forward."><img class="example-image" src="<?php echo base_url();?>resources/Caffeine and Exercise/00002.jpg" alt=""/></a>
      </li> 
      <li>
      <a class="example-image-link" href="<?php echo base_url();?>resources/Caffeine and Exercise/00003.jpg" data-lightbox="example-set" data-title="Click the right half of the image to move forward."><img class="example-image" src="<?php echo base_url();?>resources/Caffeine and Exercise/00003.jpg" alt=""/></a>
      </li>
      </ul>
    </div>
  </section>
  <section>
     <h3>Ankle Sprains and the Athlete</h3>
    <hr/>
    <div class="resourses_list">
     <ul>
      <li>
      <a class="example-image-link" href="<?php echo base_url();?>resources/Ankle Sprains and the Athlete/00001.jpg" data-lightbox="example-set" data-title="Click the right half of the image to move forward."><img class="example-image" src="<?php echo base_url();?>resources/Ankle Sprains and the Athlete/00001.jpg" alt=""/></a>
      </li> 
      <li>
      <a class="example-image-link" href="<?php echo base_url();?>resources/Ankle Sprains and the Athlete/00002.jpg" data-lightbox="example-set" data-title="Click the right half of the image to move forward."><img class="example-image" src="<?php echo base_url();?>resources/Ankle Sprains and the Athlete/00002.jpg" alt=""/></a>
      </li> 
      <li>
      <a class="example-image-link" href="<?php echo base_url();?>resources/Ankle Sprains and the Athlete/00003.jpg" data-lightbox="example-set" data-title="Click the right half of the image to move forward."><img class="example-image" src="<?php echo base_url();?>resources/Ankle Sprains and the Athlete/00003.jpg" alt=""/></a>
      </li>
      </ul>
    </div>
  </section>
  <section>
     <h3>Childhood Obesity</h3>
    <hr/>
    <div class="resourses_list">
     <ul>
      <li>
      <a class="example-image-link" href="<?php echo base_url();?>resources/Ankle Childhood Obesity/00001.jpg" data-lightbox="example-set" data-title="Click the right half of the image to move forward."><img class="example-image" src="<?php echo base_url();?>resources/Childhood Obesity/00001.jpg" alt=""/></a>
      </li> 
      <li>
      <a class="example-image-link" href="<?php echo base_url();?>resources/Childhood Obesity/00002.jpg" data-lightbox="example-set" data-title="Click the right half of the image to move forward."><img class="example-image" src="<?php echo base_url();?>resources/Childhood Obesity/00002.jpg" alt=""/></a>
      </li> 
      <li>
      <a class="example-image-link" href="<?php echo base_url();?>resources/Ankle Childhood Obesity/00003.jpg" data-lightbox="example-set" data-title="Click the right half of the image to move forward."><img class="example-image" src="<?php echo base_url();?>resources/Childhood Obesity/00003.jpg" alt=""/></a>
      </li>
      </ul>
    </div>
  </section>
  <section>
     <h3>Chronobiological Effects on Exercise</h3>
    <hr/>
    <div class="resourses_list">
     <ul>
      <li>
      <a class="example-image-link" href="<?php echo base_url();?>resources/Chronobiological Effects on Exercise/00001.jpg" data-lightbox="example-set" data-title="Click the right half of the image to move forward."><img class="example-image" src="<?php echo base_url();?>resources/Chronobiological Effects on Exercise/00001.jpg" alt=""/></a>
      </li> 
      <li>
      <a class="example-image-link" href="<?php echo base_url();?>resources/Chronobiological Effects on Exercise/00002.jpg" data-lightbox="example-set" data-title="Click the right half of the image to move forward."><img class="example-image" src="<?php echo base_url();?>resources/Chronobiological Effects on Exercise/00002.jpg" alt=""/></a>
      </li> 
      <li>
      <a class="example-image-link" href="<?php echo base_url();?>resources/Chronobiological Effects on Exercise/00003.jpg" data-lightbox="example-set" data-title="Click the right half of the image to move forward."><img class="example-image" src="<?php echo base_url();?>resources/Chronobiological Effects on Exercise/00003.jpg" alt=""/></a>
      </li>
      </ul>
    </div>
  </section>
  <section>
     <h3>Cocaine Abuse in Sports</h3>
    <hr/>
    <div class="resourses_list">
     <ul>
      <li>
      <a class="example-image-link" href="<?php echo base_url();?>resources/Cocaine Abuse in Sports/00001.jpg" data-lightbox="example-set" data-title="Click the right half of the image to move forward."><img class="example-image" src="<?php echo base_url();?>resources/Cocaine Abuse in Sports/00001.jpg" alt=""/></a>
      </li> 
      <li>
      <a class="example-image-link" href="<?php echo base_url();?>resources/Cocaine Abuse in Sports/00002.jpg" data-lightbox="example-set" data-title="Click the right half of the image to move forward."><img class="example-image" src="<?php echo base_url();?>resources/Cocaine Abuse in Sports/00002.jpg" alt=""/></a>
      </li> 
      <li>
      <a class="example-image-link" href="<?php echo base_url();?>resources/Cocaine Abuse in Sports/00003.jpg" data-lightbox="example-set" data-title="Click the right half of the image to move forward."><img class="example-image" src="<?php echo base_url();?>resources/Cocaine Abuse in Sports/00003.jpg" alt=""/></a>
      </li>
      </ul>
    </div>
  </section>
  <section>
     <h3>Corporate Wellness</h3>
    <hr/>
    <div class="resourses_list">
     <ul>
      <li>
      <a class="example-image-link" href="<?php echo base_url();?>resources/Corporate Wellness/00001.jpg" data-lightbox="example-set" data-title="Click the right half of the image to move forward."><img class="example-image" src="<?php echo base_url();?>resources/Corporate Wellness/00001.jpg" alt=""/></a>
      </li> 
      <li>
      <a class="example-image-link" href="<?php echo base_url();?>resources/Corporate Wellness/00002.jpg" data-lightbox="example-set" data-title="Click the right half of the image to move forward."><img class="example-image" src="<?php echo base_url();?>resources/Corporate Wellness/00002.jpg" alt=""/></a>
      </li> 
      <li>
      <a class="example-image-link" href="<?php echo base_url();?>resources/Corporate Wellness/00003.jpg" data-lightbox="example-set" data-title="Click the right half of the image to move forward."><img class="example-image" src="<?php echo base_url();?>resources/Corporate Wellness/00003.jpg" alt=""/></a>
      </li>
      </ul>
    </div>
  </section>
  <section>
     <h3>Dehydration and Ageing</h3>
    <hr/>
    <div class="resourses_list">
     <ul>
      <li>
      <a class="example-image-link" href="<?php echo base_url();?>resources/Dehydration and Ageing/Page_00001.jpg" data-lightbox="example-set" data-title="Click the right half of the image to move forward."><img class="example-image" src="<?php echo base_url();?>resources/Dehydration and Ageing/Page_00001.jpg" alt=""/></a>
      </li> 
      <li>
      <a class="example-image-link" href="<?php echo base_url();?>resources/Dehydration and Ageing/Page_00002.jpg" data-lightbox="example-set" data-title="Click the right half of the image to move forward."><img class="example-image" src="<?php echo base_url();?>resources/Dehydration and Ageing/Page_00002.jpg" alt=""/></a>
      </li>
      </ul>
    </div>
  </section>
  <section>
     <h3>Dehydration and Estrogen</h3>
    <hr/>
    <div class="resourses_list">
     <ul>
      <li>
      <a class="example-image-link" href="<?php echo base_url();?>resources/Dehydration and Estrogen/Page_00001.jpg" data-lightbox="example-set" data-title="Click the right half of the image to move forward."><img class="example-image" src="<?php echo base_url();?>resources/Dehydration and Estrogen/Page_00001.jpg" alt=""/></a>
      </li> 
      <li>
      <a class="example-image-link" href="<?php echo base_url();?>resources/Dehydration and Estrogen/Page_00002.jpg" data-lightbox="example-set" data-title="Click the right half of the image to move forward."><img class="example-image" src="<?php echo base_url();?>resources/Dehydration and Estrogen/Page_00002.jpg" alt=""/></a>
      </li>
      </ul>
    </div>
  </section>
  <section>
     <h3>Eating Disorders</h3>
    <hr/>
    <div class="resourses_list">
     <ul>
      <li>
      <a class="example-image-link" href="<?php echo base_url();?>resources/Eating Disorders/Page_00001.jpg" data-lightbox="example-set" data-title="Click the right half of the image to move forward."><img class="example-image" src="<?php echo base_url();?>resources/Eating Disorders/Page_00001.jpg" alt=""/></a>
      </li> 
      <li>
      <a class="example-image-link" href="<?php echo base_url();?>resources/Eating Disorders/Page_00002.jpg" data-lightbox="example-set" data-title="Click the right half of the image to move forward."><img class="example-image" src="<?php echo base_url();?>resources/Eating Disorders/Page_00002.jpg" alt=""/></a>
      </li>
      </ul>
    </div>
  </section>
  <section>
     <h3>Endurance Exercise following Stroke</h3>
    <hr/>
    <div class="resourses_list">
     <ul>
      <li>
      <a class="example-image-link" href="<?php echo base_url();?>resources/Endurance Exercise following Stroke/Page_00001.jpg" data-lightbox="example-set" data-title="Click the right half of the image to move forward."><img class="example-image" src="<?php echo base_url();?>resources/Endurance Exercise following Stroke/Page_00001.jpg" alt=""/></a>
      </li> 
      <li>
      <a class="example-image-link" href="<?php echo base_url();?>resources/Endurance Exercise following Stroke/Page_00002.jpg" data-lightbox="example-set" data-title="Click the right half of the image to move forward."><img class="example-image" src="<?php echo base_url();?>resources/Endurance Exercise following Stroke/Page_00002.jpg" alt=""/></a>
      </li>
      </ul>
    </div>
  </section>
  <section>
     <h3>Energy expenditure in different modes of exercise</h3>
    <hr/>
    <div class="resourses_list">
     <ul>
      <li>
      <a class="example-image-link" href="<?php echo base_url();?>resources/Energy expenditure in different modes of exercise/Page_00001.jpg" data-lightbox="example-set" data-title="Click the right half of the image to move forward."><img class="example-image" src="<?php echo base_url();?>resources/Energy expenditure in different modes of exercise/Page_00001.jpg" alt=""/></a>
      </li> 
      <li>
      <a class="example-image-link" href="<?php echo base_url();?>resources/Energy expenditure in different modes of exercise/Page_00002.jpg" data-lightbox="example-set" data-title="Click the right half of the image to move forward."><img class="example-image" src="<?php echo base_url();?>resources/Energy expenditure in different modes of exercise/Page_00002.jpg" alt=""/></a>
      </li>
      </ul>
    </div>
  </section>
  <section>
     <h3>Exercise and Shoulder Pain</h3>
    <hr/>
    <div class="resourses_list">
     <ul>
      <li>
      <a class="example-image-link" href="<?php echo base_url();?>resources/Exercise and Shoulder Pain/Page_00001.jpg" data-lightbox="example-set" data-title="Click the right half of the image to move forward."><img class="example-image" src="<?php echo base_url();?>resources/Exercise and Shoulder Pain/Page_00001.jpg" alt=""/></a>
      </li> 
      <li>
      <a class="example-image-link" href="<?php echo base_url();?>resources/Exercise and Shoulder Pain/Page_00002.jpg" data-lightbox="example-set" data-title="Click the right half of the image to move forward."><img class="example-image" src="<?php echo base_url();?>resources/Exercise and Shoulder Pain/Page_00002.jpg" alt=""/></a>
      </li> 
      <li>
      <a class="example-image-link" href="<?php echo base_url();?>resources/Exercise and Shoulder Pain/Page_00003.jpg" data-lightbox="example-set" data-title="Click the right half of the image to move forward."><img class="example-image" src="<?php echo base_url();?>resources/Exercise and Shoulder Pain/Page_00003.jpg" alt=""/></a>
      </li>
      </ul>
    </div>
  </section>
  <section>
     <h3>Exercise and Common Cold</h3>
    <hr/>
    <div class="resourses_list">
     <ul>
      <li>
      <a class="example-image-link" href="<?php echo base_url();?>resources/Exercise and Common Cold/Page_00001.jpg" data-lightbox="example-set" data-title="Click the right half of the image to move forward."><img class="example-image" src="<?php echo base_url();?>resources/Exercise and Common Cold/Page_00001.jpg" alt=""/></a>
      </li> 
      <li>
      <a class="example-image-link" href="<?php echo base_url();?>resources/Exercise and Common Cold/Page_00002.jpg" data-lightbox="example-set" data-title="Click the right half of the image to move forward."><img class="example-image" src="<?php echo base_url();?>resources/Exercise and Common Cold/Page_00002.jpg" alt=""/></a>
      </li> 
      <li>
      <a class="example-image-link" href="<?php echo base_url();?>resources/Exercise and Common Cold/Page_00003.jpg" data-lightbox="example-set" data-title="Click the right half of the image to move forward."><img class="example-image" src="<?php echo base_url();?>resources/Exercise and Common Cold/Page_00003.jpg" alt=""/></a>
      </li>
      </ul>
    </div>
  </section>
  <section>
     <h3>Exercise and the Older Adult</h3>
    <hr/>
    <div class="resourses_list">
     <ul>
      <li>
      <a class="example-image-link" href="<?php echo base_url();?>resources/Exercise and the Older Adult/Page_00001.jpg" data-lightbox="example-set" data-title="Click the right half of the image to move forward."><img class="example-image" src="<?php echo base_url();?>resources/Exercise and the Older Adult/Page_00001.jpg" alt=""/></a>
      </li> 
      <li>
      <a class="example-image-link" href="<?php echo base_url();?>resources/Exercise and the Older Adult/Page_00002.jpg" data-lightbox="example-set" data-title="Click the right half of the image to move forward."><img class="example-image" src="<?php echo base_url();?>resources/Exercise and the Older Adult/Page_00002.jpg" alt=""/></a>
      </li> 
      <li>
      <a class="example-image-link" href="<?php echo base_url();?>resources/Exercise and the Older Adult/Page_00003.jpg" data-lightbox="example-set" data-title="Click the right half of the image to move forward."><img class="example-image" src="<?php echo base_url();?>resources/Exercise and the Older Adult/Page_00003.jpg" alt=""/></a>
      </li>
      </ul>
    </div>
  </section>
  <section>
     <h3>Exercise and Age Related Weight Gain</h3>
    <hr/>
    <div class="resourses_list">
     <ul>
      <li>
      <a class="example-image-link" href="<?php echo base_url();?>resources/Exercise and Age Related Weight Gain/Page_00001.jpg" data-lightbox="example-set" data-title="Click the right half of the image to move forward."><img class="example-image" src="<?php echo base_url();?>resources/Exercise and Age Related Weight Gain/Page_00001.jpg" alt=""/></a>
      </li> 
      <li>
      <a class="example-image-link" href="<?php echo base_url();?>resources/Exercise and Age Related Weight Gain/Page_00002.jpg" data-lightbox="example-set" data-title="Click the right half of the image to move forward."><img class="example-image" src="<?php echo base_url();?>resources/Exercise and Age Related Weight Gain/Page_00002.jpg" alt=""/></a>
      </li>
      </ul>
    </div>
  </section>
  <section>
     <h3>Exercise During Pregnancy</h3>
    <hr/>
    <div class="resourses_list">
     <ul>
      <li>
      <a class="example-image-link" href="<?php echo base_url();?>resources/Exercise During Pregnancy/Page_00001.jpg" data-lightbox="example-set" data-title="Click the right half of the image to move forward."><img class="example-image" src="<?php echo base_url();?>resources/Exercise During Pregnancy/Page_00001.jpg" alt=""/></a>
      </li> 
      <li>
      <a class="example-image-link" href="<?php echo base_url();?>resources/Exercise During Pregnancy/Page_00002.jpg" data-lightbox="example-set" data-title="Click the right half of the image to move forward."><img class="example-image" src="<?php echo base_url();?>resources/Exercise During Pregnancy/Page_00002.jpg" alt=""/></a>
      </li>
      </ul>
    </div>
  </section>
  <section>
     <h3>Exercise for persons with COPD</h3>
    <hr/>
    <div class="resourses_list">
     <ul>
      <li>
      <a class="example-image-link" href="<?php echo base_url();?>resources/Exercise for persons with COPD/Page_00001.jpg" data-lightbox="example-set" data-title="Click the right half of the image to move forward."><img class="example-image" src="<?php echo base_url();?>resources/Exercise for persons with COPD/Page_00001.jpg" alt=""/></a>
      </li> 
      <li>
      <a class="example-image-link" href="<?php echo base_url();?>resources/Exercise for persons with COPD/Page_00002.jpg" data-lightbox="example-set" data-title="Click the right half of the image to move forward."><img class="example-image" src="<?php echo base_url();?>resources/Exercise for persons with COPD/Page_00002.jpg" alt=""/></a>
      </li>
      </ul>
    </div>
  </section>
  <section>
     <h3>Exercise Induced Asthma</h3>
    <hr/>
    <div class="resourses_list">
     <ul>
      <li>
      <a class="example-image-link" href="<?php echo base_url();?>resources/Exercise Induced Asthma/Page_00001.jpg" data-lightbox="example-set" data-title="Click the right half of the image to move forward."><img class="example-image" src="<?php echo base_url();?>resources/Exercise Induced Asthma/Page_00001.jpg" alt=""/></a>
      </li> 
      <li>
      <a class="example-image-link" href="<?php echo base_url();?>resources/Exercise Induced Asthma/Page_00002.jpg" data-lightbox="example-set" data-title="Click the right half of the image to move forward."><img class="example-image" src="<?php echo base_url();?>resources/Exercise Induced Asthma/Page_00002.jpg" alt=""/></a>
      </li>
      </ul>
    </div>
  </section>
  <section>
     <h3>Exercise in Health Clubs</h3>
    <hr/>
    <div class="resourses_list">
     <ul>
      <li>
      <a class="example-image-link" href="<?php echo base_url();?>resources/Exercise in Health Clubs/Page_00001.jpg" data-lightbox="example-set" data-title="Click the right half of the image to move forward."><img class="example-image" src="<?php echo base_url();?>resources/Exercise in Health Clubs/Page_00001.jpg" alt=""/></a>
      </li> 
      <li>
      <a class="example-image-link" href="<?php echo base_url();?>resources/Exercise in Health Clubs/Page_00002.jpg" data-lightbox="example-set" data-title="Click the right half of the image to move forward."><img class="example-image" src="<?php echo base_url();?>resources/Exercise in Health Clubs/Page_00002.jpg" alt=""/></a>
      </li>
      </ul>
    </div>
  </section>
  <section>
     <h3>Exercise while Travelling</h3>
    <hr/>
    <div class="resourses_list">
     <ul>
      <li>
      <a class="example-image-link" href="<?php echo base_url();?>resources/Exercise while Travelling.jpg" data-lightbox="example-set" data-title="Click the right half of the image to move forward."><img class="example-image" src="<?php echo base_url();?>resources/Exercise while Travelling.jpg" alt=""/></a>
      </li>
      </ul>
    </div>
  </section>
  <section>
     <h3>Exercise Induced Leg Pain</h3>
    <hr/>
    <div class="resourses_list">
     <ul>
      <li>
      <a class="example-image-link" href="<?php echo base_url();?>resources/Exercise Induced Leg Pain/Page_00001.jpg" data-lightbox="example-set" data-title="Click the right half of the image to move forward."><img class="example-image" src="<?php echo base_url();?>resources/Exercise Induced Leg Pain/Page_00001.jpg" alt=""/></a>
      </li> 
      <li>
      <a class="example-image-link" href="<?php echo base_url();?>resources/Exercise Induced Leg Pain/Page_00002.jpg" data-lightbox="example-set" data-title="Click the right half of the image to move forward."><img class="example-image" src="<?php echo base_url();?>resources/Exercise Induced Leg Pain/Page_00002.jpg" alt=""/></a>
      </li> 
      <li>
      <a class="example-image-link" href="<?php echo base_url();?>resources/Exercise Induced Leg Pain/Page_00003.jpg" data-lightbox="example-set" data-title="Click the right half of the image to move forward."><img class="example-image" src="<?php echo base_url();?>resources/Exercise Induced Leg Pain/Page_00003.jpg" alt=""/></a>
      </li> 
      <li>
      <a class="example-image-link" href="<?php echo base_url();?>resources/Exercise Induced Leg Pain/Page_00004.jpg" data-lightbox="example-set" data-title="Click the right half of the image to move forward."><img class="example-image" src="<?php echo base_url();?>resources/Exercise Induced Leg Pain/Page_00004.jpg" alt=""/></a>
      </li>
      </ul>
    </div>
  </section>
  <section>
     <h3>Explosive Exercise</h3>
    <hr/>
    <div class="resourses_list">
     <ul>
      <li>
      <a class="example-image-link" href="<?php echo base_url();?>resources/Explosive Exercise/Page_00001.jpg" data-lightbox="example-set" data-title="Click the right half of the image to move forward."><img class="example-image" src="<?php echo base_url();?>resources/Explosive Exercise/Page_00001.jpg" alt=""/></a>
      </li> 
      <li>
      <a class="example-image-link" href="<?php echo base_url();?>resources/Explosive Exercise/Page_00002.jpg" data-lightbox="example-set" data-title="Click the right half of the image to move forward."><img class="example-image" src="<?php echo base_url();?>resources/Explosive Exercise/Page_00002.jpg" alt=""/></a>
      </li>
      </ul>
    </div>
  </section>
  <section>
     <h3>Football Helmet Removal</h3>
    <hr/>
    <div class="resourses_list">
     <ul>
      <li>
      <a class="example-image-link" href="<?php echo base_url();?>resources/Football Helmet Removal.jpg" data-lightbox="example-set" data-title="Click the right half of the image to move forward."><img class="example-image" src="<?php echo base_url();?>resources/Football Helmet Removal.jpg" alt=""/></a>
      </li>
      </ul>
    </div>
  </section>
  <section>
     <h3>Growth in Young Wrestlers</h3>
    <hr/>
    <div class="resourses_list">
     <ul>
      <li>
      <a class="example-image-link" href="<?php echo base_url();?>resources/Growth in Young Wrestlers/Page_00001.jpg" data-lightbox="example-set" data-title="Click the right half of the image to move forward."><img class="example-image" src="<?php echo base_url();?>resources/Growth in Young Wrestlers/Page_00001.jpg" alt=""/></a>
      </li> 
      <li>
      <a class="example-image-link" href="<?php echo base_url();?>resources/Growth in Young Wrestlers/Page_00002.jpg" data-lightbox="example-set" data-title="Click the right half of the image to move forward."><img class="example-image" src="<?php echo base_url();?>resources/Growth in Young Wrestlers/Page_00002.jpg" alt=""/></a>
      </li>
      </ul>
    </div>
  </section>
  <section>
     <h3>Heat and Hydration in Young Tennis Players</h3>
    <hr/>
    <div class="resourses_list">
     <ul>
      <li>
      <a class="example-image-link" href="<?php echo base_url();?>resources/Heat and Hydration in Young Tennis Players/Page_00001.jpg" data-lightbox="example-set" data-title="Click the right half of the image to move forward."><img class="example-image" src="<?php echo base_url();?>resources/Heat and Hydration in Young Tennis Players/Page_00001.jpg" alt=""/></a>
      </li> 
      <li>
      <a class="example-image-link" href="<?php echo base_url();?>resources/Heat and Hydration in Young Tennis Players/Page_00002.jpg" data-lightbox="example-set" data-title="Click the right half of the image to move forward."><img class="example-image" src="<?php echo base_url();?>resources/Heat and Hydration in Young Tennis Players/Page_00002.jpg" alt=""/></a>
      </li> 
      <li>
      <a class="example-image-link" href="<?php echo base_url();?>resources/Heat and Hydration in Young Tennis Players/Page_00003.jpg" data-lightbox="example-set" data-title="Click the right half of the image to move forward."><img class="example-image" src="<?php echo base_url();?>resources/Heat and Hydration in Young Tennis Players/Page_00003.jpg" alt=""/></a>
      </li>
      </ul>
    </div>
  </section>
  <section>
     <h3>High Velocity Training</h3>
    <hr/>
    <div class="resourses_list">
     <ul>
      <li>
      <a class="example-image-link" href="<?php echo base_url();?>resources/High Velocity Training/Page_00001.jpg" data-lightbox="example-set" data-title="Click the right half of the image to move forward."><img class="example-image" src="<?php echo base_url();?>resources/High Velocity Training/Page_00001.jpg" alt=""/></a>
      </li> 
      <li>
      <a class="example-image-link" href="<?php echo base_url();?>resources/High Velocity Training/Page_00002.jpg" data-lightbox="example-set" data-title="Click the right half of the image to move forward."><img class="example-image" src="<?php echo base_url();?>resources/High Velocity Training/Page_00002.jpg" alt=""/></a>
      </li>
      </ul>
    </div>
  </section>
  <section>
     <h3>Menstrual Cycle Dysfunction</h3>
    <hr/>
    <div class="resourses_list">
     <ul>
      <li>
      <a class="example-image-link" href="<?php echo base_url();?>resources/Menstrual Cycle Dysfunction/Page_00001.jpg" data-lightbox="example-set" data-title="Click the right half of the image to move forward."><img class="example-image" src="<?php echo base_url();?>resources/Menstrual Cycle Dysfunction/Page_00001.jpg" alt=""/></a>
      </li> 
      <li>
      <a class="example-image-link" href="<?php echo base_url();?>resources/Menstrual Cycle Dysfunction/Page_00002.jpg" data-lightbox="example-set" data-title="Click the right half of the image to move forward."><img class="example-image" src="<?php echo base_url();?>resources/Menstrual Cycle Dysfunction/Page_00002.jpg" alt=""/></a>
      </li> 
      <li>
      <a class="example-image-link" href="<?php echo base_url();?>resources/Menstrual Cycle Dysfunction/Page_00003.jpg" data-lightbox="example-set" data-title="Click the right half of the image to move forward."><img class="example-image" src="<?php echo base_url();?>resources/Menstrual Cycle Dysfunction/Page_00003.jpg" alt=""/></a>
      </li>
      </ul>
    </div>
  </section>
  <section>
     <h3>Lifestyle and Paediatric Metabolic Syndrome</h3>
    <hr/>
    <div class="resourses_list">
     <ul>
      <li>
      <a class="example-image-link" href="<?php echo base_url();?>resources/Lifestyle and Paediatric Metabolic Syndrome/Page_00001.jpg" data-lightbox="example-set" data-title="Click the right half of the image to move forward."><img class="example-image" src="<?php echo base_url();?>resources/Lifestyle and Paediatric Metabolic Syndrome/Page_00001.jpg" alt=""/></a>
      </li> 
      <li>
      <a class="example-image-link" href="<?php echo base_url();?>resources/Lifestyle and Paediatric Metabolic Syndrome/Page_00002.jpg" data-lightbox="example-set" data-title="Click the right half of the image to move forward."><img class="example-image" src="<?php echo base_url();?>resources/Lifestyle and Paediatric Metabolic Syndrome/Page_00002.jpg" alt=""/></a>
      </li>
      </ul>
    </div>
  </section>
  <section>
     <h3>OverTraining with resistance exercise</h3>
    <hr/>
    <div class="resourses_list">
     <ul>
      <li>
      <a class="example-image-link" href="<?php echo base_url();?>resources/OverTraining with resistance exercise/Page_00001.jpg" data-lightbox="example-set" data-title="Click the right half of the image to move forward."><img class="example-image" src="<?php echo base_url();?>resources/OverTraining with resistance exercise/Page_00001.jpg" alt=""/></a>
      </li> 
      <li>
      <a class="example-image-link" href="<?php echo base_url();?>resources/OverTraining with resistance exercise/Page_00002.jpg" data-lightbox="example-set" data-title="Click the right half of the image to move forward."><img class="example-image" src="<?php echo base_url();?>resources/OverTraining with resistance exercise/Page_00002.jpg" alt=""/></a>
      </li> 
      <li>
      <a class="example-image-link" href="<?php echo base_url();?>resources/OverTraining with resistance exercise/Page_00003.jpg" data-lightbox="example-set" data-title="Click the right half of the image to move forward."><img class="example-image" src="<?php echo base_url();?>resources/OverTraining with resistance exercise/Page_00003.jpg" alt=""/></a>
      </li>
      </ul>
    </div>
  </section>
  <section>
     <h3>Posterior cruciate ligament injury</h3>
    <hr/>
    <div class="resourses_list">
     <ul>
      <li>
      <a class="example-image-link" href="<?php echo base_url();?>resources/Posterior cruciate ligament injury.jpg" data-lightbox="example-set" data-title="Click the right half of the image to move forward."><img class="example-image" src="<?php echo base_url();?>resources/Posterior cruciate ligament injury.jpg" alt=""/></a>
      </li>
      </ul>
    </div>
  </section>
  <section>
     <h3>Perceived Exertion</h3>
    <hr/>
    <div class="resourses_list">
     <ul>
      <li>
      <a class="example-image-link" href="<?php echo base_url();?>resources/Perceived Exertion/Page_00001.jpg" data-lightbox="example-set" data-title="Click the right half of the image to move forward."><img class="example-image" src="<?php echo base_url();?>resources/Perceived Exertion/Page_00001.jpg" alt=""/></a>
      </li> 
      <li>
      <a class="example-image-link" href="<?php echo base_url();?>resources/Perceived Exertion/Page_00002.jpg" data-lightbox="example-set" data-title="Click the right half of the image to move forward."><img class="example-image" src="<?php echo base_url();?>resources/Perceived Exertion/Page_00002.jpg" alt=""/></a>
      </li>
      </ul>
    </div>
  </section>
  <section>
     <h3>Physiology of ageing</h3>
    <hr/>
    <div class="resourses_list">
     <ul>
      <li>
      <a class="example-image-link" href="<?php echo base_url();?>resources/Physiology of ageing/Page_00001.jpg" data-lightbox="example-set" data-title="Click the right half of the image to move forward."><img class="example-image" src="<?php echo base_url();?>resources/Physiology of ageing/Page_00001.jpg" alt=""/></a>
      </li> 
      <li>
      <a class="example-image-link" href="<?php echo base_url();?>resources/Physiology of ageing/Page_00002.jpg" data-lightbox="example-set" data-title="Click the right half of the image to move forward."><img class="example-image" src="<?php echo base_url();?>resources/Physiology of ageing/Page_00002.jpg" alt=""/></a>
      </li>
      </ul>
    </div>
  </section>
  <section>
     <h3>Plyometric Training for Children and Adolescents</h3>
    <hr/>
    <div class="resourses_list">
     <ul>
      <li>
      <a class="example-image-link" href="<?php echo base_url();?>resources/Plyometric Training for Children and Adolescents/Page_00001.jpg" data-lightbox="example-set" data-title="Click the right half of the image to move forward."><img class="example-image" src="<?php echo base_url();?>resources/Plyometric Training for Children and Adolescents/Page_00001.jpg" alt=""/></a>
      </li> 
      <li>
      <a class="example-image-link" href="<?php echo base_url();?>resources/Plyometric Training for Children and Adolescents/Page_00002.jpg" data-lightbox="example-set" data-title="Click the right half of the image to move forward."><img class="example-image" src="<?php echo base_url();?>resources/Plyometric Training for Children and Adolescents/Page_00002.jpg" alt=""/></a>
      </li>
      </ul>
    </div>
  </section>
  <section>
     <h3>Pre Event Meals</h3>
    <hr/>
    <div class="resourses_list">
     <ul>
      <li>
      <a class="example-image-link" href="<?php echo base_url();?>resources/Pre Event Meals/Page_00001.jpg" data-lightbox="example-set" data-title="Click the right half of the image to move forward."><img class="example-image" src="<?php echo base_url();?>resources/Pre Event Meals/Page_00001.jpg" alt=""/></a>
      </li> 
      <li>
      <a class="example-image-link" href="<?php echo base_url();?>resources/Pre Event Meals/Page_00002.jpg" data-lightbox="example-set" data-title="Click the right half of the image to move forward."><img class="example-image" src="<?php echo base_url();?>resources/Pre Event Meals/Page_00002.jpg" alt=""/></a>
      </li>
      </ul>
    </div>
  </section>
  <section>
     <h3>Pre-Season Conditioning for Young Athletes</h3>
    <hr/>
    <div class="resourses_list">
     <ul>
      <li>
      <a class="example-image-link" href="<?php echo base_url();?>resources/Pre-Season Conditioning for Young Athletes/Page_00001.jpg" data-lightbox="example-set" data-title="Click the right half of the image to move forward."><img class="example-image" src="<?php echo base_url();?>resources/Pre-Season Conditioning for Young Athletes/Page_00001.jpg" alt=""/></a>
      </li> 
      <li>
      <a class="example-image-link" href="<?php echo base_url();?>resources/Pre-Season Conditioning for Young Athletes/Page_00002.jpg" data-lightbox="example-set" data-title="Click the right half of the image to move forward."><img class="example-image" src="<?php echo base_url();?>resources/Pre-Season Conditioning for Young Athletes/Page_00002.jpg" alt=""/></a>
      </li>
      </ul>
    </div>
  </section> 
  <section>
     <h3>Resistance Training and the older adult</h3>
    <hr/>
    <div class="resourses_list">
     <ul>
      <li>
      <a class="example-image-link" href="<?php echo base_url();?>resources/Resistance Training and the older adult/Page_00001.jpg" data-lightbox="example-set" data-title="Click the right half of the image to move forward."><img class="example-image" src="<?php echo base_url();?>resources/Resistance Training and the older adult/Page_00001.jpg" alt=""/></a>
      </li> 
      <li>
      <a class="example-image-link" href="<?php echo base_url();?>resources/Resistance Training and the older adult/Page_00002.jpg" data-lightbox="example-set" data-title="Click the right half of the image to move forward."><img class="example-image" src="<?php echo base_url();?>resources/Resistance Training and the older adult/Page_00002.jpg" alt=""/></a>
      </li> 
      <li>
      <a class="example-image-link" href="<?php echo base_url();?>resources/Resistance Training and the older adult/Page_00003.jpg" data-lightbox="example-set" data-title="Click the right half of the image to move forward."><img class="example-image" src="<?php echo base_url();?>resources/Resistance Training and the older adult/Page_00003.jpg" alt=""/></a>
      </li>
      </ul>
    </div>
  </section>
  <section>
     <h3>Resistance Training and injury prevention</h3>
    <hr/>
    <div class="resourses_list">
     <ul>
      <li>
      <a class="example-image-link" href="<?php echo base_url();?>resources/Resistance Training and injury prevention/Page_00001.jpg" data-lightbox="example-set" data-title="Click the right half of the image to move forward."><img class="example-image" src="<?php echo base_url();?>resources/Resistance Training and injury prevention/Page_00001.jpg" alt=""/></a>
      </li> 
      <li>
      <a class="example-image-link" href="<?php echo base_url();?>resources/Resistance Training and injury prevention/Page_00002.jpg" data-lightbox="example-set" data-title="Click the right half of the image to move forward."><img class="example-image" src="<?php echo base_url();?>resources/Resistance Training and injury prevention/Page_00002.jpg" alt=""/></a>
      </li>
      </ul>
    </div>
  </section>
  <section>
     <h3>Physical Training for improved occupational performance</h3>
    <hr/>
    <div class="resourses_list">
     <ul>
      <li>
      <a class="example-image-link" href="<?php echo base_url();?>resources/Physical Training for improved occupational performance/Page_00001.jpg" data-lightbox="example-set" data-title="Click the right half of the image to move forward."><img class="example-image" src="<?php echo base_url();?>resources/Physical Training for improved occupational performance/Page_00001.jpg" alt=""/></a>
      </li> 
      <li>
      <a class="example-image-link" href="<?php echo base_url();?>resources/Physical Training for improved occupational performance/Page_00002.jpg" data-lightbox="example-set" data-title="Click the right half of the image to move forward."><img class="example-image" src="<?php echo base_url();?>resources/Physical Training for improved occupational performance/Page_00002.jpg" alt=""/></a>
      </li> 
      <li>
      <a class="example-image-link" href="<?php echo base_url();?>resources/Physical Training for improved occupational performance/Page_00003.jpg" data-lightbox="example-set" data-title="Click the right half of the image to move forward."><img class="example-image" src="<?php echo base_url();?>resources/Physical Training for improved occupational performance/Page_00003.jpg" alt=""/></a>
      </li>
      </ul>
    </div>
  </section>
  <section>
     <h3>Safety of the Squat Exercise</h3>
    <hr/>
    <div class="resourses_list">
     <ul>
      <li>
      <a class="example-image-link" href="<?php echo base_url();?>resources/Safety of the Squat Exercise/Page_00001.jpg" data-lightbox="example-set" data-title="Click the right half of the image to move forward."><img class="example-image" src="<?php echo base_url();?>resources/Safety of the Squat Exercise/Page_00001.jpg" alt=""/></a>
      </li> 
      <li>
      <a class="example-image-link" href="<?php echo base_url();?>resources/Safety of the Squat Exercise/Page_00002.jpg" data-lightbox="example-set" data-title="Click the right half of the image to move forward."><img class="example-image" src="<?php echo base_url();?>resources/Safety of the Squat Exercise/Page_00002.jpg" alt=""/></a>
      </li> 
      <li>
      <a class="example-image-link" href="<?php echo base_url();?>resources/Safety of the Squat Exercise/Page_00003.jpg" data-lightbox="example-set" data-title="Click the right half of the image to move forward."><img class="example-image" src="<?php echo base_url();?>resources/Safety of the Squat Exercise/Page_00003.jpg" alt=""/></a>
      </li>
      </ul>
    </div>
  </section>
  <section>
     <h3>Sickle Cell Trait</h3>
    <hr/>
    <div class="resourses_list">
     <ul>
      <li>
      <a class="example-image-link" href="<?php echo base_url();?>resources/Sickle Cell Trait/Page_00001.jpg" data-lightbox="example-set" data-title="Click the right half of the image to move forward."><img class="example-image" src="<?php echo base_url();?>resources/Sickle Cell Trait/Page_00001.jpg" alt=""/></a>
      </li> 
      <li>
      <a class="example-image-link" href="<?php echo base_url();?>resources/Sickle Cell Trait/Page_00002.jpg" data-lightbox="example-set" data-title="Click the right half of the image to move forward."><img class="example-image" src="<?php echo base_url();?>resources/Sickle Cell Trait/Page_00002.jpg" alt=""/></a>
      </li>
      </ul>
    </div>
  </section>
  <section>
     <h3>Skiing injuries</h3>
    <hr/>
    <div class="resourses_list">
     <ul>
      <li>
      <a class="example-image-link" href="<?php echo base_url();?>resources/Skiing injuries/Page_00001.jpg" data-lightbox="example-set" data-title="Click the right half of the image to move forward."><img class="example-image" src="<?php echo base_url();?>resources/Skiing injuries/Page_00001.jpg" alt=""/></a>
      </li> 
      <li>
      <a class="example-image-link" href="<?php echo base_url();?>resources/Skiing injuries/Page_00002.jpg" data-lightbox="example-set" data-title="Click the right half of the image to move forward."><img class="example-image" src="<?php echo base_url();?>resources/Skiing injuries/Page_00002.jpg" alt=""/></a>
      </li>
      </ul>
    </div>
  </section>
  <section>
     <h3>Strength, Power and the Baby Boomer</h3>
    <hr/>
    <div class="resourses_list">
     <ul>
      <li>
      <a class="example-image-link" href="<?php echo base_url();?>resources/Strength, Power and the Baby Boomer/Page_00001.jpg" data-lightbox="example-set" data-title="Click the right half of the image to move forward."><img class="example-image" src="<?php echo base_url();?>resources/Strength, Power and the Baby Boomer/Page_00001.jpg" alt=""/></a>
      </li> 
      <li>
      <a class="example-image-link" href="<?php echo base_url();?>resources/Strength, Power and the Baby Boomer/Page_00002.jpg" data-lightbox="example-set" data-title="Click the right half of the image to move forward."><img class="example-image" src="<?php echo base_url();?>resources/Strength, Power and the Baby Boomer/Page_00002.jpg" alt=""/></a>
      </li>
      </ul>
    </div>
  </section>
  <section>
     <h3>Strength Training for Bone, Muscle and Hormones</h3>
    <hr/>
    <div class="resourses_list">
     <ul>
      <li>
      <a class="example-image-link" href="<?php echo base_url();?>resources/Strength Training for Bone, Muscle and Hormones/Page_00001.jpg" data-lightbox="example-set" data-title="Click the right half of the image to move forward."><img class="example-image" src="<?php echo base_url();?>resources/Strength Training for Bone, Muscle and Hormones/Page_00001.jpg" alt=""/></a>
      </li> 
      <li>
      <a class="example-image-link" href="<?php echo base_url();?>resources/Strength Training for Bone, Muscle and Hormones/Page_00002.jpg" data-lightbox="example-set" data-title="Click the right half of the image to move forward."><img class="example-image" src="<?php echo base_url();?>resources/Strength Training for Bone, Muscle and Hormones/Page_00002.jpg" alt=""/></a>
      </li>
      </ul>
    </div>
  </section>
  <section>
     <h3>Stress Fractures</h3>
    <hr/>
    <div class="resourses_list">
     <ul>
      <li>
      <a class="example-image-link" href="<?php echo base_url();?>resources/Stress Fractures/Page_00001.jpg" data-lightbox="example-set" data-title="Click the right half of the image to move forward."><img class="example-image" src="<?php echo base_url();?>resources/Stress Fractures/Page_00001.jpg" alt=""/></a>
      </li> 
      <li>
      <a class="example-image-link" href="<?php echo base_url();?>resources/Stress Fractures/Page_00002.jpg" data-lightbox="example-set" data-title="Click the right half of the image to move forward."><img class="example-image" src="<?php echo base_url();?>resources/Stress Fractures/Page_00002.jpg" alt=""/></a>
      </li>
      </ul>
    </div>
  </section>
  <section>
     <h3>Tennis Elbow</h3>
    <hr/>
    <div class="resourses_list">
     <ul>
      <li>
      <a class="example-image-link" href="<?php echo base_url();?>resources/Tennis Elbow/Page_00001.jpg" data-lightbox="example-set" data-title="Click the right half of the image to move forward."><img class="example-image" src="<?php echo base_url();?>resources/Tennis Elbow/Page_00001.jpg" alt=""/></a>
      </li> 
      <li>
      <a class="example-image-link" href="<?php echo base_url();?>resources/Tennis Elbow/Page_00002.jpg" data-lightbox="example-set" data-title="Click the right half of the image to move forward."><img class="example-image" src="<?php echo base_url();?>resources/Tennis Elbow/Page_00002.jpg" alt=""/></a>
      </li>
      </ul>
    </div>
  </section>
  <section>
     <h3>Vitamin and Mineral Supplements and Exercise</h3>
    <hr/>
    <div class="resourses_list">
     <ul>
      <li>
      <a class="example-image-link" href="<?php echo base_url();?>resources/Vitamin and Mineral Supplements and Exercise/Page_00001.jpg" data-lightbox="example-set" data-title="Click the right half of the image to move forward."><img class="example-image" src="<?php echo base_url();?>resources/Vitamin and Mineral Supplements and Exercise/Page_00001.jpg" alt=""/></a>
      </li> 
      <li>
      <a class="example-image-link" href="<?php echo base_url();?>resources/Vitamin and Mineral Supplements and Exercise/Page_00002.jpg" data-lightbox="example-set" data-title="Click the right half of the image to move forward."><img class="example-image" src="<?php echo base_url();?>resources/Vitamin and Mineral Supplements and Exercise/Page_00002.jpg" alt=""/></a>
      </li> 
      <li>
      <a class="example-image-link" href="<?php echo base_url();?>resources/Vitamin and Mineral Supplements and Exercise/Page_00003.jpg" data-lightbox="example-set" data-title="Click the right half of the image to move forward."><img class="example-image" src="<?php echo base_url();?>resources/Vitamin and Mineral Supplements and Exercise/Page_00003.jpg" alt=""/></a>
      </li>
      </ul>
    </div>
  </section>
  <section>
     <h3>Weight Loss in Wrestlers</h3>
    <hr/>
    <div class="resourses_list">
     <ul>
      <li>
      <a class="example-image-link" href="<?php echo base_url();?>resources/Weight Loss in Wrestlers/Page_00001.jpg" data-lightbox="example-set" data-title="Click the right half of the image to move forward."><img class="example-image" src="<?php echo base_url();?>resources/Weight Loss in Wrestlers/Page_00001.jpg" alt=""/></a>
      </li> 
      <li>
      <a class="example-image-link" href="<?php echo base_url();?>resources/Weight Loss in Wrestlers/Page_00002.jpg" data-lightbox="example-set" data-title="Click the right half of the image to move forward."><img class="example-image" src="<?php echo base_url();?>resources/Weight Loss in Wrestlers/Page_00002.jpg" alt=""/></a>
      </li>
      </ul>
    </div>
  </section>
  <section>
     <h3>Youth Strength Training</h3>
    <hr/>
    <div class="resourses_list">
     <ul>
      <li>
      <a class="example-image-link" href="<?php echo base_url();?>resources/Youth Strength Training/Page_00001.jpg" data-lightbox="example-set" data-title="Click the right half of the image to move forward."><img class="example-image" src="<?php echo base_url();?>resources/Youth Strength Training/Page_00001.jpg" alt=""/></a>
      </li> 
      <li>
      <a class="example-image-link" href="<?php echo base_url();?>resources/Youth Strength Training/Page_00002.jpg" data-lightbox="example-set" data-title="Click the right half of the image to move forward."><img class="example-image" src="<?php echo base_url();?>resources/Youth Strength Training/Page_00002.jpg" alt=""/></a>
      </li>
      </ul>
    </div>
  </section>
  <section>
     <h3>Womens Heart health and physically active lifestyle</h3>
    <hr/>
    <div class="resourses_list">
     <ul>
      <li>
      <a class="example-image-link" href="<?php echo base_url();?>resources/Womens Heart health and physically active lifestyle/Page_00001.jpg" data-lightbox="example-set" data-title="Click the right half of the image to move forward."><img class="example-image" src="<?php echo base_url();?>resources/Womens Heart health and physically active lifestyle/Page_00001.jpg" alt=""/></a>
      </li> 
      <li>
      <a class="example-image-link" href="<?php echo base_url();?>resources/Womens Heart health and physically active lifestyle/Page_00002.jpg" data-lightbox="example-set" data-title="Click the right half of the image to move forward."><img class="example-image" src="<?php echo base_url();?>resources/Womens Heart health and physically active lifestyle/Page_00002.jpg" alt=""/></a>
      </li>
      </ul>
    </div>
  </section>
  <section>
     <h3>Exercise With Persons with Cardiovascular Disease</h3>
    <hr/>
    <div class="resourses_list">
     <ul>
      <li>
      <a class="example-image-link" href="<?php echo base_url();?>resources/Exercise With Persons with Cardiovascular Disease/Page_00001.jpg" data-lightbox="example-set" data-title="Click the right half of the image to move forward."><img class="example-image" src="<?php echo base_url();?>resources/Exercise With Persons with Cardiovascular Disease/Page_00001.jpg" alt=""/></a>
      </li> 
      <li>
      <a class="example-image-link" href="<?php echo base_url();?>resources/Exercise With Persons with Cardiovascular Disease/Page_00002.jpg" data-lightbox="example-set" data-title="Click the right half of the image to move forward."><img class="example-image" src="<?php echo base_url();?>resources/Exercise With Persons with Cardiovascular Disease/Page_00002.jpg" alt=""/></a>
      </li> 
      <li>
      <a class="example-image-link" href="<?php echo base_url();?>resources/Exercise With Persons with Cardiovascular Disease/Page_00003.jpg" data-lightbox="example-set" data-title="Click the right half of the image to move forward."><img class="example-image" src="<?php echo base_url();?>resources/Exercise With Persons with Cardiovascular Disease/Page_00003.jpg" alt=""/></a>
      </li>
      </ul>
    </div>
  </section>
                </div><!--col-sm-9-->
            </div><!--row-->
            
        </div><!--container-->
        
    </div><!--main_container-->
    
    <!--Main container sec end-->
    <div class="clearfix"></div>

