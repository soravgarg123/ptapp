<?php $this->load->view('baneer_section'); ?>
          <div class="container">
                  
                
            </div>
        </div>
        
    </header>

    <!--Header sec end-->
    <!--Main container sec start-->
    <div class="main_container">
    
        <div class="container">
            
            <div class="row">
                <div class="col-sm-3">
                    
                    <div class="trainee_tabs_sect">
                          <h3>Edit Quetion </h3>
                          <!-- Nav tabs -->
                          <ul class="nav_tabs">
                            <li class="active"><a href="#informaiton" aria-controls="informaiton" role="tab" data-toggle="tab">Quetion</a></li>
                          </ul>
                    </div>
                    
                </div>
                <div class="col-sm-9">
                    <div class="trainee_tab_content">
 
                     <form method="post" id="edit_client_quetion">
                     <div id="client_plan" class="client_plan">Your Data Saved Successfully</div>
                          <!-- Tab panes -->
                          <div class="tab-content">
                            <div role="tabpanel" class="tab-pane active" id="informaiton">
                                <h3 class="trai_title_sect">Edit Quetion</h3>
                                <div class="form_wrapper">
                                    <div class="form-group">
                                        <label>Quetion</label>
                                        <input type="text" class="form-control" id="que" value="<?php echo $client_quetion[0]['que'];?>" name="que" placeholder="Quetion">
                                    </div>
                                    <div class="form-group">
                                        <label>Answer</label>
                                        <textarea class="form-control" id="answer" name="ans"><?php echo $client_quetion[0]['ans'];?></textarea>
                                    </div>
                                </div>
                            </div>
                          </div>
                          <div class="clearfix"></div>
                          <input type="hidden" name="id" id="id" value="<?php echo $client_quetion[0]['id'];?>">
                          <button id="update_quetion" class="btn submit_btn" type="button">Update</button>
                     </form>
                     <input type="hidden" name="user_id" id="user_id" value="<?php echo $client_quetion[0]['trainee_id'];?>">
                    </div>
                    
                </div>
            </div>
            
        </div>
        
    </div>
    
    <!--Main container sec end-->
     <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script type="text/javascript">
    $(document).ready(function(){
          $(".trainee_tab_content").on('click','#update_quetion',function() {
                  var data1 = new FormData($('#edit_client_quetion')[0]);
                  $.ajax({
                    url:"<?php echo base_url();?>user/update_client_quetion",
                    type:"post",
                    data: data1,
                    contentType: false,
                    processData: false,
                    success: function(response)
                    {
                      var user_id = $( "#user_id" ).val();
                      window.location.href='<?php echo base_url(); ?>user/client_profile/'+user_id;
                    }
        });
      
      
      });
    });
</script>
    
