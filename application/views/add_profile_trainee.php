<?php $this->load->view('baneer_section'); ?>
            <div class="container">
                    
                
            </div>
        </div>
        
    </header>
    <!--Header sec end-->
  <!--Main container sec start-->
    <div class="main_container">
    
        <div class="container">
            
            <div class="row">
                <div class="col-sm-3">
                    <?php $user_id = $this->session->userdata('user_id');?>
                    <div class="trainee_tabs_sect">
                          <h3>Add Clients</h3>
                          <!-- Nav tabs -->
                          <ul class="nav_tabs">
                            <li class="active" id="one"><a href="javascript:void(0);" aria-controls="informaiton" role="tab" data-toggle="tab">Personal Information</a></li>
                            <li id="two"><a href="javascript:void(0);" aria-controls="" role="" data-toggle="">Body Information</a></li>
                            <li id="three"><a href="javascript:void(0);" aria-controls="" role="" data-toggle="">Other Information</a></li>
                          </ul>
                    </div>
                    
                </div>
                <div class="col-sm-9">
                    <div class="trainee_tab_content">
                           <div id="responseemployerpass_div">Password not match</div>
                          <form method="post" id="add_profile_trainee">
                          <div id="usr_verify" class="verify" style="display:none;">Email available</div>
                          <div id="usr_verify1" class="verify1" style="display:none;">Email already exists</div>
                          <!-- Tab panes -->
                          <div class="tab-content">

                            <div role="tabpanel" class="tab-pane active" id="informaiton">
                                <h3 class="trai_title_sect">Personal Information</h3>                           
                                <div class="form_wrapper">
                                    <div class="form-group">
                                        <div class="photo_upload_sect">
                                                    <input type="file" value="" id="user_pic" name="user_pic"> 
                                                    <input type="hidden" value="" name="user_pic" id="user_pic">           
                                                    <span class="fa fa-cloud-upload"></span>
                                                    <h5>Upload your Photo</h5>
                                                </div>
                                    </div> 
                                    <div class="form-group">
                                        <label>Full Name</label>
                                        <input type="text" autofocus placeholder="Full Name" class="form-control"  id="name" name="name" autocomplete="off">
                                    </div> 
                                    <div class="form-group">
                                        <label>Email</label>
                                         <input type="email" autofocus  placeholder="Email" class="form-control"  id="email" name="email" autocomplete="off">
                                       
                                    </div>
                                    <div class="form-group">
                                        <label>Password</label>
                                        <input type="password" autofocus  placeholder="Password" class="form-control"  id="password" name="password" autocomplete="off">
                                        
                                    </div> 
                                    <div class="form-group">
                                        <label>Confirm Password</label>
                                        <input type="password" autofocus  placeholder="Confirm Password" class="form-control" id="confirm_password" name="confirm_password" autocomplete="off">
                                        
                                    </div>                                  
                                    <div class="form-group">
                                        <label>Age</label>
                                        <input type="text" autofocus placeholder="Age" class="form-control" id="age" name="age" autocomplete="off">
                                        
                                    </div>
                                    <div class="form-group">
                                        <label>Height</label>
                                        <input type="text" autofocus placeholder="Height" class="form-control" id="height" name="height" autocomplete="off">
                                       
                                    </div>
                                    <div class="form-group">
                                        <label>Width</label>
                                        <input type="text" autofocus placeholder="Width" class="form-control" id="width" name="width" autocomplete="off">
                                       
                                    </div>
                                    <div class="form-group">
                                        <label>Resting Metabolic Rate</label>
                                        <input type="text" autofocus placeholder="Resting Metabolic Rate" class="form-control" id="rmr" name="rmr" autocomplete="off">
                                       
                                    </div>
                                    <div class="form-group">
                                        <label>Blood Pressure</label>
                                        <input type="text" autofocus placeholder="Blood Pressure" class="form-control" id="bp" name="bp" autocomplete="off">
                                       
                                    </div>
                                    <div class="form-group">
                                        <label>Resting Heart Rate</label>
                                        <input type="text" autofocus placeholder="Resting Heart Rate" class="form-control" id="rhr" name="rhr" autocomplete="off">
                                       
                                    </div>
                                    <div class="form-group">
                                        <label>Lean Body Mass</label>
                                        <input type="text" autofocus placeholder="Lean Body Mass" class="form-control" id="lbm" name="lbm" autocomplete="off">
                                       
                                    </div>
                                    <div class="form-group">
                                        <label>Calorie Expenditure</label>
                                        <input type="text" autofocus placeholder="Calorie Expenditure" class="form-control" id="ce" name="ce" autocomplete="off">
                                       
                                    </div>
                                    <div class="form-group">
                                        <label>Daily Macronutrients Intake</label>
                                        <input type="text" autofocus placeholder="Daily Macronutrients Intake" class="form-control" id="dmi" name="dmi" autocomplete="off">
                                       
                                    </div>
                                    <div class="form-group">
                                        <label>Recommended Water Intake</label>
                                        <input type="text" autofocus placeholder="Recommended Water Intake" class="form-control" id="rwi" name="rwi" autocomplete="off">
                                       
                                    </div>
                                    <div class="form-group">
                                        <label>BMR</label>
                                        <input type="text" autofocus placeholder="BMR" class="form-control" id="bmr" name="bmr" autocomplete="off">
                                        
                                    </div>
                                     <div class="form-group">
                                        <label>Goals</label>
                                        <input type="text" autofocus placeholder="Goals" class="form-control" id="goals" name="goals" value = "">
                                    </div>
                                    <div class="form-group">
                                        <label>Notes</label>
                                        <input type="text" autofocus placeholder="Notes" class="form-control" id="notes" name="notes" value = "">
                                    </div>
                                   
                                    
                                </div>
                            </div>
                            <input type="hidden" id="trainner_id" name="trainner_id" value="<?php echo $user_id;?>">
                            <button id="next1" class="btn submit_btn" type="button">Next</button>
                          </form>
                          </div>                          
                          <div class="clearfix"></div> 
                    </div>
                    
                </div>
            </div>
            
        </div>
        
    </div>
    
    <!--Main container sec end-->
    
    <div class="clearfix"></div>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script type="text/javascript">
    $(document).ready(function(){
        $(".trainee_tab_content").on('click','#next1',function() {
            if($("#add_profile_trainee").valid()) {
        var data1 = new FormData($('#add_profile_trainee')[0]);
         if($("#password").val() != $("#confirm_password").val()){
            notify('error','<i class="fa fa-times"> Error ! </i>','Password not match');                      
            }else if($("#password").val() == $("#confirm_password").val()){
          $.ajax({
            url:"<?php echo base_url() ?>user/add_trainee1",
            type:"post",
            data: data1,
            contentType: false,
            processData: false,
            success: function(response)
            {
              $("#one").removeClass("active");
              $("#two").addClass("active");
              $("#informaiton").html(response);
              $("#next1").hide();
              $("#usr_verify").hide();
              
            }
        });
      }
        }
      });
    });
</script>
    
