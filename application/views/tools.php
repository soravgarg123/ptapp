 <body >
<?php $this->load->view('baneer_section'); ?>

          </div>
    </header>
    <!--Header sec end-->

      <!-- ///////////////////// start Code for show flash messages /////////////////!-->
  <div class="main_container">
    
      <div class="container">
          <div class="row">
              <div class="col-sm-12">
                <?php
                 $user_id = $this->session->userdata('user_id'); 
                if($this->session->flashdata('success'))
                { ?>
                  <script type="text/javascript">
                      var msg = "<?php echo $this->session->flashdata('success'); ?>";
                      notify('success','<i class="fa fa-check"> Success </i>',msg);
                  </script>
               <?php    
                }
                 ?>

                 <?php if($this->session->flashdata('failure'))
                  { ?>
                    <script type="text/javascript">
                      var msg1 = "<?php echo $this->session->flashdata('failure'); ?>";
                      notify('error','<i class="fa fa-times"> Error ! </i>',msg1);                      
                  </script>
                 <?php    
                  }
                 ?>
                
            <div id='view_calendar'></div>                    
                </div>
            </div>
            
        </div>
        
    </div>
<!-- /////////////////////End Code for show flash messages///////////////////// !-->

    <!--Main container sec start-->
    <div class="main_container">
      <div class="container">
            <div class="row">
              <div class="col-sm-3">
                    <div class="trainee_tabs_sect">
                        <h3>TOOLS</h3>
                        <!-- Nav tabs -->
                        
                          <ul class="nav_tabs">
                            <li class="active"><a href="#informaiton" aria-controls="informaiton" role="tab" data-toggle="tab">FMS</a></li>
                            <li><a href="<?php echo base_url(); ?>user/calculator" >Calculator</a></li>
                            <li><a href="<?php echo base_url(); ?>user/profit_loss" >Profit and Loss</a></li>
                            <li><a href="<?php echo base_url(); ?>user/resources" >Resources</a></li>
                          </ul>
                    </div>
                </div>
                <div class="col-sm-9">
                <div role="tabpanel" class="tab-pane active" id="informaiton">
                               <h3 class="trai_title_sect">FMS</h3>
                <div style="margin-right: 2%;margin-bottom: 1%;float: right;margin-top:-7%;">
                <a href="<?php echo base_url(); ?>user/fms_edit" class="add-quetion-btn" title="Add FMS" ><i class="fa fa-plus-circle"></i></a>
                </div>
                <p class="fms_text">FMS is the screening tool used to identify limitations or asymmetries in seven fundamental movement patterns that are key to functional movement quality in individuals with no current pain complaint or known musculoskeletal injury.These movement patterns are designed to provide observable performance of basic loco motor, manipulative and stabilizing movements by placing an individual in extreme positions where weaknesses and imbalances become noticeable if appropriate mobility and motor control is not utilized.</p>
                </div>
                <?php if(!empty($fms)){
                            foreach ($fms as $value) {
                             $score =json_decode($value['fms']);
                             if($value['gender'] =='1'){
                              $gender ='Male'; 
                             }else{
                              $gender ='Female';
                             }
                        $fms_val =json_decode($value['fms'],true);
                           ?>
                  <div class="trainee_tab_content">
                        <!-- Tab panes -->
                          <div class="tab-content">
                          
                            <div role="tabpanel" class="tab-pane active" id="informaiton">
                               <h3 class="trai_title_sect">Client Details</h3>
                              <div style="margin-right: 2%;margin-bottom: 1%;float: right;margin-top:1%;">
                                <a href="<?php echo base_url();?>user/fms_edit/<?php echo $value['id'];?>" title="Edit FMS" ><i class="fa fa-edit"></i></i></a>
                                <a onclick='javascript:return confirm("Are you sure to Delete this Record?")' title="Delete FMS" href="<?php echo base_url();?>user/fms_delete/<?php echo $value['id'];?>" ><i class="fa fa-trash-o"></i></a>
                              </div>
                                <table class="table table-bordered prifile-input-field client fms_main">
                                <thead><tr><th>Date</th><th><?php ?></th></tr></thead>
                                       <tr>
                                           <th><h3 class="panel-title">Name</h3></th>
                                           <td><?php echo $value['full_name']; ?></td>
                                       </tr>
                                       <tr>
                                           <th><h3 class="panel-title">Age/Gender</h3></th>
                                           <td><?php echo $value['age'].'/'.$gender; ?></td>
                                       </tr>
                                       <tr>
                                           <th><h3 class="panel-title">Height(cm)/Weight(Kg)</h3></th>
                                           <td><?php echo $value['height'].'/'.$value['weight']; ?></td>
                                       </tr>
                               </table>
                               <h3 class="trai_title_sect">THE FUNCTIONAL MOVEMENT SCREEN</h3>
                               <table class="table table-bordered prifile-input-field client fms_table fms_main">
                               <thead><tr><th>TEST</th><th>SCORE</th><th>COMMENTS</th></tr></thead>
                                       <tr>
                                           <th><h3 class="panel-title">DEEP SQUAT</h3></th>
                                           <td><?php echo $fms_val['deep_squat']; ?></td>
                                           <td><?php echo $fms_val['deep_squat_comment']; ?></td>
                                       </tr>
                                       <tr>
                                           <th><h3 class="panel-title"> HURDLE STEP (Left)/(Right)</h3></th>
                                           <td><?php echo $fms_val['hurdle_step_l']; ?>/<?php echo $fms_val['hurdle_step_r']; ?></td>
                                           <td><?php echo $fms_val['hurdle_step_l_comment']; ?>/<?php echo $fms_val['hurdle_step_r_comment']; ?></td>
                                       </tr> 
                                       <!-- <tr>
                                           <th><h3 class="panel-title">(Right)</h3></th>
                                           <td><?php echo $fms_val['hurdle_step_r']; ?></td>
                                           <td><?php echo $fms_val['hurdle_step_r_comment']; ?></td>
                                       </tr> -->
                                       <tr>
                                           <th><h3 class="panel-title">INLINE LUNGE(Left)/(Right)</h3></th>
                                           <td><?php echo $fms_val['inline_lunge_l']; ?>/<?php echo $fms_val['inline_lunge_r']; ?></td>
                                           <td><?php echo $fms_val['inline_lunge_l_comment']; ?>/<?php echo $fms_val['inline_lunge_r_comment']; ?></td>
                                       </tr>
                                       <!-- <tr>
                                           <th><h3 class="panel-title">(Right)</h3></th>
                                           <td><?php echo $fms_val['inline_lunge_r']; ?></td>
                                           <td><?php echo $fms_val['inline_lunge_r_comment']; ?></td>
                                       </tr> -->
                                       <tr>
                                           <th><h3 class="panel-title">SHOULDER MOBILITY(Left)/(Right)</h3></th>
                                           <td><?php echo $fms_val['shoulder_mobility_l']; ?>/<?php echo $fms_val['shoulder_mobility_r']; ?></td>
                                           <td><?php echo $fms_val['shoulder_mobility_l_comment']; ?>/<?php echo $fms_val['shoulder_mobility_r_comment']; ?></td>
                                       </tr>
                                       <!-- <tr>
                                           <th><h3 class="panel-title">(Right)</h3></th>
                                           <td><?php echo $fms_val['shoulder_mobility_r']; ?></td>
                                           <td><?php echo $fms_val['shoulder_mobility_r_comment']; ?></td>
                                       </tr> -->
                                       <tr>
                                           <th><h3 class="panel-title">IMPINGEMENT CLEARING TEST (Left)/(Right)</h3></th>
                                           <td><?php echo $fms_val['impingement_test_l']; ?>/<?php echo $fms_val['impingement_test_r']; ?></td>
                                           <td><?php echo $fms_val['impingement_test_l_comment']; ?>/<?php echo $fms_val['impingement_test_r_comment']; ?></td>
                                       </tr>
                                       <!-- <tr>
                                           <th><h3 class="panel-title">(Right)</h3></th>
                                           <td><?php echo $fms_val['impingement_test_r']; ?></td>
                                           <td><?php echo $fms_val['impingement_test_r_comment']; ?></td>
                                       </tr> -->
                                       <tr>
                                           <th><h3 class="panel-title">ACTIVE STRAIGHT-LEG RAISE (Left)/(Right)</h3></th>
                                           <td><?php echo $fms_val['active_straight_l']; ?>/<?php echo $fms_val['active_straight_r']; ?></td>
                                           <td><?php echo $fms_val['active_straight_l_comment']; ?>/<?php echo $fms_val['active_straight_r_comment']; ?></td>
                                       </tr>
                                       <!-- <tr>
                                           <th><h3 class="panel-title">(Right)</h3></th>
                                           <td><?php echo $fms_val['active_straight_r']; ?></td>
                                           <td><?php echo $fms_val['active_straight_r_comment']; ?></td>
                                       </tr> -->
                                       <tr>
                                           <th><h3 class="panel-title"> TRUNK STABILITY PUSH-UP</h3></th>
                                           <td><?php echo $fms_val['trunk_stablity']; ?></td>
                                           <td><?php echo $fms_val['trunk_stablity_comment']; ?></td>
                                       </tr>
                                       <tr>
                                           <th><h3 class="panel-title"> PRESS-UP CLEARING TEST</h3></th>
                                           <td><?php echo $fms_val['press_up']; ?></td>
                                           <td><?php echo $fms_val['press_up_comment']; ?></td>
                                       </tr>
                                       <tr>
                                           <th><h3 class="panel-title">ROTARY STABILITY(Left)/(Right)</h3></th>
                                           <td><?php echo $fms_val['rotary_stability_l']; ?>/<?php echo $fms_val['rotary_stability_r']; ?></td>
                                           <td><?php echo $fms_val['rotary_stability_l_comment']; ?>/<?php echo $fms_val['rotary_stability_r_comment']; ?></td>
                                       </tr>
                                       <!-- <tr>
                                           <th><h3 class="panel-title">(Right)</h3></th>
                                           <td><?php echo $fms_val['rotary_stability_r']; ?></td>
                                           <td><?php echo $fms_val['rotary_stability_r_comment']; ?></td>
                                       </tr> -->
                                       <tr>
                                           <th><h3 class="panel-title">POSTERIOR ROCKING CLEARING TEST</h3></th>
                                           <td><?php echo $fms_val['posterior']; ?></td>
                                           <td><?php echo $fms_val['posterior_comment']; ?></td>
                                       </tr>
                                       <tr>
                                           <th><h3 class="panel-title">TOTAL:</h3></th>
                                           <td><?php echo $value['total']; ?></td>
                                           <td></td>
                                       </tr>
                          </table>
                            </div>
                          </div>
                          
                          <div class="clearfix"></div>
                        <!-- <a class="btn submit_btn" href="#">Update</a> -->
                    </div>
                    <?php }
                    }?>
                </div>
            </div>
            
        </div>
        
    </div>
    
    <!--Main container sec end-->
    
   
    <div class="clearfix"></div>
    </body>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script type="text/javascript">
    $(document).ready(function(){
      
         $(".tab-pane").on('click','#submit',function() {
         
            var data1 = $('#nutrition_dairy').serialize();
           $.ajax({
             url:"<?php echo base_url() ?>user/add_nutrition",
             type:"post",
             data: data1,
             success: function(response)
             { 
             if(response){
                notify('success','<i class="fa fa-check"> Success </i>','Your details save successfully');
                notify('error','<i class="fa fa-times"> Error ! </i>','Data already available for this date !');                      
                $('#nutrition_dairy')[0].reset();
                setTimeout(function() { $("#nutrition").fadeOut(1500); }, 5000);
             
             
             }
             }
         });
     
         });
      
    });
</script>
<script src="http://code.jquery.com/jquery-1.11.1.min.js"></script>

<script>
jQuery.noConflict();
function loadDoc() {
  var user_id = document.getElementById("user_id").value;
  var name = document.getElementById("name").value;
  var xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function() {
    if (xhttp.readyState == 4 && xhttp.status == 200) {
      document.getElementById("demo").innerHTML = xhttp.responseText;
       $('#next').hide();
   
    }
  }
  xhttp.open("GET", "<?php echo base_url();?>user/search_nutrition_details?user_id="+user_id+"&name="+name, true);
  xhttp.send();
  
}
</script>

<script type="text/javascript">
    $(document).ready(function(){
         $(".tab-pane").on('click','#search',function() {
          var date = $('#datepicker').val();
          var name = $('#name').val();
           $.ajax({
             url:"<?php echo base_url() ?>user/search_date_nutrition_details?date="+date+"&name="+name,
             type:"post",
             data: "date="+date,
             contentType: false,
             processData: false,
             success: function(data)
             {
              $('#demo').html(data);
             }
         });
      });
    });
</script>
<script type="text/javascript">
    $(document).ready(function(){
      
         $(".tab-pane").on('click','#workout-submit',function() {
         
            var data1 = new FormData($('#workout-dairy')[0]);
           $.ajax({
             url:"<?php echo base_url() ?>user/add_workout",
             type:"post",
             data: data1,
             contentType: false,
             processData: false,
             success: function(response)
             {
               notify('success','<i class="fa fa-check"> Success </i>','Your details save successfully');
               $('#workout-dairy')[0].reset();
               setTimeout(function() { $("#workout").fadeOut(1500); }, 5000)
             }
         });
     
         });
  
/* ------------------------start  script for flash messages-------------------
*/
  setTimeout(function(){
    $('#success').hide();
  }, 3000);

    setTimeout(function(){
    $('#failure').hide();
  }, 3000);
   /*---------------end script for flash messages---------------------*/
   /*
--------------------------start code for meal plan----------------------------------- 
   */
  // alert('<?php echo json_encode("hi"); ?>');
   var meal = '<?php echo json_encode($meal); ?>';
   var obj = JSON.parse(meal);
   $('#trainee_id').val(obj.trainee_id);
   $.each(obj, function(key, value){
    if(value.row == 'row1')
    { 
      //$('#row1 td:not(:first)').append();
      $('#row1 td:eq(1)').append(value.name+'-' + value.cname+'|').addClass('bg-primary');
      $('#row1').find('td:last').html('<input type="button" class="btn-primary edit_plan" id="'+value.id+'" value="Edit">'
       +' <input type="submit" class="btn-danger delete_plan" id="'+value.id+'" value="Delete">');
    }
    else if(value.row == 'row2')
    {

      $('#row2 td:eq(1)').addClass('bg-success').find('a').append(value.name+'-' + value.cname +'|');
      $('#row2').find('td:last').html('<input type="button" class="btn-primary edit_plan" id="'+value.id+'" value="Edit">'
       +' <input type="button" class="btn-danger delete_plan" id="'+value.id+'" value="Delete">');
    } 
    else if(value.row == 'row3')
    {
      $('#row3 td:eq(1)').addClass('bg-info').find('a').append(value.name+'-' + value.cname +'|');
      $('#row3').find('td:last').html('<input type="button" class="btn-primary edit_plan" id="'+value.id+'" value="Edit">'
       +' <input type="button" class="btn-danger delete_plan" id="'+value.id+'" value="Delete">');
    }
    else if(value.row == 'row4')
    {
      $('#row4 td:eq(1)').addClass('bg-warning').find('a').append(value.name+'-' + value.cname +'|');
      $('#row4').find('td:last').html('<input type="button" class="btn-primary edit_plan" id="'+value.id+'" value="Edit">'
       +' <input type="button" class="btn-danger delete_plan" id="'+value.id+'" value="Delete">');
    }
    else if(value.row == 'row5')
    {
      $('#row5 td:eq(1)').addClass('bg-danger').find('a').append(value.name+'-' + value.cname +'|');
      $('#row5').find('td:last').html('<input type="button" class="btn-primary edit_plan" id="'+value.id+'" value="Edit">'
       +' <input type="button" class="btn-danger delete_plan" id="'+value.id+'" value="Delete">');
    }
    else if(value.row == 'row6')
    {
      $('#row6 td:eq(1)').addClass('bg-custom1').find('a').append(value.name+'-' + value.cname +'|');
      $('#row6').find('td:last').html('<input type="button" class="btn-primary edit_plan" id="'+value.id+'" value="Edit">'
       +' <input type="button" class="btn-danger delete_plan" id="'+value.id+'" value="Delete">');
    }
    else if(value.row == 'row7')
    {
      $('#row7 td:eq(1)').addClass('bg-custom2').find('a').append(value.name+'-' + value.cname +'|');
      $('#row7').find('td:last').html('<input type="button" class="btn-primary edit_plan" id="'+value.id+'" value="Edit">'
       +' <input type="button" class="btn-danger delete_plan" id="'+value.id+'" value="Delete">');
    }
    else if(value.row == 'row8')
    {
      $('#row8 td:eq(1)').addClass('bg-custom3').find('a').append(value.name+'-' + value.cname +'|');
      $('#row8').find('td:last').html('<input type="button" class="btn-primary edit_plan" id="'+value.id+'" value="Edit">'
       +' <input type="button" class="btn-danger delete_plan" id="'+value.id+'" value="Delete">');
    } 
   });
   /*
--------------------------end code for meal plan----------------------------------- 
   */
/*---------------------------  end of code---------------------------------------- 
   */ 
   /*
------------------ start code for edit meal plan ----------------------------
   */
   $('.edit_plan').click(function(){
      var row_id = $(this).closest('tr').attr('id');
      var client_id = '<?php echo $client_id; ?>';
      var plan_id = $(this).attr('id');


       $('#datepicker4').datepicker({daysOfWeekDisabled: [0,2,3,4,5,6], format : 'd M yyyy'});
        $('#datepicker4').blur(function(){
          var date = new Date($('#datepicker4').val());
          date.setDate(date.getDate()+7);
          var str = date.toString();
          var a = new Array();
          var a = str.split(" ", 4);
          $('#datepicker5').val(a[2]+' '+a[1]+ ' '+a[3]);
        });

      $.ajax({
        type    : "POST",
        url     : "<?php echo base_url('user/get_meal_by_id'); ?>",
        data    : {client_id : client_id, plan_id : plan_id},
        success : function(data){
          var edit_obj = JSON.parse(data);
           $('#plan_id').val(edit_obj.id);
           $('#edit_name').val(edit_obj.name);
           $('#edit_trainee').val(edit_obj.trainee_id);
           $('#edit_category').val(edit_obj.category_id);
           $('#datepicker4').val(edit_obj.start_date);
           $('#datepicker5').val(edit_obj.end_date);
           $('#edit_row').val(edit_obj.row);
           $('#meal_Modal_edit').modal('show');
        }
      });
   });

   $('.delete_plan').click(function(){
      var plan_id = $(this).attr('id');
      if(confirm("are you sure ?")){
      $.ajax({
        type    : "POST",
        url     : "<?php echo base_url('user/delete_meal_plans'); ?>", 
        data    : {plan_id : plan_id},
        success : function(data){
            alert(data);
            location.reload();
        }
      });
    }
   });


  
   /*
------------------ end code for edit meal plan ----------------------------
   */

    });
</script>
<script type="text/javascript">
    $(document).ready(function(){
      
         $(".tab-pane").on('click','#image',function() {
            var data1 = new FormData($('#add_image')[0]);
           $.ajax({
             url:"<?php echo base_url();?>user/add_image",
             type:"post",
             data: data1,
             contentType: false,
             processData: false,
             success: function(data)
             {

                /*$("#upload_images").html(data);*/
                notify('success','<i class="fa fa-check"> Success </i>','Image Added Successfully');
                $('#workout-dairy')[0].reset();
                setTimeout(function() { window.location.reload(); }, 1000)
             }
         });
     
         });
      
    });
</script>
<script type="text/javascript">
    $(document).ready(function(){
      
         $(".tab-pane").on('click','#previous',function() {
           var start_date = $('#start_date').val();        
           var end_date = $('#end_date').val();
           var user_id = $('#user_id').val();
           $.ajax({
             url:"<?php echo base_url();?>user/nutrition_details_by_week?start_date="+start_date+"&end_date="+end_date+"&user_id="+user_id,
             type:"post",             
             contentType: false,
             processData: false,
             success: function(data)
             {
              var start_date =  $($.parseHTML(data)).find("#start_date");
              console.log(start_date);
              if(start_date["length"] > 0){
               var end_date =  $($.parseHTML(data)).find("#end_date");
               $('#start_date').val(start_date);
               $('#end_date').val(end_date);
               $('#next').show();
               jQuery('#demo').html(data);
              }else
              {
                $('#previous').hide();
                //$('#next').show();
              }
             }
         });
     
         });
      
    });
</script>
<script type="text/javascript">
    $(document).ready(function(){
         $(".tab-pane").on('click','#trainningplan_previous',function() {
           var plan_start_date = $('#plan_start_date').val();        
           var plan_end_date = $('#plan_end_date').val();
           var plan_user_id = $('#plan_user_id').val();
           $.ajax({
             url:"<?php echo base_url();?>user/trainning_plan_by_week?plan_start_date="+plan_start_date+"&plan_end_date="+plan_end_date+"&plan_user_id="+plan_user_id,
             type:"post",             
             contentType: false,
             processData: false,
             success: function(data)
             {
              var start_date =  $($.parseHTML(data)).find("#plan_start_date");
              console.log(start_date);
              if(start_date["length"] > 0){
               var end_date =  $($.parseHTML(data)).find("#plan_end_date");
               $('#start_date').val(start_date);
               $('#end_date').val(end_date);
                $('#trainningplan_next').show();
               jQuery('#client_plan_result').html(data);
              }else
              {
                $('#trainningplan_previous').hide();
                //$('#next').show();
              }
             
             }
         });
     
         });
      
    });
</script>
<script type="text/javascript">
    $(document).ready(function(){
      
         $(".tab-pane").on('click','#next',function() {
           var start_date = $('#start_date').val();        
           var end_date = $('#end_date').val();
           var user_id = $('#user_id').val();
           $.ajax({
             url:"<?php echo base_url();?>user/nutrition_details_by_week_next?start_date="+start_date+"&end_date="+end_date+"&user_id="+user_id,
             type:"post",             
             contentType: false,
             processData: false,
             success: function(data)
             {
              var start_date =  $($.parseHTML(data)).find("#start_date");
              var end_date =  $($.parseHTML(data)).find("#end_date");
              var today = new Date();
              var dayOfWeekStartingSundayZeroIndexBased = today.getDay();
              var mondayOfWeek = new Date(today.getFullYear(), today.getMonth(), today.getDate() - today.getDay()+0);
              var strDate = mondayOfWeek.getFullYear() + "-" + (mondayOfWeek.getMonth()+1) + "-" + '0' + mondayOfWeek.getDate();
              $('#start_date').val(start_date);
              $('#end_date').val(end_date);
              $('#previous').show();
              if(start_date[0]["defaultValue"] == strDate){
                $('#next').hide();

              }            
              
        jQuery('#demo').html(data);
             }
         });
     
         });
      
    });
</script>

        
