<?php $this->load->view('baneer_section'); ?>
        	<div class="container">
                	<div class="profile_header">
                    	<div class="col-sm-2">
                        	<span class="user_profile_pic"><img class=" img-responsive" src="<?php echo base_url(); ?>images/profile_img.png" alt=""/></span>
                        </div>
                        <div class="col-sm-8">
                        	<div class="user_profile_info">
                            	<h3><?php echo $user[0]['trainee_name'].' '.$user[0]['trainee_surname']; ?><!-- John Schwarzenegger --></h3>
                                <span class="user_degi">Student</span>
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur imperdiet volutpat orci, et consectetur nibh vulputate sed. Vestibulum mollis, nisl vel tristique congue, erat purus ullamcorper eros, non gravida risus elit sit amet justo. </p> 
                            </div>
                        </div>
                        <div class="col-sm-2">
                        	<ul class="social_icon_user">
                                	<li><a href="#"><span class="fa fa-facebook"></span></a></li>
                                    <li><a href="#"><span class="fa fa-twitter"></span></a></li>
                                    <li><a href="#"><span class="fa fa-linkedin"></span></a></li>
                            </ul>
                        </div>
                    	
                    </div>
                
            </div>
        </div>
        
    </header>
    <!--Header sec end-->
    
    <!--Main container sec start-->
    <div class="main_container">
    
    	<div class="container">
        	<div class="row">
            	<div class="col-sm-12">
                
                	<div class="panel panel_app">
                      <div class="panel-heading">
                        <h3 class="panel-title">Biography </h3>
                      </div>
                      <div class="panel-body">
                       	<p>born July 30, 1947 Schwarzenegger began weight training at the age of 15. He won the Mr. Universe title at age 20 and went on to win the Mr. Olympia contest seven times. Schwarzenegger has remained a prominent presence in bodybuilding and has written many books and articles on the sport. Schwarzenegger gained worldwide fame as a Hollywood action film icon </p>
                      </div>
                    </div>
                    
                    <div class="panel panel_app">
                      <div class="panel-heading">
                        <h3 class="panel-title">Certification  </h3>
                      </div>
                      <div class="panel-body">
                       	<p>BA in Speech Communication/Business Organization <br/>PT certs: CI-CPT & NASM </p>
                      </div>
                    </div>
                    
                    <div class="panel panel_app">
                      <div class="panel-heading">
                        <h3 class="panel-title">Awards  </h3>
                      </div>
                      <div class="panel-body">
                       	<p>BA in Speech Communication/Business Organization <br/>PT certs: CI-CPT & NASM </p>
                      </div>
                    </div>
                    
                    <div class="panel panel_app">
                      <div class="panel-heading">
                        <h3 class="panel-title">Accomplishments   </h3>
                      </div>
                      <div class="panel-body">
                       	<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas facilisis interdum vulputate. Nam elementum eros a purus rhoncus, eu viverra tortor feugiat. Sed quis mollis tellus, non auctor tortor. Curabitur eros orci, lobortis vitae ipsum eget, aliquam ultricies velit </p>
                      </div>
                    </div>
                    
                    <div class="panel panel_app">
                      <div class="panel-heading">
                        <h3 class="panel-title">Location   </h3>
                      </div>
                      <div class="panel-body">
                       	<p>BA in Speech Communication/Business Organization <br/>PT certs: CI-CPT & NASM </p>
                      </div>
                    </div>
                    
                    <div class="panel panel_app">
                      <div class="panel-heading">
                        <h3 class="panel-title">Credentials    </h3>
                      </div>
                      <div class="panel-body">
                       	<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas facilisis interdum vulputate. Nam elementum eros a purus rhoncus, eu viverra tortor feugiat. Sed quis mollis tellus. </p>
                      </div>
                    </div>
                    
                    <div class="panel panel_app">
                      <div class="panel-heading">
                        <h3 class="panel-title"> Interests & hobby    </h3>
                      </div>
                      <div class="panel-body">
                       	<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas facilisis interdum vulputate. Nam elementum eros a purus rhoncus, eu viverra tortor feugiat. Sed quis mollis tellus, non auctor tortor. Curabitur eros orci, lobortis vitae ipsum eget, aliquam ultricies velit  </p>
                      </div>
                    </div>
                    
                </div>
            </div>
            
        </div>
        
    </div>
    
    <!--Main container sec end-->
    
    <div class="clearfix"></div>
