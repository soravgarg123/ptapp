<?php $this->load->view('baneer_section'); ?>
          <div class="container">
                  <div class="profile_header">
                      <div class="col-sm-2">
                          <span class="user_profile_pic"><img class=" img-responsive" src="<?php echo base_url(); ?>images/profile_img.png" alt=""/></span>
                        </div>
                        <div class="col-sm-8">
                          <div class="user_profile_info">   
    
                              <h3><?php echo $user[0]['name']; ?></h3>
                                <span class="user_degi">Student</span>
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur imperdiet volutpat orci, et consectetur nibh vulputate sed. Vestibulum mollis, nisl vel tristique congue, erat purus ullamcorper eros, non gravida risus elit sit amet justo. </p> 
                            </div>
                            <div class="add_profile"><a class="btn submit_profile" id="update" href="<?php echo base_url();?>user/edit_profile_trainee">Edit Profile</a></div>
                        </div>
                        <div class="col-sm-2">
                          <ul class="social_icon_user">
                                  <li><a href="#"><span class="fa fa-facebook"></span></a></li>
                                    <li><a href="#"><span class="fa fa-twitter"></span></a></li>
                                    <li><a href="#"><span class="fa fa-linkedin"></span></a></li>
                            </ul>
                        </div>
                      
                    </div>
                
            </div>
        </div>
        
    </header>
    <!--Header sec end-->
    
    <!--Main container sec start-->
    <div class="main_container">
    
      <div class="container">
          
            <div class="row">
              <div class="col-sm-12">
                  <div class="meal_plan_sec">

                      <!-- Nav tabs -->
                      <ul class="meal_plan_nav" role="tablist">
                        <li role="presentation" class="active"><a href="#home" aria-controls="home" role="tab" data-toggle="tab">Home</a></li>
                        <li role="presentation"><a href="#appointments" aria-controls="appointments" role="tab" data-toggle="tab">Appointments</a></li>
                        <li role="presentation"><a href="#messages" aria-controls="messages" role="tab" data-toggle="tab">Messages</a></li>
                        <li role="presentation"><a href="#my_trainers" aria-controls="my_trainers" role="tab" data-toggle="tab">My Trainers</a></li>    
                        <li role="presentation"><a href="#video_call" aria-controls="video_call" role="tab" data-toggle="tab">Video Call</a></li>       
                        <li role="presentation"><a href="#help" aria-controls="help" role="tab" data-toggle="tab">Progress Album</a></li>
                      </ul>
                    
                      <!-- Tab panes -->
                      <div class="tab-content">
                        <div role="tabpanel" class="tab-pane active" id="home">
                          <div class="meal_plan_heading">
                              <div class="row">
                                  <div class="col-sm-4 mph_left">
                                      <h2>Weekly Meal Plan</h2>
                                    </div>
                                    <div class="col-sm-8 mph_right">
                                      <h3>Trainer :</h3>
                                        <div class="dropdown">
                                          <button id="dLabel" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            joseph
                                            <span class="wcaret"></span>
                                          </button>
                                          <ul class="dropdown-menu" aria-labelledby="dLabel">
                                            <li><a href="#">joseph kahn</a></li>
                                            <li><a href="#">joseph kahn1</a></li>
                                            <li><a href="#">joseph kahn2</a></li>
                                          </ul>
                                        </div>
                                        <nav class="pagination_nav">
                        <ul class="mp_pagination">
    <li>
      <a href="#">
        <span>Prev</span>
      </a>
    </li>
    <li><a href="#"><span class="caret_left"></span></a></li>
    <li class="active"><a href="#">1</a></li>
    <li><a href="#">2</a></li>
    <li><a href="#">3</a></li>
    <li><a href="#">4</a></li>
    <li><a href="#">5</a></li>
    <li><a href="#"><span class="caret_right"></span></a></li>
    <li>
      <a href="#">
        <span>Last</span>
      </a>
    </li>
  </ul>
                    </nav>
                                    </div>
                                </div>
                            </div>
                            
                            <div class="meal_plan_body clearfix">
                              <div class="mp_types">
                                  <nav>
                                      <ul>
                                          <li>Day</li>
                                            <li><span class="flaticon-bread29"></span> Breakfast</li>
                                            <li><span class="flaticon-baked3"></span> Snack</li>
                                            <li><span class="flaticon-knife36"></span> Lunch</li>
                                            <li><span class="flaticon-baked3"></span> Snack</li>
                                            <li><span class="flaticon-pizzas4"></span> Dinner</li>
                                            <li><span class="flaticon-baked3"></span> Snack</li>
                                        </ul>
                                    </nav>
                                </div>
                                <div class="mp_days">
                                  <table>
                                      <thead class="table_heading">
                                          <tr>
                                              <th><div>Sunday</div></th>
                                                <th><div>Monday</div></th>
                                                <th><div>Tuesday</div></th>
                                                <th><div>Wednesday</div></th>
                                                <th><div>Thursday</div></th>
                                                <th><div>Friday</div></th>
                                                <th><div>Saturday</div></th>
                                            </tr>
                                        </thead>
                                        <tbody class="table_body">
                                          <tr>
                                              <td>Baked Omelet</td>
                                                <td>Stell-cut Oats w/Berries &amp;  Pecans</td>
                                                <td>Greek Yourt Granola &amp; Berry Parfait</td>
                                                <td>Stell-cut Oats  w/Berries &amp; Pecans</td>
                                                <td>Baked Omelet</td>
                                                <td>Greek Yourt Granola &amp; Berry Parfait</td>
                                                <td>Stell-cut Oats  w/Berries &amp; Pecans</td>
                                            </tr>
                                            <tr>
                                              <td>Baked Omelet</td>
                                                <td>Stell-cut Oats w/Berries &amp;  Pecans</td>
                                                <td>Greek Yourt Granola &amp; Berry Parfait</td>
                                                <td>Stell-cut Oats  w/Berries &amp; Pecans</td>
                                                <td>Baked Omelet</td>
                                                <td>Greek Yourt Granola &amp; Berry Parfait</td>
                                                <td>Stell-cut Oats  w/Berries &amp; Pecans</td>
                                            </tr>
                                            <tr>
                                              <td>Baked Omelet</td>
                                                <td>Stell-cut Oats w/Berries &amp;  Pecans</td>
                                                <td>Greek Yourt Granola &amp; Berry Parfait</td>
                                                <td>Stell-cut Oats  w/Berries &amp; Pecans</td>
                                                <td>Baked Omelet</td>
                                                <td>Greek Yourt Granola &amp; Berry Parfait</td>
                                                <td>Stell-cut Oats  w/Berries &amp; Pecans</td>
                                            </tr>
                                            <tr>
                                              <td>Baked Omelet</td>
                                                <td>Stell-cut Oats w/Berries &amp;  Pecans</td>
                                                <td>Greek Yourt Granola &amp; Berry Parfait</td>
                                                <td>Stell-cut Oats  w/Berries &amp; Pecans</td>
                                                <td>Baked Omelet</td>
                                                <td>Greek Yourt Granola &amp; Berry Parfait</td>
                                                <td>Stell-cut Oats  w/Berries &amp; Pecans</td>
                                            </tr>
                                            <tr>
                                              <td>Baked Omelet</td>
                                                <td>Stell-cut Oats w/Berries &amp;  Pecans</td>
                                                <td>Greek Yourt Granola &amp; Berry Parfait</td>
                                                <td>Stell-cut Oats  w/Berries &amp; Pecans</td>
                                                <td>Baked Omelet</td>
                                                <td>Greek Yourt Granola &amp; Berry Parfait</td>
                                                <td>Stell-cut Oats  w/Berries &amp; Pecans</td>
                                            </tr>
                                            <tr>
                                              <td>Baked Omelet</td>
                                                <td>Stell-cut Oats w/Berries &amp;  Pecans</td>
                                                <td>Greek Yourt Granola &amp; Berry Parfait</td>
                                                <td>Stell-cut Oats  w/Berries &amp; Pecans</td>
                                                <td>Baked Omelet</td>
                                                <td>Greek Yourt Granola &amp; Berry Parfait</td>
                                                <td>Stell-cut Oats  w/Berries &amp; Pecans</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            
                            
                        </div>
                        <div role="tabpanel" class="tab-pane" id="appointments">appointments</div>
                        <div role="tabpanel" class="tab-pane" id="messages">messages</div>
                        <div role="tabpanel" class="tab-pane" id="my_trainers">my_trainers</div>
                        <div role="tabpanel" class="tab-pane" id="video_call">video_call</div>
                        
                        <div role="tabpanel" class="tab-pane" id="help">
                        <form method="post" id="gallery">
                        	<div class="dash_photo_gallery">
                                  <!-- Tab panes -->
                                  <div class="tab-content photos_tab_content">
                                    <div role="tabpanel" class="tab-pane active" id="progress_photo">
                                    	<div class="photo_gall_content">
                                        	<ul class="photo_gallery_list">
                                            	<li class="photo_upload_sect">
                                                	<input value="Photo Upload" type="file" name="photoimg" id="photoimg"/>
                                                    <span class="fa fa-cloud-upload"></span>
                                                    <h5>Upload your Photo</h5>
                                                </li>
                                                <div id="result">
                                                <?php foreach ($image as $trainee_image) {
                                             
                                                ?>

                                            	<li>
                                                 <a href="javascript:void(0);">
                                                 <span class="delte_img1" main="MTQ=" main2="561f849ccb610.jpg">
                                                 <i class="fa fa-close"></i>
                                                 </span>
                                                 </a>
                                            	<a href="#"><img class="img-responsive" src="<?php echo base_url();?>trainee_gallery/<?php echo $trainee_image['image_name'];?>" style="height:150px;" alt=""/></a>
                                                <input type="hidden" name="photo_id" id="photo_id" value="<?php echo $trainee_image['photo_id'];
                                                ?>">

                                            	</li>
                                            	<?php
                                            	  }
                                            	?>
                                            	</div>
                                            </ul>
                                            <div class="clearfix"></div>
                                        </div>
                                    </div>
                                  </div>
                                
                            </div>
                             </form>
                        </div>
                       
                      </div>
                    
                    </div>
                </div>
            </div>
            
            <div class="row">
              <div class="col-sm-8">
                  <div class="row">
                      <div class="col-sm-6">
                          <div class="panel_block my_trainees">
                              <div class="panel_block_heading">
                                  <h4>My Trainees</h4>
                                </div>
                                <div class="panel_block_body">
                                  <ul>
                                      <li>
                                          <div class="pbb_left">
                                              <div class="pbbl_img">
                                                <img src="<?php echo base_url(); ?>images/trainer4.jpg" alt="img" class="img-circle">
                                                </div>
                                                <div class="pbbl_txt">
                                                  Lorem ipsum
                                                </div>
                                            </div>
                                            <div class="pbb_right">
                                              <div class="location_name">
                                                  Kyoto, Japan
                                                </div>
                                            </div>
                                        </li>
                                        <li>
                                          <div class="pbb_left">
                                              <div class="pbbl_img">
                                                <img src="<?php echo base_url(); ?>images/trainer4.jpg" alt="img" class="img-circle">
                                                </div>
                                                <div class="pbbl_txt">
                                                  Lorem ipsum
                                                </div>
                                            </div>
                                            <div class="pbb_right">
                                              <div class="location_name">
                                                  Kyoto, Japan
                                                </div>
                                            </div>
                                        </li>
                                        <li>
                                          <div class="pbb_left">
                                              <div class="pbbl_img">
                                                <img src="<?php echo base_url(); ?>images/trainer4.jpg" alt="img" class="img-circle">
                                                </div>
                                                <div class="pbbl_txt">
                                                  Lorem ipsum
                                                </div>
                                            </div>
                                            <div class="pbb_right">
                                              <div class="location_name">
                                                  Kyoto, Japan
                                                </div>
                                            </div>
                                        </li>
                                        <li>
                                          <div class="pbb_left">
                                              <div class="pbbl_img">
                                                <img src="<?php echo base_url(); ?>images/trainer4.jpg" alt="img" class="img-circle">
                                                </div>
                                                <div class="pbbl_txt">
                                                  Lorem ipsum
                                                </div>
                                            </div>
                                            <div class="pbb_right">
                                              <div class="location_name">
                                                  Kyoto, Japan
                                                </div>
                                            </div>
                                        </li>
                                        <li>
                                          <div class="pbb_left">
                                              <div class="pbbl_img">
                                                <img src="<?php echo base_url(); ?>images/trainer4.jpg" alt="img" class="img-circle">
                                                </div>
                                                <div class="pbbl_txt">
                                                  Lorem ipsum
                                                </div>
                                            </div>
                                            <div class="pbb_right">
                                              <div class="location_name">
                                                  Kyoto, Japan
                                                </div>
                                            </div>
                                        </li>
                                        <li>
                                          <div class="pbb_left">
                                              <div class="pbbl_img">
                                                <img src="<?php echo base_url(); ?>images/trainer4.jpg" alt="img" class="img-circle">
                                                </div>
                                                <div class="pbbl_txt">
                                                  Lorem ipsum
                                                </div>
                                            </div>
                                            <div class="pbb_right">
                                              <div class="location_name">
                                                  Kyoto, Japan
                                                </div>
                                            </div>
                                        </li>
                                        <li>
                                          <div class="pbb_left">
                                              <div class="pbbl_img">
                                                <img src="<?php echo base_url(); ?>images/trainer4.jpg" alt="img" class="img-circle">
                                                </div>
                                                <div class="pbbl_txt">
                                                  Lorem ipsum
                                                </div>
                                            </div>
                                            <div class="pbb_right">
                                              <div class="location_name">
                                                  Kyoto, Japan
                                                </div>
                                            </div>
                                        </li>
                                        <li>
                                          <div class="pbb_left">
                                              <div class="pbbl_img">
                                                <img src="<?php echo base_url(); ?>images/trainer4.jpg" alt="img" class="img-circle">
                                                </div>
                                                <div class="pbbl_txt">
                                                  Lorem ipsum
                                                </div>
                                            </div>
                                            <div class="pbb_right">
                                              <div class="location_name">
                                                  Kyoto, Japan
                                                </div>
                                            </div>
                                        </li>
                                        <li>
                                          <div class="pbb_left">
                                              <div class="pbbl_img">
                                                <img src="<?php echo base_url(); ?>images/trainer4.jpg" alt="img" class="img-circle">
                                                </div>
                                                <div class="pbbl_txt">
                                                  Lorem ipsum
                                                </div>
                                            </div>
                                            <div class="pbb_right">
                                              <div class="location_name">
                                                  Kyoto, Japan
                                                </div>
                                            </div>
                                        </li>
                                        <li>
                                          <div class="pbb_left">
                                              <div class="pbbl_img">
                                                <img src="<?php echo base_url(); ?>images/trainer4.jpg" alt="img" class="img-circle">
                                                </div>
                                                <div class="pbbl_txt">
                                                  Lorem ipsum
                                                </div>
                                            </div>
                                            <div class="pbb_right">
                                              <div class="location_name">
                                                  Kyoto, Japan
                                                </div>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6">
                          <div class="panel_block my_trainees">
                              <div class="panel_block_heading">
                                  <h4>Message</h4>
                                </div>
                                <div class="panel_block_body">
                                  <ul>
                                      <li>
                                          <div class="pbb_left">
                                              <div class="pbbl_img">
                                                <img src="<?php echo base_url(); ?>images/trainer4.jpg" alt="img" class="img-circle">
                                                </div>
                                                <div class="pbbl_txt">
                                                  <h5>Lorem ipsum</h5>
                                                    <p>Lorem ipsm doller sit amet</p>
                                                </div>
                                            </div>
                                            <div class="pbb_right">
                                              <div class="mesg_time">
                                                  04 Nov 18:00
                                                </div>
                                            </div>
                                        </li>
                                        <li>
                                          <div class="pbb_left">
                                              <div class="pbbl_img">
                                                <img src="<?php echo base_url(); ?>images/trainer4.jpg" alt="img" class="img-circle">
                                                </div>
                                                <div class="pbbl_txt">
                                                  <h5>Lorem ipsum</h5>
                                                    <p>Lorem ipsm doller sit amet</p>
                                                </div>
                                            </div>
                                            <div class="pbb_right">
                                              <div class="mesg_time">
                                                  04 Nov 18:00
                                                </div>
                                            </div>
                                        </li>
                                        <li>
                                          <div class="pbb_left">
                                              <div class="pbbl_img">
                                                <img src="<?php echo base_url(); ?>images/trainer4.jpg" alt="img" class="img-circle">
                                                </div>
                                                <div class="pbbl_txt">
                                                  <h5>Lorem ipsum</h5>
                                                    <p>Lorem ipsm doller sit amet</p>
                                                </div>
                                            </div>
                                            <div class="pbb_right">
                                              <div class="mesg_time">
                                                  04 Nov 18:00
                                                </div>
                                            </div>
                                        </li>
                                        <li>
                                          <div class="pbb_left">
                                              <div class="pbbl_img">
                                                <img src="<?php echo base_url(); ?>images/trainer4.jpg" alt="img" class="img-circle">
                                                </div>
                                                <div class="pbbl_txt">
                                                  <h5>Lorem ipsum</h5>
                                                    <p>Lorem ipsm doller sit amet</p>
                                                </div>
                                            </div>
                                            <div class="pbb_right">
                                              <div class="mesg_time">
                                                  04 Nov 18:00
                                                </div>
                                            </div>
                                        </li>
                                        <li>
                                          <div class="pbb_left">
                                              <div class="pbbl_img">
                                                <img src="<?php echo base_url(); ?>images/trainer4.jpg" alt="img" class="img-circle">
                                                </div>
                                                <div class="pbbl_txt">
                                                  <h5>Lorem ipsum</h5>
                                                    <p>Lorem ipsm doller sit amet</p>
                                                </div>
                                            </div>
                                            <div class="pbb_right">
                                              <div class="mesg_time">
                                                  04 Nov 18:00
                                                </div>
                                            </div>
                                        </li>
                                        <li>
                                          <div class="pbb_left">
                                              <div class="pbbl_img">
                                                <img src="<?php echo base_url(); ?>images/trainer4.jpg" alt="img" class="img-circle">
                                                </div>
                                                <div class="pbbl_txt">
                                                  <h5>Lorem ipsum</h5>
                                                    <p>Lorem ipsm doller sit amet</p>
                                                </div>
                                            </div>
                                            <div class="pbb_right">
                                              <div class="mesg_time">
                                                  04 Nov 18:00
                                                </div>
                                            </div>
                                        </li>
                                        <li>
                                          <div class="pbb_left">
                                              <div class="pbbl_img">
                                                <img src="<?php echo base_url(); ?>images/trainer4.jpg" alt="img" class="img-circle">
                                                </div>
                                                <div class="pbbl_txt">
                                                  <h5>Lorem ipsum</h5>
                                                    <p>Lorem ipsm doller sit amet</p>
                                                </div>
                                            </div>
                                            <div class="pbb_right">
                                              <div class="mesg_time">
                                                  04 Nov 18:00
                                                </div>
                                            </div>
                                        </li>
                                        <li>
                                          <div class="pbb_left">
                                              <div class="pbbl_img">
                                                <img src="<?php echo base_url(); ?>images/trainer4.jpg" alt="img" class="img-circle">
                                                </div>
                                                <div class="pbbl_txt">
                                                  <h5>Lorem ipsum</h5>
                                                    <p>Lorem ipsm doller sit amet</p>
                                                </div>
                                            </div>
                                            <div class="pbb_right">
                                              <div class="mesg_time">
                                                  04 Nov 18:00
                                                </div>
                                            </div>
                                        </li>
                                        <li>
                                          <div class="pbb_left">
                                              <div class="pbbl_img">
                                                <img src="<?php echo base_url(); ?>images/trainer4.jpg" alt="img" class="img-circle">
                                                </div>
                                                <div class="pbbl_txt">
                                                  <h5>Lorem ipsum</h5>
                                                    <p>Lorem ipsm doller sit amet</p>
                                                </div>
                                            </div>
                                            <div class="pbb_right">
                                              <div class="mesg_time">
                                                  04 Nov 18:00
                                                </div>
                                            </div>
                                        </li>
                                        <li>
                                          <div class="pbb_left">
                                              <div class="pbbl_img">
                                                <img src="<?php echo base_url(); ?>images/trainer4.jpg" alt="img" class="img-circle">
                                                </div>
                                                <div class="pbbl_txt">
                                                  <h5>Lorem ipsum</h5>
                                                    <p>Lorem ipsm doller sit amet</p>
                                                </div>
                                            </div>
                                            <div class="pbb_right">
                                              <div class="mesg_time">
                                                  04 Nov 18:00
                                                </div>
                                            </div>
                                        </li>
                                        <li>
                                          <div class="pbb_left">
                                              <div class="pbbl_img">
                                                <img src="<?php echo base_url(); ?>images/trainer4.jpg" alt="img" class="img-circle">
                                                </div>
                                                <div class="pbbl_txt">
                                                  <h5>Lorem ipsum</h5>
                                                    <p>Lorem ipsm doller sit amet</p>
                                                </div>
                                            </div>
                                            <div class="pbb_right">
                                              <div class="mesg_time">
                                                  04 Nov 18:00
                                                </div>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                      <div class="col-sm-12">
                          <div class="account_billing_tab">
                <div class="abt_inner">
                                  <!-- Nav tabs -->
                                  <ul class="accunt_billing_nav" role="tablist">
                                    <li role="presentation" class="active"><a href="#account" aria-controls="account" role="tab" data-toggle="tab">Account</a></li>
                                    <li role="presentation"><a href="#billing" aria-controls="billing" role="tab" data-toggle="tab">Billing</a></li>
                                  </ul>
                                  <!-- Tab panes -->
                                  <div class="tab-content">
                                    <div role="tabpanel" class="tab-pane active" id="account">
                                      <div class="account_details">
                                          <ul>
                                              <li>
                                                  <strong>Plan Name :- </strong> <span>Lorem ipsum dolor sit amet.</span>
                                                </li>
                                                <li>
                                                  <strong>Trainer Name :-</strong> <span>Lorem ipsum dolor sit amet.</span>
                                                </li>
                                                <li>
                                                  <strong>Purchase Date :- </strong> <span>28/05/2015</span>
                                                </li>
                                                <li>
                                                  <strong>Expiry Date :- </strong> <span>28/06/2015</span>
                                                </li>
                                                <li>
                                                  <strong>Remaining Days  :- </strong> <span>15</span>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div role="tabpanel" class="tab-pane" id="billing">Billing</div>
                                  </div>
                              </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-4">
                  <aside class="innerp_right_bar">
                  <div class="panel_block notification">
                              <div class="panel_block_heading">
                                  <h4>Notification</h4>
                                </div>
                                <div class="panel_block_body">
                                  <ul>
                                      <li>
                                          <div class="pbb_left">
                                              <div class="pbbl_img">
                                                <img src="<?php echo base_url(); ?>images/trainer4.jpg" alt="img" class="img-circle">
                                                </div>
                                                <div class="pbbl_txt">
                                                  <h5>Shay Lawrence <strong>Booked you </strong></h5>
                                                </div>
                                            </div>
                                            <div class="pbb_right">
                                              <div class="mesg_time">
                                                  04 Nov 18:00
                                                </div>
                                            </div>
                                        </li>
                                        <li>
                                          <div class="pbb_left">
                                              <div class="pbbl_img">
                                                <img src="<?php echo base_url(); ?>images/trainer4.jpg" alt="img" class="img-circle">
                                                </div>
                                                <div class="pbbl_txt">
                                                  <h5>Shay Lawrence <strong>Booked you </strong></h5>
                                                </div>
                                            </div>
                                            <div class="pbb_right">
                                              <div class="mesg_time">
                                                  04 Nov 18:00
                                                </div>
                                            </div>
                                        </li>
                                        <li>
                                          <div class="pbb_left">
                                              <div class="pbbl_img">
                                                <img src="<?php echo base_url(); ?>images/trainer4.jpg" alt="img" class="img-circle">
                                                </div>
                                                <div class="pbbl_txt">
                                                  <h5>Shay Lawrence <strong>Booked you </strong></h5>
                                                </div>
                                            </div>
                                            <div class="pbb_right">
                                              <div class="mesg_time">
                                                  04 Nov 18:00
                                                </div>
                                            </div>
                                        </li>
                                        <li>
                                          <div class="pbb_left">
                                              <div class="pbbl_img">
                                                <img src="<?php echo base_url(); ?>images/trainer4.jpg" alt="img" class="img-circle">
                                                </div>
                                                <div class="pbbl_txt">
                                                  <h5>Shay Lawrence <strong>Booked you </strong></h5>
                                                </div>
                                            </div>
                                            <div class="pbb_right">
                                              <div class="mesg_time">
                                                  04 Nov 18:00
                                                </div>
                                            </div>
                                        </li>
                                        <li>
                                          <div class="pbb_left">
                                              <div class="pbbl_img">
                                                <img src="<?php echo base_url(); ?>images/trainer4.jpg" alt="img" class="img-circle">
                                                </div>
                                                <div class="pbbl_txt">
                                                  <h5>Shay Lawrence <strong>Booked you </strong></h5>
                                                </div>
                                            </div>
                                            <div class="pbb_right">
                                              <div class="mesg_time">
                                                  04 Nov 18:00
                                                </div>
                                            </div>
                                        </li>
                                        <li>
                                          <div class="pbb_left">
                                              <div class="pbbl_img">
                                                <img src="<?php echo base_url(); ?>images/trainer4.jpg" alt="img" class="img-circle">
                                                </div>
                                                <div class="pbbl_txt">
                                                  <h5>Shay Lawrence <strong>Booked you </strong></h5>
                                                </div>
                                            </div>
                                            <div class="pbb_right">
                                              <div class="mesg_time">
                                                  04 Nov 18:00
                                                </div>
                                            </div>
                                        </li>
                                        <li>
                                          <div class="pbb_left">
                                              <div class="pbbl_img">
                                                <img src="<?php echo base_url(); ?>images/trainer4.jpg" alt="img" class="img-circle">
                                                </div>
                                                <div class="pbbl_txt">
                                                  <h5>Shay Lawrence <strong>Booked you </strong></h5>
                                                </div>
                                            </div>
                                            <div class="pbb_right">
                                              <div class="mesg_time">
                                                  04 Nov 18:00
                                                </div>
                                            </div>
                                        </li>
                                        <li>
                                          <div class="pbb_left">
                                              <div class="pbbl_img">
                                                <img src="<?php echo base_url(); ?>images/trainer4.jpg" alt="img" class="img-circle">
                                                </div>
                                                <div class="pbbl_txt">
                                                  <h5>Shay Lawrence <strong>Booked you </strong></h5>
                                                </div>
                                            </div>
                                            <div class="pbb_right">
                                              <div class="mesg_time">
                                                  04 Nov 18:00
                                                </div>
                                            </div>
                                        </li>
                                        <li>
                                          <div class="pbb_left">
                                              <div class="pbbl_img">
                                                <img src="<?php echo base_url(); ?>images/trainer4.jpg" alt="img" class="img-circle">
                                                </div>
                                                <div class="pbbl_txt">
                                                  <h5>Shay Lawrence <strong>Booked you </strong></h5>
                                                </div>
                                            </div>
                                            <div class="pbb_right">
                                              <div class="mesg_time">
                                                  04 Nov 18:00
                                                </div>
                                            </div>
                                        </li>
                                        <li>
                                          <div class="pbb_left">
                                              <div class="pbbl_img">
                                                <img src="<?php echo base_url(); ?>images/trainer4.jpg" alt="img" class="img-circle">
                                                </div>
                                                <div class="pbbl_txt">
                                                  <h5>Shay Lawrence <strong>Booked you </strong></h5>
                                                </div>
                                            </div>
                                            <div class="pbb_right">
                                              <div class="mesg_time">
                                                  04 Nov 18:00
                                                </div>
                                            </div>
                                        </li>
                                        <li>
                                          <div class="pbb_left">
                                              <div class="pbbl_img">
                                                <img src="<?php echo base_url(); ?>images/trainer4.jpg" alt="img" class="img-circle">
                                                </div>
                                                <div class="pbbl_txt">
                                                  <h5>Shay Lawrence <strong>Booked you </strong></h5>
                                                </div>
                                            </div>
                                            <div class="pbb_right">
                                              <div class="mesg_time">
                                                  04 Nov 18:00
                                                </div>
                                            </div>
                                        </li>
                                        <li>
                                          <div class="pbb_left">
                                              <div class="pbbl_img">
                                                <img src="<?php echo base_url(); ?>images/trainer4.jpg" alt="img" class="img-circle">
                                                </div>
                                                <div class="pbbl_txt">
                                                  <h5>Shay Lawrence <strong>Booked you </strong></h5>
                                                </div>
                                            </div>
                                            <div class="pbb_right">
                                              <div class="mesg_time">
                                                  04 Nov 18:00
                                                </div>
                                            </div>
                                        </li>
                                        <li>
                                          <div class="pbb_left">
                                              <div class="pbbl_img">
                                                <img src="<?php echo base_url(); ?>images/trainer4.jpg" alt="img" class="img-circle">
                                                </div>
                                                <div class="pbbl_txt">
                                                  <h5>Shay Lawrence <strong>Booked you </strong></h5>
                                                </div>
                                            </div>
                                            <div class="pbb_right">
                                              <div class="mesg_time">
                                                  04 Nov 18:00
                                                </div>
                                            </div>
                                        </li>
                                        <li>
                                          <div class="pbb_left">
                                              <div class="pbbl_img">
                                                <img src="<?php echo base_url(); ?>images/trainer4.jpg" alt="img" class="img-circle">
                                                </div>
                                                <div class="pbbl_txt">
                                                  <h5>Shay Lawrence <strong>Booked you </strong></h5>
                                                </div>
                                            </div>
                                            <div class="pbb_right">
                                              <div class="mesg_time">
                                                  04 Nov 18:00
                                                </div>
                                            </div>
                                        </li>
                                        <li>
                                          <div class="pbb_left">
                                              <div class="pbbl_img">
                                                <img src="<?php echo base_url(); ?>images/trainer4.jpg" alt="img" class="img-circle">
                                                </div>
                                                <div class="pbbl_txt">
                                                  <h5>Shay Lawrence <strong>Booked you </strong></h5>
                                                </div>
                                            </div>
                                            <div class="pbb_right">
                                              <div class="mesg_time">
                                                  04 Nov 18:00
                                                </div>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                     </aside>
                </div>
            </div>
            
            
        </div>
        
    </div>
    
    <!--Main container sec end-->
    
<div class="clearfix"></div>
<script type="text/javascript" src="http://ajax.googleapis.com/
ajax/libs/jquery/1.5/jquery.min.js"></script>
<script type="text/javascript">
$(document).ready(function(){

$('#photoimg').on('change', function(){

var data = new FormData($('#gallery')[0]);
      $.ajax({
                  type: "post",
                  url: "<?php echo base_url(); ?>user/trainee_upload_image",
                  data: data,
                  contentType: false,
                  processData: false,
                  
                  success: function(data){

                  	if(data == "true"){

                           location.reload();
                            // $("div#help").show();

                  	}
                            
                                
                             }
                            
                            
                  
                });
});

});
</script>
<script type="text/javascript">
$(document).ready(function(){

$('.fa').on('click', function(){

var status = confirm("Are you sure you want to delete ?");  
  if(status==true)
  {

      var photo_id = $('#photo_id').val();
      $.ajax({
                  type: "post",
                  url: "<?php echo base_url(); ?>user/trainee_delete_image",
                  data: "photo_id="+photo_id,
                  
                  success: function(data){

                            
                                
                             }
                            
                            
                  
                });
  }
});

});
</script>