
     <!-- <div class="chating_wind_wrapper"> -->
      <div class="panel panel-default">
            <div class="panel-heading top-bar">
                <h3 class="panel-title"><?php echo $profile_data[0]['name']; ?> <!-- <span class="online_Status_g active_status"></span> --></h3>
                    <div class="chat_seting_sect">
                      <a href="javascript:void(0);" class="close_chatbox" main="<?php echo $profile_data[0]['user_id']; ?>"><span class="fa fa-close"></span></a>
                  </div>
            </div>
            <div class="panel-body msg_container_base chat_msg_box_<?php echo $profile_data[0]['user_id']; ?>">
                <?php
                    if(!empty($chating_msg)){
                        foreach($chating_msg as $cm){ 
                          if($cm['chat_sender_id'] == $this->session->userdata('user_id')) { ?>
                            <div class="msg_container base_sent">
                            <div class="messages msg_sent">
                                    <p><?php echo $cm['chat_msg']; ?></p>
                                </div>
                            <div class="chat_avatar">
                                <img src="<?php echo base_url(); ?><?php if($this->session->userdata('user_type') == "trainner") { echo "trainner"; } else { echo "trainee"; } ?>/<?php echo $own_profile[0]['user_pic']; ?>" class="img-responsive img-circle">
                            </div>
                        </div></br>
                        <?php  }else{ ?>
                                <div class="msg_container base_receive">
                                    <div class="chat_avatar">
                                        <img src="<?php echo base_url(); ?><?php if($this->session->userdata('user_type') == "trainner") { echo "trainee"; } else { echo "trainner"; } ?>/<?php echo $profile_data[0]['user_pic']; ?>" class="img-responsive img-circle">
                                    </div>
                                    <div class="messages msg_receive">
                                            <p><?php echo $cm['chat_msg']; ?></p>
                                        </div>
                                </div></br>
                        <?php } 
                     }
                    }
                ?>
            </div>
            <div class="panel-footer footer_chat_<?php echo $profile_data[0]['user_id']; ?>">
                <div class="input-group">
                    <input id="btn-input" type="text" class="form-control input-sm chat_input chat_input_box_<?php echo $profile_data[0]['user_id']; ?>" placeholder="Write your message here..." />
                    <span class="input-group-btn">
                    <!-- <button class="chat_btn btn" href="#"><span class="fa fa-paperclip"></span></button> -->
                    <a href="javascript:void(0);" class="chat_btn btn" main="<?php echo $profile_data[0]['user_id']; ?>"><span class="fa fa-paper-plane"></span></a>
                    </span>
                </div>
            </div>
        </div>
     <!-- </div>  -->


    <!-- Message Chating Script Start -->

    <script type="text/javascript">
        $(document).ready(function(){
        
        $(".chat_msg_box_<?php echo $profile_data[0]['user_id']; ?>").animate({scrollTop: $(".chat_msg_box_<?php echo $profile_data[0]['user_id']; ?>")[0].scrollHeight}, 1000);

        $('body').on('click','.chat_btn',function(){
            var to_id = $(this).attr('main'); // reciever_id
            var from_id = "<?php echo $this->session->userdata('user_id'); ?>"; // sender_id
            var user_type = "<?php echo $this->session->userdata('user_type'); ?>";
            var msg = $('.chat_input_box_'+ to_id).val();
            if(msg == ""){
                return false;
            }
            if(user_type == "trainner"){
                var folder_name = "trainner";
            }else{
                var folder_name = "trainee";
            }
            $.ajax({
                url: "<?php echo base_url(); ?>user/insertChatMsg",
                type: "POST",
                data: {msg:msg,reciever_id:to_id,sender_id:from_id},
                async:false,
                success:function(response){
                    console.log(response);
                }
            });
            var from_htmlDiv = '</br><div class="msg_container base_sent"><div class="messages msg_sent"><p>' + msg + '</p></div><div class="chat_avatar"><img src="<?php echo base_url(); ?>' + folder_name + '/<?php echo $own_profile[0]["user_pic"]; ?>" class="img-responsive img-circle"></div></div></br>';
            var to_htmlDiv = '</br><div class="msg_container base_receive"><div class="messages msg_sent"><p>' + msg + '</p></div><div class="chat_avatar"><img src="<?php echo base_url(); ?>' + folder_name + '/<?php echo $own_profile[0]["user_pic"]; ?>" class="img-responsive img-circle"></div></div></br>';
            var to_data = to_id+"$&"+from_id+"$&"+to_htmlDiv;
            send(to_data);
            $('.chat_msg_box_'+ to_id).append(from_htmlDiv);
            $('.chat_input_box_'+ to_id).val("");
            $('.chat_msg_box_'+to_id).animate({scrollTop: $('.chat_msg_box_'+to_id)[0].scrollHeight}, 1000);

        });


        $('body').on('keyup','.chat_input',function(event){
           if(event.keyCode == 13){
                $(".chat_btn").trigger("click");
             }
           });
        });
    </script>

    <!-- Message Chating Script End -->

