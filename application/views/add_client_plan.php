<?php $this->load->view('baneer_section'); ?>
          <div class="container">
                  
                
            </div>
        </div>
        
    </header>
    <!--Header sec end-->
    <!--Main container sec start-->
    <div class="main_container">
    
      <div class="container">
          
            <div class="row">
              <div class="col-sm-3">
                  
                    <div class="trainee_tabs_sect">
                        <h3>Add Training Plan</h3>
                        <!-- Nav tabs -->
                          <ul class="nav_tabs">
                            <li class="active"><a href="#informaiton" aria-controls="informaiton" role="tab" data-toggle="tab">Training Plan</a></li>
                          </ul>
                    </div>
                    
                </div>
                <div class="col-sm-9">
                  <div class="trainee_tab_content">
<link rel="stylesheet" href="<?php echo base_url(); ?>css/jquery-ui.css">
                                       <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7/jquery.min.js"></script>
                                       <script>
                                        var d = new Date();
          var n = d.getUTCDay(); 
          console.log(n); // get index of current date
          var prev = n;
          console.log(prev); // get total prev dates
          var next = 6-n;
          console.log(next); // get total next date
          var startdate = new Date();
          startdate.setDate(startdate.getDate()-prev);
          var enddate = new Date();
          enddate.setDate(enddate.getDate()+next);
          $(document).ready(function(){
          $('#datepicker1').datepicker({
              autoclose:true,
              format: 'yyyy-mm-dd',
              startDate: startdate,
              endDate : enddate
          });
          });
                                       </script>
                     <form method="post" id="add_client_plan">
                     <div id="client_plan" class="client_plan">Your Data Saved Successfully</div>
                        <!-- Tab panes -->
                          <div class="tab-content">
                            <div role="tabpanel" class="tab-pane active" id="informaiton">
                              <h3 class="trai_title_sect">Training Plan Information</h3>
                                <div class="form_wrapper">
                                    <div class="form-group">
                                        <label>Clients</label>
                                         <select class="form-control" autocomplete="off" name="user_id" required="">
                                             <option value="">Select Client</option>
                                             <?php foreach ($select_client as $clients) {
                                                    if($client_id == $clients['user_id']){
                                                        $val ='selected="selected"';
                                                    }else{
                                                      $val ='';
                                                    }
                                                ?>
                                               <option <?php echo $val;?> value="<?php echo $clients['user_id'];?>"><?php echo $clients['name'];?></option>
                                              
                                             <?php } ?>
                                         </select>
                                    </div>                                  
                                    <div class="form-group">
                                        <label>Exercise</label>
                                        <input type="text" class="form-control" id="exercise" name="exercise" placeholder="Exercise">
                                    </div>
                                    <div class="form-group">
                                        <label>Weight</label>
                                        <input type="text" class="form-control" id="weight" name="weight" placeholder="Weight">
                                    </div>
                                    <div class="form-group">
                                        <label>Sets</label>
                                        <input type="text" class="form-control" id="sets" name="sets" placeholder="Sets">
                                    </div>
                                    <div class="form-group">
                                        <label>Reps</label>
                                        <input type="text" class="form-control" id="reps" name="reps" placeholder="Reps">
                                    </div>
                                    <div class="form-group">
                                        <label>Selected date</label>
                                        <input type="text" id="datepicker1" name="selected_date" class="form-control" placeholder="Select Date">                             
                                    </div>
                                    <div class="form-group">
                                        <label>Time</label>
                                        <input class="form-control" value="" type="text" name="time" placeholder="Time">
                                    </div>
                                    <div class="form-group">
                                        <label>Notes</label>
                                        <textarea class="form-control" id="notes" name="notes" placeholder="Notes"></textarea>
                                    </div>
                                </div>
                            </div>
                          </div>
                          <div class="clearfix"></div>
                        <button id="plan" class="btn submit_btn" type="button">Add</button>
                     </form>
                    </div>
                    
                </div>
            </div>
            
        </div>
        
    </div>
    
    <!--Main container sec end-->
     <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script type="text/javascript">
    $(document).ready(function(){
          $(".trainee_tab_content").on('click','#plan',function() {
           
                  var data1 = $('#add_client_plan').serialize();
                  $.ajax({
                    url:"<?php echo base_url() ?>user/add_client_plan",
                    type:"post",
                    data: data1,
                    success: function(response)
                    {
                      notify('success','<i class="fa fa-check"> Success </i>','Your Data Saved Successfully');
                      $('#add_client_plan')[0].reset();
                      setTimeout(function() { $("#client_plan").fadeOut(1500); }, 5000)
                    }
        });
      
      
      });
    });
</script>
    
