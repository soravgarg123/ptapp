 <aside class="right-side">
                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1>
                        <form action="#" method="get" class="sidebar-form" style="width:0%">
                        <div>
                            <input type="hidden" name="q"/>
                            <span>
                               
                            </span>
                        </div>
                    </form>
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                        <li class="active">Orders</li>
                    </ol>
                </section>

         


<table id="DataTables_Table_0" class="table table-striped table-bordered bootstrap-datatable datatable responsive dataTable no-footer" role="grid" aria-describedby="DataTables_Table_0_info">

<table id="DataTables_Table_0" class="table table-striped table-bordered bootstrap-datatable datatable responsive dataTable no-footer" role="grid" aria-describedby="DataTables_Table_0_info">
<thead>
<tr role="row">
<th class="sorting_asc" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" style="width: 41px;" aria-sort="ascending" aria-label="S.No.: activate to sort column descending">S.No.</th>
<th class="sorting" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" style="width: 127px;" aria-label="Name: activate to sort column ascending">Order Id</th>

<th class="sorting" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" style="width: 127px;" aria-label="Name: activate to sort column ascending">Order By</th>

<th class="sorting" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" style="width: 167px;" aria-label="Name: activate to sort column ascending">Address</th>

<th class="sorting" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" style="width: 127px;" aria-label="Name: activate to sort column ascending">Phone</th>

<th class="sorting" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" style="width: 50px;" aria-label="Name: activate to sort column ascending">Order Date</th>
<th class="sorting" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" style="width: 50px;" aria-label="Name: activate to sort column ascending">Total Amount</th>


<th class="sorting" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" style="width: 150px;" aria-label="Actions: activate to sort column ascending">Actions</th>
</tr>
</thead>
<tbody>
<?php $i = 1; foreach ($order_list as $row) {  ?>

<tr class="odd" role="row">
<td class="sorting_1"><?php echo $i++; ?></td>

<td class="center"><?php echo $row->order_id; ?></td>
<td class="center"><?php echo $row->full_name; ?></td>
<td class="center"><?php echo $row->address; ?></td>
<td class="center"><?php echo $row->mobile_no;?></td>
<td class="center"><?php echo $row->order_date; ?></td>
<td class="center">$<?php echo $row->total_amount;?></td>


<td class="center">

<a class="btn btn-primary btn-sm Show-Order-Item" title="Edit Cahnnel" href="javascript:"  oid="<?php echo $row->order_id; ?>">
View Order Item
</a>
<a class="btn btn-primary btn-sm Show-payment" title=" Cahnnel" href="javascript:"  pid="<?php echo $row->order_payment_id; ?>">
<i class="fa fa-credit-card" aria-hidden="true"></i>
</a>
</td>
</tr>
</tbody>
<?php } ?>
</table>

</div>
</div>
</div>
</div>
</div>
</div>
</div>


<script type="text/javascript">
       $(document).ready(function(){
     
          $('.Show-Order-Item').click(function(){
             
            var order_id=$(this).attr('oid');
             
            $.ajax({
              type: "post",
              url: "<?php echo base_url('admin/admin/order_items'); ?>",
              data:"orderId="+order_id,
               success: function(data){
                    $('#order_item_modal').modal('show'); 
                     $('.order_item_body').html(data);
                
                },


               
              
               
            }); 
        });

          // payment detail

          $('.Show-payment').click(function(){
             
            var payment_id=$(this).attr('pid');
             
            $.ajax({
              type: "post",
              url: "<?php echo base_url('admin/admin/payment_order'); ?>",
              data:"payment_id="+payment_id,
               success: function(data){
                    $('#order_payment_modal').modal('show'); 
                     $('.order_payment_body').html(data);
                
                },


               
              
               
            }); 
        });




     });
     
</script>


  <div class="modal fade bs-example-modal-md" id="order_item_modal" tabindex="-1" role="dialog" aria-labelledby="order_item_modal">
          <div class="modal-dialog modal-md" role="document">
            <div class="modal-content custom_modal">
               <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title text-center" id="myModalLabel">Order Items</h4>
              </div>
                <div class="modal-body  order_item_body">

                <div class="">
                   


                </div>
                    
                
               </div>
              
            </div>
          </div>
        </div>





<div class="modal fade bs-example-modal-md" id="order_payment_modal" tabindex="-1" role="dialog" aria-labelledby="order_payment_modal">
          <div class="modal-dialog modal-md" role="document">
            <div class="modal-content custom_modal">
               <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title text-center" id="myModalLabel">Payment Detail</h4>
              </div>
                <div class="modal-body  order_payment_body">

                <div class="">
                   


                </div>
                    
                
               </div>
              
            </div>
          </div>
        </div>


