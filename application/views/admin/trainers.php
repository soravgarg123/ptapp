 <aside class="right-side">
                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1>
                        <form action="#" method="get" class="sidebar-form" style="width:0%">
                        <div>
                            <input type="hidden" name="q"/>
                            <span>
                               
                            </span>
                        </div>
                    </form>
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                        <li class="active">Manage Trainers</li>
                    </ol>
                </section>

               <!--- <div style="margin-left:2%;margin-top:1%;">
                    <a class="btn btn-primary" href="<?php echo site_url('users/add_user'); ?>"><i class="glyphicon glyphicon-plus"></i> Add New User</a>
                </div>--->
                <div>
<div class="col-md-4">
</div>
</div>
<div>
<div class="box col-md-12">
<div class="box-inner">
<h2>
<i class="glyphicon glyphicon-user"></i>
  Manage Trainers
</h2>
<div class="box-content">
<div id="DataTables_Table_0_wrapper" class="dataTables_wrapper" role="grid">
<div id="DataTables_Table_0_wrapper" class="dataTables_wrapper no-footer">
<div class="row">
<div class="col-md-6">
<div id="DataTables_Table_0_length" class="dataTables_length">

</div>
</div>
<!--<div class="col-md-6">
<div id="DataTables_Table_0_filter" class="dataTables_filter">
<form action="" method = "post">
<label>
Search:
<input class="" name="search" type="text" placeholder="" aria-controls="DataTables_Table_0">
<input type="submit" name="submit" value="Search" class="btn btn-primary btn-sm" style="height:27px;">
</label>
</form>
</div>
</div>-->
</div>
<?php if($this->input->post('search')){ ?>
<table id="DataTables_Table_0" class="table table-striped table-bordered bootstrap-datatable datatable responsive dataTable no-footer" role="grid" aria-describedby="DataTables_Table_0_info">
<thead>
<tr role="row">
<th class="sorting_asc" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" style="width: 41px;" aria-sort="ascending" aria-label="S.No.: activate to sort column descending">S.No.</th>
<th class="sorting" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" style="width: 127px;" aria-label="Name: activate to sort column ascending">Full name 11</th>

<th class="sorting" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" style="width: 127px;" aria-label="Name: activate to sort column ascending">Email</th>

<th class="sorting" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" style="width: 127px;" aria-label="Name: activate to sort column ascending">Publish Date</th>


<th class="sorting" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" style="width: 150px;" aria-label="Actions: activate to sort column ascending">Actions</th>
</tr>
</thead>
<?php
foreach($users as $row1){
if($this->input->post('search')){ ?>
<tbody>
<tr class="odd" role="row">
<td class="sorting_1"><?php echo $row1['user_id'] ?></td>
<td class="center"><?php echo $row1['name'].' '.$row1['surname'] ?></td>
<td class="center"><?php echo $row1['email'] ?></td>
<td class="center"><?php echo $row1['publish_date'] ?></td>



<td class="center">
<!---<a class="btn btn-primary btn-sm" title="Edit Cahnnel" href="<?php echo site_url('users/add_user/'.$row1['id']) ?>">
<i class="glyphicon glyphicon-edit icon-white"></i>
Edit
</a>-->
<a class="btn btn-danger btn-sm" title="Delete Cahnnel" href="<?php echo site_url('users/delete/'.$row1['id']) ?>">
<i class="glyphicon glyphicon-trash icon-white"></i>
Delete
</a>
</td>
</tr>
</tbody>
<?php } }?>
</table>
<?php } else { ?>
<table id="DataTables_Table_0" class="table table-striped table-bordered bootstrap-datatable datatable responsive dataTable no-footer" role="grid" aria-describedby="DataTables_Table_0_info">
<thead>
<tr role="row">
<th class="sorting_asc" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" style="width: 41px;" aria-sort="ascending" aria-label="S.No.: activate to sort column descending">S.No.</th>

<th class="sorting" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" style="width: 127px;" aria-label="Name: activate to sort column ascending">First Name</th>

<th class="sorting" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" style="width: 127px;" aria-label="Name: activate to sort column ascending">Last Name</th>

<th class="sorting" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" style="width: 127px;" aria-label="Name: activate to sort column ascending">City</th>

<th class="sorting" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" style="width: 127px;" aria-label="Name: activate to sort column ascending">Occupation</th>

<th class="sorting" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" style="width: 127px;" aria-label="Name: activate to sort column ascending">Qualifications</th>

<th class="sorting" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" style="width: 127px;" aria-label="Name: activate to sort column ascending">Email</th>

<th class="sorting" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" style="width: 127px;" aria-label="Name: activate to sort column ascending">Payment Status</th>

<th class="sorting" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" style="width: 150px;" aria-label="Actions: activate to sort column ascending">Actions</th>
</tr>
</thead>
<?php $i = 1; foreach ($users as $row) { ; ?>
<tbody>
<tr class="odd" role="row">
<td class="sorting_1"><?php echo $i++; ?></td>
<td class="center"><?php echo $row['name']; ?></td>
<td class="center"><?php echo $row['surname']; ?></td>
<td class="center"><?php echo $row['location']; ?></td>
<td class="center"><?php echo $row['occupation']; ?></td>
<td class="center"><?php echo $row['qualifications']; ?></td>
<td class="center"><?php echo $row['email'] ?></td>
<td class="center">
<?php 
    if($row['trainer_payment_status'] == 0){
        echo "Success";
    }
    if($row['trainer_payment_status'] == 1){
        echo "Pending";
    }
    if($row['trainer_payment_status'] == 2){
        echo "Failed";
    }
?>
</td>
<td class="center">
<?php if($row['trainer_payment_status'] == 1 || $row['trainer_payment_status'] == 2){ ?>
    <a class="btn btn-primary btn-sm" title="click here to change payment status" href="<?php echo site_url('admin/admin/change_payment_status/'.$row['user_id']) ?>" onclick="return confirm('Are you sure to change payment status in success mode ?');">
        <i class="glyphicon glyphicon-user"></i>
        Change Status
    </a>   
    </br>
    </br>
<?php } ?>
<a class="btn btn-danger btn-sm" title="Delete Cahnnel" href="<?php echo site_url('admin/admin/trainers_delete/'.$row['user_id']) ?>" onclick="return confirm('Are You Sure ! You Want To Delete This Record');">
<i class="glyphicon glyphicon-trash icon-white"></i>
Delete
</a>
</td>
</tr>
</tbody>
<?php } ?>
</table>
<?php } ?>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
