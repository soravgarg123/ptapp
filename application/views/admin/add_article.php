     <aside class="right-side">
         
                <!-- Main content -->
                
                   <div id="content" class="col-lg-10 col-sm-10" style="margin-top:1%;">
                   <!-- <div class="box-header well"></div> -->
                   <?php 
                    echo form_open('admin/admin/add_article');
                  ?>
                  <input type="hidden" name="id" value="<?php echo $id; ?>">
                  <?php  if($this->session->flashdata('error') !='' ||  null !== validation_errors()) { ?>
                   <div class="form-group has-error"><label class="control-label" for="inputError"> <?php echo $this->session->flashdata('error');   echo ' '.validation_errors(); ?></label></div>
                    <?php } ?>
                  <?php  if($this->session->flashdata('success') !='') { ?>
                   <div class="form-group has-success"><label class="control-label" for="inputSuccess"> <?php echo $this->session->flashdata('success');  ?></label></div>
                    <?php } ?>
                       
                            <div class="form-group">
                              <label for="first_name">Title</label>
                                 <input class="form-control" type="text"  value="<?php if(isset($article[0]['title'])) {  echo $article[0]['title']; }else{ echo set_value('title'); } ?>" name="title" >
                              </div>
                            <div class="form-group">
                              <label for="article">Article</label>
                             <textarea class="form-control" name="article"><?php if(isset($article[0]['article'])) {  echo $article[0]['article']; } ?>
                             </textarea>
                             
                              </div>
                            <input class="btn btn-primary" type="submit" value="Save" name="submit" >
                       
                       <?php echo form_close(); ?>
                       </div>
                   </div>
                    <!-- top row -->
                    
                    <!-- /.row -->

                    <!-- Main row -->
                    <div class="row">
                       
                    </div><!-- /.row (main row) -->

                </section><!-- /.content -->
            </aside><!-- /.right-side --> 
        </div><!-- ./wrapper -->

        <!-- add new calendar event modal -->


        <!-- jQuery 2.0.2 -->
        
    </body>
</html>
<script src="//tinymce.cachefly.net/4.1/tinymce.min.js"></script>
<script type="text/javascript">
       tinymce.init({
    selector: "textarea",
    theme: "modern",
    font_size_classes : "fontSize1, fontSize2, fontSize3, fontSize4, fontSize5, fontSize6",
    plugins: [
         "advlist autolink link image lists charmap print preview hr anchor pagebreak spellchecker",
         "searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking",
         "save table contextmenu directionality emoticons template paste textcolor"
   ],

   toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | print preview media fullpage | forecolor backcolor emoticons | sizeselect | fontselect | fontsize | fontsizeselect", 
   style_formats: [
        {title: 'Bold text', inline: 'b'},
        {title: 'Red text', inline: 'span', styles: {color: '#ff0000'}},
        {title: 'Red header', block: 'h1', styles: {color: '#ff0000'}},
        {title: 'Example 1', inline: 'span', classes: 'example1'},
        {title: 'Example 2', inline: 'span', classes: 'example2'},
        {title: 'Table styles'},
        {title: 'Table row 1', selector: 'tr', classes: 'tablerow1'}
    ]
 });
</script>
