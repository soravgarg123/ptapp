     <aside class="right-side">
         
                <!-- Main content -->
                
                   <div id="content" class="col-lg-10 col-sm-10" style="margin-top:1%;">
                   <!-- <div class="box-header well"></div> -->
                   <form method="post" action ="" enctype="multipart/form-data">
                  <?php  if($this->session->flashdata('error') !='' ||  null !== validation_errors()) { ?>
                   <div class="form-group has-error"><label class="control-label" for="inputError"> <?php echo $this->session->flashdata('error');   echo ' '.validation_errors(); ?></label></div>
                    <?php } ?>
                  <?php  if($this->session->flashdata('success') !='') { ?>
                   <div class="form-group has-success"><label class="control-label" for="inputSuccess"> <?php echo $this->session->flashdata('success');  ?></label></div>
                    <?php } ?>
                            
                              <div class="form-group">
                              <label for="first_name">Name </label></br>
                              <select name="category_name" class="form-control">
                                <option value="">Select Category</option>
                                <option value="facebook" <?php if(!empty($edit_social) && $edit_social['name'] == 'facebook' ) { echo "selected='selected'"; } ?>>Facebook</option>
                                <option value="linkedin" <?php if(!empty($edit_social) && $edit_social['name'] == 'linkedin' ) { echo "selected='selected'"; } ?>>LinkedIn</option>
                                <option value="twitter" <?php if(!empty($edit_social) && $edit_social['name'] == 'twitter' ) { echo "selected='selected'"; } ?>>Twitter</option>
                                <option value="youtube" <?php if(!empty($edit_social) && $edit_social['name'] == 'youtube' ) { echo "selected='selected'"; } ?>>YouTube</option>
                              </select>
                            </div>

                             <div class="form-group">
                              <label for="first_name">Url </label></br>
                              <input type="text" class="form-control" name="url" id="url" value="<?php if(!empty($edit_social)) { echo $edit_social['url']; } ?>"/>
                            </div>

                            <input class="btn btn-primary" type="submit" value="Save" name="submit" >
                       
                      </form>
                       </div>
                   </div>
                    <!-- top row -->
                    
                    <!-- /.row -->

                    <!-- Main row -->
                    <div class="row">
                        <!-- Left col -->
                        <!-- /.Left col -->
                        <!-- right col (We are only adding the ID to make the widgets sortable)-->

                    </div><!-- /.row (main row) -->

                </section><!-- /.content -->
            </aside><!-- /.right-side --> 
        </div><!-- ./wrapper -->

        <!-- add new calendar event modal -->


        <!-- jQuery 2.0.2 -->
        
    </body>
</html>
