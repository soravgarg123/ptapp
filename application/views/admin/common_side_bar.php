<div class="wrapper row-offcanvas row-offcanvas-left">
            <!-- Left side column. contains the logo and sidebar -->
            <aside class="left-side sidebar-offcanvas">
                <!-- sidebar: style can be found in sidebar.less -->
                <section class="sidebar">
                    <!-- Sidebar user panel -->
                    <div class="user-panel">
                        <div class="pull-left image">
                            <img src="<?php echo base_url(); ?>themeadmin/img/avatar3.png" class="img-circle" alt="User Image" />
                        </div>
                        <div class="pull-left info">
                            <p><?php 
                        if ($this->session->userdata('username')!=''){
                        print_r(ucfirst($this->session->userdata('username')));
                    }
                        else
                       {
                       redirect('admin/admin.logout');
                       }

                        ?></p>

                            <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
                        </div>
                    </div>
                    <!-- search form -->
                    
                    <!-- /.search form -->
                    <!-- sidebar menu: : style can be found in sidebar.less -->
                    <ul class="sidebar-menu">
                        <li class="active">
                            <a href="<?php echo site_url('admin/admin/trainers'); ?>">
                                <i class="fa fa-dashboard"></i> <span>Dashboard</span>
                            </a>
                        </li>
                    
                      <li class="active">
                            <a href="<?php echo site_url('admin/admin/trainers'); ?>">
                                <i class="fa fa-dashboard"></i> <span>Manage Trainers </span>
                            </a>
                        </li>
                      <li class="active">
                            <a href="<?php echo site_url('admin/admin/clients'); ?>">
                                <i class="fa fa-dashboard"></i> <span>Manage Clients </span>
                            </a>
                        </li>
                        <li class="active">
                            <a href="<?php echo site_url('admin/admin/users'); ?>">
                                <i class="fa fa-dashboard"></i> <span>Manage Users </span>
                            </a>
                        </li> 
                       <li class="active">
                            <a href="<?php echo site_url('admin/admin/services'); ?>">
                                <i class="fa fa-dashboard"></i> <span>Manage Services </span>
                            </a>
                        </li>
                      <li class="active">
                            <a href="<?php echo site_url('admin/admin/plans'); ?>">
                                <i class="fa fa-dashboard"></i> <span>Manage Plans </span>
                            </a>
                        </li>
                    <!-- ////////////////Code Written by Aditya /////////////// !-->                      
                       <li class="active">
                            <a href="<?php echo site_url('admin/admin/people_say'); ?>">
                                <i class="fa fa-dashboard"></i> <span>What People Say </span>
                            </a>
                        </li>
                        <li class="active">
                            <a href="<?php echo site_url('admin/admin/why_us'); ?>">
                                <i class="fa fa-dashboard"></i> <span>Why Choose Us </span>
                            </a>
                        </li>

                         <li class="active">
                            <a href="<?php echo site_url('admin/admin/latest_update'); ?>">
                                <i class="fa fa-dashboard"></i> <span>Manage Latest Update </span>
                            </a>
                        </li>

                         <li class="active">
                            <a href="<?php echo site_url('admin/admin/payments'); ?>">
                                <i class="fa fa-credit-card" aria-hidden="true"></i><span>Payment</span>
                            </a>
                        </li>

                         <li class="active">
                            <a href="<?php echo site_url('admin/admin/orders'); ?>">
                                <i class="fa fa-shopping-cart" aria-hidden="true"></i> <span>Orders</span>
                            </a>
                        </li>
                        
                        <li class="active">
                            <a href="<?php echo site_url('admin/admin/products'); ?>">
                                <i class="fa fa-dashboard"></i> <span>Manage Products</span>
                            </a>
                        </li>
                         <li class="active">
                            <a href="<?php echo site_url('admin/admin/manage_banner'); ?>">
                                <i class="fa fa-dashboard"></i> <span>Manage Home Banner </span>
                            </a>
                        </li>

                        <li class="active">
                            <a href="<?php echo site_url('admin/admin/manage_product_banner'); ?>">
                                <i class="fa fa-dashboard"></i> <span>Manage Product Banner </span>
                            </a>
                        </li>


                        <li class="active">
                            <a href="<?php echo site_url('admin/admin/manage_story'); ?>">
                                <i class="fa fa-dashboard"></i> <span>Manage My Story </span>
                            </a>
                        </li>
                        <li class="active">
                            <a href="<?php echo site_url('admin/admin/manage_profile_images'); ?>">
                                <i class="fa fa-dashboard"></i> <span>Manage Profile Images </span>
                            </a>
                        </li>

                        <li class="active">
                            <a href="<?php echo site_url('admin/admin/manage_about_us'); ?>">
                                <i class="fa fa-dashboard"></i> <span>Manage About Us </span>
                            </a>
                        </li>

                        <li class="active">
                            <a href="<?php echo site_url('admin/admin/manage_terms'); ?>">
                                <i class="fa fa-dashboard"></i> <span>Manage Terms-Conditions </span>
                            </a>
                        </li>

                         <li class="active">
                            <a href="<?php echo site_url('admin/admin/manage_social_link'); ?>">
                                <i class="fa fa-dashboard"></i> <span>Manage Social Link </span>
                            </a>
                        </li>

                        <li class="active">
                            <a href="<?php echo site_url('admin/admin/manage_subscribers'); ?>">
                                <i class="fa fa-dashboard"></i> <span>Manage Subscribers </span>
                            </a>
                        </li>
                        <li class="active">
                            <a href="<?php echo site_url('admin/admin/manage_clientlogin'); ?>">
                                <i class="fa fa-dashboard"></i> <span>Manage Client Login Page </span>
                            </a>
                        </li>
                        <li class="active">
                            <a href="<?php echo site_url('admin/admin/article'); ?>">
                                <i class="fa fa-dashboard"></i> <span>Manage Trainers Article/blog Page </span>
                            </a>
                        </li>

                    <!-- ////////////////End of Code ///////////////  !-->
                    </ul>
                </section>
                <!-- /.sidebar -->
            </aside>
