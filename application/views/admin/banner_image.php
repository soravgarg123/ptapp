 <aside class="right-side">
                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1>
                       
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                        <li class="active">Manage Services</li>
                    </ol>
                </section>

         <div style="margin-right: 2%;margin-bottom: 1%;float: right;margin-top:1%;">
                    <a class="btn btn-primary" href="<?php echo site_url('admin/admin/add_banner_manage'); ?>"><i class="glyphicon glyphicon-plus"></i> Add New Service</a>
                </div>
                <div>
<div class="col-md-4">
<!-- ////////////////add section for flash messages start here////////////////// !-->
<?php
            if($this->session->flashdata('file_error'))
            {?>
        <div style="color:red;" id="file_error">
            <?php echo "Warning !".$this->session->flashdata('file_error'); ?>
        </div>
        <?php } ?>

         <?php
            if($this->session->flashdata('success'))
            {?>
        <div style="color:green;" id="file_error">
            <?php echo "Success !".$this->session->flashdata('success'); ?>
        </div>
        <?php } ?>

          <?php
            if($this->session->flashdata('failure'))
            {?>
        <div style="color:red;" id="failure">
            <?php echo "Failure !".$this->session->flashdata('failure'); ?>
        </div>
        <?php } ?>
<!-- ////////////////add section for flash messages end here////////////////// !-->
</div>
</div>
<div>
<div class="box col-md-12">
<div class="box-inner">
<h2>
<i class="glyphicon glyphicon-user"></i>
  Manage Banner Images
</h2>
<div class="box-content">
<div id="DataTables_Table_0_wrapper" class="dataTables_wrapper" role="grid">
<div id="DataTables_Table_0_wrapper" class="dataTables_wrapper no-footer">
<div class="row">
<div class="col-md-6">
<div id="DataTables_Table_0_length" class="dataTables_length">

</div>
</div>
<!--<div class="col-md-6">
<div id="DataTables_Table_0_filter" class="dataTables_filter">
            <form action="" method = "post">
                <label>
                    Search:
                    <input class="" name="search" type="text" placeholder="" aria-controls="DataTables_Table_0">
                    <input type="submit" name="submit" value="Search" class="btn btn-primary btn-sm" style="height:27px;">
                </label>
            </form>
</div>
</div>-->
</div>
<?php if($this->input->post('search')){ ?>
        <table id="DataTables_Table_0" class="table table-striped table-bordered bootstrap-datatable datatable responsive dataTable no-footer" role="grid" aria-describedby="DataTables_Table_0_info">
            <thead>
                <tr role="row">
                    <th class="sorting_asc" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" style="width: 41px;" aria-sort="ascending" aria-label="S.No.: activate to sort column descending">S.No.</th>
                    <th class="sorting" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" style="width: 127px;" aria-label="Name: activate to sort column ascending">Image</th>

                    <th class="sorting" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" style="width: 150px;" aria-label="Actions: activate to sort column ascending">Actions</th>
                </tr>
            </thead>
        <?php
        foreach($banner as $row1){
        if($this->input->post('search')){ ?>
        <tbody>
            <tr class="odd" role="row">
                <td class="sorting_1"><?php echo $row1['id'] ?></td>
                <td class="center">
                    <img src="<?php echo base_url().'images/'.$row1['image']; ?>" width="200" height="80"/>
        
                </td>
               
                <td class="center">
                
                <a class="btn btn-danger btn-sm" title="Delete Cahnnel" href="<?php echo site_url('admin/admin/delete_banner_image/'.$row1['id']) ?>">
                <i class="glyphicon glyphicon-trash icon-white"></i>
                Delete
                </a>
                </td>
            </tr>
        </tbody>
        <?php } }?>
        </table>
<?php } else {  ?>
    <table id="DataTables_Table_0" class="table table-striped table-bordered bootstrap-datatable datatable responsive dataTable no-footer" role="grid" aria-describedby="DataTables_Table_0_info">
        <thead>
        <tr role="row">
        <th class="sorting_asc" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" style="width: 41px;" aria-sort="ascending" aria-label="S.No.: activate to sort column descending">S.No.</th>
        <th class="sorting" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" style="width: 127px;" aria-label="Name: activate to sort column ascending">Image</th>

       
        <th class="sorting" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" style="width: 150px;" aria-label="Actions: activate to sort column ascending">Actions</th>
        </tr>
        </thead>
    <?php foreach($banner as $row) { ?>
    <tbody>
    <tr class="odd" role="row">
        <td class="sorting_1"><?php echo $row['id'] ?></td>
        <td class="sorting_1">
        <img src="<?php echo base_url().'images/'.$row['image']; ?>" width="200" height="80"/>
        

        </td>
        
        <td class="center">
        <a class="btn btn-danger btn-sm" title="Delete Cahnnel" href="<?php echo site_url('admin/admin/delete_banner_image/'.$row['id']) ?>" onclick="return confirm('Are You Sure ! You Want To Delete This Record');">
        <i class="glyphicon glyphicon-trash icon-white"></i>
        Delete
        </a>
        </td>
    </tr>
    </tbody>
    <?php } ?>
    </table>
<?php } ?>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
<script>
    $(document).ready(function(){
        setTimeout(function(){
            $('#success').hide();
        }, 3000);

        setTimeout(function(){
            $('#failure').hide();
        }, 3000);

        setTimeout(function(){
            $('#file_error').hide();
        }, 3000);
    });
</script>
