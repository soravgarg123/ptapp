     <aside class="right-side">
         
                <!-- Main content -->
                
                   <div id="content" class="col-lg-10 col-sm-10" style="margin-top:1%;">
                   <!-- <div class="box-header well"></div> -->
                  <form method="post" action="" enctype="multipart/form-data">
                  <?php  if($this->session->flashdata('error') !='' ||  null !== validation_errors()) { ?>
                   <div class="form-group has-error"><label class="control-label" for="inputError"> <?php echo $this->session->flashdata('error');   echo ' '.validation_errors(); ?></label></div>
                    <?php } ?>
                  <?php  if($this->session->flashdata('success') !='') { ?>
                   <div class="form-group has-success"><label class="control-label" for="inputSuccess"> <?php echo $this->session->flashdata('success');  ?></label></div>
                    <?php } ?>
                       
                            <div class="form-group">
                              <label for="first_name">heading</label>
                               <textarea class="form-control" name="heading" rows="8">
                             <?php if(isset($edit_why_us)) {  echo $edit_why_us['heading']; }else{ echo set_value('heading'); } ?>
                             </textarea>
                                 <!-- <input class="form-control" type="text" value="<?php if(isset($edit_why_us)) {  echo $edit_why_us['heading']; }else{ echo set_value('heading'); } ?>" name="heading" required="required"> -->
                              </div>
                            <div class="form-group">
                              <label for="first_name">Description</label>
                             <textarea class="form-control" name="text" rows="8">
                             <?php if(isset($edit_why_us)) {  echo $edit_why_us['text']; } ?>
                             </textarea>
                             
                              </div>
                            
                              <div class="form-group">
                              <label for="first_name">Image </label></br>
                              <?php if(isset($edit_why_us))
                                { ?>
                                <input type="hidden" name="image_name" value="<?php echo $edit_why_us['image']; ?>"/>
                               <?php }
                               ?>
                                
                              <input type="file" class="form-control" name="image" id="file" size="20" accept="image/*" />
                              </br></br>
                              <img src="" width="88" height="71" id="img"/>
                            </div>
                            <input class="btn btn-primary" type="submit" value="Save" name="submit" >
                       
                      </form>
                       </div>
                   </div>
                    <!-- top row -->
                    
                    <!-- /.row -->

                    <!-- Main row -->
                    <div class="row">
                        <!-- Left col -->
                        <!-- /.Left col -->
                        <!-- right col (We are only adding the ID to make the widgets sortable)-->

                    </div><!-- /.row (main row) -->

                </section><!-- /.content -->
            </aside><!-- /.right-side --> 
        </div><!-- ./wrapper -->

        <!-- add new calendar event modal -->


        <!-- jQuery 2.0.2 -->
        
    </body>
</html>
<script>
   function showImage(src,target) { 
          var fr=new FileReader();
          // when image is loaded, set the src of the image where you want to display it
          fr.onload = function(e) { target.src = this.result; };
          src.addEventListener("change",function() {
            // fill fr with image data    
            fr.readAsDataURL(src.files[0]);
          });
}

var src = document.getElementById("file");
var target = document.getElementById("img");
showImage(src,target);
</script>
<script src="//tinymce.cachefly.net/4.1/tinymce.min.js"></script>
<script type="text/javascript">
       tinymce.init({
    selector: "textarea",
    theme: "modern",
    font_size_classes : "fontSize1, fontSize2, fontSize3, fontSize4, fontSize5, fontSize6",
    plugins: [
         "advlist autolink link image lists charmap print preview hr anchor pagebreak spellchecker",
         "searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking",
         "save table contextmenu directionality emoticons template paste textcolor"
   ],

   toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | print preview media fullpage | forecolor backcolor emoticons | sizeselect | fontselect | fontsize | fontsizeselect", 
   style_formats: [
        {title: 'Bold text', inline: 'b'},
        {title: 'Red text', inline: 'span', styles: {color: '#ff0000'}},
        {title: 'Red header', block: 'h1', styles: {color: '#ff0000'}},
        {title: 'Example 1', inline: 'span', classes: 'example1'},
        {title: 'Example 2', inline: 'span', classes: 'example2'},
        {title: 'Table styles'},
        {title: 'Table row 1', selector: 'tr', classes: 'tablerow1'}
    ]
 });
</script>