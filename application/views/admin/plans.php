 <aside class="right-side">
                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1>
                        <form action="#" method="get" class="sidebar-form" style="width:0%">
                        <div>
                            <input type="hidden" name="q"/>
                            <span>
                               
                            </span>
                        </div>
                    </form>
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                        <li class="active">Manage Plans</li>
                    </ol>
                </section>

         <div style="    margin-right: 2%;margin-bottom: 1%;float: right;margin-top:1%;">
                    <a class="btn btn-primary" href="<?php echo site_url('admin/admin/add_services'); ?>"><i class="glyphicon glyphicon-plus"></i> Add New Service</a>
                </div>
                <div>
<div class="col-md-4">
</div>
</div>
<div>
<div class="box col-md-12">
<div class="box-inner">
<h2>
<i class="glyphicon glyphicon-user"></i>
  Manage Plans
</h2>
<div class="box-content">
<div id="DataTables_Table_0_wrapper" class="dataTables_wrapper" role="grid">
<div id="DataTables_Table_0_wrapper" class="dataTables_wrapper no-footer">
<div class="row">
<div class="col-md-6">
<div id="DataTables_Table_0_length" class="dataTables_length">

</div>
</div>
<!--<div class="col-md-6">
<div id="DataTables_Table_0_filter" class="dataTables_filter">
<form action="" method = "post">
<label>
Search:
<input class="" name="search" type="text" placeholder="" aria-controls="DataTables_Table_0">
<input type="submit" name="submit" value="Search" class="btn btn-primary btn-sm" style="height:27px;">
</label>
</form>
</div>
</div>-->
</div>
<?php if($this->input->post('search')){ ?>
<table id="DataTables_Table_0" class="table table-striped table-bordered bootstrap-datatable datatable responsive dataTable no-footer" role="grid" aria-describedby="DataTables_Table_0_info">
<thead>
<tr role="row">
<th class="sorting_asc" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" style="width: 41px;" aria-sort="ascending" aria-label="S.No.: activate to sort column descending">S.No.</th>
<th class="sorting" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" style="width: 127px;" aria-label="Name: activate to sort column ascending">Title</th>
<th class="sorting" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" style="width: 127px;" aria-label="Name: activate to sort column ascending">Price</th>
<th class="sorting" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" style="width: 127px;" aria-label="Name: activate to sort column ascending">Time</th>

<th class="sorting" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" style="width: 127px;" aria-label="Name: activate to sort column ascending">Decription</th>
--->
<th class="sorting" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" style="width: 127px;" aria-label="Name: activate to sort column ascending">Image</th>


<th class="sorting" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" style="width: 150px;" aria-label="Actions: activate to sort column ascending">Actions</th>
</tr>
</thead>
<?php
foreach($plans as $row1){
if($this->input->post('search')){ ?>
<tbody>
<tr class="odd" role="row">
<td class="sorting_1"><?php echo $row1['id'] ?></td>
<td class="center"><?php echo $row1['plan_title'] ?></td>
<td class="center"><?php echo $row1['price'] ?></td>
<td class="center"><?php echo $row1['time'] ?></td>
<td class="center"><?php echo $row1['description'] ?></td>
<td class="center"><?php echo $row1['image'] ?></td>



<td class="center">
<a class="btn btn-primary btn-sm" title="Edit Cahnnel" href="<?php echo site_url('admin/admin/add_plans/'.$row1['id']) ?>">
<i class="glyphicon glyphicon-edit icon-white"></i>
Edit
</a>--->
<a class="btn btn-danger btn-sm" title="Delete Cahnnel" href="<?php echo site_url('admin/admin/plan_delete/'.$row1['id']) ?>">
<i class="glyphicon glyphicon-trash icon-white"></i>
Delete
</a>
</td>
</tr>
</tbody>
<?php } }?>
</table>
<?php } else { ?>
<table id="DataTables_Table_0" class="table table-striped table-bordered bootstrap-datatable datatable responsive dataTable no-footer" role="grid" aria-describedby="DataTables_Table_0_info">
<thead>
<tr role="row">
<th class="sorting_asc" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" style="width: 41px;" aria-sort="ascending" aria-label="S.No.: activate to sort column descending">S.No.</th>

<th class="sorting" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" style="width: 127px;" aria-label="Name: activate to sort column ascending">Title</th>

<th class="sorting" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" style="width: 127px;" aria-label="Name: activate to sort column ascending">Price</th>
<th class="sorting" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" style="width: 127px;" aria-label="Name: activate to sort column ascending">Time</th>

<th class="sorting" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" style="width: 127px;" aria-label="Name: activate to sort column ascending">Description</th>

<th class="sorting" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" style="width: 127px;" aria-label="Name: activate to sort column ascending">Image</th>

<th class="sorting" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" style="width: 150px;" aria-label="Actions: activate to sort column ascending">Actions</th>
</tr>
</thead>
<?php foreach ($plans as $row) { ?>
<tbody>
<tr class="odd" role="row">
<td class="sorting_1"><?php echo $row['id'] ?></td>
<td class="center"><?php echo $row['plan_title'] ?></td>
<td class="center"><?php echo $row['price'] ?></td>
<td class="center"><?php echo $row['time'] ?></td>
<td class="center"><?php echo $row['description'] ?></td>
<td class="center"><img src= "<?php echo base_url().'/upload/'. $row['image'] ?>" width='100'/></td>


<td class="center">

<a class="btn btn-primary btn-sm" title="Edit Cahnnel" href="<?php echo site_url('admin/admin/add_plans/'.$row['id']) ?>">
<i class="glyphicon glyphicon-edit icon-white"></i>
Edit
</a>
<a class="btn btn-danger btn-sm" title="Delete Cahnnel" href="<?php echo site_url('admin/admin/plan_delete/'.$row['id']) ?>">
<i class="glyphicon glyphicon-trash icon-white"></i>
Delete
</a>
</td>
</tr>
</tbody>
<?php } ?>
</table>
<?php } ?>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
