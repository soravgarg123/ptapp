 <aside class="right-side">
  <?php  if($this->session->flashdata('error') !='' ||  null !== validation_errors()) { ?>
                   <div class="form-group has-error"><label class="control-label" for="inputError"> <?php echo $this->session->flashdata('error');   echo ' '.validation_errors(); ?></label></div>
                    <?php } ?>
                  <?php  if($this->session->flashdata('success') !='') { ?>
                   <div class="form-group has-success"><label class="control-label" for="inputSuccess"> <?php echo $this->session->flashdata('success');  ?></label></div>
                    <?php } ?>
                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1>
                        <form action="#" method="get" class="sidebar-form" style="width:0%">
                        <div>
                            <input type="hidden" name="q"/>
                            <span>
                               
                            </span>
                        </div>
                    </form>
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                        <li class="active">Article/blog</li>
                    </ol>
                </section>

         <div style="    margin-right: 2%;margin-bottom: 1%;float: right;margin-top:1%;">
                    <a class="btn btn-primary" href="<?php echo site_url('admin/admin/add_article/add'); ?>"><i class="glyphicon glyphicon-plus"></i> Add New Article</a>
                </div>
                <div>
<div class="col-md-4">
</div>
</div>
<div>
<div class="box col-md-12">
<div class="box-inner">
<h2>
<i class="glyphicon glyphicon-user"></i>
  Article/blog
</h2>
<div class="box-content">
<div id="DataTables_Table_0_wrapper" class="dataTables_wrapper" role="grid">
<div id="DataTables_Table_0_wrapper" class="dataTables_wrapper no-footer">
<div class="row">
<div class="col-md-6">
<div id="DataTables_Table_0_length" class="dataTables_length">

</div>
</div>
</div>
<?php if($this->input->post('search')){ ?>
<table id="DataTables_Table_0" class="table table-striped table-bordered bootstrap-datatable datatable responsive dataTable no-footer" role="grid" aria-describedby="DataTables_Table_0_info">
<thead>
<tr role="row">
<th class="sorting_asc" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" style="width: 41px;" aria-sort="ascending" aria-label="S.No.: activate to sort column descending">S.No.</th>
<th class="sorting" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" style="width: 127px;" aria-label="Name: activate to sort column ascending">Date</th>
<th class="sorting" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" style="width: 127px;" aria-label="Name: activate to sort column ascending">Title</th>
<th class="sorting" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" style="width: 127px;" aria-label="Name: activate to sort column ascending">Article</th>
<th class="sorting" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" style="width: 127px;" aria-label="Name: activate to sort column ascending">Comment</th>
<th class="sorting" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" style="width: 127px;" aria-label="Name: activate to sort column ascending">Like</th>
<th class="sorting" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" style="width: 150px;" aria-label="Actions: activate to sort column ascending">Actions</th>
</tr>
</thead>
<?php
$i =0;
foreach($article as $row1){
    $i++;
if($this->input->post('search')){ ?>
<tbody>
<tr class="odd" role="row">
<td class="sorting_1"><?php echo $i; ?></td>
<td class="center"><?php echo $row1['date_added'] ?></td>
<td class="center"><?php echo $row1['title'] ?></td>
<td class="center"><?php echo $row1['article'] ?></td>
<td class="center"><a class="btn btn-primary btn-sm" title="Comment Cahnnel" href="<?php echo site_url('admin/admin/comment/'.$row1['id']) ?>">
<i class="fa fa-comment icon-white"></i>
Comment
</a></td>
<td class="center"><a class="btn btn-primary btn-sm" title="Like Cahnnel"  href="<?php echo site_url('admin/admin/like/'.$row1['id']) ?>">
<i class=" fa fa-thumbs-o-up icon-white"></i>
Like
</a></td>
<td class="center">
<a class="btn btn-primary btn-sm" title="Edit Cahnnel" href="<?php echo site_url('admin/admin/add_article/'.$row1['id']) ?>">
<i class="glyphicon glyphicon-edit icon-white"></i>
Edit
</a>
<a class="btn btn-danger btn-sm" title="Delete Cahnnel" href="<?php echo site_url('admin/admin/delete_article/'.$row1['id']) ?>">
<i class="glyphicon glyphicon-trash icon-white"></i>
Delete
</a>
</td>
</tr>
</tbody>
<?php } }?>
</table>
<?php } else { ?>
<table id="DataTables_Table_0" class="table table-striped table-bordered bootstrap-datatable datatable responsive dataTable no-footer" role="grid" aria-describedby="DataTables_Table_0_info">
<thead>
<tr role="row">
<th class="sorting_asc" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" style="width: 41px;" aria-sort="ascending" aria-label="S.No.: activate to sort column descending">S.No.</th>
<th class="sorting" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" style="width: 127px;" aria-label="Name: activate to sort column ascending">Date</th>
<th class="sorting" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" style="width: 127px;" aria-label="Name: activate to sort column ascending">Title</th>
<th class="sorting" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" style="width: 127px;" aria-label="Name: activate to sort column ascending">Article</th>
<th class="sorting" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" style="width: 127px;" aria-label="Name: activate to sort column ascending">Comment</th>
<th class="sorting" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" style="width: 127px;" aria-label="Name: activate to sort column ascending">Like</th>
<th class="sorting" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" style="width: 150px;" aria-label="Actions: activate to sort column ascending">Actions</th>
</tr>
</thead>
<?php
$i =0; 
foreach ($article as $row) { 
   $i++; ?>
<tbody>
<tr class="odd" role="row">
<td class="sorting_1"><?php echo $i; ?></td>
<td class="center"><?php echo $row['date_added'] ?></td>
<td class="center"><?php echo $row['title'] ?></td>
<td class="center"><?php echo $row['article'] ?></td>
<td class="center"><a class="btn btn-primary btn-sm" title="Comment Cahnnel" href="<?php echo site_url('admin/admin/comment/'.$row['id']) ?>">
<i class="fa fa-comment icon-white"></i>
Comment
</a></td>
<td class="center"><a class="btn btn-primary btn-sm" title="Like Cahnnel"  href="<?php echo site_url('admin/admin/like/'.$row['id']) ?>">
<i class="fa fa-thumbs-o-up icon-white"></i>
Like
</a></td>
<td class="center">
<a class="btn btn-primary btn-sm" title="Edit Cahnnel" href="<?php echo site_url('admin/admin/add_article/'.$row['id']) ?>">
<i class="glyphicon glyphicon-edit icon-white"></i>
Edit
</a>
<a class="btn btn-danger btn-sm" title="Delete Cahnnel" href="<?php echo site_url('admin/admin/delete_article/'.$row['id']) ?>">
<i class="glyphicon glyphicon-trash icon-white"></i>
Delete
</a>
</td>
</tr>
</tbody>
<?php } ?>
</table>
<?php } ?>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
