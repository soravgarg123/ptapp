
     <aside class="right-side">
                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1>
                        <form action="#" method="get" class="sidebar-form" style="width:70%">
                        <div class="input-group" style="width:100%">
                            <input type="text" name="q" class="form-control" placeholder="Search..."/>
                            <span class="input-group-btn">
                                <button type='submit' name='seach' id='search-btn' class="btn btn-flat"><i class="fa fa-search"></i></button>
                            </span>
                        </div>
                    </form>
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                        <li class="active">Manage Users</li>
                    </ol>
                </section>

                <!-- Main content -->
                
                   <div id="content" class="col-lg-10 col-sm-10" style="margin-top:1%;">
                   <!-- <div class="box-header well"></div> -->
                   <?php echo form_open_multipart('users/add_user');
                  ?>
                  <?php  if($this->session->flashdata('error') !='' ||  null !== validation_errors()) { ?>
                   <div class="form-group has-error"><label class="control-label" for="inputError"> <?php echo $this->session->flashdata('error');   echo ' '.validation_errors(); ?></label></div>
                    <?php } ?>
                  <?php  if($this->session->flashdata('success') !='') { ?>
                   <div class="form-group has-success"><label class="control-label" for="inputSuccess"> <?php echo $this->session->flashdata('success');  ?></label></div>
                    <?php } ?>
                       
                            <div class="form-group">
                              <label for="first_name">Fullname</label>
                              <input class="form-control" type="hidden" value="<?php if(isset($id)) {  echo $id; } ?>" name="id" >
                                <input class="form-control" type="text" placeholder="Full name"  value="<?php if(isset($fullname)) {  echo $fullname; }else{ echo set_value('fullname'); }  ?>" name="fullname" >
                              </div>
                            
                            <div class="form-group">
                              <label for="first_name">Email</label>
                              <input class="form-control" type="text" value="<?php if(isset($email)) {  echo $email; }else{ echo set_value('email'); } ?>" name="email" >
                               
                              </div>
                            <div class="form-group">
                              <label for="first_name">Mobile</label>
                              <input class="form-control" type="text"  <?php if(isset($mobile)) {  echo 'readonly'; } ?> value="<?php if(isset($mobile)) {  echo $mobile; }else{ echo set_value('mobile'); } ?>" name="mobile" >
                          
                              </div>
                              
                            <div class="form-group">
                              <label for="first_name">Password</label>
                              <?php if(isset($mobile)) { echo "Leave password blank if you dont want to update. "; } ?>
                              <input class="form-control" type="password" value="" name="password" >
                                
                              </div>
                            <div class="form-group">
								
                              <label for="first_name">Confirm Password</label>
                             <?php if(isset($mobile)) { echo "Leave confirm password blank if you dont want to update. "; } ?>
                              <input class="form-control" type="password" value="" name="confirm_password" >
                                
                              </div>
                            
                             
                            <input class="btn btn-primary" type="submit" value="Save" name="submit" >
                       
                       <?php echo form_close(); ?>
                       </div>
                   </div>
                    <!-- top row -->
                    
                    <!-- /.row -->

                    <!-- Main row -->
                    <div class="row">
                        <!-- Left col -->
                        <!-- /.Left col -->
                        <!-- right col (We are only adding the ID to make the widgets sortable)-->

                    </div><!-- /.row (main row) -->

                </section><!-- /.content -->
            </aside><!-- /.right-side --> 
        </div><!-- ./wrapper -->

        <!-- add new calendar event modal -->


        <!-- jQuery 2.0.2 -->
        
    </body>
</html>
