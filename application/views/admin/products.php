 <aside class="right-side">
                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1>
                        <form action="#" method="get" class="sidebar-form" style="width:0%">
                        <div>
                            <input type="hidden" name="q"/>
                            <span>
                               
                            </span>
                        </div>
                    </form>
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                        <li class="active">Manage Clients</li>
                    </ol>
                </section>

         


<table id="DataTables_Table_0" class="table table-striped table-bordered bootstrap-datatable datatable responsive dataTable no-footer" role="grid" aria-describedby="DataTables_Table_0_info">

<table id="DataTables_Table_0" class="table table-striped table-bordered bootstrap-datatable datatable responsive dataTable no-footer" role="grid" aria-describedby="DataTables_Table_0_info">
<thead>
<tr role="row">
<th class="sorting_asc" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" style="width: 41px;" aria-sort="ascending" aria-label="S.No.: activate to sort column descending">S.No.</th>
<th class="sorting" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" style="width: 127px;" aria-label="Name: activate to sort column ascending">Product</th>

<th class="sorting" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" style="width: 127px;" aria-label="Name: activate to sort column ascending">Brand</th>

<th class="sorting" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" style="width: 167px;" aria-label="Name: activate to sort column ascending">Category > Sub category</th>

<th class="sorting" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" style="width: 127px;" aria-label="Name: activate to sort column ascending">Image</th>

<th class="sorting" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" style="width: 50px;" aria-label="Name: activate to sort column ascending">Price</th>

<th class="sorting" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" style="width: 150px;" aria-label="Actions: activate to sort column ascending">Actions</th>
</tr>
</thead>
<tbody>
<?php $i = 1; foreach ($product_list as $row) {  ?>

<tr class="odd" role="row">
<td class="sorting_1"><?php echo $i++; ?></td>

<td class="center"><?php echo $row->title; ?></td>
<td class="center"><?php echo $row->brand; ?></td>
<?php  $catar=explode('>',$row->product_category_path); if(trim($catar[0])==""){ $catar[0]="Gear";} ?>
<td class="center"><?php echo $catar[0]." > ".$catar[1]; ?></td>
<td class="center"><img style="height:50px;width:50px" src="<?php echo $row->image;?>"></td>
<td class="center"><?php echo $row->price+$row->adon_price; ?></td>


<td class="center">

<a class="btn btn-primary btn-sm editproduct_price" title="Edit Cahnnel" href="javascript:" price="<?php echo $row->price; ?>" aprice="<?php echo $row->adon_price; ?>" pid="<?php echo $row->product_id;?>">
<i class="glyphicon glyphicon-edit icon-white"></i>
Edit Price
</a>
<a class="btn btn-danger btn-sm" title="Delete Cahnnel" href="<?php echo site_url('admin/admin/clients_delete/'.$row->product_id) ?>">
<i class="glyphicon glyphicon-trash icon-white"></i>
Delete
</a>
</td>
</tr>
</tbody>
<?php } ?>
</table>

</div>
</div>
</div>
</div>
</div>
</div>
</div>


<script type="text/javascript">
       $(document).ready(function(){
     
          $('.editproduct_price').click(function(){

            var price=$(this).attr('price');
            var aprice=$(this).attr('aprice');
            var pid=$(this).attr('pid');

            $('.product_price').html(price);
            $('#product_adon_price').val(aprice);
            $('#product_id').val(pid);


                $('#edit_price_modal').modal({
                 show: 'true'
                 }); 
                $('#product_adon_price').click();
            }); 
        });
     
</script>


 <div class="modal fade bs-example-modal-sm" id="edit_price_modal" tabindex="-1" role="dialog" aria-labelledby="edit_price_modal">
          <div class="modal-dialog modal-sm" role="document">
            <div class="modal-content custom_modal">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Edit Price</h4>
              </div>

              <form action="<?php echo base_url('admin/admin/update_product_price');?>" method="post">
                  <div class="modal-body">
                      <input type="hidden" name="product_id" id="product_id">
                        <h2 class="text-center" class="product_actual_price"> <i class="fa fa-usd" aria-hidden="true"></i><span class="product_price">12</span></h2>

                   <div class="row">
                      <div class="col-md-6">
                      <h4 class="text-center product_actual_price">Adon Price: </h4>
                      </div>
                      <div class="col-md-6">
                       <input type="text" required class="form-control site_plans" style="border:none !important; margin-left:-30px;font-size:20px;" name="product_adon_price" id="product_adon_price" placeholder="Adon Price">
                      </div>
                   </div>
                        
                           
                        
                      
                  </div>
                  <div class="modal-body">
                         <input type="submit" class="btn btn-lg orange_btn btn-block" value="Update" id="update-roduct-price">
                  </div>

              </form>
              
            </div>
          </div>
        </div>

