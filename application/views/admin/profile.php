
     <aside class="right-side">
                <!-- Content Header (Page header) -->
                        <!-- Main content -->
                
                   <div id="content" class="col-lg-10 col-sm-10" style="margin-top:1%;">
                   <!-- <div class="box-header well"></div> -->
                   <?php echo form_open_multipart('admin/admin/profile');
                  ?>
                  <?php  if($this->session->flashdata('error') ||  null !== validation_errors()) { ?>
                   <div class="form-group has-error"><label class="control-label" for="inputError"> <?php echo $this->session->flashdata('error');   echo ' '.validation_errors(); ?></label></div>
                    <?php } ?>
                  <?php  if($this->session->flashdata('success')) { ?>
                   <div class="form-group has-success"><label class="control-label" for="inputSuccess"> <?php echo $this->session->flashdata('success');  ?></label></div>
                    <?php } ?>
                       
                            <div class="form-group">
                              <label for="first_name">Fullname</label>
                              <input class="form-control" type="hidden" value="<?php if(isset($user_id)) {  echo $user_id; } ?>" name="id" >
                                <input class="form-control" type="text" placeholder="Full name"  value="<?php if(isset($fullname)) {  echo $fullname; }else{ echo set_value('fullname'); }  ?>" name="fullname" >
                              </div>
                            
                            <div class="form-group">
                              <label for="first_name">Email</label>
                              <input class="form-control" type="text" <?php if(isset($email)) {  echo 'readonly'; } ?> value="<?php if(isset($email)) {  echo $email; }else{ echo set_value('email'); } ?>" name="email" >
                               
                              </div>
                            <div class="form-group">
                              <label for="first_name">Mobile</label>
                              <input class="form-control" type="text"   value="<?php if(isset($mobile)) {  echo $mobile; }else{ echo set_value('mobile'); } ?>" name="mobile" >
                          
                              </div>
                              
                            <div class="form-group">
                              <label for="first_name">Password</label>
                              <?php if(isset($mobile)) { echo "Leave password blank if you dont want to update. "; } ?>
                              <input class="form-control" type="password" value="" name="password" >
                                
                              </div>
                            <div class="form-group">
								
                              <label for="first_name">Confirm Password</label>
                             <?php if(isset($mobile)) { echo "Leave confirm password blank if you dont want to update. "; } ?>
                              <input class="form-control" type="password" value="" name="confirm_password" >
                                
                              </div>
                            
                             
                            <input class="btn btn-primary" type="submit" value="Save" name="submit" >
                       
                       <?php echo form_close(); ?>
                       </div>
                   </div>
                    <!-- top row -->
                    
                    <!-- /.row -->

                    <!-- Main row -->
                    <div class="row">
                        <!-- Left col -->
                        <!-- /.Left col -->
                        <!-- right col (We are only adding the ID to make the widgets sortable)-->

                    </div><!-- /.row (main row) -->

                </section><!-- /.content -->
            </aside><!-- /.right-side --> 
        </div><!-- ./wrapper -->

        <!-- add new calendar event modal -->


        <!-- jQuery 2.0.2 -->
        
    </body>
</html>
