 <aside class="right-side">
                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1>
                        <form action="#" method="get" class="sidebar-form" style="width:0%">
                        <div>
                            <input type="hidden" name="q"/>
                            <span>
                               
                            </span>
                        </div>
                    </form>
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                        <li class="active">Manage Users</li>
                    </ol>
                </section>

         <div style="    margin-right: 2%;margin-bottom: 1%;float: right;margin-top:1%;">
                    <a class="btn btn-primary" href="<?php echo site_url('admin/admin/add_user'); ?>"><i class="glyphicon glyphicon-plus"></i> Add New User</a>
                </div>
                <div>
<div class="col-md-4">
</div>
</div>
<div>
<div class="box col-md-12">
<div class="box-inner">
<h2>
<i class="glyphicon glyphicon-user"></i>
  Manage Users
</h2>
<div class="box-content">
<div id="DataTables_Table_0_wrapper" class="dataTables_wrapper" role="grid">
<div id="DataTables_Table_0_wrapper" class="dataTables_wrapper no-footer">
<div class="row">
<div class="col-md-6">
<div id="DataTables_Table_0_length" class="dataTables_length">

</div>
</div>
<!--<div class="col-md-6">
<div id="DataTables_Table_0_filter" class="dataTables_filter">
<form action="" method = "post">
<label>
Search:
<input class="" name="search" type="text" placeholder="" aria-controls="DataTables_Table_0">
<input type="submit" name="submit" value="Search" class="btn btn-primary btn-sm" style="height:27px;">
</label>
</form>
</div>
</div>-->
</div>
<table id="DataTables_Table_0" class="table table-striped table-bordered bootstrap-datatable datatable responsive dataTable no-footer" role="grid" aria-describedby="DataTables_Table_0_info">
<thead>
<tr role="row">
<th class="sorting_asc" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" style="width: 41px;" aria-sort="ascending" aria-label="S.No.: activate to sort column descending">S.No.</th>
<th class="sorting" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" style="width: 150px;" aria-label="Actions: activate to sort column ascending">Email</th>
<th class="sorting" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" style="width: 127px;" aria-label="Name: activate to sort column ascending">Start Date</th>

<th class="sorting" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" style="width: 127px;" aria-label="Name: activate to sort column ascending">End Date</th>
<th class="sorting" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" style="width: 127px;" aria-label="Name: activate to sort column ascending">Publishe Date</th>

<th class="sorting" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" style="width: 150px;" aria-label="Actions: activate to sort column ascending">Actions</th>
</tr>
</thead>
<?php
if(!empty($users_access)){
    $i=0;
foreach($users_access as $row){
    $i++;?>
<tbody>
<tr class="odd" role="row">
    <td class="sorting_1"><?php echo  $i; ?></td>
    <td class="center"><?php echo $row['email'] ?></td>
    <td class="center"><?php echo $row['start_date'] ?></td>
    <td class="center"><?php echo $row['end_date'] ?></td>
    <td class="center"><?php echo $row['date_added'] ?></td>



    <td class="center">
    <a class="btn btn-primary btn-sm" title="Edit Cahnnel" href="<?php echo site_url('admin/admin/add_user/'.$row['id']) ?>">
    <i class="glyphicon glyphicon-edit icon-white"></i>
    Edit
    </a>
    <a class="btn btn-danger btn-sm" title="Delete Cahnnel" href="<?php echo site_url('admin/admin/user_delete/'.$row['id']) ?>">
    <i class="glyphicon glyphicon-trash icon-white"></i>
    Delete
    </a>
    </td>
</tr>
</tbody>
<?php } }?>
</table>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
