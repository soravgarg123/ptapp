     <aside class="right-side">
         
                <!-- Main content -->
                
                   <div id="content" class="col-lg-10 col-sm-10" style="margin-top:1%;">
                   <!-- <div class="box-header well"></div> -->
                   <form method="post" action="">
                  <?php  if($this->session->flashdata('error') !='' ||  null !== validation_errors()) { ?>
                   <div class="form-group has-error"><label class="control-label" for="inputError"> <?php echo $this->session->flashdata('error');   echo ' '.validation_errors(); ?></label></div>
                    <?php } ?>
                  <?php  if($this->session->flashdata('success') !='') { ?>
                   <div class="form-group has-success"><label class="control-label" for="inputSuccess"> <?php echo $this->session->flashdata('success');  ?></label></div>
                    <?php } ?>
                       
                            <div class="form-group">
                              <label for="first_name">People Type</label>
                                 <select class="form-control" name="people_type">
                                 <option value="">Select People Type</option>
                                 <option value="client" <?php if( isset($get_people_say) && $get_people_say['people_type'] == "client") { echo "selected='selected' "; }else {} ?>>Client</option>
                                 <option value="personal_trainner" <?php if( isset($get_people_say) && $get_people_say['people_type'] == "personal_trainner") { echo "selected='selected' "; }else {} ?>>Personal Trainner</option>
                                 </select> 
                              </div>

                               <div class="form-group">
                              <label for="first_name">Name</label>
                                 <input class="form-control" type="text" value="<?php if(isset($get_people_say)) {  echo $get_people_say['name']; }else{ echo set_value('name'); } ?>" name="name" >
                              </div>


                            <div class="form-group">
                              <label for="first_name">What Say</label>
                             <textarea class="form-control" name="what_say"><?php if(isset($get_people_say)) {  echo $get_people_say['what_say']; } ?>
                             </textarea>
                             
                              </div>
                            
                            <input class="btn btn-primary" type="submit" value="Save" name="submit" >
                       
                       <?php echo form_close(); ?>
                       </div>
                   </div>
                    <!-- top row -->
                    
                    <!-- /.row -->

                    <!-- Main row -->
                    <div class="row">
                        <!-- Left col -->
                        <!-- /.Left col -->
                        <!-- right col (We are only adding the ID to make the widgets sortable)-->

                    </div><!-- /.row (main row) -->

                </section><!-- /.content -->
            </aside><!-- /.right-side --> 
        </div><!-- ./wrapper -->

        <!-- add new calendar event modal -->


        <!-- jQuery 2.0.2 -->
        
    </body>
</html>
<script type="text/javascript">
  $(document).ready(function(){
   
    setTimeout(function(){
      $('.has-success').hide();     
    }, 3000);
  });
</script>