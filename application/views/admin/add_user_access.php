     <aside class="right-side">
         
                <!-- Main content -->
                
                   <div id="content" class="col-lg-10 col-sm-10" style="margin-top:1%;">
                   <!-- <div class="box-header well"></div> -->
                   <form method="post" action="">
                  <?php  if($this->session->flashdata('error') !='' ||  null !== validation_errors()) { ?>
                   <div class="form-group has-error"><label class="control-label" for="inputError"> <?php echo $this->session->flashdata('error');   echo ' '.validation_errors(); ?></label></div>
                    <?php } ?>
                  <?php  if($this->session->flashdata('success') !='') { ?>
                   <div class="form-group has-success"><label class="control-label" for="inputSuccess"> <?php echo $this->session->flashdata('success');  ?></label></div>
                    <?php } ?>
                            <div class="form-group">
                              <label for="first_name">Email</label>
                                 <input class="form-control" type="email" value="<?php if(isset($users_access[0]['email'])) {  echo $users_access[0]['email']; }else{ echo set_value('email'); } ?>" name="email" >
                              </div>

                               <div class="form-group">
                              <label for="first_name">Date Range</label>
                                 <input class="form-control" type="text" value="<?php if(isset($users_access[0]['start_date'])) {  echo $users_access[0]['start_date'].' / '.$users_access[0]['end_date']; }else{ echo set_value('date_range'); } ?>" name="date_range" >
                              </div>
                            
                            <input class="btn btn-primary" type="submit" value="Save" name="submit" >
                       
                       <?php echo form_close(); ?>
                       </div>
                   </div>
                    <!-- top row -->
                    
                    <!-- /.row -->

                    <!-- Main row -->
                    <div class="row">
                        <!-- Left col -->
                        <!-- /.Left col -->
                        <!-- right col (We are only adding the ID to make the widgets sortable)-->

                    </div><!-- /.row (main row) -->

                </section><!-- /.content -->
            </aside><!-- /.right-side --> 
        </div><!-- ./wrapper -->

        <!-- add new calendar event modal -->


        <!-- jQuery 2.0.2 -->
        
    </body>
</html>
<script type="text/javascript">
  $(document).ready(function(){
   
    setTimeout(function(){
      $('.has-success').hide();     
    }, 3000);
$(function() {
    $('input[name="date_range"]').daterangepicker({
        timePicker: false,
        format: 'YYYY-MM-DD',
        separator: ' / ',
        timePickerIncrement: 30,
    });
});
  });
</script>