 <body >
<?php $this->load->view('baneer_section'); ?>
            <div class="container">
                    <div class="profile_header">
                          <div class="col-sm-2">
                            <span class="user_profile_pic">
                            <?php 
                           
                            $img = $client_details[0]['user_pic'];
                            if($img != ""){?>
                            <img class=" img-responsive" src="<?php echo base_url(); ?>trainee/<?php echo $client_details[0]['user_pic'];?>" alt=""/>
                            <?php }else{?>
                            <img class=" img-responsive" src="<?php echo base_url(); ?>trainee/strainer1.jpg" alt=""/>
                              <?php } ?>
                            </span>
                          </div>
                          <div class="col-sm-7">
                            <div class="user_profile_info">
                                <h3><?php echo $client_details[0]['name']; ?></h3>
                                  <p><?php echo $client_details[0]['heading'];?></p> 
                            </div>
                            <?php 
                              $user_type = $this->session->userdata('user_type');                          
                              $client_id = $client_id;
                            ?>
                          </div>
                         
                          <div class="col-sm-3"><div class="add_profile"><a class="btn submit_profile" style="float:left;" id="update" href="<?php echo base_url().'user/edit_profile_trainee/'.$client_id;?>">Edit Profile</a>
                          <!--  <?php if($user_type =='trainner'){ ?><a class="btn btn_delete submit_profile " onClick='javascript:return confirm("Are you sure you want to delete your clients profile? All information will be lost.")' href="<?php echo base_url(); ?>user/delete_profile_trainee/<?php echo $client_id?>" >Delete Profile</a>
                            <?php } ?> -->
                            </div></div> 
                          
                      </div>
              </div>
          </div>
    </header>
    <!--Header sec end-->

      <!-- ///////////////////// start Code for show flash messages /////////////////!-->
  <div class="main_container">
    
      <div class="container">
          <div class="row">
              <div class="col-sm-12">
                <?php if($this->session->flashdata('success'))
                { ?>
                  <script type="text/javascript">
                      var msg = "<?php echo $this->session->flashdata('success'); ?>";
                      notify('success','<i class="fa fa-check"> Success </i>',msg);
                  </script>
               <?php    
                }
                 ?>

                 <?php if($this->session->flashdata('failure'))
                  { ?>
                    <script type="text/javascript">
                      var msg1 = "<?php echo $this->session->flashdata('failure'); ?>";
                      notify('error','<i class="fa fa-times"> Error ! </i>',msg1);                      
                  </script>
                 <?php    
                  }
                 ?>
                
            <div id='view_calendar'></div>                    
                </div>
            </div>
            
        </div>
        
    </div>
<!-- /////////////////////End Code for show flash messages///////////////////// !-->

    <!--Main container sec start-->
    <div class="main_container">
      <div class="container">
            <div class="row">
              <div class="col-sm-3">
                    <div class="trainee_tabs_sect">
                        <h3>Client Profile</h3>
                        <!-- Nav tabs -->
                        
                          <ul class="nav_tabs">
                            <li class="active"><a href="#informaiton" aria-controls="informaiton" role="tab" data-toggle="tab">Client Information</a></li>
                            <?php
                             if($user_type == "trainee"){                           
                            ?>
                            <!-- <li><a href="#biography" aria-controls="biography" role="tab" data-toggle="tab">Add Nutrition</a></li> -->
                            <?php }?>
                             <li onclick="loadDoc()"><a href="#awards" aria-controls="awards" role="tab" data-toggle="tab" >Food Diary</a></li>
                            <?php
                            // if($user_type == "trainee"){                           
                            ?>
                            <!--<li><a href="#accomplishments" aria-controls="accomplishments" role="tab" data-toggle="tab">Add Workout</a></li>-->
                            <?php //}?>
                            <!-- ajay 3 oct -->
                            <li onclick="client_plan()"><a href="#location" aria-controls="location" role="tab" data-toggle="tab">Training Plan</a></li>
                           <!--   <li ><a href="#meal" aria-controls="location" role="tab" data-toggle="tab">Meal Plan</a></li> -->
                             <!-- ajay 3 oct end-->
                             <li onclick="show_image()"><a href="#credentials" aria-controls="credentials" role="tab" data-toggle="tab">Add Progress Photo’s</a></li>
                             <li onclick="other_plan()"><a href="#other" aria-controls="location" role="tab" data-toggle="tab">Other</a></li>
                            
                            <!--<li><a href="#hobby" aria-controls="hobby" role="tab" data-toggle="tab"> View Workout</a></li>-->
                            <!-- <li><a href="#interests" aria-controls="interests" role="tab" data-toggle="tab">interests</a></li> -->
                          </ul>
                    </div>
                </div>
                <div class="col-sm-9">
                  <div class="trainee_tab_content">
                        <!-- Tab panes -->
                          <div class="tab-content">
                            <div role="tabpanel" class="tab-pane active" id="informaiton">
                              <h3 class="trai_title_sect">Client Information</h3>
                                <table class="table table-bordered prifile-input-field client">
                                       <tr>
                                           <th><h3 class="panel-title">Age </h3></th>
                                           <td><?php echo $client_details[0]['age'];?></td>
                                       </tr>
                                       <tr>
                                           <th><h3 class="panel-title">Height</h3></th>
                                           <td><?php echo $client_details[0]['height'];?></td>
                                       </tr>
                                       <tr>
                                           <th><h3 class="panel-title">Width</h3></th>
                                           <td><?php echo $client_details[0]['width'];?></td>
                                       </tr>
                                       <tr>
                                           <th><h3 class="panel-title">Resting Metabolic Rate</h3></th>
                                           <td><?php echo $client_details[0]['rmr'];?></td>
                                       </tr>
                                        <tr>
                                           <th><h3 class="panel-title">Blood Pressure</h3></th>
                                           <td><?php echo $client_details[0]['bp'];?></td>
                                       </tr>
                                       <tr>
                                           <th><h3 class="panel-title">Resting Heart Rate</h3></th>
                                           <td><?php echo $client_details[0]['rhr'];?></td>
                                       </tr>
                                       <tr>
                                           <th><h3 class="panel-title">Lean Body Mass</h3></th>
                                           <td><?php echo $client_details[0]['lbm'];?></td>
                                       </tr>
                                       <tr>
                                           <th><h3 class="panel-title">Calorie Expenditure</h3></th>
                                           <td><?php echo $client_details[0]['ce'];?></td>
                                       </tr>
                                       <tr>
                                           <th><h3 class="panel-title">Daily Macronutrients Intake</h3></th>
                                           <td><?php echo $client_details[0]['dmi'];?></td>
                                       </tr>
                                       <tr>
                                           <th><h3 class="panel-title">Recommended Water Intake</h3></th>
                                           <td><?php echo $client_details[0]['rwi'];?></td>
                                       </tr>
                                       <tr>
                                           <th><h3 class="panel-title">BMR</h3></th>
                                           <td><?php echo $client_details[0]['bmr'];?></td>
                                       </tr>
                                        <tr>
                                           <th><h3 class="panel-title">Goals</h3></th>
                                           <td><?php echo $client_details[0]['goals'];?></td>
                                       </tr>
                                       <tr>
                                           <th><h3 class="panel-title">Notes</h3></th>
                                           <td><?php echo $client_details[0]['notes'];?></td>
                                       </tr>
                               </table>
                               <h3 class="trai_title_sect">Skinfolds</h3>
                               <table class="table table-bordered prifile-input-field client">
                                      <!-- <tr>
                                           <th><h3 class="panel-title">Skinfolds</h3></th>
                                           <td><?php echo $client_details[0]['skinfolds'];?></td>
                                       </tr>-->
                                       <tr>
                                           <th><h3 class="panel-title">Bicep</h3></th>
                                           <td><?php echo $client_details[0]['bicep'];?></td>
                                       </tr>
                                       <tr>
                                           <th><h3 class="panel-title"> Tricep</h3></th>
                                           <td><?php echo $client_details[0]['tricep'];?></td>
                                       </tr>
                                       <tr>
                                           <th><h3 class="panel-title"> Abs</h3></th>
                                           <td><?php echo $client_details[0]['abs'];?></td>
                                       </tr>
                                       <tr>
                                           <th><h3 class="panel-title"> Subscapular</h3></th>
                                           <td><?php echo $client_details[0]['subscapular'];?></td>
                                       </tr>
                                       <tr>
                                           <th><h3 class="panel-title"> Chin</h3></th>
                                           <td><?php echo $client_details[0]['chin'];?></td>
                                       </tr>
                                       <tr>
                                           <th><h3 class="panel-title"> Cheek</h3></th>
                                           <td><?php echo $client_details[0]['cheek'];?></td>
                                       </tr>
                                       <tr>
                                           <th><h3 class="panel-title"> Pectoral</h3></th>
                                           <td><?php echo $client_details[0]['pectoral'];?></td>
                                       </tr>
                                       <tr>
                                           <th><h3 class="panel-title"> Suprailliac</h3></th>
                                           <td><?php echo $client_details[0]['Suprailliac'];?></td>
                                       </tr>
                                       <tr>
                                           <th><h3 class="panel-title"> Mid-axillary</h3></th>
                                           <td><?php echo $client_details[0]['mid-axillary'];?></td>
                                       </tr>
                                       
                                       <tr>
                                           <th><h3 class="panel-title">Umbilical</h3></th>
                                           <td><?php echo $client_details[0]['umbilical'];?></td>
                                       </tr>
                                       <tr>
                                           <th><h3 class="panel-title">Knee</h3></th>
                                           <td><?php echo $client_details[0]['knee'];?></td>
                                       </tr>
                                       <tr>
                                           <th><h3 class="panel-title">Medial-calf</h3></th>
                                           <td><?php echo $client_details[0]['medial-calf'];?></td>
                                       </tr>
                                       <tr>
                                           <th><h3 class="panel-title">Quadriceps</h3></th>
                                           <td><?php echo $client_details[0]['quadriceps'];?></td>
                                       </tr>
                                       <tr>
                                           <th><h3 class="panel-title">Hamstrings</h3></th>
                                           <td><?php echo $client_details[0]['hamstrings'];?></td>
                                       </tr>
                                      
                          </table>
                           <h3 class="trai_title_sect">Tape Measurements</h3>
                               <table class="table table-bordered prifile-input-field client">
                          <tr>
                                           <th><h3 class="panel-title">Arm(Left/Right)</h3></th>
                                           <td><?php echo $client_details[0]['arm'].'/'.$client_details[0]['arm_right'];?></td>
                                       </tr>
                                       <tr>
                                           <th><h3 class="panel-title">Chest(Left/Right)</h3></th>
                                           <td><?php echo $client_details[0]['chest'].'/'.$client_details[0]['chest_right'];?></td>
                                       </tr>
                                       <tr>
                                           <th><h3 class="panel-title">Waist(Left/Right)</h3></th>
                                           <td><?php echo $client_details[0]['waist'].'/'.$client_details[0]['waist_right'];?></td>
                                       </tr>
                                       <tr>
                                           <th><h3 class="panel-title">Hips(Left/Right)</h3></th>
                                           <td><?php echo $client_details[0]['hips'].'/'.$client_details[0]['hips_right'];?></td>
                                       </tr>
                                       <tr>
                                           <th><h3 class="panel-title">Thigh(Left/Right)</h3></th>
                                           <td><?php echo $client_details[0]['thigh'].'/'.$client_details[0]['thigh_right'];?></td>
                                       </tr>
                                       <tr>
                                           <th><h3 class="panel-title">Calf(Left/Right)</h3></th>
                                           <td><?php echo $client_details[0]['calf'].'/'.$client_details[0]['calf_right'];?></td>
                                       </tr>
                          </table>
                            </div>
                            <div role="tabpanel" class="tab-pane" id="biography">
                             <div id="err_nutrition" style="text-align:center;padding:10px;background:#eee;border: 1px solid #29488F;color:red;display:none;margin:10px;">Data already available for this date</div>
                               <div id="nutrition">Your details save successfully</div>
                              <form method="post" id="nutrition_dairy">
                              <link rel="stylesheet" href="<?php echo base_url(); ?>css/jquery-ui.css">
                                       <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7/jquery.min.js"></script>
                                       <script>
                                        var d = new Date();
          var n = d.getUTCDay(); 
          console.log(n); // get index of current date
          var prev = n;
          console.log(prev); // get total prev dates
          var next = 6-n;
          console.log(next); // get total next date
          var startdate = new Date();
          startdate.setDate(startdate.getDate()-prev);
          var enddate = new Date();
          enddate.setDate(enddate.getDate()+next);
          $(document).ready(function(){
          $('#datepicker1').datepicker({
              autoclose:true,
              format: 'yyyy-mm-dd',
              startDate: startdate,
              endDate : enddate
          });
          });
                                       </script>
                            
                              
                              <h3 class="trai_title_sect">Add Nutrition</h3>
                                <?php $current_date_time = date("Y-m-d h:i:sa");?>
                                <input type="hidden" name="date" id="date" value="<?php echo $current_date_time;?>"> 
                                <input type="hidden" name="user_id" id="user_id" value="<?php echo $client_details[0]['user_id'];?>">
                                <input type="hidden" name="name" id="name" value="<?php echo $client_details[0]['name'];?>">  
                                <div class="form_wrapper">
                                    <div class="form-group">
                                        <label>Food</label>
                                        <input type="text" autofocus placeholder="Food" class="form-control"  id="title" name="title" autocomplete="off">
                                    </div> 
                                    <div class="form-group">
                                        <label>Meal</label>
                                         <select class="form-control" autocomplete="off" name="category" required="">
                                             <option value="">Select Meal</option>
                                             <option value="breakfast">Breakfast</option>
                                             <option value="lunch">Lunch</option>
                                             <option value="dinner">Dinner</option>
                                         </select>
                                    </div>
                                    <div class="form-group">
                                        <label>Calories</label>
                                        <input type="text" autofocus  placeholder="Calories" class="form-control"  id="calory" name="calory" autocomplete="off">
                                    </div> 
                                    <div class="form-group">
                                        <label>Notes</label>
                                        <textarea class="form-control" name="description" id="description" placeholder="Notes"></textarea>
                                    </div>
                                    <div class="form-group">
                                        <label>Date</label>
                                        <input type="text" id="datepicker1" name="datepicker" class="form-control" placeholder="Date">                             
                                    </div>
                                  </div> 
                                  <button id="submit" class="btn submit_btn" type="button">Submit</button>
                                </form>
                            </div>
                            <div role="tabpanel" class="tab-pane" id="awards">
                              <h3 class="trai_title_sect">Food Diary</h3>
                              <script src="http://platform.fatsecret.com/js?key=0ddf8fd14e9f4cfca478f5587d850146&auto_load=true&theme=blue_small"></script>
                              <div id="my_container" class="fatsecret_container"></div>
                                <script>
                                fatsecret.setContainer("my_container");
                                fatsecret.setCanvas("home");
                                </script>
                              <!-- <form id="search_date" method="post">
                            
                                <table class="table prifile-input-field"> -->
                               
                                       <tr>
                                       <link rel="stylesheet" href="<?php echo base_url(); ?>css/jquery-ui.css">
                                       <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7/jquery.min.js"></script>
                                       <script>
                                         $(document).ready(function() {
                                         $( "#datepicker" ).datepicker({dateFormat: 'yy-mm-dd'});
                                          });
                                       </script>
                                        <script>
                                         $(document).ready(function() {
                                         $( "#datepicker2" ).datepicker({dateFormat: 'Y-m-d'});
                                          });
                                       </script>
                                       <script>
                                         $(document).ready(function() {
                                         $( "#datepicker3" ).datepicker({dateFormat: 'Y-m-d'});
                                          });
                                       </script>
                                       <script>
                                         $(document).ready(function() {
                                         $( "#datepicker4" ).datepicker({dateFormat: 'Y-m-d'});
                                          });
                                       </script>
                                       <script>
                                         $(document).ready(function() {
                                         $( "#datepicker5" ).datepicker({dateFormat: 'Y-m-d'});
                                          });
                                       </script>
                                      
                  <?php
                     
                      $cdate = date("Y-m-d");
          $week =  date('W', strtotime($cdate));
          $year =  date('Y', strtotime($cdate));      
          $firstdayofweek = date("Y-m-d", strtotime("{$year}-W{$week}-0"));       
          $lastdayofweek = date("Y-m-d", strtotime("{$year}-W{$week}-6"));
                   ?>
                                    <!-- </table> -->
                                    <input type="hidden" name="start_date" value="<?php echo $firstdayofweek; ?>" id="start_date">
                                    <input type="hidden" name="end_date" value="<?php echo $lastdayofweek;?>" id="end_date">
                                     <input type="hidden" name="user_id" value="<?php echo $client_details[0]['user_id'];?>" id="user_id">
                                
                            </div>
                            <div role="tabpanel" class="tab-pane" id="accomplishments">
                                <form method="post" id="workout-dairy">
                              <div id="workout">Your details save successfully</div>
                              <h3 class="trai_title_sect">Add Workout</h3>
                                <?php $current_date_time = date("Y-m-d h:i:sa");?>
                                <input type="hidden" name="date" id="date" value="<?php echo $current_date_time;?>"> 
                                <input type="hidden" name="user_id" id="user_id" value="<?php echo $client_details[0]['user_id'];?>">
                                <input type="hidden" name="name" id="name" value="<?php echo $client_details[0]['name'];?>">  
                                <div class="form_wrapper">
                                    <div class="form-group">
                                        <label>Weight</label>
                                        <input type="text" autofocus placeholder="Weight" class="form-control"  id="weight" name="weight" autocomplete="off">
                                    </div> 
                                    <div class="form-group">
                                        <label>Sleep(hrs)</label>
                                         <input type="text" autofocus placeholder="Sleep" class="form-control"  id="sleep" name="sleep" autocomplete="off">
                                    </div>
                                    <div class="form-group">
                                        <label>Calories</label>
                                        <input type="text" autofocus  placeholder="Calories" class="form-control"  id="calories" name="calories" autocomplete="off">
                                    </div> 
                                    <div class="form-group">
                                        <label>Notes</label>
                                        <textarea class="form-control" placeholder="Notes" name="notes" id="notes"></textarea>
                                    </div>
                                  </div> 
                                  <button id="workout-submit" class="btn submit_btn" type="button">Submit</button>
                                </form>
                            </div>
                           <!--  ajay 3 oct -->
                            <div role="tabpanel" class="tab-pane" id="location">
                              <h3 class="trai_title_sect">Training Plan</h3>
                              <div style="margin-right: 2%;margin-bottom: 1%;float: right;margin-top:1%;">
                              <?php if($user_type =='trainner'){ ?><a class="btn btn-primary" href="<?php echo base_url();?>user/client_plan/<?php if(isset($client_details[0]['user_id'])){echo $client_details[0]['user_id'];}else{ }?>" >Add Training Plan</a><?php } ?>
                                 </div>
                                 <table class="table table-bordered prifile-input-field" id="client_plan_result"></table>

                                  <input type="hidden" name="plan_start_date" value="<?php echo $firstdayofweek; ?>" id="plan_start_date">
                                    <input type="hidden" name="plan_end_date" value="<?php echo $lastdayofweek;?>" id="plan_end_date">
                                     <input type="hidden" name="plan_user_id" value="<?php echo $client_details[0]['user_id'];?>" id="plan_user_id">                                    
                                   
                                     <button id="trainningplan_previous" class="btn submit_btn" type="button">previous</button>
                                     <button id="trainningplan_next" class="btn submit_btn" type="button">Next</button>
                            </div>

                            <div role="tabpanel" class="tab-pane" id="other">
                            <div role="tabpanel" class="tab-pane bank_details active" id="bank_details">
                                <h3 class="trai_title_sect">Other</h3>
                                                                <ul class="tai_sub_tab" role="tablist">
                                        <li role="presentation" class="active"><a href="#amazon" aria-controls="gallery" role="tab" data-toggle="tab" aria-expanded="true">FMS</a></li>
                                        <li role="presentation" class=""><a href="#paypal" aria-controls="photos" role="tab" data-toggle="tab" aria-expanded="false">Medical Questionnaire</a></li>
                                        <!-- <li role="presentation"><a href="#money_order" aria-controls="gallery" role="tab" data-toggle="tab">Money Order</a></li> -->
                                </ul>
                                <br><br>
                                <div class="tab-content">
                                    <div role="tabpanel" class="tab-pane" id="paypal">
                                        <h3 class="trai_title_sect">Medical Questionnaire</h3>
                                     <div style="margin-right: 2%;margin-bottom: 1%;float: right;margin-top:1%;">
                                      <a href="<?php echo base_url();?>user/client_quetion_add/<?php echo $client_id;?>" class="add-quetion-btn" > <i class="fa fa-plus-circle"></i></a>
                                    </div>
                                 <table class="table table-bordered prifile-input-field" id="other_plan_result"></table>
                                    </div>
                                    <div role="tabpanel" class="tab-pane active" id="amazon">
                                     <h3 class="trai_title_sect">FMS</h3>
                                    <div class="trainee_tabs_sect" >
                <?php if(!empty($fms)){
                            foreach ($fms as $value) {
                             $score =json_decode($value['fms']);
                             if($value['gender'] =='1'){
                              $gender ='Male'; 
                             }else{
                              $gender ='Female';
                             }
                        $fms_val =json_decode($value['fms'],true);
                           ?>
                  <div class="trainee_tab_content" id="client_fms">
                        <!-- Tab panes -->
                          <div class="tab-content">
                          
                            <div role="tabpanel" class="tab-pane active" id="informaiton">
                               <h3 class="trai_title_sect">Details</h3>
                              <div style="margin-right: 2%;margin-bottom: 1%;float: right;margin-top:1%;">
                              <!-- <a href="<?php echo base_url(); ?>user/fms_edit" title="Add FMS" ><i class="fa fa-plus-circle"></i></a> -->
                                <!-- <a href="<?php echo base_url();?>user/fms_edit/<?php echo $value['id'];?>" title="Edit FMS" ><i class="fa fa-edit"></i></i></a> -->
                                <!-- <a onclick='javascript:return confirm("Are you sure to Delete this Record?")' title="Delete FMS" href="<?php echo base_url();?>user/fms_delete/<?php echo $value['id'];?>" ><i class="fa fa-trash-o"></i></a> -->
                              </div>
                                <table class="table table-bordered prifile-input-field client fms_main">
                                <thead><tr><th>Date</th><th><?php ?></th></tr></thead>
                                       <tr>
                                           <th><h3 class="panel-title">Name</h3></th>
                                           <td><?php echo $value['full_name']; ?></td>
                                       </tr>
                                       <tr>
                                           <th><h3 class="panel-title">Age/Gender</h3></th>
                                           <td><?php echo $value['age'].'/'.$gender; ?></td>
                                       </tr>
                                       <tr>
                                           <th><h3 class="panel-title">Height(cm)/Weight(Kg)</h3></th>
                                           <td><?php echo $value['height'].'/'.$value['weight']; ?></td>
                                       </tr>
                               </table>
                               <h3 class="trai_title_sect">THE FUNCTIONAL MOVEMENT SCREEN</h3>
                               <table class="table table-bordered prifile-input-field client fms_table fms_main">
                               <thead><tr><th>TEST</th><th>SCORE</th><th>COMMENTS</th></tr></thead>
                                      <tr>
                                           <th><h3 class="panel-title">DEEP SQUAT</h3></th>
                                           <td><?php echo $fms_val['deep_squat']; ?></td>
                                           <td><?php echo $fms_val['deep_squat_comment']; ?></td>
                                       </tr>
                                       <tr>
                                           <th><h3 class="panel-title"> HURDLE STEP (Left)/(Right)</h3></th>
                                           <td><?php echo $fms_val['hurdle_step_l']; ?>/<?php echo $fms_val['hurdle_step_r']; ?></td>
                                           <td><?php echo $fms_val['hurdle_step_l_comment']; ?>/<?php echo $fms_val['hurdle_step_r_comment']; ?></td>
                                       </tr> 
                                       <!-- <tr>
                                           <th><h3 class="panel-title">(Right)</h3></th>
                                           <td><?php echo $fms_val['hurdle_step_r']; ?></td>
                                           <td><?php echo $fms_val['hurdle_step_r_comment']; ?></td>
                                       </tr> -->
                                       <tr>
                                           <th><h3 class="panel-title">INLINE LUNGE(Left)/(Right)</h3></th>
                                           <td><?php echo $fms_val['inline_lunge_l']; ?>/<?php echo $fms_val['inline_lunge_r']; ?></td>
                                           <td><?php echo $fms_val['inline_lunge_l_comment']; ?>/<?php echo $fms_val['inline_lunge_r_comment']; ?></td>
                                       </tr>
                                       <!-- <tr>
                                           <th><h3 class="panel-title">(Right)</h3></th>
                                           <td><?php echo $fms_val['inline_lunge_r']; ?></td>
                                           <td><?php echo $fms_val['inline_lunge_r_comment']; ?></td>
                                       </tr> -->
                                       <tr>
                                           <th><h3 class="panel-title">SHOULDER MOBILITY(Left)/(Right)</h3></th>
                                           <td><?php echo $fms_val['shoulder_mobility_l']; ?>/<?php echo $fms_val['shoulder_mobility_r']; ?></td>
                                           <td><?php echo $fms_val['shoulder_mobility_l_comment']; ?>/<?php echo $fms_val['shoulder_mobility_r_comment']; ?></td>
                                       </tr>
                                       <!-- <tr>
                                           <th><h3 class="panel-title">(Right)</h3></th>
                                           <td><?php echo $fms_val['shoulder_mobility_r']; ?></td>
                                           <td><?php echo $fms_val['shoulder_mobility_r_comment']; ?></td>
                                       </tr> -->
                                       <tr>
                                           <th><h3 class="panel-title">IMPINGEMENT CLEARING TEST (Left)/(Right)</h3></th>
                                           <td><?php echo $fms_val['impingement_test_l']; ?>/<?php echo $fms_val['impingement_test_r']; ?></td>
                                           <td><?php echo $fms_val['impingement_test_l_comment']; ?>/<?php echo $fms_val['impingement_test_r_comment']; ?></td>
                                       </tr>
                                       <!-- <tr>
                                           <th><h3 class="panel-title">(Right)</h3></th>
                                           <td><?php echo $fms_val['impingement_test_r']; ?></td>
                                           <td><?php echo $fms_val['impingement_test_r_comment']; ?></td>
                                       </tr> -->
                                       <tr>
                                           <th><h3 class="panel-title">ACTIVE STRAIGHT-LEG RAISE (Left)/(Right)</h3></th>
                                           <td><?php echo $fms_val['active_straight_l']; ?>/<?php echo $fms_val['active_straight_r']; ?></td>
                                           <td><?php echo $fms_val['active_straight_l_comment']; ?>/<?php echo $fms_val['active_straight_r_comment']; ?></td>
                                       </tr>
                                       <!-- <tr>
                                           <th><h3 class="panel-title">(Right)</h3></th>
                                           <td><?php echo $fms_val['active_straight_r']; ?></td>
                                           <td><?php echo $fms_val['active_straight_r_comment']; ?></td>
                                       </tr> -->
                                       <tr>
                                           <th><h3 class="panel-title"> TRUNK STABILITY PUSH-UP</h3></th>
                                           <td><?php echo $fms_val['trunk_stablity']; ?></td>
                                           <td><?php echo $fms_val['trunk_stablity_comment']; ?></td>
                                       </tr>
                                       <tr>
                                           <th><h3 class="panel-title"> PRESS-UP CLEARING TEST</h3></th>
                                           <td><?php echo $fms_val['press_up']; ?></td>
                                           <td><?php echo $fms_val['press_up_comment']; ?></td>
                                       </tr>
                                       <tr>
                                           <th><h3 class="panel-title">ROTARY STABILITY(Left)/(Right)</h3></th>
                                           <td><?php echo $fms_val['rotary_stability_l']; ?>/<?php echo $fms_val['rotary_stability_r']; ?></td>
                                           <td><?php echo $fms_val['rotary_stability_l_comment']; ?>/<?php echo $fms_val['rotary_stability_r_comment']; ?></td>
                                       </tr>
                                       <!-- <tr>
                                           <th><h3 class="panel-title">(Right)</h3></th>
                                           <td><?php echo $fms_val['rotary_stability_r']; ?></td>
                                           <td><?php echo $fms_val['rotary_stability_r_comment']; ?></td>
                                       </tr> -->
                                       <tr>
                                           <th><h3 class="panel-title">POSTERIOR ROCKING CLEARING TEST</h3></th>
                                           <td><?php echo $fms_val['posterior']; ?></td>
                                           <td><?php echo $fms_val['posterior_comment']; ?></td>
                                       </tr>
                                       <tr>
                                           <th><h3 class="panel-title">TOTAL:</h3></th>
                                           <td><?php echo $value['total']; ?></td>
                                           <td></td>
                                       </tr>
                          </table>
                            </div>
                          </div>
                          
                          <div class="clearfix"></div>
                    </div>
                    <?php }
                    }else{?>
                    <div class="trainee_tab_content">
                        <!-- Tab panes -->
                          </div>
                          
                          <div class="clearfix"></div>
                        <!-- <a class="btn submit_btn" href="#">Update</a> -->
                    </div>
                    <?php } ?>
                                    </div>
                                </div>
                            </div>
                              <!-- <h3 class="trai_title_sect">Medical Questionnaire</h3>
                               <div style="margin-right: 2%;margin-bottom: 1%;float: right;margin-top:1%;">
                                <a href="<?php echo base_url();?>user/client_quetion_add/<?php echo $client_id;?>" class="btn btn-primary"><i class="glyphicon glyphicon-plus"></i> Add New Quetion</a>
                              </div>
                                 <table class="table table-bordered prifile-input-field" id="other_plan_result"></table> -->

                            </div>

                            <!--  ajay 3 oct end-->
                            <div role="tabpanel" class="tab-pane" id="credentials">
                              <h3 class="trai_title_sect">Add Progress Photo’s</h3>
                              <form method="post" id="add_image">
                              <div id="images">Image Added Successfully</div>
                                <div class="form_wrapper">
                                    <div class="form-group">
                                   
                                         <div class="photo_upload_sect">
                                                  <input type="file" value="" id="user_pic" name="user_pic">   
                                                  <input type="hidden" name="user_id" id="user_id" value="<?php echo $client_details[0]['user_id'];?>">      
                                                    <span class="fa fa-cloud-upload"></span>
                                                    <h5>Upload your Photo</h5>
                                         </div> 
                                         <div id="upload_images">
                                        
                                          
                                         </div>
                                    </div>
                                </div>
                                <!-- <button id="image" class="btn submit_btn pull-right" type="button">Upload</button> -->
                                </form>
                            </div>
                            <div role="tabpanel" class="tab-pane" id="hobby">
                              <h3 class="trai_title_sect">View Workout</h3>
                                <table class="table table-bordered prifile-input-field">
                                       <tr>
                                           <th><h3 class="panel-title">Weight </h3></th>
                                           <th><h3 class="panel-title">sleep</h3></th>
                                           <th><h3 class="panel-title">Calories</h3></th>
                                           <th><h3 class="panel-title">Notes</h3></th>.
                                           <th><h3 class="panel-title">Date</h3></th>
                                       </tr>
                                       <tr>
                                          
                                            <td><?php echo $client_workout[0]['weight'];?></td>
                                            <td><?php echo $client_workout[0]['sleep'];?></td>
                                            <td><?php echo $client_workout[0]['calories'];?></td>
                                            <td><?php echo $client_workout[0]['notes'];?></td>
                                            <td><?php echo $client_workout[0]['date'];?></td>
                                       </tr>
                                       
                                </table>
                            </div>
                            <div role="tabpanel" class="tab-pane" id="interests">
                              <h3 class="trai_title_sect">Interests</h3>
                                <div class="form_wrapper">
                                    <div class="form-group">
                                        <input type="text" class="form-control" value="show meal plan"/>
                                    </div>
                                </div>
                            </div>
 <!-- ///////////////////////////code written by Aditya //////////////////////////// !-->                        
                           <!-- !-->


                            <div role="tabpanel" class="tab-pane" id="meal">
                              <h3 class="trai_title_sect">Meal Plan</h3>
                              <?php if($this->session->userdata('user_type') == "trainner") {?>
                              <input type="button" data-toggle="modal" data-target="#myModal" style="float:right;" class="btn-primary" id="add_meal_plans" value="Add Meal Plans">
                            
                              <?php } ?>
                                 <div class="form_wrapper">
                                    <div class="form-group">

                                        <table id="example" class="table table-striped table-bordered">
                                        <thead>
                                          <tr id="">
                                            <th>Row</th>
                                            <th>Sun</th>
                                            <th>Mon</th>
                                            <th>Tue</th>
                                            <th>Wed</th>
                                            <th>Thu</th>
                                            <th>Fri</th>
                                            <th>Sat</th>
                                             <th>Action</th>
                                          </tr>
                                        </thead>
                                        <tbody>
                                        <?php
                                        if(!empty($meal)) { ?>
                                         <tr id="row1">
                                            <td>Row1</td>
                                            <td colspan="7">  <a href="#" class="meal_plan_text">  </a> </td>
                                            <td> 
                                             </td>
                                          </tr>
                                          <tr id="row2">
                                            <td>Row2</td>
                                            <td colspan="7"> <a href="#" class="meal_plan_text">  </a>  </td>
                                            <td>  
                                             </td>
                                          </tr>
                                          <tr id="row3">
                                            <td>Row3</td>
                                            <td colspan="7">  <a href="#" class="meal_plan_text">  </a> </td>
                                            <td>  
                                             </td>
                                          </tr>
                                          <tr id="row4">
                                            <td>Row4</td>
                                            <td colspan="7"> <a href="#" class="meal_plan_text"> </td>
                                            <td>  
                                             </td>
                                          </tr>
                                          <tr id="row5">
                                            <td>Row5</td>
                                            <td colspan="7"> <a href="#" class="meal_plan_text"> </td>
                                            <td>  
                                             </td>
                                          </tr>
                                          <tr id="row6">
                                            <td>Row6</td>
                                            <td colspan="7">  <a href="#" class="meal_plan_text">  </a> </td>
                                            <td>  
                                             </td>
                                          </tr>
                                          <tr id="row7">
                                            <td>Row7</td>
                                            <td colspan="7"> <a href="#" class="meal_plan_text"></td>
                                            <td>  
                                             </td>
                                          </tr>
                                          <tr id="row8">
                                            <td>Row8</td>
                                            <td colspan="7"> <a href="#" class="meal_plan_text"> </td>
                                            <td>  
                                             </td>
                                          </tr>
                                          <?php }else { ?>
                                          <tr>
                                            <td class="bg-danger" colspan="9">No Record Found</td>
                                          </tr>
                                            <?php } ?>
                                        </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                           <!-- !-->

<!-- ///////////////////start of Add meal plan modal ///////////////////////// !-->
<?php if($this->session->userdata('user_type') == "trainner"){  ?>
      <!-- Book Appoinment Modal Start -->
        <div class="modal fade" id="meal_Modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
          <div class="modal-dialog" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Add Meal</h4>
              </div>
              <div class="modal-body">
                <form method="post" action="" >
                
                <div class="col-md-12">                        
                      <div class="col-md-4">Name</div>
                      <div class="col-md-8">
                         <input type="text" name="name" id="name" val="" class="form-control"/>
                        <span class="error"></span>
                      </div>
                  </div></br></br>

                  <div class="col-md-12">                        
                      <div class="col-md-4"> Client</div>
                      <div class="col-md-8">
                      <?php 
                      $data = array(null =>  'Select Trainee');
                      if(isset($trainee))
                      {
                        foreach($trainee as $a)
                        {
                          $data[$a['user_id']] = $a['name'];
                        }
                        echo form_dropdown('trainee', $data, '', 'class="form-control" id="trainee" required');
                      }
                      ?>
                      </div>
                  </div></br></br>
                  <div class="col-md-12">                        
                      <div class="col-md-4">Category</div>
                      <div class="col-md-8">
                      <?php
                        unset($data);
                        $data=  array(null => 'Select Category');
                        if(isset($category))
                        {
                          foreach($category as $a){
                            $data[$a['id']] = $a['name'];
                          }
                          echo form_dropdown('category', $data, '', 'class="form-control" id="category" required');
                        }
                      ?>  
                      </div> 
                  </div></br></br>
                  <div class="col-md-12">                        
                      <div class="col-md-4">Start Date</div>
                      <div class="col-md-8">
                      <input type="text" id="datepicker2" name="start_date" class="form-control" placeholder="Start Date">
                        <!-- <input type="text"  id="start_date" required="required" name="start_date" class="form-control" /> -->
                      </div>
                  </div></br></br>
                  <div class="col-md-12">                        
                      <div class="col-md-4">End Date</div>
                      <div class="col-md-8">
                      <input type="text" id="datepicker3" name="end_date" class="form-control" placeholder="End Date">
                        <!-- <input type="text"  id="end_date" name="end_date" class="form-control" /> -->
                      </div>
                  </div></br></br>

                    <div class="col-md-12">                        
                      <div class="col-md-4">Row</div>
                      <div class="col-md-8">
                         <select  id="row"  name="row" class="form-control" />
                         <option value="0">Select Row</option>
                         <option value="row1">Row1</option>
                         <option value="row2">Row2</option>
                         <option value="row3">Row3</option>
                         <option value="row4">Row4</option>
                         <option value="row5">Row5</option>
                         <option value="row6">Row6</option>
                         <option value="row7">Row7</option>
                         <option value="row8">Row8</option>
                         </select> 
                        <span class="error"></span>
                      </div>
                  </div></br></br>
               </div></br>
              <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="submit" id="submit-btn" class="btn btn-primary">Submit</button>
              </div>
              </form>
            </div>
          </div>
        </div>
<?php  } ?>
<!-- ///////////////////End of Add meal plan modal/////////////////////////////////////////// !-->
 
<!-- //////////////////////////////end of code//////////////////////////////////  !-->

<!-- ///////////////////start of Edit meal plan modal ///////////////////////// !-->
<?php //if($this->session->userdata('user_type') == "trainner"){  ?>
      <!-- Book Appoinment Modal Start -->
        <div class="modal fade" id="meal_Modal_edit" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
          <div class="modal-dialog" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Edit Meal</h4>
              </div>
              <div class="modal-body">
                <form method="post" action="<?php echo base_url('user/edit_meal_plan'); ?>" >
               <input type="hidden" name="plan_id" id="plan_id"/>
                <div class="col-md-12">                        
                      <div class="col-md-4">Name</div>
                      <div class="col-md-8">
                         <input type="text" name="edit_name" id="edit_name" class="form-control"/>
                        <span class="error"></span>
                      </div>
                  </div></br></br>

                  <div class="col-md-12">                        
                      <div class="col-md-4"> Client</div>
                      <div class="col-md-8">
                      <?php 
                      $data = array(null =>  'Select Trainee');
                      if(isset($trainee_new))
                      {
                      
            
                        foreach($trainee_new as $a)
                        {
                          $data[$a['user_id']] = $a['name'];
                        }
                        echo form_dropdown('edit_trainee', $data, '', 'class="form-control" id="edit_trainee" required');
                      }
                      ?>
                      </div>
                  </div></br></br>
                  <div class="col-md-12">                        
                      <div class="col-md-4">Category</div>
                      <div class="col-md-8">
                      <?php
                        unset($data);
                        $data=  array(null => 'Select Category');
                        if(isset($category))
                        {
                          foreach($category as $a){
                            $data[$a['id']] = $a['name'];
                          }
                          echo form_dropdown('edit_category', $data, '', 'class="form-control" id="edit_category" required');
                        }
                      ?>  
                      </div> 
                  </div></br></br>
                  <div class="col-md-12">                        
                      <div class="col-md-4">Start Date</div>
                      <div class="col-md-8">

                        <!-- <input type="text"  id="edit_start_date" required="required" name="edit_start_date" class="form-control" /> -->
                        <input type="text" id="datepicker4" name="edit_start_date" class="form-control" placeholder="Start Date">
                      </div>
                  </div></br></br>
                  <div class="col-md-12">                        
                      <div class="col-md-4">End Date</div>
                      <div class="col-md-8">
                      <input type="text" id="datepicker5" name="edit_end_date" class="form-control" placeholder="Start Date">
                       <!--  <input type="text"  id="edit_end_date" name="edit_end_date" class="form-control" /> -->
                      </div>
                  </div></br></br>

                    <div class="col-md-12">                        
                      <div class="col-md-4">Row</div>
                      <div class="col-md-8">
                         <select  id="edit_row"  name="edit_row" class="form-control" />
                         <option value="0">Select Row</option>
                         <option value="row1">Row1</option>
                         <option value="row2">Row2</option>
                         <option value="row3">Row3</option>
                         <option value="row4">Row4</option>
                         <option value="row5">Row5</option>
                         <option value="row6">Row6</option>
                         <option value="row7">Row7</option>
                         <option value="row8">Row8</option>
                         </select> 
                        <span class="error"></span>
                      </div>
                  </div></br></br>
               </div></br>
              <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="submit" id="submit-btn" class="btn btn-primary">Submit</button>
              </div>
              </form>
            </div>
          </div>
        </div>
<?php  //} ?>
<!-- ///////////////////End of Edit meal plan modal/////////////////////////////////////////// !-->

                          </div>
                          
                          <div class="clearfix"></div>
                        <!-- <a class="btn submit_btn" href="#">Update</a> -->
                    </div>
                    
                </div>
            </div>
            
        </div>
        
    </div>
    
    <!--Main container sec end-->
    
   
    <div class="clearfix"></div>
    </body>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script type="text/javascript">
    $(document).ready(function(){
      
         $(".tab-pane").on('click','#submit',function() {
         
            var data1 = $('#nutrition_dairy').serialize();
           $.ajax({
             url:"<?php echo base_url() ?>user/add_nutrition",
             type:"post",
             data: data1,
             success: function(response)
             { 
             if(response){
                notify('success','<i class="fa fa-check"> Success </i>','Your details save successfully');
                notify('error','<i class="fa fa-times"> Error ! </i>','Data already available for this date !');                      
                $('#nutrition_dairy')[0].reset();
                setTimeout(function() { $("#nutrition").fadeOut(1500); }, 5000);
             
             
             }
             }
         });
     
         });
      
    });
</script>
<script src="http://code.jquery.com/jquery-1.11.1.min.js"></script>

<script>
jQuery.noConflict();
function loadDoc() {
  var user_id = document.getElementById("user_id").value;
  var name = document.getElementById("name").value;
  var xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function() {
    if (xhttp.readyState == 4 && xhttp.status == 200) {
      document.getElementById("demo").innerHTML = xhttp.responseText;
       $('#next').hide();
   
    }
  }
  xhttp.open("GET", "<?php echo base_url();?>user/search_nutrition_details?user_id="+user_id+"&name="+name, true);
  xhttp.send();
  
}
</script>

<script type="text/javascript">
    $(document).ready(function(){
         $(".tab-pane").on('click','#search',function() {
          var date = $('#datepicker').val();
          var name = $('#name').val();
           $.ajax({
             url:"<?php echo base_url() ?>user/search_date_nutrition_details?date="+date+"&name="+name,
             type:"post",
             data: "date="+date,
             contentType: false,
             processData: false,
             success: function(data)
             {
              $('#demo').html(data);
             }
         });
      });
    });
</script>
<script type="text/javascript">
    $(document).ready(function(){
      
         $(".tab-pane").on('click','#workout-submit',function() {
         
            var data1 = new FormData($('#workout-dairy')[0]);
           $.ajax({
             url:"<?php echo base_url() ?>user/add_workout",
             type:"post",
             data: data1,
             contentType: false,
             processData: false,
             success: function(response)
             {
               notify('success','<i class="fa fa-check"> Success </i>','Your details save successfully');
               $('#workout-dairy')[0].reset();
               setTimeout(function() { $("#workout").fadeOut(1500); }, 5000)
             }
         });
     
         });
      
       /* ------------------------Code Written by Aditya ---------------------
  ------------------------script start for meal plan--------------------------
  */
  $('#add_meal_plans').click(function(){
    $('#meal_Modal').modal('show');
    $('#start_date').datepicker({daysOfWeekDisabled: [0,2,3,4,5,6], format : 'd M yyyy'});
        $('#start_date').blur(function(){
          var date = new Date($('#start_date').val());         
          date.setDate(date.getDate()+7);
          var str = date.toString();
          var a = new Array();
          var a = str.split(" ", 4);
         $('#end_date').val(a[2]+' '+a[1]+ ' '+a[3]);
        });
    
  });
  /*
-------------------------script end for meal plan-------------------------------
  */
/* ------------------------start  script for flash messages-------------------
*/
  setTimeout(function(){
    $('#success').hide();
  }, 3000);

    setTimeout(function(){
    $('#failure').hide();
  }, 3000);
   /*---------------end script for flash messages---------------------*/
   /*
--------------------------start code for meal plan----------------------------------- 
   */
  // alert('<?php echo json_encode("hi"); ?>');
   var meal = '<?php echo json_encode($meal); ?>';
   var obj = JSON.parse(meal);
   $('#trainee_id').val(obj.trainee_id);
   $.each(obj, function(key, value){
    if(value.row == 'row1')
    { 
      //$('#row1 td:not(:first)').append();
      $('#row1 td:eq(1)').append(value.name+'-' + value.cname+'|').addClass('bg-primary');
      $('#row1').find('td:last').html('<input type="button" class="btn-primary edit_plan" id="'+value.id+'" value="Edit">'
       +' <input type="submit" class="btn-danger delete_plan" id="'+value.id+'" value="Delete">');
    }
    else if(value.row == 'row2')
    {

      $('#row2 td:eq(1)').addClass('bg-success').find('a').append(value.name+'-' + value.cname +'|');
      $('#row2').find('td:last').html('<input type="button" class="btn-primary edit_plan" id="'+value.id+'" value="Edit">'
       +' <input type="button" class="btn-danger delete_plan" id="'+value.id+'" value="Delete">');
    } 
    else if(value.row == 'row3')
    {
      $('#row3 td:eq(1)').addClass('bg-info').find('a').append(value.name+'-' + value.cname +'|');
      $('#row3').find('td:last').html('<input type="button" class="btn-primary edit_plan" id="'+value.id+'" value="Edit">'
       +' <input type="button" class="btn-danger delete_plan" id="'+value.id+'" value="Delete">');
    }
    else if(value.row == 'row4')
    {
      $('#row4 td:eq(1)').addClass('bg-warning').find('a').append(value.name+'-' + value.cname +'|');
      $('#row4').find('td:last').html('<input type="button" class="btn-primary edit_plan" id="'+value.id+'" value="Edit">'
       +' <input type="button" class="btn-danger delete_plan" id="'+value.id+'" value="Delete">');
    }
    else if(value.row == 'row5')
    {
      $('#row5 td:eq(1)').addClass('bg-danger').find('a').append(value.name+'-' + value.cname +'|');
      $('#row5').find('td:last').html('<input type="button" class="btn-primary edit_plan" id="'+value.id+'" value="Edit">'
       +' <input type="button" class="btn-danger delete_plan" id="'+value.id+'" value="Delete">');
    }
    else if(value.row == 'row6')
    {
      $('#row6 td:eq(1)').addClass('bg-custom1').find('a').append(value.name+'-' + value.cname +'|');
      $('#row6').find('td:last').html('<input type="button" class="btn-primary edit_plan" id="'+value.id+'" value="Edit">'
       +' <input type="button" class="btn-danger delete_plan" id="'+value.id+'" value="Delete">');
    }
    else if(value.row == 'row7')
    {
      $('#row7 td:eq(1)').addClass('bg-custom2').find('a').append(value.name+'-' + value.cname +'|');
      $('#row7').find('td:last').html('<input type="button" class="btn-primary edit_plan" id="'+value.id+'" value="Edit">'
       +' <input type="button" class="btn-danger delete_plan" id="'+value.id+'" value="Delete">');
    }
    else if(value.row == 'row8')
    {
      $('#row8 td:eq(1)').addClass('bg-custom3').find('a').append(value.name+'-' + value.cname +'|');
      $('#row8').find('td:last').html('<input type="button" class="btn-primary edit_plan" id="'+value.id+'" value="Edit">'
       +' <input type="button" class="btn-danger delete_plan" id="'+value.id+'" value="Delete">');
    } 
   });
   /*
--------------------------end code for meal plan----------------------------------- 
   */
/*---------------------------  end of code---------------------------------------- 
   */ 
   /*
------------------ start code for edit meal plan ----------------------------
   */
   $('.edit_plan').click(function(){
      var row_id = $(this).closest('tr').attr('id');
      var client_id = '<?php echo $client_id; ?>';
      var plan_id = $(this).attr('id');


       $('#datepicker4').datepicker({daysOfWeekDisabled: [0,2,3,4,5,6], format : 'd M yyyy'});
        $('#datepicker4').blur(function(){
          var date = new Date($('#datepicker4').val());
          date.setDate(date.getDate()+7);
          var str = date.toString();
          var a = new Array();
          var a = str.split(" ", 4);
          $('#datepicker5').val(a[2]+' '+a[1]+ ' '+a[3]);
        });

      $.ajax({
        type    : "POST",
        url     : "<?php echo base_url('user/get_meal_by_id'); ?>",
        data    : {client_id : client_id, plan_id : plan_id},
        success : function(data){
          var edit_obj = JSON.parse(data);
           $('#plan_id').val(edit_obj.id);
           $('#edit_name').val(edit_obj.name);
           $('#edit_trainee').val(edit_obj.trainee_id);
           $('#edit_category').val(edit_obj.category_id);
           $('#datepicker4').val(edit_obj.start_date);
           $('#datepicker5').val(edit_obj.end_date);
           $('#edit_row').val(edit_obj.row);
           $('#meal_Modal_edit').modal('show');
        }
      });
   });

   $('.delete_plan').click(function(){
      var plan_id = $(this).attr('id');
      if(confirm("are you sure ?")){
      $.ajax({
        type    : "POST",
        url     : "<?php echo base_url('user/delete_meal_plans'); ?>", 
        data    : {plan_id : plan_id},
        success : function(data){
            alert(data);
            location.reload();
        }
      });
    }
   });


  
   /*
------------------ end code for edit meal plan ----------------------------
   */

    });
</script>

<!-- ajay 3 oct -->
<script>
function client_plan() {

  var user_id = document.getElementById("user_id").value;
  var name = document.getElementById("name").value;
  var xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function() {
    if (xhttp.readyState == 4 && xhttp.status == 200) {
       document.getElementById("client_plan_result").innerHTML = xhttp.responseText;
       $('#trainningplan_next').hide();

    }
  }
  xhttp.open("GET", "<?php echo base_url();?>user/search_client_plan?user_id="+user_id+"&name="+name, true);
  xhttp.send();
}

function other_plan() {

  var user_id = document.getElementById("user_id").value;
  var name = document.getElementById("name").value;
  var xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function() {
    if (xhttp.readyState == 4 && xhttp.status == 200) {
       document.getElementById("other_plan_result").innerHTML = xhttp.responseText;
       $('#trainningplan_next').hide();

    }
  }
  xhttp.open("GET", "<?php echo base_url();?>user/other?user_id="+user_id+"&name="+name, true);
  xhttp.send();
}
</script>
<!-- ajay 3 oct end-->
<script type="text/javascript">
    $(document).ready(function(){
      
         $(".tab-pane").on('change','#user_pic',function() {
            var data1 = new FormData($('#add_image')[0]);
           $.ajax({
             url:"<?php echo base_url();?>user/add_image",
             type:"post",
             data: data1,
             contentType: false,
             processData: false,
             success: function(data)
             {
                /*$("#upload_images").html(data);*/
                notify('success','<i class="fa fa-check"> Success </i>','Image Added Successfully');
                $('#workout-dairy')[0].reset();
                setTimeout(function() { window.location.reload(); }, 1000)
             }
         });
         });
      
    });
</script>
<script>
function show_image() {

  var user_id = document.getElementById("user_id").value;
  var xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function() {
    if (xhttp.readyState == 4 && xhttp.status == 200) {
       document.getElementById("upload_images").innerHTML = xhttp.responseText;

    }
  }
  xhttp.open("GET", "<?php echo base_url();?>user/show_image?user_id="+user_id, true);
  xhttp.send();
}
</script>
<script type="text/javascript">
    $(document).ready(function(){
      
         $(".tab-pane").on('click','#previous',function() {
           var start_date = $('#start_date').val();        
           var end_date = $('#end_date').val();
           var user_id = $('#user_id').val();
           $.ajax({
             url:"<?php echo base_url();?>user/nutrition_details_by_week?start_date="+start_date+"&end_date="+end_date+"&user_id="+user_id,
             type:"post",             
             contentType: false,
             processData: false,
             success: function(data)
             {
              var start_date =  $($.parseHTML(data)).find("#start_date");
              console.log(start_date);
              if(start_date["length"] > 0){
               var end_date =  $($.parseHTML(data)).find("#end_date");
               $('#start_date').val(start_date);
               $('#end_date').val(end_date);
               $('#next').show();
               jQuery('#demo').html(data);
              }else
              {
                $('#previous').hide();
                //$('#next').show();
              }
             }
         });
     
         });
      
    });
</script>
<script type="text/javascript">
    $(document).ready(function(){
         $(".tab-pane").on('click','#trainningplan_previous',function() {
           var plan_start_date = $('#plan_start_date').val();        
           var plan_end_date = $('#plan_end_date').val();
           var plan_user_id = $('#plan_user_id').val();
           $.ajax({
             url:"<?php echo base_url();?>user/trainning_plan_by_week?plan_start_date="+plan_start_date+"&plan_end_date="+plan_end_date+"&plan_user_id="+plan_user_id,
             type:"post",             
             contentType: false,
             processData: false,
             success: function(data)
             {
              var start_date =  $($.parseHTML(data)).find("#plan_start_date");
              console.log(start_date);
              if(start_date["length"] > 0){
               var end_date =  $($.parseHTML(data)).find("#plan_end_date");
               $('#start_date').val(start_date);
               $('#end_date').val(end_date);
                $('#trainningplan_next').show();
               jQuery('#client_plan_result').html(data);
              }else
              {
                $('#trainningplan_previous').hide();
                //$('#next').show();
              }
             
             }
         });
     
         });
      
    });
</script>
<script type="text/javascript">
    $(document).ready(function(){
      
         $(".tab-pane").on('click','#next',function() {
           var start_date = $('#start_date').val();        
           var end_date = $('#end_date').val();
           var user_id = $('#user_id').val();
           $.ajax({
             url:"<?php echo base_url();?>user/nutrition_details_by_week_next?start_date="+start_date+"&end_date="+end_date+"&user_id="+user_id,
             type:"post",             
             contentType: false,
             processData: false,
             success: function(data)
             {
              var start_date =  $($.parseHTML(data)).find("#start_date");
              var end_date =  $($.parseHTML(data)).find("#end_date");
              var today = new Date();
              var dayOfWeekStartingSundayZeroIndexBased = today.getDay();
              var mondayOfWeek = new Date(today.getFullYear(), today.getMonth(), today.getDate() - today.getDay()+0);
              var strDate = mondayOfWeek.getFullYear() + "-" + (mondayOfWeek.getMonth()+1) + "-" + '0' + mondayOfWeek.getDate();
              $('#start_date').val(start_date);
              $('#end_date').val(end_date);
              $('#previous').show();
              if(start_date[0]["defaultValue"] == strDate){
                $('#next').hide();

              }            
              
        jQuery('#demo').html(data);
             }
         });
     
         });
      
    });
</script>
<script type="text/javascript">
    $(document).ready(function(){
         $(".tab-pane").on('click','#trainningplan_next',function() {
           var plan_start_date = $('#plan_start_date').val();
           var plan_end_date = $('#plan_end_date').val();
           var plan_user_id = $('#plan_user_id').val();
           $.ajax({
             url:"<?php echo base_url();?>user/client_plan_by_week_next?plan_start_date="+plan_start_date+"&plan_end_date="+plan_end_date+"&plan_user_id="+plan_user_id,
             type:"post",             
             contentType: false,
             processData: false,
             success: function(data)
             {
              var start_date =  $($.parseHTML(data)).find("#plan_start_date");
              var end_date =  $($.parseHTML(data)).find("#plan_end_date");
              var today = new Date();
              var dayOfWeekStartingSundayZeroIndexBased = today.getDay();
              var mondayOfWeek = new Date(today.getFullYear(), today.getMonth(), today.getDate() - today.getDay()+0);
              var strDate = mondayOfWeek.getFullYear() + "-" + (mondayOfWeek.getMonth()+1) + "-" + '0' + mondayOfWeek.getDate();
              $('#plan_start_date').val(start_date);
              $('#plan_end_date').val(end_date);
              $('#trainningplan_previous').show();
              if(start_date[0]["defaultValue"] == strDate){
              $('#trainningplan_next').hide();

              }            
              
        jQuery('#client_plan_result').html(data);
             }
         });
     
         });
      
    });
</script>
<!-- Chating Script Start --> 

<script type="text/javascript">

</script>


<!-- Chating Script End --> 

        
