<?php $this->load->view('baneer_section'); ?>
            <div class="container">
                    
                
            </div>
        </div>
        
    </header>
    <!--Header sec end-->
  <!--Main container sec start-->
    <div class="main_container">
    
        <div class="container">
            
            <div class="row">
                <div class="col-sm-3">
                    
                    <div class="trainee_tabs_sect">
                          <h3>Complete Profile</h3>
                          <!-- Nav tabs -->
                          <ul class="nav_tabs">
                           <li class="active"><a href="#interests" aria-controls="interests" role="tab" data-toggle="tab">Profile Heading</a></li>
                            <li><a href="#informaiton" aria-controls="informaiton" role="tab" data-toggle="tab">Personal Information</a></li>
                            <li><a href="#password" aria-controls="password" role="tab" data-toggle="tab">Password</a></li>
                            <li><a href="#biography" aria-controls="biography" role="tab" data-toggle="tab">Biography </a></li>
                            <li><a href="#certification" aria-controls="certification" role="tab" data-toggle="tab">Certification </a></li>
                            <li><a href="#accomplishments" aria-controls="accomplishments" role="tab" data-toggle="tab">Accomplishments  </a></li>
                            <li><a href="#hobby" aria-controls="hobby" role="tab" data-toggle="tab"> Interests & Hobbies   </a></li>
                           
                            
                          </ul>
                    </div>
                    
                </div>
                <div class="col-sm-9">
                    <div class="trainee_tab_content">
                          <div id="responseemployerpass">Data Submitted Successfully</div>
                          <div id="responseemployerpass_div">Password not match</div>
                          <div id="usr_verify" class="verify" style="display:none;">Current password not match</div>
                          <div id="usr_verify1" class="verify1" style="display:none;">Current password match</div>
                          <form method="post" id="add_profile">
                          <!-- Tab panes -->
                          <div class="tab-content">
                            <div role="tabpanel" class="tab-pane active" id="interests">
                                <h3 class="trai_title_sect">Profile Heading</h3>
                                <div class="form_wrapper">
                                    <div class="form-group">
                                        <label>Profile Heading</label>
                                        <textarea class="form-control" cols="5" role="4" name="intrest" id="intrest"><?php echo $user_details[0]['intrest'];?></textarea>
                                        
                                    </div>
                                    <div class="form-group">
                                            <label>User Type</label>
                                            <input type="text" class="form-control" id="user_type" name="user_type" value = "<?php echo $user_details[0]['type'];?>">
                                    </div>
                                </div>
                            </div>
                            <div role="tabpanel" class="tab-pane" id="informaiton">
                                <h3 class="trai_title_sect">Personal Information</h3>                           
                                <div class="form_wrapper">
                                    <div class="form-group">
                                        <div class="photo_upload_sect">
                                                    <input type="file" value="" id="user_pic" name="user_pic"> <input type="hidden" value="<?php echo $user_details[0]['user_pic'];?>" name="user_pic" id="user_pic">           
                                                    <span class="fa fa-cloud-upload"></span>
                                                    <h5>Upload your Photo</h5>
                                                </div>
                                    </div> 

                                    <div class="form-group">
                                        <label>First Name</label>
                                        <input type="hidden" value="<?php echo $user_details[0]['user_id'];?>" name="user_id" id="user_id">
                                        <input type="text" class="form-control" name ="name" value="<?php echo $user_details[0]['name'];?>">
                                    </div>   
                                    <div class="form-group">
                                        <label>Surname</label>                                       
                                        <input type="text" class="form-control" name="surname" value="<?php echo $user_details[0]['surname'];?>">
                                    </div>                                    
                                     <div class="form-group">
                                        <label>Youtube URL</label>
                                        <input type="text" class="form-control" id="youtube" name="youtube" value = "<?php echo $user_details[0]['youtube'];?>">
                                    </div>
                                    <div class="form-group">
                                        <label>Social URL</label>
                                        <div class="well">
                                            <div class="form-group"><label>Facebook URL</label>
                                            <input type="url" class="form-control" id="fb_url" name="fb_url" value = "<?php echo $user_details[0]['fb_url'];?>"></div>
                                            <div class="form-group"><label>Twitter URL</label>
                                            <input type="url" class="form-control" id="twit_url" name="twit_url" value = "<?php echo $user_details[0]['twit_url'];?>"></div>
                                            <div class="form-group"><label>Linkedin URL</label>
                                            <input type="url" class="form-control" id="link_url" name="link_url" value = "<?php echo $user_details[0]['link_url'];?>"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div role="tabpanel" class="tab-pane" id="password">
                                <h3 class="trai_title_sect">Password</h3>
                                <div class="form_wrapper">
                                    <div class="form-group">
                                            <label>Current Password</label>
                                            <input type="password" class="form-control" id="current_password" name="current_password" value = "<?php echo $user_details[0]['password'];?>">
                                    </div>
                                    <div class="form-group">
                                            <label>New Password</label>
                                            <input type="password" class="form-control" id="new_password" name="new_password" value = "">
                                    </div>
                                    <div class="form-group">
                                            <label>Confirm Password</label>
                                            <input type="password" class="form-control" id="confirm_password" name="confirm_password" value = "">
                                    </div>
                               </div>
                            </div>
                            <div role="tabpanel" class="tab-pane" id="biography">
                                <h3 class="trai_title_sect">Biography</h3>
                                <div class="form_wrapper">
                                    <div class="form-group">
                                        <textarea class="form-control" cols="5" role="4" name="bio" id="bio"><?php echo $user_details[0]['bio'];?></textarea>
                                    </div>
                                </div>
                            </div>
                            <div role="tabpanel" class="tab-pane" id="certification">
                                <h3 class="trai_title_sect">Certification</h3>
                                <div class="form_wrapper">
                                    <div class="form-group">
                                        <textarea class="form-control" cols="5" role="4" name="certi" id="certi"><?php echo $user_details[0]['certi'];?> </textarea>
                                    </div>
                                </div>
                            </div>
                            <div role="tabpanel" class="tab-pane" id="awards">
                                <h3 class="trai_title_sect">Awards</h3>
                                <div class="form_wrapper">
                                    <div class="form-group">
                                        <textarea class="form-control" cols="5" role="4" name="award" id="award"><?php echo $user_details[0]['award'];?> </textarea>
                                    </div>
                                </div>
                            </div>
                            <div role="tabpanel" class="tab-pane" id="accomplishments">
                                <h3 class="trai_title_sect">Accomplishments</h3>
                                <div class="form_wrapper">
                                    <div class="form-group">
                                        <textarea class="form-control" cols="5" role="4" name="accomplish" id="accomplish"><?php echo $user_details[0]['accomplish'];?></textarea>
                                    </div>
                                </div>
                            </div>
                            <div role="tabpanel" class="tab-pane" id="location">
                                <h3 class="trai_title_sect">Location</h3>
                                <div class="form_wrapper">
                                    <div class="form-group">
                                        <textarea class="form-control" cols="5" role="4" name ="loc" id ="loc"><?php echo $user_details[0]['loc'];?></textarea>
                                    </div>
                                </div>
                            </div>
                            <div role="tabpanel" class="tab-pane" id="credentials">
                                <h3 class="trai_title_sect">Credentials</h3>
                                <div class="form_wrapper">
                                    <div class="form-group">
                                        <textarea class="form-control" cols="5" role="4" name ="credential" id="credential"><?php echo $user_details[0]['credential'];?> </textarea>
                                    </div>
                                </div>
                            </div>
                            <div role="tabpanel" class="tab-pane" id="hobby">
                                <h3 class="trai_title_sect">Interests & Hobbies</h3>
                                <div class="form_wrapper">
                                    <div class="form-group">
                                        <textarea class="form-control" cols="5" role="4" name="hob" id="hob"><?php echo $user_details[0]['hob'];?></textarea>
                                    </div>
                                </div>
                            </div>
                          </div>
                          
                          <div class="clearfix"></div>
                          <button id="update" class="btn submit_btn" type="button">update</button>
                          </form>
                    </div>
                    
                </div>
            </div>
            
        </div>
        
    </div>
    
    <!--Main container sec end-->
    
    <div class="clearfix"></div>

     <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
     <script type="text/javascript">
      $(document).ready(function(){
        $(".trainee_tab_content").on('click','#update',function() {
           var data = new FormData($('#add_profile')[0]);

                $.ajax({
                  type: "post",
                  url: "<?php echo base_url(); ?>user/user_information_trainner",
                  data: data,
                  contentType: false,
                  processData: false,
                  success: function(response){
                      notify('success','<i class="fa fa-check"> Success </i>','Data Submitted Successfully');
                  }
                });
      });
    });
</script>
    
