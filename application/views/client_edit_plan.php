<?php $this->load->view('baneer_section'); ?>
          <div class="container">
                  
                
            </div>
        </div>
        
    </header>

    <!--Header sec end-->
    <!--Main container sec start-->
    <div class="main_container">
    
    	<div class="container">
        	
            <div class="row">
            	<div class="col-sm-3">
                	
                    <div class="trainee_tabs_sect">
                    	  <h3>Edit Training Plan</h3>
                    	  <!-- Nav tabs -->
                          <ul class="nav_tabs">
                            <li class="active"><a href="#informaiton" aria-controls="informaiton" role="tab" data-toggle="tab">Training Plan</a></li>
                          </ul>
                    </div>
                    
                </div>
                <div class="col-sm-9">
                	<div class="trainee_tab_content">
 
                     <form method="post" id="edit_client_plan">
                     <div id="client_plan" class="client_plan">Your Data Saved Successfully</div>
                    	  <!-- Tab panes -->
                          <div class="tab-content">
                            <div role="tabpanel" class="tab-pane active" id="informaiton">
                            	<h3 class="trai_title_sect">Edit Training Plan Information</h3>
                                <div class="form_wrapper">
                                    <div class="form-group">
                                        <label>Clients</label>
                                         <select class="form-control" autocomplete="off" name="user_id" required="">
                                             <option value="">Select Client</option>
                                             <?php foreach ($select_client as $clients) {

                                                ?>
                                               <option value="<?php echo $clients['id'];?>" <?php if($clients['id'] == $training_plan_details[0]['user_id']){ echo "selected ='selected'";}?>><?php echo $clients['name'];?></option>
                                              
                                             <?php } ?>
                                         </select>
                                    </div>                                	
                                    <div class="form-group">
                                        <label>Exercise</label>
                                        <input type="text" class="form-control" id="exercise" value="<?php echo $training_plan_details[0]['exercise'];?>" name="exercise" placeholder="Exercise">
                                    </div>
                                    <div class="form-group">
                                        <label>Weight</label>
                                        <input type="text" class="form-control" id="weight" name="weight" value="<?php echo $training_plan_details[0]['weight'];?>" placeholder="Weight">
                                    </div>
                                    <div class="form-group">
                                        <label>Sets</label>
                                        <input type="text" class="form-control" id="sets" name="sets" value="<?php echo $training_plan_details[0]['sets'];?>" placeholder="Sets">
                                    </div>
                                    <div class="form-group">
                                        <label>Reps</label>
                                        <input type="text" class="form-control" id="reps" name="reps" value="<?php echo $training_plan_details[0]['reps'];?>" placeholder="Reps">
                                    </div>
                                    <div class="form-group">
                                        <label>Time</label>
                                        <input class="form-control" type="text" name="time" value="<?php echo $training_plan_details[0]['time'];?>" placeholder="Time">
                                    </div>
                                    <div class="form-group">
                                        <label>Notes</label>
                                        <textarea class="form-control" id="notes" name="notes" placeholder="Notes"><?php echo $training_plan_details[0]['notes'];?></textarea>
                                    </div>
                                </div>
                            </div>
                          </div>
                          <div class="clearfix"></div>
                          <input type="hidden" name="id" id="id" value="<?php echo $training_plan_details[0]['id'];?>">
                          <input type="hidden" name="user_id" id="user_id" value="<?php echo $training_plan_details[0]['user_id'];?>"> 
                    	  <button id="update_plan" class="btn submit_btn" type="button">Update</button>
                     </form>
                    </div>
                    
                </div>
            </div>
            
        </div>
        
    </div>
    
    <!--Main container sec end-->
     <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script type="text/javascript">
    $(document).ready(function(){
          $(".trainee_tab_content").on('click','#update_plan',function() {
                  var data1 = new FormData($('#edit_client_plan')[0]);
                  $.ajax({
                    url:"<?php echo base_url();?>user/update_client_plan",
                    type:"post",
                    data: data1,
                    contentType: false,
                    processData: false,
                    success: function(response)
                    {
                      var user_id = $( "#user_id" ).val();
                      window.location.href='<?php echo base_url(); ?>user/client_profile?id='+user_id;
                    }
        });
      
      
      });
    });
</script>
    
