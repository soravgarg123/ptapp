<?php $this->load->view('baneer_section'); ?>
          <div class="container">
                  <div class="row">
                      <div class="col-sm-12">
                          <h3 class="page_title">Edit Nutrition Details</h3>
                        </div>
                        
                    </div>
            </div>
        </div>
        
    </header>
    <!--Header sec end-->
    
    <!--Main container sec start-->
    <div class="main_container">
    
      <div class="container">
          <div class="row">
              <div class="col-sm-12">
                 <div class="tab-pane" id="awards">
                  <form method="post" id="update_nutrition">
                              <div id="nutrition">Your details save successfully</div>
                              <h3 class="trai_title_sect">Edit Nutrition Details</h3>
                                <div class="form_wrapper">
                                    <div class="form-group">
                                        <label>Title</label>
                                        <input type="text" autofocus placeholder="Title" class="form-control" value="<?php echo $nutrition_details[0]['title'];?>" id="title" name="title" autocomplete="off">
                                    </div> 
                                    <div class="form-group">
                                        <label>Category</label>
                                         <select class="form-control" autocomplete="off" name="category" required="">
                                             <option value="">Select Category</option>
                                             <option value="breakfast"  <?php if($nutrition_details[0]['category'] == "breakfast"){ echo "selected ='selected'";}?>>Breakfast</option>
                                             <option value="lunch" <?php if($nutrition_details[0]['category'] == "lunch"){ echo "selected = 'selected'";}?>>Lunch</option>
                                             <option value="dinner" <?php if($nutrition_details[0]['category'] == "dinner"){ echo "selected = 'selected'";}?>>Dinner</option>
                                         </select>
                                    </div>
                                    <div class="form-group">
                                        <label>Calory</label>
                                        <input type="text" autofocus  placeholder="Calory" class="form-control" value="<?php echo $nutrition_details[0]['calory'];?>" id="calory" name="calory" autocomplete="off">
                                    </div> 
                                    <div class="form-group">
                                        <label>Description</label>
                                        <textarea class="form-control" name="description" id="description"><?php echo $nutrition_details[0]['description'];?></textarea>
                                    </div>
                                  </div>
                                  <input type="hidden" name="id" id="id" value="<?php echo  $nutrition_details[0]['id'];?>" >
                                   <input type="hidden" name="user_id" id="user_id" value="<?php echo  $nutrition_details[0]['user_id'];?>" >
                                  <button id="update" class="btn submit_btn" type="button">Update</button>
                                </form>
                    </div>
                </div>
            </div>
            
        </div>
        
    </div>
    
    <!--Main container sec end-->
    
    <div class="clearfix"></div>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script type="text/javascript">
    $(document).ready(function(){
         $(".tab-pane").on('click','#update',function() {
           var data1 = new FormData($('#update_nutrition')[0]);
           $.ajax({
             url:"<?php echo base_url() ?>user/update_nutrition_details",
             type:"post",
             data: data1,
             contentType: false,
             processData: false,
             success: function(response)
             {
              var user_id = $( "#user_id" ).val();
              window.location.href='<?php echo base_url(); ?>user/client_profile?id='+user_id;
             }
         });
      });
   });    
   
</script>


