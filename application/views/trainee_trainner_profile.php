<?php $this->load->view('baneer_section'); ?>
        	<div class="container">
                	<div class="profile_header">
                    	<div class="col-sm-2 col-md-2">
                        	<span class="user_profile_pic"><img class=" img-responsive" src="<?php echo base_url(); ?>trainner/<?php echo $client_trainner[0]['user_pic'];?>" alt=""/></span>
                        </div>
                        <div class="col-sm-7 col-md-8">
                        	<div class="user_profile_info">
                            	<h3><?php echo $client_trainner[0]['name'].' '.$client_trainner[0]['surname']; ?></h3>
                                <span class="user_degi"><?php echo $client_trainner[0]['type'];?></span>
                                <p><?php echo $client_trainner[0]['intrest'];?></p> 
                            </div>
                            
                            <!--<div class="add_profile" ><a class="btn submit_profile" target="_blank" href="<?php echo $user[0]['youtube'];?>">Youtube</a></div>-->
                        </div>
                        <div class="col-md-2 col-sm-3">
                        	<ul class="social_icon_user">
                                	<li><a href="<?php echo $client_trainner[0]['fb_url'];?>" target="_blank"><span class="fa fa-facebook"></span></a></li>
                                    <li><a href="<?php echo $client_trainner[0]['twit_url'];?>" target="_blank"><span class="fa fa-twitter"></span></a></li>
                                    <li><a href="<?php echo $client_trainner[0]['link_url'];?>" target="_blank"><span class="fa fa-linkedin"></span></a></li>
                                    <li><a href="<?php echo $client_trainner[0]['youtube'];?>" target="_blank"><span class="fa fa-youtube"></span></a></li>
                            </ul>
                            

                        </div>
                    	 
                    </div>
                
            </div>
        </div>
        
    </header>
    <!--Header sec end-->
    
    <!--Main container sec start-->
    <div class="main_container">
    
    	<div class="container">
        	<div class="row">
            	<div class="col-sm-12">
                
                	<div class="panel panel_app">
                      <div class="panel-heading">
                        <h3 class="panel-title">Biography </h3>
                      </div>
                      <div class="panel-body">
                       	<p><?php echo $client_trainner[0]['bio'];?></p>
                      </div>
                    </div>
                    
                    <div class="panel panel_app">
                      <div class="panel-heading">
                        <h3 class="panel-title">Certification  </h3>
                      </div>
                      <div class="panel-body">
                       	<p><?php echo $client_trainner[0]['certi'];?></p>
                      </div>
                    </div>
                    
                    <div class="panel panel_app">
                      <div class="panel-heading">
                        <h3 class="panel-title">Accomplishments   </h3>
                      </div>
                      <div class="panel-body">
                       	<p><?php echo $client_trainner[0]['accomplish'];?></p>
                      </div>
                    </div>
                    
                    <div class="panel panel_app">
                      <div class="panel-heading">
                        <h3 class="panel-title"> Interests & hobby    </h3>
                      </div>
                      <div class="panel-body">
                       	<p><?php echo $client_trainner[0]['hob'];?></p>
                      </div>
                    </div>
                    
                </div>
            </div>
            
        </div>
        
    </div>
    
    <!--Main container sec end-->
    
    <div class="clearfix"></div>
