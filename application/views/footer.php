        <div class="chatting_main">

        </div>  

        <!-- PT login model  -->
        <div class="modal fade bs-example-modal-sm" id="login_modal" tabindex="-1" role="dialog" aria-labelledby="login_modal">
          <div class="modal-dialog modal-sm" role="document">
            <div class="modal-content custom_modal">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">PT sign in</h4>
              </div>
              <div class="modal-body" id="login_body">
                    <?php
                      $successmsg = "";
                      $successmsg .=  '<div class="alert alert-success alert-dismissable" style="display:none;" id="success_msg">';
                      $successmsg .= '<button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>';
                      $successmsg .= '<b></b> <center> <i class="fa fa-times"></i></center></div>';
                      echo $successmsg;

                      $errormsg = "";
                      $errormsg .= '<div class="alert alert-danger alert-dismissable" style="display:none;" id="error_msg">';
                      $errormsg .= '<button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>';
                      $errormsg .= '<b></b> <center> <i class="fa fa-times"></i></center></div>';
                      echo $errormsg;

                      $loading_img = "";
                      $loading_img .= '<center><img style="display:none;" id="loading-img" src="'. base_url() .'images/loading-spinner-grey.gif" /></center></br>';
                      echo $loading_img;

                    ?>
                    <form class="form-signin"  method="post">
                  
                    <input required type="email" autofocus required placeholder="Email address" class="form-control" id="inputEmail" name="email">
                   
                    <input type="password" required placeholder="Password" class="form-control" id="inputPassword" name="password">
                    <div class="checkbox">
                      <label>
                        <input type="checkbox" value="remember-me"> Remember me
                      </label>
                    </div>
                    <input type="button" class="btn btn-lg orange_btn btn-block" value="Sign in" id="login-btn">
                  </form>
                
              </div>
              
            </div>
          </div>
        </div>
        <!-- PT login model  -->

        <!-- cart popup -->

        <div class="modal fade bs-example-modal-md" id="cart_modal" tabindex="-1" role="dialog" aria-labelledby="cart_modal">
          <div class="modal-dialog modal-md" role="document">
            <div class="modal-content custom_modal">
               <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title text-center" id="myModalLabel">Item Added To Your Cart</h4>
              </div>
              <div class="modal-body ">

                <div class="row cart_body">
                   


                </div>


                 
              </div>

              <div class="modal-footer">

                      <div class="row">
                  
                  <div class="col-md-4"><a href="<?php echo base_url('product/product_list_view');?>"><button class="ad_cart" price="<?php echo $product_detail[0]['price'];?>" pid="<?php echo $product_detail[0]['product_id'];?>" >Continue Shopping</button></a>
                   </div>
                   <div class="col-md-4"></div>

                   <div class="col-md-4 text-right"><a href="<?php echo base_url('product/checkout');?>"><button class="check-out"  price="<?php echo $product_detail[0]['price'];?>" pid="<?php echo $product_detail[0]['product_id'];?>" >Go To Checkout</button></a>
                   </div>


                  </div>
              </div>
              
            </div>
          </div>
        </div>


         <!-- end cart popup --> 







        <!-- Client login model  -->
        <div class="modal fade bs-example-modal-sm" id="client_login_modal" tabindex="-1" role="dialog" aria-labelledby="login_modal">
          <div class="modal-dialog modal-sm" role="document">
            <div class="modal-content custom_modal">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Client sign in</h4>
              </div>
              <div class="modal-body" id="login_body1">
              <?php
                      $successmsg = "";
                      $successmsg .=  '<div class="alert alert-success alert-dismissable" style="display:none;" id="success_msg">';
                      $successmsg .= '<button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>';
                      $successmsg .= '<b></b> <center> <i class="fa fa-times"></i></center></div>';
                      echo $successmsg;

                      $errormsg = "";
                      $errormsg .= '<div class="alert alert-danger alert-dismissable" style="display:none;" id="error_msg">';
                      $errormsg .= '<button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>';
                      $errormsg .= '<b></b> <center> <i class="fa fa-times"></i></center></div>';
                      echo $errormsg;

                      $loading_img = "";
                      $loading_img .= '<center><img style="display:none;" id="loading-img" src="'. base_url() .'images/loading-spinner-grey.gif" /></center></br>';
                      echo $loading_img;

                    ?>
                    <form class="form-signin"  method="post">
                    <input required type="email" autofocus required placeholder="Email address" class="form-control" id="inputEmail1" name="email1">
                    <input type="password" required placeholder="Password" class="form-control" id="inputPassword1" name="password1">
                    <div class="checkbox">
                      <label>
                        <input type="checkbox" value="remember-me"> Remember me
                      </label>
                    </div>
                    <input type="button" class="btn btn-lg orange_btn btn-block" value="Sign in" id="login-btn1">
                  </form>
                
              </div>
              
            </div>
          </div>
        </div>
        <!-- Client login model  -->
    
        <!-- Regsiter model  -->
        <div class="modal fade bs-example-modal-md" id="register_modal" tabindex="-1" role="dialog" aria-labelledby="register_modal">
          <div class="modal-dialog modal-md" role="document">
            <div class="modal-content custom_modal">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Register Form</h4>
              </div>
              <div class="modal-body usersignup">
                    <div id="responseemployerpass_div" style="text-align:center;padding:10px;background:#eee;border: 1px solid #29488F;
color:#29488F;display:none;margin:10px;">Password not match</div>

                    <div id="responseemployerpass" style="text-align:center;padding:10px;background:#eee;border: 1px solid #29488F;
color:#29488F;display:none;margin:10px;">Registration Successfully</div>

                    <?php //echo validation_errors(); ?>
                    <form class="form-signin" id="register" method="post">
                      <div id="usr_verify" class="verify" style="display:none;">Email available</div>
                    <div id="usr_verify1" class="verify1" style="display:none;">Email already exists</div>
                    <div id="usr_verify2" class="verify2" style="display:none;">Please fill email field</div>
                    <input type="hidden" name="user_type" id="user_type" value="trainner">

                    <input type="text" autofocus required placeholder="Full Name" required class="form-control" id="fname" name="name" autocomplete="off">

                    <div class="footer_cont_wid">
                        <ul class="social_icon">
                          <li><a class="facebook_icon" id="fb_login" title="Facebook Login" href="javascript:void(0);"><span class="fa fa-facebook"></span></a></li>
                          <li><a class="google_icon_login g-signin2" id="gp_login" data-longtitle="true" data-onsuccess="Google_signIn" data-width="35" data-theme="light" title="Google Login" href="javascript:void(0);"><span class="fa fa-google"></span></a></li>
                        </ul>
                    </div> 
                    
                   <!--  <input type="text" autofocus required placeholder="Surname" class="form-control" required id="surname" name="surname" autocomplete="off"> -->

                    <select class="form-control" required name="country" id="country" autocomplete="off"> 
                       <option value="">Country</option>
                    <?php foreach ($country as $count) {
                      echo "<option>".$count['country_name']."</option>";
                    }?>
                    </select>

                    <input type="text" autofocus required placeholder="City"  class="form-control"  id="location" name="location" autocomplete="off">

                    <input type="text" autofocus required placeholder="Occupation"  class="form-control"  id="occupation" name="occupation" autocomplete="off">

                    <input type="text" autofocus required placeholder="Highest Relevant Qualification" class="form-control" id="qualifications" name="qualifications" autocomplete="off">
                    
                    <input type="email" autofocus required placeholder="Email Address" class="form-control" id="email" name="email" autocomplete="off">
                    
                    <input type="password" autofocus required placeholder="Password" minlength='5' class="form-control" id="password" name="password" autocomplete="off">
                    
                    <input type="password" autofocus required placeholder="Confirm Password" minlength='5' class="form-control" id="confirm_password" name="confirm_password" autocomplete="off">

                    <div class="checkbox">
                      <label>
                        <input type="checkbox" value="remember-me" name="terms" id="terms"> <a target="_blank" href="<?php echo base_url(); ?>user/terms">I have read the terms of use</a>
                      </label>
                    </div>                   
                    <input type="button" class="btn btn-lg orange_btn btn-block" id="signup" value="Sign Up" >
                  </form>
                
              </div>
              
            </div>
          </div>
        </div>
        <!-- Regsiter model  -->   


      <!-- payment model  -->
        <div class="modal fade bs-example-modal-sm" id="payment_modal" tabindex="-1" role="dialog" aria-labelledby="payment_modal">
          <div class="modal-dialog modal-sm" role="document">
            <div class="modal-content custom_modal">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Payment</h4>
              </div>
              <div class="modal-body">
                  <input type="hidden" name="hidden_user_id" id="hidden_user_id">
                    <select class="form-control site_plans" name="plan_id" id="plan_id">
                        <?php foreach ($plans as $plan){ ?>
                          <option value="<?php echo $plan['id']; ?>">&pound; <?php echo $plan['price']; ?> - <?php echo $plan['plan_title']; ?></option>
                        <?php } ?>
                  </select>
                    <img class="form-control" src="<?php echo base_url();?>images/paypal.png">
                  <form class="form-signin" method="post" action="https://www.paypal.com/cgi-bin/webscr">
                    <input type="hidden" name="cmd" value="_xclick" />
                    <input type="hidden" name="charset" value="utf-8" />
                    <input type="hidden" name="business" value="Omar@theptapp.com" />
                    <input type="hidden" name="item_name" id="item_name" value="<?php echo $plans[0]['plan_title']; ?>" />
                    <input type="hidden" name="item_number" id="item_number" value="<?php echo $plans[0]['description']; ?>" />
                    <input type="hidden" id="paypal_ammount" name="amount" value="<?php echo $plans[0]['price']; ?>" />
                    <input type="hidden" name="currency_code" value="GBP" />
                    <input type="hidden" name="return" value="<?php echo base_url(); ?>user/paypalPayemnt" />
                    <input type="hidden" name="custom" id="paypal_payemnt" value="" />
                    <input type="hidden" name="cancel_return" id="cancel_return" value="" />
                    <input type="hidden" name="bn" value="Business_BuyNow_WPS_SE" />
                    <input type="submit" class="btn btn-lg orange_btn btn-block" value="Pay Now">
                  </form>
              </div>
              
            </div>
          </div>
        </div>
        <!-- payment model  -->




        <!-- Order Sucess model  -->
        <div class="modal fade bs-example-modal-sm" id="order_sucess_modal" tabindex="-1" role="dialog" aria-labelledby="order_sucess_modal">
          <div class="modal-dialog modal-sm" role="document">
            <div class="modal-content custom_modal">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel"><?php echo $this->session->userdata['payment_sucess']['msg'];?></h4>
              </div>
             </div>
          </div>
        </div>
        <!-- payment model  -->













<input type="text" id="wheight" value="" class="hidden">
<input type="text" id="top" value="" class="hidden">
<input type="text" id="ifelse" value="" class="hidden">
    <!--Footer sec start-->
    <footer id="footer" class="footer_sect">
      <div class="container">
          <div class="row">
              <?php if(!empty($latest_update)) { ?>
                <div class="col-sm-6 footer_widget_sect">
                  <h3 class="footer_title"><?php echo $latest_update['heading']; ?></h3>
                    <div class="footer_cont_wid">
                      <p> <?php echo $latest_update['text']; ?> </p>
                        <div class="subscribe_wrap">
                            <div class="form-group">
                                <div class="input-group">
                                  <form method="post" id="subs_form"> 
                                  <div class="row">
                                  <div class="col-sm-10 footer_widget_sect">
                                  <input type="text" class="form-control" required name="firstname" placeholder="Name">
                                  </div></br></br>
                                  <div class="col-sm-10 footer_widget_sect">
                                  <input type="email" class="form-control" required name="email" placeholder="Email">
                                  </div>
                                   <div class="col-sm-2 col-xs-12 footer_widget_sect">
                                  <div class="input-group-addon" id="subs"><button id="subs_btn">subscribe</button></div>
                                  </div>
                                  </div>
                                  </form>
                                </div>
                              </div>
                        </div>
                    </div>
                </div>
                <?php } ?>
                <div class="col-sm-2 footer_widget_sect">
                  <h3 class="footer_title">Quick Link</h3>
                    <div class="footer_contwid quick_links">
                      <ul>
                        
                      <?php 
                      if($this->session->userdata('user_type') == 'trainner')
                      {             
                         ?>
                       
                        <li><a href="<?php echo base_url('user/notification'); ?>">Business Dashboard</a></li>
                         <li><a href="<?php echo base_url('user/profiles'); ?>">PT Profile</a></li>
                         <li><a href="<?php echo base_url('user/client_list'); ?>">Clients</a></li>
                         <li><a href="<?php echo base_url(); ?>user/profit_loss">Profit/Loss</a></li>
                         <li><a href="<?php echo base_url('user/setting'); ?>">Settings</a></li>
                      <?php 
                      }else if($this->session->userdata('user_type') == 'trainee')
                      { ?>
                        <li><a href="<?php echo base_url('user/client_profile').'?id='.$this->session->userdata('user_id'); ?>" id="profile">Profile</a></li>
                        <li><a href="<?php echo base_url('user/appointment'); ?>">Diary</a></li>
                <?php }
                      else
                      {
                ?>
                        <li><a href="<?php echo base_url(); ?>">Home</a></li>
                        <li><a href="<?php echo base_url(); ?>user/about">About Us</a></li>
                       <?php if(empty($user_type)){?>
                        <li><a href="<?php echo base_url();?>user/some_service">More...</a></li>
                        <?php }?>
                        <li><a href="<?php echo base_url(); ?>user/terms">Terms</a></li>

                <?php } 
                ?>
                        </ul>
                    </div>
                </div>
                
                <div class="col-sm-3 footer_widget_sect">
                  <h3 class="footer_title">Follow Us</h3>
                    <div class="footer_cont_wid">
                      <ul class="social_icon">
                        <?php if(!empty($social_link)) {
                        foreach($social_link as $a) { 
                          if($a['name'] == 'facebook')
                          { ?>
                          <li><a class="facebook_icon" target="_blank" href="<?php echo $a['url']; ?>"><span class="fa fa-facebook"></span></a></li>
                          <?php }
                          else if($a['name'] == 'twitter')
                          { ?>
                        <li><a class="twitter_icon" target="_blank" href="<?php echo $a['url']; ?>"><span class="fa fa-twitter"></span></a></li>
                          <?php }
                          else if($a['name'] == 'youtube')
                          { ?>
                           <li><a class="youtube_icon" target="_blank" href="<?php echo $a['url']; ?>"><span class="fa fa-youtube"></span></a></li>
                         <?php }
                          else if($a['name'] == 'linkedin')
                          { ?>
                        <li><a class="linkedin_icon" target="_blank" href="<?php echo $a['url']; ?>"><span class="fa fa-linkedin"></span></a></li>
                         <?php }
                          ?>
                        <?php }
                          }?>
                          
                        </ul>
                    </div>
                </div>
                <div class="col-sm-1 footer_widget_sect">
                  
                    <div class="footer_cont_wid">
                    </div>
                </div>
                
            </div>
        </div>
    </footer>
    <input type="hidden" id="hidden_client_id">
    <div class="footer_section2">
      <div class="container">
          <div class="row">
              <div class="col-sm-4"><p>&copy; Copyright © <?php echo date('Y'); ?> www.theptapp.com</p></div>

              <div class="col-sm-5 text-right">

                   <div class="row">
                      <div class="col-sm-4"><img src="<?php echo base_url('images/footericon/dhl_96x56_679_17092.png');?>" height="28" width="48"/>
                      <img src="<?php echo base_url('images/footericon/dpd_96x56_1176_17203.png');?>" height="28" width="48"/> 
                      </div>

                      <div class="col-sm-1"> </div>
                      <div class="col-sm-6 text-right">
                      <img src="<?php echo base_url('images/footericon/paypal_96x56_674_17095.png');?>" height="28" width="48"/>
                      <img src="<?php echo base_url('images/footericon/visa_96x56_675_17096.png');?>" height="28" width="48"/>
                      <img src="<?php echo base_url('images/footericon/mastercard_96x56_676_17097.png');?>" height="28" width="48"/>
                      </div>
                   </div>
              </div>

              <div class="col-sm-2 text-right"></div>
            </div>
        </div>
    </div>
    <!--Footer sec end-->
</main>
    <script src="<?php echo base_url(); ?>js/jquery.min.js"></script>
    <script src="<?php echo base_url(); ?>js/jquery-ui.js"></script>
    <script src="<?php echo base_url(); ?>js/owl.carousel.js"></script>
    <script src="<?php echo base_url(); ?>js/classie.js"></script>
    <script src="<?php echo base_url(); ?>js/bootstrap.min.js"></script>
    <script src="<?php echo base_url(); ?>js/plugin.js"></script>
    <script src="<?php echo base_url(); ?>js/custom.js"></script>
    <script src="<?php echo base_url();?>js/jquery.validate.js"></script>
    <script src="<?php echo base_url();?>js/jquery.wallform.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>js/ng_all.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>js/ng_ui.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>js/timepicker.js"></script>
    <link rel='stylesheet' href='<?php echo base_url(); ?>fullcalendar/fullcalendar.css' />
    <input type="hidden" value="" id="hidden_array">
    <script src='<?php echo base_url(); ?>fullcalendar/lib/moment.min.js'></script>
    <script src='<?php echo base_url(); ?>fullcalendar/fullcalendar.js'></script>
    <script src='<?php echo base_url(); ?>js/bootstrap-clockpicker.min.js'></script>
    <script src='<?php echo base_url(); ?>js/bootstrap-datepicker.js'></script>
    <script src='<?php echo base_url(); ?>js/socket.io-1.2.0.js'></script>
    <script src='<?php echo base_url(); ?>js/notify.js'></script>
    <script src='<?php echo base_url(); ?>js/notify-metro.js'></script>
    <script src="<?php echo base_url(); ?>js/lightbox.js"></script>
    <script src="<?php echo base_url(); ?>js/highcharts.js"></script>
    <script src="<?php echo base_url(); ?>js/exporting.js"></script>
    <script type="text/javascript">


     $(document).ready(function(){
     
      
    // if ($(window).scrollTop() == $(document).height() - $(window).height()){
    //   alert('done');
    // });



$("#subs_btn").click(function() {
            var search = $('#subs_form').serialize();
            // alert('<?php echo base_url(); ?>user/sub_function');
             // return false;
        $.ajax({
              type: "post",
              url: "<?php echo base_url(); ?>mailchamp/demo.php",
              data: search,
                  success: function(data){
                    if(data == 'success'){
                      notify('success','<i class="fa fa-check">You Have Successfully Subscribed Our Services</i>','');
                      location.reload();
                    }else{
                      notify('error','<i class="fa fa-times"> failure ! </i>','Your Subscribtion is Failed!');
                      return false;
                    }
                  }
                });
        });

});//ready

       function notify(style,title,text) {
        $.notify({
            title: title,
            text:  text,
        }, {
            style: 'metro',
            className: style,
            autoHide: true,
            clickToHide: true
        });
      }
    </script>
    <?php if($this->session->flashdata('success'))
    { ?>
      <script type="text/javascript">
          var msg = "<?php echo $this->session->flashdata('success'); ?>";
          notify('success','<i class="fa fa-check"> Success </i>',msg);
      </script>
    <?php    
    }
     ?>

     <?php if($this->session->userdata['payment_sucess']['msg'])
    { ?>
      <script type="text/javascript">
       $(document).ready(function(){
          $('#order_sucess_modal').modal({
                show: 'true'
            }); 
        });
      </script>
    <?php  
     $this->session->unset_userdata('payment_sucess'); 
    }
     ?>




     <?php if($this->session->flashdata('failure'))
      { ?>
        <script type="text/javascript">
          var msg1 = "<?php echo $this->session->flashdata('failure'); ?>";
          notify('error','<i class="fa fa-times"> Error ! </i>',msg1);                      
      </script>
     <?php    
      }
     ?>
          
      <script type="text/javascript">
        $(document).ready(function(){
        var edit_client_id = "<?php echo $this->uri->segment(3); ?>";
        $('#hidden_client_id').val(edit_client_id);
        var client_add = sessionStorage.getItem("client_add");
        if(client_add == "success"){
          notify('success','<i class="fa fa-check"> Success </i>','Your data submitted successfully');
          sessionStorage.removeItem("client_add");
        }
        var client_add = sessionStorage.getItem("client_edit");
        if(client_add == "success_update"){
          notify('success','<i class="fa fa-check"> Success </i>','Profile updated successfully');
          sessionStorage.removeItem("client_edit");
        }
          var payment_msg = "<?php echo $this->session->userdata('payment_msg'); ?>";
          if(payment_msg != ""){
            notify('success','<i class="fa fa-check"> Success </i>',payment_msg);
            "<?php $this->session->unset_userdata('payment_msg'); ?>";
          }
        });
      </script>
    <script type="text/javascript">
      var socket;
      var from_id = "<?php echo $this->session->userdata('user_id'); ?>";
      function send(text) {
        socket.emit('chatptapp', text);
      }

      function setupcall(data)
      {
          console.log(data);
          var rdata = data.split("$&");
            if(from_id == rdata[0])
            {
              var user_string = $('#hidden_array').val();
              var user_array = user_string.split(",");
              if($.inArray(rdata[1],user_array) == -1){
                  user_array.push(rdata[1]); // insert id in id array
                  var usertostring = user_array.toString();
                  $('#hidden_array').val(usertostring); // set array string in hidden value 
                  var user_id = btoa(rdata[1]);
                  $(".chatting_main").append('<div class="chating_wind_wrapper chat_window_'+rdata[1]+'"></div>');
                  $(".chat_window_"+rdata[1]).load('<?php echo base_url(); ?>user/getChatBox?user_id='+user_id, function(){
                    $('.chat_msg_box_'+ rdata[1]).append(rdata[2]);
                    $('.chat_msg_box_'+rdata[1]).animate({scrollTop: $('.chat_msg_box_'+rdata[1])[0].scrollHeight}, 1000);  
                  });
              }else
              {
                $('.chat_msg_box_'+ rdata[1]).append(rdata[2]);
                $('.chat_msg_box_'+rdata[1]).animate({scrollTop: $('.chat_msg_box_'+rdata[1])[0].scrollHeight}, 1000);  
              }
            }
      }

      $(document).ready(function(){
        socket = io('http://localhost:3600');
        
        socket.on('chatptapp', function(msg){
            setupcall(msg);
        });
      });

  </script>
    <script>
      var menuLeft = document.getElementById( 'cbp-spmenu-s1' ),
        showLeft = document.getElementById( 'showLeft' ),
        body = document.body;
        var isOpenMenu = false;
        showLeft.onclick = function() {
                  classie.toggle( this, 'active' );
                  classie.toggle( menuLeft, 'cbp-spmenu-open' );
                  disableOther( 'showLeft' );
              };
      function disableOther( button ) {
        if( button !== 'showLeft' ) {
          classie.toggle( showLeft, 'disabled' );
        }
      }
   
      // $('body').on('click',function(){
      //     console.log($(this).hasClass("menu_bar_targ"));
      //     if($(this).hasClass("menu_bar_targ") == false && $(this).hasClass("active") == true ){
      //       $('#cbp-spmenu-s1').removeClass('cbp-spmenu-open');
      //       classie.toggle( showLeft, 'disabled' );
      //     }
      // });

    </script>
    
    
  </body>
</html>

     <script type="text/javascript">
     $(document).ready(function(){

      $(document).click(function(event) {
        console.log(event.target);
        //console.log("open:: "+$(event.target).is('.is-open'));
        //console.log("open--:: "+$(event.target).is('is-open'));
        console.log("document:: "+$(event.target).attr('class'));
        console.log("document:: "+$(event.target).hasClass("is-open"));
        if($(event.target).hasClass("is-open") == false && $("#showLeft").hasClass("active") == true){
          classie.toggle( showLeft, 'active' );
          classie.toggle( menuLeft, 'cbp-spmenu-open' );
        }
        
      })
        $(".usersignup").on('click','#signup',function() {
        if($("#register").valid()) {
          var user_type = $('#user_type').val(); 
          var name = $('#fname').val();
          // var surname = $('#surname').val();
          var email = $('#email').val();
          var country = $('#country').val();
          var password = $('#password').val();
          var occupation = $('#occupation').val();
          var location = $('#location').val();
          var qualifications = $('#qualifications').val();
          var confirm_password = $('#confirm_password').val();
          
         if($("#password").val() != $("#confirm_password").val()){
                  
          notify('error','<i class="fa fa-times"> Error ! </i>','Password not matched !');
          return false;
                  //"&surname="+surname+

           }else if($("#password").val() == $("#confirm_password").val()){
                $.ajax({
                  type: "post",
                  url: "<?php echo base_url(); ?>user/register",
                  data: "user_type="+user_type+"&name="+name+"&email="+email+"&country="+country+"&password="+password+"&occupation="+occupation+"&location="+location+"&qualifications="+qualifications,
                  
                  success: function(response){
                    var result = JSON.parse(response);
                    if(result.lid == "exist"){
                        notify('error','<i class="fa fa-times"> Error ! </i>','Email-id already exists !');
                        return false;
                    }else if(result.lid == "success"){
                         notify('success','<i class="fa fa-check"> Success </i>','Signup Successfully');
                        window.location.reload();
                    }else{
                        var plan_id = "<?php if(!empty($plans)) { echo $plans[0]['id']; } else { echo '0'; } ?>";
                        user_last_id = result.lid;
                        $('#responseemployerpass').show();                    
                        $('#responseemployerpass_div').hide();
                        $("#usr_verify").hide();
                        $("#usr_verify1").hide();
                        $('#register')[0].reset();
                        $('#hidden_user_id').val(result.lid);  
                        $('#paypal_payemnt').val(plan_id + "-" + result.lid);
                        $('#cancel_return').val("<?php echo base_url(); ?>user/cancelPaypalPayemnt?id="+result.lid);
                        $('#payment_modal').modal({backdrop:'static',keyboard:false, show:true});
                    }
                  }
                });
           } 

        }
      });
    });
</script>
<!-- script written by Aditya Dubey start here
Date : 3 Nov 2015 
///////////////////////////////////////////////////////////////////////////////
 !-->
<script type="text/javascript">
  $(document).ready(function() { 


    $('.remove_cart_item').click(function(){

       var rowid=$(this).attr('productkey');
       
         $.ajax({
                  url:"<?php echo base_url(); ?>product/remove_cart_item/"+rowid,
                  type:"post",
                  data:"",
                  success: function(response){
                    location.reload();
                  },
                  error:function(error){
                    
                  }
                });
    });


  /*---------------------script for make payment div start here----------------
  */
  $('#payment').hide();
  $('.payment').click(function(){ 
    $('#payment').modal('show');
    $('input[name=item_name]').val($(this).attr('plan_title'));
    $('input[name=amount]').val($(this).attr('plan_price'));
    $('input[name=time]').val($(this).attr('plan_time')+' Days');
    $('input[name=date]').val('<?php echo date("d M Y"); ?>');
    $('input[name=custom]').val($(this).attr('plan_id'));
  });
  /*---------------------script for make payment div end here----------------
  */

 
  
});
</script>
<!-- script written by Aditya Dubey start here
Date : 3 Nov 2015 
///////////////////////////////////////////////////////////////////////////////
 !-->
 <!-- Close Chat Box Script Start Written By Sorav Garg -->

<script type="text/javascript">
    $(document).ready(function(){
        $('body').on('click','.close_chatbox',function(){
            var user_id = $(this).attr('main');
            var user_string1 = $('#hidden_array').val();
            var user_array1 = user_string1.split(",");
            var index = user_array1.indexOf(user_id);
            user_array1.splice(index, 1);
            var usertostring1 = user_array1.toString();
            $('#hidden_array').val(usertostring1);
            $('.chat_window_' + user_id).remove();
        });
    });
</script>

<!-- Close Chat Box Script End Written By Sorav Garg -->

<!-- Facebook Login Script Start -->

<script type="text/javascript">
  $(document).ready(function(){

    (function(d){
       var js, id = 'facebook-jssdk', ref = d.getElementsByTagName('script')[0];
       if (d.getElementById(id)) {return;}
       js = d.createElement('script'); js.id = id; js.async = true;
       js.src = "//connect.facebook.net/en_US/all.js";
       ref.parentNode.insertBefore(js, ref);
     }(document));

    window.fbAsyncInit = function() {
    FB.init({
      appId      : '1605857949631000',  
      status     : true, 
      cookie     : true, 
      xfbml      : true  
    });
    };

    $('body').on('click','#fb_login',function(){
        FB.login(function(response) {
           if (response.authResponse) 
           {
              getUserInfo();
            } else 
            {
             console.log('User cancelled login or did not fully authorize.');
            }
         },{scope: 'email,user_photos,user_videos'});

      function getUserInfo() {
      FB.api('/me', function(response) {
          $('#fname').val(response.first_name);
          $('#surname').val(response.last_name);
          $('#email').val(response.email);
          /*$.ajax({
                url : "<?php echo base_url(); ?>user/fblogin",
                type: "post",
                data: {response:response},
                success:function(result){
                  var resp = JSON.parse(result);
                  if(resp.msg == "trainee"){
                    notify('error','<i class="fa fa-times"> Error ! </i>','Clients can`t login !');
                  }else{
                    window.location.href = "<?php echo base_url(); ?>user/profiles";
                  }
                },
                error:function(error){
                  console.log(error);
                }
          });*/
          /*FB.logout(function(){ console.log('logout'); });*/
        });
      }
    });
  });
</script>

<!-- Facebook Login Script End -->

<!-- Google Login Script Start -->
<meta name="google-signin-client_id" content="658634277917-l7ka7gvfiiu1opgli9ufpsmn6iiokflo.apps.googleusercontent.com">
<script src="https://apis.google.com/js/platform.js" async defer></script>
<script type="text/javascript">
    function Google_signIn(googleUser){
      var profile = googleUser.getBasicProfile();
      $('#fname').val(profile.getName());
      $('#email').val(profile.getEmail());
      /*$.ajax({
              url : "<?php echo base_url(); ?>user/gplogin",
              type: "post",
              data: {email:profile.getEmail(),name:profile.getName()},
              success:function(result){
                var resp = JSON.parse(result);
                if(resp.msg == "trainee"){
                  notify('error','<i class="fa fa-times"> Error ! </i>','Clients can`t login !');
                }else{
                  window.location.href = "<?php echo base_url(); ?>user/profiles";
                }
              },
              error:function(error){
                console.log(error);
              }
          });*/
    }
</script>
<!-- Google Login Script End -->

<!-- Cancel Payment Start -->    

<script type="text/javascript">
    $(document).ready(function(){
      $('body').on('click','#cancel_payment',function(){
          var user_id = $('#hidden_user_id').val();
          if(user_id == ""){
            return false;
          }
          if (confirm("Are You Sure?")) {
          $.ajax({
                  url:"<?php echo base_url(); ?>user/cancelPayment",
                  type:"post",
                  data:{user_id:user_id},
                  success: function(response){
                    $('#hidden_user_id').val("");
                    $('#paypal_ammount').val("");
                    $('#cancel_return').val("");
                    window.location.reload();
                  },
                  error:function(error){
                    console.log(error);
                  }
                });
             }
          else{
            return false;
          }
      });
    });
</script>

<!-- Cancel Payment End -->  

<!-- Plans Change Script Start --> 

<script type="text/javascript">
  $(document).ready(function(){
    $('body').on('change','.site_plans',function(){
      var plan_id = $('.site_plans').val();
      var plan_user_id = plan_id + "-" + user_last_id;
      $('#paypal_payemnt').val(plan_user_id);
      $.ajax({
              url:"<?php echo base_url(); ?>user/getPlanData",
              type:"post",
              data:{plan_id:plan_id},
              success: function(response){
                var result = JSON.parse(response);
                $('#item_name').val(result[0]['plan_title']);
                $('#item_number').val(result[0]['description']);
                $('#paypal_ammount').val(result[0]['price']);
              },
              error:function(error){
                console.log(error);
              }
        });
    });
  });
</script>

<!-- Plans Change Script End --> 

<!-- PT Login Script Start -->

<script type="text/javascript">
  $(document).ready(function(){
    $('body').on('click','#login-btn',function(){
      $('#success_msg, #error_msg, #loading-img').hide();
      $('#login-btn').attr('disabled',false).text('Sign in');
      var email = $('#inputEmail').val();
      var password = $('#inputPassword').val();

      $('#login_body #loading-img').show();
      $('#login-btn').attr('disabled',true).text('loading....');
      $.ajax({
            url : "<?php echo base_url();  ?>user/login",
            type: "post",
            data: {email:email,password:password},
            success:function(response){
              $('#login_body #loading-img').hide();
              var result = JSON.parse(response);
              switch (result.msg) {
                  case "invalid":
                      notify('error','<i class="fa fa-times"> Error ! </i>','Invalid email-id or password !');
                      break;
                  case "trainee":
                      notify('error','<i class="fa fa-times"> Error ! </i>','Client can`t login here !');
                      break;
                  case "pending payment":
                      notify('error','<i class="fa fa-times"> Error ! </i>','Your payment is still pending please wait for admin approval !');
                      break;
                  case "failed payment":
                      notify('error','<i class="fa fa-times"> Error ! </i>','Your payment was failed !');
                      break;
                  case "expiry":
                      notify('error','<i class="fa fa-times"> Error ! </i>','Your plan has been expired !');
                      break;
                  case "not_start":
                      notify('error','<i class="fa fa-times"> Error ! </i>','Your plan start from '+result.start_date);
                      break;
                  case "success":
                      notify('success','<i class="fa fa-check"> Success </i>','Login Successfully please wait..');
                      window.location.href = "<?php echo base_url(); ?>user/notification";
                      break;
                  default :
                      notify('error','<i class="fa fa-times"> Error ! </i>','Something is Wrong Please Try Again !');
              }
              $('#login-btn').attr('disabled',false).text('Sign in');
              console.log(result);
            },
            error:function(error){
              $('#login_body #loading-img').hide();
              notify('error','<i class="fa fa-times"> Error ! </i>','Something is Wrong Please Try Again !');
              $('#login-btn').attr('disabled',false).text('Sign in');
              console.log(error);
            }
        });
      });

    $('body').on('keyup','#inputEmail, #inputPassword',function(event){
       if(event.keyCode == 13){
          $("#login-btn").trigger("click");
        }
    });
  });
</script>

<!-- PT Login Script End -->

<!-- Client Login Script Start -->

<script type="text/javascript">
  $(document).ready(function(){
    $('body').on('click','#login-btn1',function(){
      $('#success_msg, #error_msg, #loading-img').hide();
      $('#login-btn1').attr('disabled',false).text('Sign in');
      var email = $('#inputEmail1').val();
      var password = $('#inputPassword1').val();
      $('#login_body1 #loading-img').show();
      $('#login-btn1').attr('disabled',true).text('loading....');
      $.ajax({
            url : "<?php echo base_url();  ?>user/login1",
            type: "post",
            data: {email:email,password:password},
            success:function(response){
              $('#login_body1 #loading-img').hide();
              var result = JSON.parse(response);
              switch (result.msg) {
                  case "invalid":
                      notify('error','<i class="fa fa-times"> Error ! </i>','Invalid email-id or password !');
                      break;
                  case "trainer":
                      notify('error','<i class="fa fa-times"> Error ! </i>','PT can`t login here !');
                      break;
                  case "success":
                      notify('success','<i class="fa fa-check"> Success </i>','Login Successfully please wait..');
                      window.location.href ="<?php echo base_url(); ?>user/profiles";
                      break;
                  default :
                      notify('error','<i class="fa fa-times"> Error ! </i>','Something is Wrong Please Try Again !');
              }
              $('#login-btn1').attr('disabled',false).text('Sign in');
              console.log(result);
            },
            error:function(error){
              $('#login_body1 #loading-img').hide();
              notify('error','<i class="fa fa-times"> Error ! </i>','Something is Wrong Please Try Again !');
              $('#login-btn1').attr('disabled',false).text('Sign in');
              console.log(error);
            }
        });
      });

    $('body').on('keyup','#inputEmail1, #inputPassword1',function(event){
       if(event.keyCode == 13){
          $("#login-btn1").trigger("click");
        }
    });
  });
$( ".like_btn" ).click(function() {
  var data_val = $(this).text();
  if(data_val =='Like'){
  var data_like = $(this).attr('main');
      $(this).text('');
      $(this).text('Liked');
   $.ajax({
            url : "<?php echo base_url();  ?>user/like_article",
            type: "post",
            data: {article_id:data_like},
            success:function(response){
              $(this).text('Liked');
            }
          });
    }
  });

$( "#comment_btn" ).click(function() {
  var comment_data = $('#comment_text').val();
  var value = $('#comment_form').serialize();
  if(comment_data !=''){
   $.ajax({
            url : "<?php echo base_url();?>user/comment_article",
            type: "post",
            data: value,
            success:function(response){
              window.location.reload();
            },
            errors:function(error){
              notify('error','<i class="fa fa-times"> Error ! </i>','Something is Wrong Please Try Again !');
            }
          });
   return false;
  }
  });
  
  $("#comment").click(function(){
    $(".comment_box").fadeToggle(300);
});

// filter product
   $(".subCategory").click(function(){
    var searchKey="";
    var len=$(".subCategory").each(function(){
       var sThisVal = (this.checked ? $(this).val() : "");
        if(sThisVal!="")
        {
           if(searchKey==""){
               searchKey=sThisVal;
           }
           else{
               searchKey+=","+sThisVal;
           }
        }
    });
    
   found_total_product(searchKey);

    $.ajax({
            url : "<?php echo base_url();?>product/search_product",
            type: "post",
            data: "key="+searchKey,
            success:function(response){
            
             $('.Product_list').html(response);
            },
            errors:function(error){
              alert(error);

              notify('error','<i class="fa fa-times"> Error ! </i>','Something is Wrong Please Try Again !');
            }
          });
   
});
//
function found_total_product(searchKey)
{
   
     $.ajax({
            url : "<?php echo base_url();?>product/get_total_product",
            type: "post",
            data: "key="+searchKey,
            success:function(response){
            
             $('.product_count').html(response);
            },
            errors:function(error){
            
            }
          });



}


$(".Categorydiv").click(function(){
 
  
       $(this).find('.Category').click();
   

});



$(".Category").click(function(){

var sThisVal = (this.checked ? 1 : 0);

if(sThisVal==1)
{
  $(this).parent('label').parent('div').children('.sub_cat_div').show();
  $(this).parent('label').parent('div').children('div').children('i').attr('class','fa fa-minus');
}
else{
  $(this).parent('label').parent('div').children('.sub_cat_div').hide();
  $(this).parent('label').parent('div').children('div').children('i').attr('class','fa fa-plus');
  $(this).parent('label').parent('div').children('.sub_cat_div').children('div').children('i').childern('.subCategory').prop('checked', false);
}

});

$(window).scroll(function(e) {

var wheight=$('.product_list').height();
var wheight1=wheight-10;
var wheight2=wheight-30;
var ss=$(document).scrollTop();
var top=document.documentElement.clientHeight + $(document).scrollTop();
top=parseInt(top)-parseInt(855);
//alert(wheight+'='+top);
//top  >= wheight2 && top  <= wheight1
// $('#top').val('top ' + top);
// $('#wheight').val('wheight ' + wheight);
if ( top >= wheight || top >= (wheight-1))
 
    { 
      // $('#ifelse').val('if');
      var searchKey="";
      $(".subCategory").each(function(){
        var sThisVal = (this.checked ? $(this).val() : "");
        if(sThisVal!="")
        {
           if(searchKey==""){
               searchKey=sThisVal;
           }
           else{
               searchKey+=","+sThisVal;
           }
        }
      });
   
      var offset=$('#offset').val();
      var totalproduct=$('#total_product').val();

      if(parseInt(totalproduct) <= parseInt(offset))
      {
        
            return false;
      }
      $('.load_more').show();
      $.ajax({
            url: "<?php echo base_url();?>product/search_product",
            type: "post",
            data: "key="+searchKey+"&offset="+offset,
            success: function(data){
                  
                  $('.load_more').hide();
                  offset=parseInt(offset)+parseInt(30);
                  if(parseInt(totalproduct) <= parseInt(offset))
                  {
                     data+='<div class="row subs_btn">No More Product.. </div>';
                  }

                 
                   $('#offset').val(offset); 
                  $('.Product_list').append(data);
                 
             
             
            },
            error: function(){}           
        });
      
    }
    else{
      // $('#ifelse').val('else');
    }
});




$(".ad_cart").click(function(){

  var pid=$(this).attr('pid');
  var price=$(this).attr('price');
  var qty=$('.product_qty').val();
  $.ajax({
     url:"<?php echo base_url();?>product/add_cart/"+pid+"/"+qty+"/"+price,
     type:"post",
     data:"",
     success:function(data){
       show_cart_last_item(pid,qty);
       show_cart_toatl();
       $('.cart_count').html(data);
      },
     error:function(){} 

  })

});

function show_cart_toatl()
{

    $.ajax({
     url:"<?php echo base_url();?>product/show_cart_total/",
     type:"post",
     data:"",
     success:function(data){
       //$('.cart_total').html(data);
       },
     error:function(){} 

  });


}

function show_cart_last_item(pid,qty)
{
     $.ajax({
     url:"<?php echo base_url();?>product/show_cart_item/"+pid+"/"+qty,
     type:"post",
     data:"",
     success:function(data){

     
      $('#cart_modal').modal('show');
      $('.cart_body').html(data);
       
     },
     error:function(){} 

  })

}

$('body').on('keyup','.search_product',function(){
  var searchkey=$(this).val().trim().toLowerCase();
  $('body .product').each(function(){

    var brand=$(this).find('.product_brand').html().toLowerCase();
    var title=$(this).find('.product_name').html().toLowerCase();
    // alert(brand+'-'+title);
    if(searchkey!="")
    {
        var bn = brand.search(searchkey); 
        var tn = title.search(searchkey); 
         if(bn>0  || tn>0) {
              $(this).show();  
         }
         else{
           $(this).hide();  
         }
    }
    else{
       $(this).show();  
    }
     
   


  });

});

/*$('body').on('keyup','.search_product',function(){
  var searchkey = $(this).val().trim();
});*/

$("select[name^=cart_qty]").change(function(){

  var cart_qty=$(this).val();
  var ind=$(this).attr('ind');
  var price=$('.cart_product_price'+ind).html();
  var rowcost=price*cart_qty;
  $('.cart_row_product_price'+ind).html(rowcost.toFixed(2));
  var grand_total=0;
  $('.cart_row_sum').each(function(){
     var this_one=$(this).html();
     grand_total=parseFloat(grand_total)+parseFloat(this_one);
     // var this_val=this_one.toFixed(2);
     // alert(this_one);
     // var grand_total=grand_total.toFixed(2);
     // alert(this_val+'-'+grand_total);
     // grand_total=this_val+grand_total.toFixed(2);
  });
  $('.grand_total').html(grand_total);
});





</script>

<!-- Client Login Script End -->

<!-- Save user player id for push notifications start -->

  <script src="https://cdn.onesignal.com/sdks/OneSignalSDK.js" async='async'></script>
  <script>
    var user = "<?php echo $this->session->userdata('user_type'); ?>";
    if(user != ''){
      var OneSignal = window.OneSignal || [];
      OneSignal.push(function() {
      OneSignal.on('subscriptionChange', function(isSubscribed) {
        if (isSubscribed) {
          OneSignal.getUserId( function(userId) {
            console.log('User-id',userId);
          });
          }
        });
      });
      OneSignal.push(["init", {
        appId: "8ead9473-f4de-4493-b123-cd8b30a8d773", // One signal App Id
        autoRegister: false, 
        persistNotification: false,
        subdomainName: 'mwdemoserver',   
        welcomeNotification: { 
            "title": "The PT App",
            "message": "Thanks for subscribing! Welcome to PT App",
        },
        notifyButton: {
            enable: true, 
            displayPredicate: function() {
              return OneSignal.isPushNotificationsEnabled()
                .then(function(isPushEnabled) {
                    return !isPushEnabled;
                });
            },
        },
        promptOptions: {
            siteName: 'http://www.theptapp.com',
            actionMessage: "The PT App",
            exampleNotificationTitle: 'The PT App push notifications',
            exampleNotificationMessage: 'Thanks for subscribing',
            exampleNotificationCaption: 'You can unsubscribe anytime',
            acceptButtonText: "ALLOW",
            cancelButtonText: "NO THANKS"
        }
      }]);
    }
  </script>

<!-- Save user player id for push notifications end  -->


