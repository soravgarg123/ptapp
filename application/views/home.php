 <!--Header sec start-->
    <header class="header_sect" id="header">
    <div class="clearfix"></div>  
         
    <div class="slider_wrapper">
        <div id="home_slider" class="owl-carusel">
        <?php if(!empty($banner)) {
            foreach($banner as $b){
                ?>
                  <div class="item"><img class="img-responsive" src="<?php echo base_url().'images/'.$b['image']; ?>" alt=""/></div>
                <?php
            }
            }?>
        </div>
        
        </div>  
        </header>
<!--Header sec end-->
<!--Main container sec start-->
    <div class="service_section1">
    
        <div class="container">
            <div class="row">
            
                <div class="title_sect1">
                    <h3>Why <span>Choose Us</span></h3>
                    <span class="title1_strip"></span>
                </div>
                
                <div class="clearfix"></div>
                <?php
                    if(isset($why_us))
                    {
                        foreach($why_us as $a)
                        { ?>
                     <div class="col-sm-4">
                    <div class="icon_block_section">
                        <span class="icon_block">
                            <img src="<?php echo base_url().'images/'.$a['image']; ?>" width="88" height="70"/>
                        </span>
                        <h3><?php echo $a['heading']; ?></h3>
                        <p> <?php echo $a['text']; ?> </p>
                    </div>
                </div>

                <?php   }
                    }
                 ?>
            </div>
            
        </div>
        
    </div>
    <div class="clearfix"></div>
    
    <!-- <div class="service_section2">
            <div class="container">
                <div class="row" id="services">
                    <div class="title_sect2">
                        <h3>some of our <span>services</span></h3>
                        <span class="title1_strip2"></span>
                    </div>
                    <div class="clearfix"></div>
                     <div class="col-sm-6">
                    <?php 
                    $i = 1;
                    foreach ($services as $service) {
                        

                        if($i == '1'){
                        ?>
                        <div class="services_block1">
                            <div class="services_block_img"><a href="<?php echo base_url();?>user/service?id=<?php echo $service['id'];?>"><img class="img-responsive" src="<?php echo base_url(); ?>upload/<?php echo $service['image'];?>" alt=""/></a></div>
                            <div class="services_block_cont">
                                <h3><a href="<?php echo base_url();?>user/service?id=<?php echo $service['id'];?>"><?php echo $service['service_title'];?></a></h3>
                                <p><?php 
                                $description = $service['description'];
                                echo substr($description,0,200);?></p>
                                <a class="readmore_btn" href="<?php echo base_url();?>user/service?id=<?php echo $service['id'];?>">Read more <span class="flaticon-arrow487"></span></a>
                            </div>
                        </div>
                        
                        <div class="clearfix"></div>
                        <?php } 
                        if($i == '2'){
                        ?>
                        <div class="services_block2 margin_right1">
                            <div class="services_block_img"><a href="<?php echo base_url();?>user/service?id=<?php echo $service['id'];?>"><img class="img-responsive" src="<?php echo base_url(); ?>upload/<?php echo $service['image'];?>" alt=""/></a></div>
                            <div class="services_block_cont">
                                <h3><a href="<?php echo base_url();?>user/service?id=<?php echo $service['id'];?>"><?php echo $service['service_title'];?></a></h3>
                                <p><?php 
                                $description = $service['description'];
                                echo substr($description,0,200);?></p>
                                <a class="readmore_btn" href="<?php echo base_url();?>user/service?id=<?php echo $service['id'];?>">Read more <span class="flaticon-arrow487"></span></a>
                            </div>
                        </div>
                        <?php  } 
                        
                        if($i == '3'){
                        ?>
                        <div class="services_block2 margin_left1">
                            <div class="services_block_img"><a href="<?php echo base_url();?>user/service?id=<?php echo $service['id'];?>"><img class="img-responsive" src="<?php echo base_url(); ?>upload/<?php echo $service['image'];?>" alt=""/></a></div>
                            <div class="services_block_cont">
                                <h3><a href="<?php echo base_url();?>user/service?id=<?php echo $service['id'];?>"><?php echo $service['service_title'];?></a></h3>
                                <p><?php 
                                $description = $service['description'];
                                echo substr($description,0,200);?></p>
                                <a class="readmore_btn" href="<?php echo base_url();?>user/service?id=<?php echo $service['id'];?>">Read more <span class="flaticon-arrow487"></span></a>
                            </div>
                        </div>
                     <?php  } ?>
                   
                    <?php  $i++; } ?>
                     </div>
                    <div class="col-sm-6">
                        <?php 
                         $j=1;
                        foreach ($abc as $xyz) {

                           
                           if($j == '4'){
                      ?>
                        <div class="services_block2">
                            <div class="services_block_img"><a href="<?php echo base_url();?>user/service?id=<?php echo $xyz['id'];?>"><img class="img-responsive" src="<?php echo base_url(); ?>upload/<?php echo $xyz['image'];?>" alt=""/></a></div>
                            <div class="services_block_cont">
                                <h3><a href="<?php echo base_url();?>user/service?id=<?php echo $xyz['id'];?>"><?php echo $xyz['service_title'];?></a></h3>
                                <p><?php
                                $description = $xyz['description'];
                                echo substr($description,0,200);?></p>
                                <a class="readmore_btn" href="<?php echo base_url();?>user/service?id=<?php echo $xyz['id'];?>">Read more <span class="flaticon-arrow487"></span></a>
                            </div>
                        </div>
                        <?php }
                           if($j == '5'){

                        ?>
                        <div class="services_block2 margin_left1">
                            <div class="services_block_img"><a href="<?php echo base_url();?>user/service?id=<?php echo $xyz['id'];?>"><img class="img-responsive" src="<?php echo base_url(); ?>upload/<?php echo $xyz['image'];?>" alt=""/></a></div>
                            <div class="services_block_cont">
                                <h3><a href="<?php echo base_url();?>user/service?id=<?php echo $xyz['id'];?>"><?php echo $xyz['service_title'];?></a></h3>
                                <p><?php
                                $description = $xyz['description'];
                                echo substr($description,0,200);?></p>
                                <a class="readmore_btn" href="<?php echo base_url();?>user/service?id=<?php echo $xyz['id'];?>">Read more <span class="flaticon-arrow487"></span></a>
                            </div>
                        </div>
                      <?php } ?>
                       <?php
                           if($j == '6'){

                        ?>
                        <div class="services_block1">
                            <div class="services_block_img"><a href="<?php echo base_url();?>user/service?id=<?php echo $xyz['id'];?>"><img class="img-responsive" src="<?php echo base_url(); ?>upload/<?php echo $xyz['image'];?>" alt=""/></a></div>
                            <div class="services_block_cont">
                                <h3><a href="<?php echo base_url();?>user/service?id=<?php echo $xyz['id'];?>"><?php echo $xyz['service_title'];?></a></h3>
                                <p><?php
                                $description = $xyz['description'];
                                echo substr($description,0,230);?></p>
                                <a class="readmore_btn" href="<?php echo base_url();?>user/service?id=<?php echo $xyz['id'];?>">Read more <span class="flaticon-arrow487"></span></a>
                            </div>
                        </div>
                         <?php } ?>
                        <?php  $j++; } ?>
                        
                    
                    </div>


                   
                   
                </div>
            </div>
    </div> -->
    
    <div class="clearfix"></div>
    <div class="clearfix"></div>
    
    <div class="section4" style="background-image:url('images/<?php echo $my_story[0]['image_name']; ?>')">
        <div class="container">
            <div class="row">
                
                <div class="title_sect2">
                    <h3>What <span>People Are Saying</span></h3>
                    <span class="title1_strip2"></span>
                </div>  
                
                <div class="testimonial_sect">
                        <div id="testimon_carousl" class="owl-carusel">
<!-- /////////////////////// Code Written by Aditya ////////////////////////////////// !-->                            
                            <?php if(isset($people_say)){ 
                                foreach($people_say as $a)
                                { ?>
                            <div class="testimonial_block">
                                <div class="testimonial_title">
                                    <h3>
                                        <span>Personal Trainer</span>
                                        <?php echo $a['name']; ?>
                                    </h3>
                                </div>
                                <div class="testimonial_cont">
                                 <?php echo $a['what_say']; ?>   
                                </div>
                            </div>
                               <?php }
                                ?>

                            <?php } ?>
<!-- /////////////////////// End of Code block (Written by Aditya) ////////////////////////////////// !-->
                        </div>
                </div>
                
                    
            </div>
        </div>
    </div>
    
    <div class="clearfix"></div>
    
   <!--  <div class="section5">
        <div class="container">
            <div class="row">
                
                <div class="title_sect1">
                    <h3>Our <span>Plan</span></h3>
                    <span class="title1_strip"></span>
                </div>  
                <div class="clearfix"></div>
                <div class="plan_div">
                <?php foreach ($plans as $plan){
                    
               ?> -->
                
                    <!--<div class="plan_block">
                        <h4>&pound;<?php echo $plan['price'];?></h4>
                        <p><?php echo $plan['plan_title'];?></p>
                        <?php
                        $user_id = $this->session->userdata('user_id');
                        if($user_id == ""){ ?>
                        <p><a data-target="#login_modal" data-toggle="modal" href="#">Make Payment</a></p>
                        <?php }else{ ?>
                         <p> <a href="#" class="payment" plan_id="<?php echo $plan['id']; ?>" plan_time="<?php echo $plan['time']; ?>" plan_title="<?php echo $plan['plan_title']; ?>" plan_price="<?php echo $plan['price']; ?>">Make Payment</a> </p>
                        <?php } ?>
                    </div>-->
                   <!--  <div class="plan_block">
                       <h2><?php echo $plan['plan_title'];?></h2>
                      <div class="plan_block_head">
                         <h1> &pound;<?php echo $plan['price'];?> <span>  </span></h1>
                      </div>
                      <div class="plan_block_content">
                        <p><?php echo $plan['description'];?></p> -->
                        <!-- <p>778878347</p> -->
              <!--         </div>
                      <button type="button" data-toggle="modal" data-target="#register_modal" data-keyboard="false" data-backdrop="static">sign up</button>
                    </div>
                    
                
                
                <?php  } ?>
                <div class="plan_block basic">
                       <h2><?php echo $plan['plan_title'];?></h2>
                       <p>from 5$ a month</p>
                      <div class="plan_block_head">
                         <div class="icon_drop"><i class="fa fa-dropbox" aria-hidden="true"></i></div>
                      </div>
                      <div class="plan_block_content">
                        <p><?php echo $plan['description'];?></p>
                        <!-- <p>778878347</p> -->
                  <!--     </div>
                      
                    </div>
                <div class="clearfix"></div>
                <div class="plan_block pro">
                       <h2><?php echo $plan['plan_title'];?></h2>
                       <p>your current plan</p>
                      <div class="plan_block_head">
                         <div class="icon_drop"><i class="fa fa-dropbox" aria-hidden="true"></i></div>
                      </div>
                      <div class="plan_block_content">
                        <p><?php echo $plan['description'];?></p>
                      </div>
                      <button type="button" data-toggle="modal" data-target="#register_modal" data-keyboard="false" data-backdrop="static">upgrade to pro</button>
                    </div>
                <div class="clearfix"></div>
                    </div>
            </div>
        </div>
    </div> -->
    
    <!--Main container sec end-->
    
    <div class="clearfix"></div>
<!-- /////////////////form for paypal payment details start here////////////// !-->
<?php if($this->session->userdata('user_type') == "trainner"){ ?>
<div class="modal fade" id="payment" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
          <div class="modal-dialog" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Plan Details</h4>
              </div>
              <div class="modal-body">
                <form method="post" action="https://www.sandbox.paypal.com/cgi-bin/webscr" >
                 <input type="hidden" name="cmd" value="_xclick" />
                  <input type="hidden" name="charset" value="utf-8" class="form-control" />
                  <input type="hidden" id="app_date" name="date" value="_xclick" class="form-control" />
                   <input type="hidden" name="business" value="aadidubey7-facilitator@gmail.com" class="form-control" />
                    <input type="hidden"  name="item_name" value="" class="form-control" />
                   <input type="hidden" name="item_number" value="BEAR05" class="form-control" />
                    <input type="hidden" name="currency_code" value="USD" class="form-control" />
                    <input type="hidden" class="plan_ammount" name="amount" value="" />
                    <input type="hidden" name="custom" value="" class="form-control" />
                    <input type="hidden" name="time" value="" class="form-control" />
                     <input type="hidden" name="return" value="<?php echo base_url(); ?>user/payment_success" />
                    <input type="hidden"  name="cancel_return" value="<?php echo base_url(); ?>user/index" class="form-control" />
                     <input type="hidden"  name="bn" value="Business_BuyNow_WPS_SE" class="form-control" />
                     <!--  <input type="image" src="https://www.paypal.com/en_US/i/btn/btn_buynowCC_LG.gif" name="submit" alt="Buy Now" /> -->
                
                   <div class="col-md-12">                        
                      <div class="col-md-4">Plan Name</div>
                      <div class="col-md-8">
                        <input type="text" readonly name="item_name" value="" class="form-control" />
                      </div>
                  </div></br></br>

                  <div class="col-md-12">                        
                    <div class="col-md-4"> Price</div>
                    <div class="col-md-8">
                      <input type="text" readonly name="amount" value="" class="form-control plan_ammount" />
                    </div>
                </div></br></br>


                  <div class="col-md-12">                        
                    <div class="col-md-4"> Time</div>
                    <div class="col-md-8">
                      <input type="text" readonly name="time" value="" class="form-control" />
                    </div>
                </div></br></br>

               </div></br>
              <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="submit" id="submit-btn" class="btn btn-primary">Submit</button>
              </div> 
              </form>
            </div>
          </div>
        </div>
<?php } ?>

       

<!-- /////////////////form for paypal payment details end here////////////// !-->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
  <script>
  // $(document).ready(function(){
    
    setTimeout(function(){
        $('#success').hide();
    }, 3000);
    setTimeout(function(){
        $('#failure').hide();
    }, 3000);
  // });
  </script>    
  <script type="text/javascript">
  $(document).ready(function(){
    setTimeout(function(){
        $('.owl-dots').remove();
    }, 1000);
  });
  </script>
        
  
