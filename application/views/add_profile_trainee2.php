<div role="tabpanel" class="tab-pane" id="password">
<div id="message">All field are required.</div>
<form id="form2" method="post">
                                <h3 class="trai_title_sect">Body Information</h3>
                                <div class="form_wrapper">
                                    <div class="form-group">
                                        <label>Bicep</label>
                                        <input type="text" autofocus placeholder="Bicep" class="form-control" id="bicep" name="bicep" autocomplete="off" maxlength="3" value="<?php echo $res["bicep"];?>">
                                       
                                    </div>
                                    <div class="form-group">
                                        <label>Tricep</label>
                                        <input type="text" autofocus placeholder="Tricep" class="form-control" id="tricep" name="tricep" autocomplete="off" maxlength="3" value="<?php echo $res["tricep"];?>">
                                      
                                    </div>
                                    <div class="form-group">
                                            <label>Abs</label>
                                             <input type="text" placeholder="Abs" class="form-control" id="abs" name="abs" autocomplete="off" value="<?php echo $res["abs"];?>">
                                             
                                            
                                    </div>
                                    <div class="form-group">
                                            <label>Subscapular</label>
                                            <input type="text" placeholder="Subscapular" class="form-control" id="subscapular" name="subscapular" autocomplete="off" value="<?php echo $res["subscapular"];?>">
                                            
                                    </div>
                                    <div class="form-group">
                                            <label>Chin</label>
                                            <input type="text" placeholder="Chin" class="form-control" id="chin" name="chin" autocomplete="off" value="<?php echo $res["chin"];?>" maxlength="3">
                                            
                                    </div>
                                    
                                    <div class="form-group">
                                            <label>Cheek</label>
                                            <input type="text" placeholder="Cheek" class="form-control" id="cheek" name="cheek" autocomplete="off" value="<?php echo $res["cheek"];?>" maxlength="3">
                                            
                                    </div>
                                    <div class="form-group">
                                            <label>Pectoral</label>
                                            <input type="text" placeholder="Pectoral" class="form-control" id="pectoral" name="pectoral" autocomplete="off" value="<?php echo $res["pectoral"];?>" maxlength="3">
                                            
                                    </div>
                                    <div class="form-group">
                                            <label>Suprailliac</label>
                                            <input type="text" placeholder="Suprailliac" class="form-control" id="suprailliac" name="Suprailliac" autocomplete="off" value="<?php echo $res["Suprailliac"];?>" maxlength="3">
                                            
                                    </div>
                                    <div class="form-group">
                                            <label>Mid-axillary</label>
                                            <input type="text" placeholder="Mid-axillary" class="form-control" id="mid-axillary" name="mid-axillary" autocomplete="off" value="<?php echo $res["mid-axillary"];?>" maxlength="3">
                                            
                                    </div>
                                    <div class="form-group">
                                            <label>Arm(Left)</label>
                                            <input type="text" placeholder="Arm(Left)" class="form-control" id="arm" name="arm" autocomplete="off" value="<?php echo $res["arm"];?>" maxlength="3">
                                    </div>
                                    <div class="form-group">
                                            <label>Right</label>
                                            <input type="text" placeholder="Arm(Right)" class="form-control" id="arm_right" name="arm_right" autocomplete="off" value="<?php echo $res["arm_right"];?>" maxlength="3">
                                    </div>
                                    <div class="form-group">
                                            <label>Chest(Left)</label>
                                            <input type="text" placeholder="Chest(Left)" class="form-control" id="chest" name="chest" autocomplete="off" value="<?php echo $res["chest"];?>" maxlength="3">
                                            
                                    </div>
                                    <div class="form-group">
                                            <label>Right</label>
                                            <input type="text" placeholder="Chest(Right)" class="form-control" id="chest_right" name="chest_right" autocomplete="off" value="<?php echo $res["chest_right"];?>" maxlength="3">
                                    </div>
                                    <div class="form-group">
                                            <label>Waist(Left)</label>
                                            <input type="text" placeholder="Waist(Left)" class="form-control" id="waist" name="waist" autocomplete="off" value="<?php echo $res["waist"];?>" maxlength="3">
                                            
                                    </div>
                                    <div class="form-group">
                                            <label>Right</label>
                                            <input type="text" placeholder="Waist(Right)" class="form-control" id="waist_right" name="waist_right" autocomplete="off" value="<?php echo $res["waist_right"];?>" maxlength="3">
                                    </div>
                                    <div class="form-group">
                                            <label>Hips(Left)</label>
                                            <input type="text" placeholder="Hips(Left)" class="form-control" id="hips" name="hips" autocomplete="off" value="<?php echo $res["hips"];?>" maxlength="3">
                                            
                                    </div>
                                    <div class="form-group">
                                            <label>Right</label>
                                            <input type="text" placeholder="Hips(Right)" class="form-control" id="hips_right" name="hips_right" autocomplete="off" value="<?php echo $res["hips_right"];?>" maxlength="3">
                                    </div>
                                    <div class="form-group">
                                            <label>Thigh(Left)</label>
                                            <input type="text" placeholder="Thigh(Left)" class="form-control" id="thigh" name="thigh" autocomplete="off" value="<?php echo $res["thigh"];?>" maxlength="3">
                                            
                                    </div>
                                    <div class="form-group">
                                            <label>Right</label>
                                            <input type="text" placeholder="Thigh(Right)" class="form-control" id="thigh_right" name="thigh_right" autocomplete="off" value="<?php echo $res["thigh_right"];?>" maxlength="3">
                                    </div>
                                    <div class="form-group">
                                            <label>Calf(Left)</label>
                                            <input type="text" placeholder="Calf(Left)" class="form-control" id="calf" name="calf" autocomplete="off" value="<?php echo $res["calf"];?>" maxlength="3">
                                            
                                    </div>
                                    <div class="form-group">
                                            <label>Right</label>
                                            <input type="text" placeholder="Calf(Right)" class="form-control" id="calf_right" name="calf_right" autocomplete="off" value="<?php echo $res["calf_right"];?>" maxlength="3">
                                    </div>
                               </div>
                                <h3 class="trai_title_sect">Other Information</h3>                           
                                <div class="form_wrapper">
                                     <div class="form-group">
                                            <label>Umbilical</label>
                                            <input type="text" placeholder="Umbilical" class="form-control" id="umbilical" name="umbilical" autocomplete="off" value="" maxlength="3">
                                            
                                    </div>
                                    <div class="form-group">
                                            <label>Knee</label>
                                            <input type="text" placeholder="Knee" class="form-control" id="knee" name="knee" autocomplete="off" value="" maxlength="3">
                                            
                                    </div>
                                    <div class="form-group">
                                            <label>Medial Calf</label>
                                            <input type="text" placeholder="Medial Calf" class="form-control" id="medial-calf" name="medial-calf" autocomplete="off" value="" maxlength="3">
                                           
                                    </div>
                                    <div class="form-group">
                                            <label>Quadriceps</label>
                                            <input type="text" class="form-control" id="quadriceps" name="quadriceps" value = "">
                                    </div>
                                    <div class="form-group">
                                            <label>Hamstrings</label>
                                            <input type="text" class="form-control" id="hamstrings" name="hamstrings" value = "">
                                    </div>  
                                      <div class="form-group">
                                            <label>Heading</label>
                                            <input type="text" class="form-control" id="heading" name="heading" value = "">
                                    </div> 
                                </div>
                               <button type="button" id="back1" class="btn submit_btn">Back</button>
                               <button type="button" id="nextform2" class="btn submit_btn">Next</button>                                   
                               </form>
                            </div>
                            
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
 <script type="text/javascript">
          $(document).ready(function(){
            $('#back1').click(function(){
                  $.ajax({
                  url:"<?php echo base_url() ?>user/trainee_profile_back",
                  type:"post",
                  //data:data1, 
                  success: function(response)
                  {                       
                     $("#one").addClass("active");
                     $("#two").removeClass("active");
                     $("#informaiton").html(response);
                     
                  }

              });
              
            });
          });
 </script>
    <script type="text/javascript">
    $(document).ready(function(){
        $(".tab-pane").on('click','#nextform2',function() {
          $('html, body').animate({ scrollTop: $(".inner_head_sect").offset().top }, 1000);
        var skinfolds = $('#skinfolds').val();
        var bicep = $('#bicep').val();  
        var tricep = $('#tricep').val();
        var abs = $('#abs').val();
        var subscapular = $('#subscapular').val();  
        var chin = $('#chin').val();
        var cheek = $('#cheek').val();
        var pectoral = $('#pectoral').val();
        var suprailliac = $('#suprailliac').val();
        var midaxillary = $('#mid-axillary').val();
       
        
        var data1 = new FormData($('#form2')[0]);

          $.ajax({
            url:"<?php echo base_url() ?>user/add_trainee3",
            type:"post",
            data: data1,
            contentType: false,
            processData: false,
            success: function(response)
            {
                $("#two").removeClass("active");
                $("#three").addClass("active");
                $("#password").html(response);
                
              
            }
        });
    
      });
    });
</script>
