<?php $this->load->view('baneer_section'); ?>
        	<div class="container">
                	
                    <div class="trainer_search">
                     <!--  <form> -->
                        <div class="form-group">
                          <div class="input-group">
                            <input type="text" placeholder="Search Trainer" class="form-control" id="search">
                            <div class="input-group-addon"> <span class="fa fa-search"></span> </div>
                          </div>
                        </div>
                       
                      <!-- </form> -->
                    </div>

                
            </div>
        </div>
        
    </header>
   
    <!--Header sec end-->
    <!--Main container sec start-->
    <div class="main_container">
    
    	<div class="container">
        	
            <div class="row">
            	
                <div class="col-sm-12">
                  <div class="ourtrainer_wrap">
                    <h3>Our Trainer</h3>
                    <ul id="search_result"></ul>
                    <ul class="row" id="row_search">
                    <?php 
                    foreach ($trainner_details as $trainner_detail) {
                     
                     $trainner_id = $trainner_detail['id'];                  
                      ?>
                      <li class="col-sm-4 col-md-3">
                        <div class="trainer_sec">
                          <div class="trainer_img"> 
                          <a href ="<?php echo base_url(); ?>user/trainner_profile?id=<?php echo $trainner_id;?>">
                          <img class="img-responsive" alt="trainer_img" src="<?php echo base_url(); ?>images/strainer1.jpg" alt=""></a>
                            
                          </div>
                          <div class="trainer_info">
                            <p><?php echo $trainner_detail['name'].' '.$trainner_detail['surname'];?></p>
                            <p><strong>Location : </strong> <?php echo $trainner_detail['country'];?></p>
                            <p><strong>Rank :</strong> <?php echo $trainner_detail['rank_level']?></p>
                          </div>
                        </div>
                      </li>
                      <?php } ?>                     
                    </ul>
                  </div>
                </div>

                
            </div>
            
        </div>
        
    </div>
    
    <!--Main container sec end-->
    
    <div class="clearfix"></div>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
     <script type="text/javascript">
     $(document).ready(function(){
       $(".trainer_search").on('keyup','#search',function() {
        
            var search = $('#search').val();
           
                $.ajax({
                  type: "post",
                  url: "<?php echo base_url(); ?>user/search_trainner_details",
                  data: "search="+search,
                  
                  success: function(data){

                            $('#row_search').hide();
                            $('#search_result').html(data);
                            if($('#search').val() == ""){

location.reload();

                            }
                            
                            
                  }
                });
            
        });
    });
</script>