      <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7/jquery.min.js">
</script>
<style type="text/css">
  .add-profit-btn {
    color: black;
    float: right;
    font-size: 22px;
    margin-right: 2%;
}
</style>
<?php $this->load->view('baneer_section'); ?>
          <div class="container">
                 
                
            </div>
        </div>
        
    </header>
    <!--Header sec end-->
    <!--Main container sec start-->
    <div class="main_container"><!--main_container-->
    
      <div class="container"><!--container-->
          
      <div class="row">
      <?php if($this->session->flashdata('success')) { ?>
          <script type="text/javascript">
              var msg = "<?php echo $this->session->flashdata('success'); ?>";
              notify('success','<i class="fa fa-check"> Success </i>',msg);
          </script>
      <?php } ?>

      <?php if($this->session->flashdata('failure')) { ?>
          <script type="text/javascript">
              var msg1 = "<?php echo $this->session->flashdata('failure'); ?>";
              notify('error','<i class="fa fa-times"> Error ! </i>',msg1);                      
           </script>
      <?php } ?>
            

               <div> <a href="<?php echo base_url('user/add_profit_loss'); ?>" class="add-profit-btn" ><i class="fa fa-plus-circle"></i></a></div>
                <div class="col-sm-12"><!--col-sm-9-->
                  <div class="trainee_tab_content">
                        <!-- Tab panes -->
                          <div class="tab-content">
                            <div role="tabpanel" class="tab-pane active" id="informaiton">
                              <h3 class="trai_title_sect">Profit and Loss</h3>
                              <div id="container" style="min-width: 310px; height: 400px; margin: 0 auto"></div>
                             <form method="post" action="">
                               <div class="panel panel_app">
                               <h3 class="trai_title_sect">Income</h3>
                                  <div class="panel-body">
                                  <table class="table table-striped table-bordered table-hover ">
                                    <thead>
                                      <tr>
                                        <!-- <th>S.No.</th> -->
                                        <th>name</th>
                                        <th>jan</th>
                                        <th>feb</th>
                                        <th>march</th>
                                        <th>april</th>
                                        <th>may</th>
                                        <th>june</th>
                                        <th>july</th>
                                        <th>aug</th>
                                        <th>sep</th>
                                        <th>oct</th>
                                        <th>nov</th>
                                        <th>dec</th>
                                        <th>total</th>
                                        <th>action</th>
                                       
                                      </tr>
                                    </thead>  

                                    <tbody>
                                      <?php if(!empty($pl)){
                                          $jan_in ='';
                                           $feb_in = '';
                                           $march_in = '';
                                           $april_in = '';
                                           $may_in ='';
                                           $june_in = '';
                                           $july_in = '';
                                           $aug_in ='';
                                           $sep_in ='';
                                           $oct_in ='';
                                           $nov_in ='';
                                           $dec_in ='';
                                           $total_in ='';
                                        $i =1;
                                          foreach($pl as $a)
                                          { 
                                           if($a['type'] == 'Income'){
                                           $jan_in += $a['jan'];
                                           $feb_in += $a['feb'];
                                           $march_in += $a['march'];
                                           $april_in += $a['april'];
                                           $may_in += $a['may'];
                                           $june_in += $a['june'];
                                           $july_in += $a['july'];
                                           $aug_in += $a['aug'];
                                           $sep_in += $a['sep'];
                                           $oct_in += $a['oct'];
                                           $nov_in += $a['nov'];
                                           $dec_in += $a['dec'];
                                           $total_in += $a['total'];
                                           ?>
                                            <tr>
                                              <!-- <td> <?php echo $i; ?> </td> -->
                                              <td> <?php echo $a['name']; ?> </td>
                                              <td> <?php  if($a['jan'] !==''){ echo '£'.$a['jan'];}else{ } ?> </td>
                                              <td> <?php if($a['feb'] !==''){ echo '£'.$a['feb'];}else{ } ?> </td>
                                              <td><?php if($a['march'] !==''){ echo '£'.$a['march'];}else{ } ?></td>
                                              <td><?php if($a['april'] !==''){ echo '£'.$a['april'];}else{ } ?></td>
                                              <td><?php if($a['may'] !==''){ echo '£'.$a['may'];}else{ } ?></td>
                                              <td><?php if($a['june'] !==''){ echo '£'.$a['june'];}else{ } ?></td>
                                              <td><?php if($a['july'] !==''){ echo '£'.$a['july'];}else{ } ?></td>
                                              <td><?php if($a['aug'] !==''){ echo '£'.$a['aug'];}else{ } ?></td>
                                              <td><?php if($a['sep'] !==''){ echo '£'.$a['sep'];}else{ } ?></td>
                                              <td><?php if($a['oct'] !==''){ echo '£'.$a['oct'];}else{ } ?></td>
                                              <td><?php if($a['nov'] !==''){ echo '£'.$a['nov'];}else{ } ?></td>
                                              <td><?php if($a['dec'] !==''){ echo '£'.$a['dec']; }else{ }?></td>
                                              <td><?php  echo '£'.$a['total']; ?></td>
                                              <td>
                                              <a href="<?php echo base_url(); ?>user/add_profit_loss/<?php echo $a['id']; ?>" class="btn"><i class="fa fa-pencil-square-o" aria-hidden="true"></i>
</a>
                                              <a href="<?php echo base_url(); ?>user/delete_profit_loss/<?php echo $a['id']; ?>" onclick='javascript:return confirm("Are you sure to Delete this Record?")' class="btn"><i class="fa fa-trash-o"></i></a>
                                              </td>
                                            </tr>
                                        <?php $i++; }  
                                        }?>
                                        <tr>
                                          <!-- <td></td> -->
                                              <td>total</td>
                                              <td><?php echo '£'.number_format($jan_in,2); ?></td>
                                              <td><?php echo '£'.number_format($feb_in,2); ?></td>
                                              <td><?php echo '£'.number_format($march_in,2); ?></td>
                                              <td><?php echo '£'.number_format($april_in,2); ?></td>
                                              <td><?php echo '£'.number_format($may_in,2); ?></td>
                                              <td><?php echo '£'.number_format($june_in,2); ?></td>
                                              <td><?php echo '£'.number_format($july_in,2); ?></td>
                                              <td><?php echo '£'.number_format($aug_in,2); ?></td>
                                              <td><?php echo '£'.number_format($sep_in,2); ?></td>
                                              <td><?php echo '£'.number_format($oct_in,2); ?></td>
                                              <td><?php echo '£'.number_format($nov_in,2); ?></td>
                                              <td><?php echo '£'.number_format($dec_in,2); ?></td>
                                              <td><?php echo '£'.number_format($total_in,2); ?></td>
                                              <td></td>
                                             
                                             </tr>
                                             <br/>
                                             <tr>
                                               <td colspan="16" class="text-success text-center">Total Income - <?php echo '£'.number_format($total_in,2); ?></td>
                                             </tr>
                                        <?php
                                         }else{ ?>
                                            <tr> 
                                              <td colspan="16" class="text-danger text-center"> No Records Found </td> 
                                            </tr>
                                         <?php
                                          } ?>
                                    </tbody>
                                  </table>
                                                  
                                  </div>
                                  <br/>

                                </div>
                                <div class="panel panel_app">
                                <h3 class="trai_title_sect">Expense</h3>
                                  <div class="panel-body">
                                  <table class="table table-striped table-bordered table-hover ">
                                    <thead>
                                      <tr>
                                        <!-- <th>S.No.</th> -->
                                        <th>name</th>
                                        <th>jan</th>
                                        <th>feb</th>
                                        <th>march</th>
                                        <th>april</th>
                                        <th>may</th>
                                        <th>june</th>
                                        <th>july</th>
                                        <th>aug</th>
                                        <th>sep</th>
                                        <th>oct</th>
                                        <th>nov</th>
                                        <th>dec</th>
                                        <th>total</th>
                                        <th>action</th>
                                       
                                      </tr>
                                    </thead>  

                                    <tbody>
                                      <?php if(!empty($pl)){
                                            $jan_ex ='';
                                             $jan_ex ='';
                                             $feb_ex ='';
                                             $march_ex ='';
                                             $april_ex ='';
                                             $may_ex ='';
                                             $june_ex ='';
                                             $july_ex ='';
                                             $aug_ex ='';
                                             $sep_ex ='';
                                             $oct_ex ='';
                                             $nov_ex ='';
                                             $dec_ex ='';
                                             $total_ex ='';
                                        $j =1;
                                          foreach($pl as $ex)
                                          { 
                                            
                                           if($ex['type'] == 'Expense'){
                                             $jan_ex += $ex['jan'];
                                             $feb_ex += $ex['feb'];
                                             $march_ex += $ex['march'];
                                             $april_ex += $ex['april'];
                                             $may_ex += $ex['may'];
                                             $june_ex += $ex['june'];
                                             $july_ex += $ex['july'];
                                             $aug_ex += $ex['aug'];
                                             $sep_ex += $ex['sep'];
                                             $oct_ex += $ex['oct'];
                                             $nov_ex += $ex['nov'];
                                             $dec_ex += $ex['dec'];
                                             $total_ex += $ex['total'];
                                            ?>
                                            <tr>
                                              <!-- <td> <?php echo $j; ?> </td> -->
                                              <td> <?php echo $ex['name']; ?> </td>
                                              <td> <?php if($ex['jan'] !==''){ echo '£'.$ex['jan'];}else{ } ?> </td>
                                              <td> <?php if($ex['feb'] !==''){ echo '£'.$ex['feb'];}else{ } ?> </td>
                                              <td><?php if($ex['march'] !==''){ echo '£'.$ex['march']; }else{ }?></td>
                                              <td><?php if($ex['april'] !==''){ echo '£'.$ex['april'];}else{ } ?></td>
                                              <td><?php if($ex['may'] !==''){ echo '£'.$ex['may'];}else{ } ?></td>
                                              <td><?php if($ex['june'] !==''){ echo '£'.$ex['june']; }else{ }?></td>
                                              <td><?php if($ex['july'] !==''){ echo '£'.$ex['july'];}else{ } ?></td>
                                              <td><?php  if($ex['aug'] !==''){echo '£'.$ex['aug'];}else{ } ?></td>
                                              <td><?php if($ex['sep'] !==''){ echo '£'.$ex['sep'];}else{ } ?></td>
                                              <td><?php if($ex['oct'] !==''){ echo '£'.$ex['oct'];}else{ } ?></td>
                                              <td><?php if($ex['nov'] !==''){ echo '£'.$ex['nov']; }else{ }?></td>
                                              <td><?php if($ex['dec'] !==''){ echo '£'.$ex['dec'];}else{ } ?></td>
                                              <td><?php  echo '£'.$ex['total']; ?></td>
                                              <td>
                                              <a href="<?php echo base_url(); ?>user/add_profit_loss/<?php echo $ex['id']; ?>" class="btn"><i class="fa fa-pencil-square-o" aria-hidden="true"></i>
</a>
                                              <a href="<?php echo base_url(); ?>user/delete_profit_loss/<?php echo $ex['id']; ?>" onclick='javascript:return confirm("Are you sure to Delete this Record?")' class="btn"><i class="fa fa-trash-o"></i></a>
                                              </td>
                                            </tr>

                                        <?php $j++; } 
                                         }
                                         if($total_ex !==''){
                                         ?>
                                        <tr>
                                          <!-- <td></td> -->
                                              <td>total</td>
                                              <td><?php echo '£'.number_format($jan_ex,2); ?></td>
                                              <td><?php echo '£'.number_format($feb_ex,2); ?></td>
                                              <td><?php echo '£'.number_format($march_ex,2); ?></td>
                                              <td><?php echo '£'.number_format($april_ex,2); ?></td>
                                              <td><?php echo '£'.number_format($may_ex,2); ?></td>
                                              <td><?php echo '£'.number_format($june_ex,2); ?></td>
                                              <td><?php echo '£'.number_format($july_ex,2); ?></td>
                                              <td><?php echo '£'.number_format($aug_ex,2); ?></td>
                                              <td><?php echo '£'.number_format($sep_ex,2); ?></td>
                                              <td><?php echo '£'.number_format($oct_ex,2); ?></td>
                                              <td><?php echo '£'.number_format($nov_ex,2); ?></td>
                                              <td><?php echo '£'.number_format($dec_ex,2); ?></td>
                                              <td><?php echo '£'.number_format($total_ex,2); ?></td>
                                              <td></td>
                                             
                                             </tr>
                                             <br/>
                                             <tr>
                                               <td colspan="16" class="text-danger text-center">Total Expense :- <?php echo '£'.number_format($total_ex,2); ?></td>
                                             </tr>
                                        <?php }else{
                                          echo '<tr> 
                                              <td colspan="16" class="text-danger text-center"> No Records Found </td> 
                                            </tr>';
                                        }
                                         }else{ ?>
                                            <tr> 
                                              <td colspan="16" class="text-danger text-center"> No Records Found </td> 
                                            </tr>
                                         <?php
                                          } ?>
                                           <br/>
                                             <tr>
                                             
                                    </tbody>
                                  </table>
                                                  
                                  </div>
                                  <?php if(!empty($pl)){?>
                                  <tbody>
                                  <table class="table table-striped table-bordered table-hover ">
                                  <?php
                                             $total =$total_in -$total_ex;
                                              if($total < 0){
                                                  $class ='text-danger';
                                              }else{
                                                $class ='text-success';
                                                }?>
                                               <td colspan="16" class="<?php echo $class; ?> text-center">NET Profit :- <?php echo '£'.number_format($total,2); ?></td>
                                             </tr>
                                             </tbody>
                                  </table>
                                  <?php } ?>
                                </div>
                                </form>
                            </div>
                            
                          </div>
                          <div class="clearfix"></div>
                        </div>
                    
                </div><!--col-sm-9-->
            </div><!--row-->
            
        </div><!--container-->
        
    </div><!--main_container-->
    
    <!--Main container sec end-->
    <div class="clearfix"></div>
    

   
  <script>
  $(document).ready(function(){
    setTimeout(function(){
        $('#success').hide();
    }, 3000);
    setTimeout(function(){
        $('#failure').hide();
    }, 3000);
  });
  $(function () {
  <?php
      $income_jan ='';
      $income_feb ='';
      $income_march = '';
      $income_april = '';
      $income_may = '';
      $income_june = '';
      $income_july = '';
      $income_aug = '';
      $income_sep = '';
      $income_oct = '';
      $income_nov = '';
      $income_dec = '';
      $Expense_jan = '';
      $Expense_feb = '';
      $Expense_march = '';
      $Expense_april = '';
      $Expense_may = '';
      $Expense_june = '';
      $Expense_july = '';
      $Expense_aug = '';
      $Expense_sep = '';
      $Expense_oct = '';
      $Expense_nov = '';
      $Expense_dec = '';
  foreach ($pl as $value) {
    if($value['type']=='Income'){
      $income_jan += $value['jan'];
      $income_feb += $value['feb'];
      $income_march += $value['march'];
      $income_april += $value['april'];
      $income_may += $value['may'];
      $income_june += $value['june'];
      $income_july += $value['july'];
      $income_aug += $value['aug'];
      $income_sep += $value['sep'];
      $income_oct += $value['oct'];
      $income_nov += $value['nov'];
      $income_dec += $value['dec'];
    }else{
      $Expense_jan += $value['jan'];
      $Expense_feb += $value['feb'];
      $Expense_march += $value['march'];
      $Expense_april += $value['april'];
      $Expense_may += $value['may'];
      $Expense_june += $value['june'];
      $Expense_july += $value['july'];
      $Expense_aug += $value['aug'];
      $Expense_sep += $value['sep'];
      $Expense_oct += $value['oct'];
      $Expense_nov += $value['nov'];
      $Expense_dec += $value['dec'];
    }
   } 
  ?>
    $('#container').highcharts({
        credits: {
           enabled: false
        },
        chart: {
            type: 'line'
        },
        title: {
            text: 'Monthly Average Profit'
        },
        xAxis: {
            categories: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
        },
        plotOptions: {
            line: {
                dataLabels: {
                    enabled: true
                },
                enableMouseTracking: true
            }
        },
        series: [{
            name: 'Income',
            data: [<?php echo $income_jan.','.$income_feb.','. $income_march.','. $income_april.','. $income_may.','. $income_june.','. $income_july.','. $income_aug.','. $income_sep.','. $income_oct.','. $income_nov.','. $income_dec?>]
       },{
            name:'Expense',
            data:'Income',
            data:[ <?php echo $Expense_jan.','. $Expense_feb.','. $Expense_march.','. $Expense_april.','. $Expense_may.','. $Expense_june.','. $Expense_july.','. $Expense_aug.','. $Expense_sep.','. $Expense_oct.','. $Expense_nov.','.$Expense_dec;?>]
        }]
    });
}); 
  </script>      
        