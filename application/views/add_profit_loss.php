      <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7/jquery.min.js">
</script>
<?php $this->load->view('baneer_section'); ?>
          <div class="container">
                 
              
        </div>
        
    </header>
    <!--Header sec end-->
    <!--Main container sec start-->
    <div class="main_container"><!--main_container-->
    
      <div class="container"><!--container-->
          
            <div class="row">
               <div> <a href="<?php echo base_url('user/profit_loss'); ?>" class="btn btn-default" style="margin-left:20px;margin-bottom:10px;">Profit Loss </a></div>
                <div class="col-sm-12"><!--col-sm-9-->
                  <div class="trainee_tab_content">
                        <!-- Tab panes -->
                          <div class="tab-content">
                            <div role="tabpanel" class="tab-pane active" id="informaiton">
                              <h3 class="trai_title_sect">Profit and Loss</h3>
                             <form method="post" action="">
                               <div class="panel panel_app">
                                  <div class="panel-body">
                                    <div class="form-group">
                                      <label>Type</label>
                                        <select class="form-control" autocomplete="off" name="type" required>
                                             <option value="">Select Type</option>
                                             <option value="Income" <?php if(isset($profit[0]['type'])){ if($profit[0]['type'] == 'Income'){ echo "selected='selected'";} }else{ } ?>>Income</option>
                                             <option value="Expense" <?php if(isset($profit[0]['type'])){ if($profit[0]['type'] == 'Expense'){ echo "selected='selected'";} }else{ } ?>>Expense</option>
                                         </select>
                                    </div>
                                    <div class="form-group">
                                      <label>Name</label>
                                      <input type="text" placeholder='Name' class="form-control" value="<?php if(isset($profit[0]['name'])){ echo $profit[0]['name'];}else{ }?>" id="name" name="name">
                                    </div>
                                    <div class="form-group">
                                      <label>Jan</label>
                                      <input type="number" class="form-control" placeholder='Jan' value="<?php if(isset($profit[0]['jan'])){ echo $profit[0]['jan'];}else{ }?>" id="jan" name="jan">
                                    </div>
                                  <div class="form-group">
                                      <label>Feb</label>
                                      <input type="number" class="form-control" placeholder='Feb' value="<?php if(isset($profit[0]['feb'])){ echo $profit[0]['feb'];}else{ }?>" id="feb" name="feb">
                                  </div>
                                  <div class="form-group">
                                      <label>March</label>
                                      <input type="number" class="form-control" placeholder='March' value="<?php if(isset($profit[0]['march'])){ echo $profit[0]['march'];}else{ }?>" id="march" name="march">
                                  </div>
                                  <div class="form-group">
                                      <label>April</label>
                                      <input type="number" class="form-control" placeholder='April' value="<?php if(isset($profit[0]['april'])){ echo $profit[0]['april'];}else{ }?>" id="april" name="april">
                                  </div>
                                  <div class="form-group">
                                      <label>May</label>
                                      <input type="number" class="form-control" placeholder='May' value="<?php if(isset($profit[0]['may'])){ echo $profit[0]['may'];}else{ }?>"  id="may" name="may">
                                  </div>
                                  <div class="form-group">
                                      <label>June</label>
                                      <input type="number" class="form-control" placeholder='June' value="<?php if(isset($profit[0]['june'])){ echo $profit[0]['june'];}else{ }?>" id="june" name="june">
                                  </div>
                                  <div class="form-group">
                                      <label>July</label>
                                      <input type="number" class="form-control" placeholder='July' value="<?php if(isset($profit[0]['july'])){ echo $profit[0]['july'];}else{ }?>" id="july" name="july">
                                  </div>
                                  <div class="form-group">
                                      <label>Aug</label>
                                      <input type="number" class="form-control" placeholder='Aug' value="<?php if(isset($profit[0]['aug'])){ echo $profit[0]['aug'];}else{ }?>" id="aug" name="aug">
                                  </div>
                                  <div class="form-group">
                                      <label>Sep</label>
                                      <input type="number" class="form-control" placeholder='Sep' value="<?php if(isset($profit[0]['sep'])){ echo $profit[0]['sep'];}else{ }?>" id="sep" name="sep">
                                  </div>
                                  <div class="form-group">
                                      <label>Oct</label>
                                      <input type="text" class="form-control" placeholder='Oct' value="<?php if(isset($profit[0]['oct'])){ echo $profit[0]['oct'];}else{ }?>" id="oct" name="oct">
                                  </div>
                                  <div class="form-group">
                                      <label>Nov</label>
                                      <input type="number" class="form-control" placeholder='Nov' value="<?php if(isset($profit[0]['nov'])){ echo $profit[0]['nov'];}else{ }?>" id="nov"  name="nov">
                                  </div>
                                  <div class="form-group">
                                      <label>Dec</label>
                                      <input type="number" class="form-control" placeholder='Dec' value="<?php if(isset($profit[0]['dec'])){ echo $profit[0]['dec'];}else{ }?>" id="dec" name="dec">
                                  </div>    
                                    <input type="submit" class="btn submit_btn" name="submit" value="Submit">          
                                  </div>
                                </div>
                                </form>
                            </div>
                            
                          </div>
                          <div class="clearfix"></div>
                        </div>
                    
                </div><!--col-sm-9-->
            </div><!--row-->
            
        </div><!--container-->
        
    </div><!--main_container-->
    
    <!--Main container sec end-->
    <div class="clearfix"></div>

    <?php if($this->session->flashdata('success')) { ?>
        <script type="text/javascript">
            var msg = "<?php echo $this->session->flashdata('success'); ?>";
            notify('success','<i class="fa fa-check"> Success </i>',msg);
        </script>
    <?php } ?>

    <?php if($this->session->flashdata('failure')) { ?>
        <script type="text/javascript">
            var msg1 = "<?php echo $this->session->flashdata('failure'); ?>";
            notify('error','<i class="fa fa-times"> Error ! </i>',msg1);                      
         </script>
    <?php } ?>    
