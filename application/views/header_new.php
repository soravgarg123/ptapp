<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
    <meta name="google-site-verification" content="r5x7a4-wy3Ez7Mo6S26Agwewkk3IMAHbgagcKaTsl4Y"/>
    <title>The PT App</title>
    <!-- Bootstrap -->
    <link href="<?php echo base_url(); ?>css/bootstrap-clockpicker.min.css" rel="stylesheet">
  <link href="<?php echo base_url(); ?>css/bootstrap-datepicker.css" rel="stylesheet"> 
  
    <link href="<?php echo base_url(); ?>css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>css/animsition.min.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>css/plugin.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>css/font-awesome.min.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>css/custom.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>css/responsive.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>css/flaticon.css" rel="stylesheet">
    <link rel="stylesheet" href="<?php echo base_url(); ?>css/owl.carousel.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>css/component.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>css/notify-metro.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>css/lightbox.css">
   
    <script src="<?php echo base_url(); ?>js/modernizr.custom.28101.js"></script>
  </head>
  <?php
    $clientURL = $this->uri->segment(2);
    if($clientURL == "clientlogin"){
        $cl_id = "clientlogin";
    }else{
        $cl_id = "";
    }
  ?>

  <body class="cbp-spmenu-push" id="<?php echo $cl_id; ?>">
  <main class="animsition" >
  <nav class="cbp-spmenu cbp-spmenu-vertical cbp-spmenu-left"  id="cbp-spmenu-s1">
   <?php 
        $user_id = $this->session->userdata('user_id');
        $user_type = $this->session->userdata('user_type');
        ?>
        <?php if(empty($user_type)) { 
            if($clientURL == "clientlogin") { ?>
                <a href="<?php echo base_url(); ?>user/clientlogin">Home</a>
            <?php } else { ?>
                <a href="<?php echo base_url(); ?>">Home</a>
            <?php } ?>
        <?php } if(!empty($user_type) && $user_type == "trainner") { ?>
            <a href="<?php echo base_url(); ?>">Home</a>  
        <?php } if(!empty($user_type) && $user_type == "trainee") { ?>
            <a href="<?php echo base_url(); ?>user/clientlogin">Home</a>  
        <?php } ?>
             <?php if($user_type == 'trainner'){ ?> 
            <a href="<?php echo base_url();?>user/notification" onclick="show_msg();">Business Dashboard</a>
            <?php } ?>
            
            <?php
             if($user_type == 'trainner'){
             ?>
             <a href="<?php echo base_url();?>user/profiles?id=<?php echo $this->session->userdata('user_id');?>"> PT Profile</a>
             <?php } if($user_type == 'trainner'){
             ?>
             <a href="<?php echo base_url();?>user/article">Article/blog</a>
             <?php
             } if($user_type == 'trainee'){ ?>
              <a href="<?php echo base_url();?>user/profiles">Profile</a>
             
            <?php  }
             //if($user_id != ''){
             ?>
            <!-- <a href="<?php echo base_url();?>user/search_trainner">Our Tarinner</a>-->
             <?php //} ?>
             <!-- <a href="<?php echo base_url();?>user/search_trainee">Our Tarinee</a> -->
             <?php 

           //  if($this->session->userdata('user_type') == 'trainner'){             

             if($user_type == 'trainner'){             

             ?>
            <a href="<?php echo base_url();?>user/client_list">Clients</a>
            <?php } 
            if($user_type == 'trainee'){ ?>
            <a href="<?php echo base_url('user/client_trainner'); ?>">My PT</a>
            
            <a href="<?php echo base_url();?>user/appointment">Diary</a>
            <?php } ?>
            <?php if($user_type == 'trainner'){ ?>
                            <li class="dropdown">
                                <a href="#" data-toggle="dropdown" class="dropdown-toggle">Tools</a>
                                 <ul class="dropdown-menu sub-menu">
                                  <li><a href="<?php echo base_url();?>user/tools">FMS</a></li>
                                  <li><a href="<?php echo base_url();?>user/calculator">Calculator</a></li>
                                  <li><a href="<?php echo base_url();?>user/profit_loss">Profit/Loss</a></li>
                                  <li><a href="<?php echo base_url();?>user/resources">Resources</a></li>
                                 </ul>
                            </li>
            <?php }
            if(!empty($user_type)){             
             ?>
            <a href="<?php echo base_url('user/setting'); ?>">Settings</a>
             <a href="<?php echo base_url('product/product_list_view'); ?>">Products</a>
            <?php } 
             if($clientURL != "clientlogin" && $user_type != 'trainee'){ ?>
            <a href="<?php echo base_url();?>user/about">About Us</a>
            <?php } 
             if(empty($user_type)){?>
            <a href="<?php echo base_url();?>user/some_service">More...</a>
            <?php }?>

            <a href="<?php echo base_url();?>user/terms">Terms and Conditions</a>
        </nav>
    
    <div class="header_u_sect navbar-fixed-top">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-sm-4"><a class="menu_bar_targ is-open" id="showLeft" href="#"><span class="is-open fa fa-bars"></span> Menu</a></div>
                    <div class="col-sm-4">
                    <?php
                        if($clientURL == "clientlogin") { ?>
                        <a href="<?php echo base_url(); ?>user/clientlogin"><h3 class="logo_pt_app">THE PT App</h3></a>
                    <?php } else { ?>
                        <a href="<?php echo base_url(); ?>"><h3 class="logo_pt_app">THE PT App</h3></a>
                    <?php } ?>
                        
                    </div>
                    <?php if($this->session->userdata('email')){
                        $login_class='';
                      }else{
                        $login_class='login-top';
                        } ?>
                    <div class="col-sm-4 text-right">
                        <div class="top_menu_log <?php echo $login_class;?>">
                            <ul> 


                            <?php if($this->uri->segment('1')=="product") { ?>
                             <li class="cart_list"><a href="<?php echo base_url();?>product/checkout" class="check_out"><i class="fa fa-shopping-cart cart"></i> <span class="cart_count"><?php echo count($this->cart->contents());?></span> 

                             

                             </a>$<r class="cart_total"><?php $CI =& get_instance();$CI->show_cart_total();?></r></li>
                             <?php } ?>


                                <?php if($this->session->userdata('email')){?> 
                                   
                                     <li> <strong style="color:#fff;"><?php echo $this->session->userdata('email');?></strong><a href="<?php echo base_url();?>user/logout" class="logout_btn">Log Out</a></li>
                                <?php
                                }else{
                                ?>
                                <?php $seg =  $this->uri->segment(2); 
                                    if($seg == "clientlogin"){ ?>
                                    <?php if($this->session->userdata('email')){?> 
                                        <strong style="color:#fff;"><?php echo $this->session->userdata('email');?></strong>
                                        <li><a href="<?php echo base_url();?>user/logout" class="logout_btn" >Log Out</a></li>
                                    <?php } else { ?>
                                        <li><a href="javascript:void(0);" data-toggle="modal" data-target="#client_login_modal" data-keyboard="false" data-backdrop="static">Client Login</a></li>
                                    <?php } ?>
                                <?php }else{ ?>
                                    <li class="dropdown"><a href="javascript:void(0);" title="PT Login" data-toggle="modal" data-target="#login_modal" data-keyboard="false" data-backdrop="static">Login</a>
                                       <ul class="dropdown-menu">
                                         <!-- <li><a href="javascript:void(0);" title="PT Login" data-toggle="modal" data-target="#login_modal" data-keyboard="false" data-backdrop="static">PT Login</a></li> -->
                                         <!-- <li><a href="<?php echo base_url(); ?>user/clientlogin" title="Client Login" >Client Login</a></li> -->
                                      </ul>
                                    </li>
                                    <li><a class="sign_up_btn" href="javascript:void(0);"  data-toggle="modal" data-target="#register_modal" data-keyboard="false" data-backdrop="static">Signup</a></li>
                                <?php } } ?>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
   </div>
