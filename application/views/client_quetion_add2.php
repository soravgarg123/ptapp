<?php $this->load->view('baneer_section'); ?>
          <div class="container">
            </div>
        </div>
    </header>

    <!--Header sec end-->
    <!--Main container sec start-->
    <div class="main_container">
    
        <div class="container">
            
            <div class="row">
                <div class="col-sm-3">
                    
                    <div class="trainee_tabs_sect">
                          <h3>Medical Questionnaire </h3>
                          <!-- Nav tabs -->
                          <ul class="nav_tabs">
                            <li class="active"><a href="#informaiton" aria-controls="informaiton" role="tab" data-toggle="tab">Quetion</a></li>
                          </ul>
                    </div>
                    
                </div>
                <div class="col-sm-9">
                    <div class="trainee_tab_content">
 
                     <form method="post" id="edit_client_quetion">
                     <div id="client_plan" class="client_plan">Your Data Saved Successfully</div>
                          <!-- Tab panes -->
                          <div class="tab-content">
                            <div role="tabpanel" class="tab-pane active" id="informaiton">
                                <h3 class="trai_title_sect">Pre-Activity Readiness Questionnaire</h3>
                                <div class="form_wrapper">
                                    <div>
                                        <label>Has a doctor ever said that you have a heart condition or high blood pressure and that you should only do physical activity recommended by a doctor?</label>
                                        <div class="radio">
                                        <label><input type="radio"  id="heart_condition" required='' name="heart_condition"  value="1"<?php  if(isset($client_quetion[0]['heart_condition'])){if($client_quetion[0]['heart_condition']=='1'){ echo 'checked="checked"';}else{ } }?>>Yes</label>
                                        </div>
                                        <div class="radio"><label><input type="radio" id="heart_condition" required='' name="heart_condition" <?php  if(isset($client_quetion[0]['heart_condition'])){if($client_quetion[0]['heart_condition']=='0'){ echo 'checked="checked"';}else{ } }?> value="0">No</label>
                                    </div>
                                    </div>
                                    <div>
                                        <label>Has any of your immediate family suffered a heart attack before 55 years old in Men and 65 years old in Females?</label>
                                        <div class="radio">
                                        <label><input type="radio" id="heart_attack" required='' name="heart_attack"  value="1"<?php  if(isset($client_quetion[0]['heart_attack'])){if($client_quetion[0]['heart_attack']=='1'){ echo 'checked="checked"';}else{ } }?>>Yes</label>
                                        </div>
                                        <div class="radio">
                                        <label><input type="radio"  id="heart_attack" required='' name="heart_attack" <?php  if(isset($client_quetion[0]['heart_attack'])){if($client_quetion[0]['heart_attack']=='0'){ echo 'checked="checked"';}else{ } }?> value="0">No</label>
                                    </div>
                                    <div>
                                        <label>Do you feel any pain in your chest when you perform physical activity?</label>
                                        <div class="radio">
                                        <label><input type="radio"  id="chest_pain" required='' name="chest_pain"  value="1"<?php  if(isset($client_quetion[0]['heart_attack'])){if($client_quetion[0]['heart_attack']=='1'){ echo 'checked="checked"';}else{ } }?>>
                                        Yes</label>
                                        </div>
                                        <div class="radio">
                                        <label><input type="radio" id="chest_pain" required='' name="chest_pain"<?php  if(isset($client_quetion[0]['heart_attack'])){if($client_quetion[0]['heart_attack']=='0'){ echo 'checked="checked"';}else{ } }?> value="0">No</label>
                                    </div><div>
                                        <label>Do you ever lose your balance due to dizziness or loss of consciousness?</label>
                                        <div class="radio">
                                        <label><input type="radio"id="consciousness" required='' name="consciousness"  value="1"<?php  if(isset($client_quetion[0]['consciousness'])){if($client_quetion[0]['consciousness']=='1'){ echo 'checked="checked"';}else{ } }?>>Yes</label></div>
                                        <div class="radio"><label><input type="radio"  id="consciousness" required='' name="consciousness" <?php  if(isset($client_quetion[0]['consciousness'])){if($client_quetion[0]['consciousness']=='0'){ echo 'checked="checked"';}else{ } }?> value="0">No</label></div>
                                    </div><div>
                                        <label>Are you currently being prescribed any drugs for a specific condition?</label>
                                        <div class="radio">
                                        <label><input type="radio" id="drugs" required='' name="drugs"  value="1"<<?php  if(isset($client_quetion[0]['drugs'])){if($client_quetion[0]['drugs']=='1'){ echo 'checked="checked"';}else{ } }?>>Yes</label></div>
                                        <div class="radio"><label><input type="radio" id="drugs" required='' name="drugs" <?php  if(isset($client_quetion[0]['drugs'])){if($client_quetion[0]['drugs']=='0'){ echo 'checked="checked"';}else{ } }?> value="0">No</label></div>
                                    </div><div>
                                        <label>Do you have any bone or joint problems that can be made worse by physical activity?</label>
                                        <div class="radio">
                                        <label><input type="radio" id="bone" required='' name="bone"  value="1"<?php  if(isset($client_quetion[0]['bone'])){if($client_quetion[0]['bone']=='1'){ echo 'checked="checked"';}else{ } }?>>Yes</label></div>
                                        <div class="radio"><label><input type="radio" id="bone" required='' name="bone" <?php  if(isset($client_quetion[0]['bone'])){if($client_quetion[0]['bone']=='0'){ echo 'checked="checked"';}else{ } }?> value="0">No</label></div>
                                    </div><div class="form-group">
                                        <label>Do you smoke, if so how many a day?</label>
                                        <div class="radio">
                                        <label><input type="radio" id="smoke" required='' name="smoke"  value="1"<?php  if(isset($client_quetion[0]['smoke'])){if($client_quetion[0]['smoke']=='1'){ echo 'checked="checked"';}else{ } }?>>Yes</label></div>
                                        <div class="radio"><label><input type="radio"  id="smoke" required='' name="smoke"<?php  if(isset($client_quetion[0]['smoke'])){if($client_quetion[0]['smoke']=='0'){ echo 'checked="checked"';}else{ } }?> value="0">No</label></div>
                                    </div><div>
                                        <label>Do you suffer from any other medical conditions? Please state...</label>
                                        <div class="radio">
                                        <label><input type="radio"  id="medical_conditions" required='' name="medical_conditions"  value="1"<?php  if(isset($client_quetion[0]['medical_conditions'])){if($client_quetion[0]['medical_conditions']=='1'){ echo 'checked="checked"';}else{ } }?>>Yes</label></div>
                                        <div class="radio"><label><input type="radio"  id="medical_conditions" required='' name="medical_conditions"<?php  if(isset($client_quetion[0]['medical_conditions'])){if($client_quetion[0]['medical_conditions']=='0'){ echo 'checked="checked"';}else{ } }?> value="0">No</label></div>
                                    </div><div>
                                        <label>Do you currently perform any exercise or physical activity?</label>
                                        <div class="radio">
                                        <label><input type="radio" id="exercise" required='' name="exercise"  value="1"<<?php  if(isset($client_quetion[0]['medical_conditions'])){if($client_quetion[0]['medical_conditions']=='1'){ echo 'checked="checked"';}else{ } }?>>Yes</label></div>
                                        <div class="radio"><label><input type="radio"  id="exercise" required='' name="exercise" <?php  if(isset($client_quetion[0]['medical_conditions'])){if($client_quetion[0]['medical_conditions']=='0'){ echo 'checked="checked"';}else{ } }?> value="0">No</label></div>
                                    </div>

                                    <div class="form-group">
                                        <label>Please state all current or past injuries or any issues you may have with exercise:</label>
                                        <input type="text" class="form-control" id="injuries" required name="injuries" placeholder="Your Answar" value="<?php if(isset($client_quetion[0]['injuries'])){ echo $client_quetion[0]['injuries'];}else{ }?>">
                                    </div>
                                    <div class="form-group">
                                        <label>By ticking this box you are agreeing to the terms and conditions of ThePTapp and all its companies. These can be found online at our site www.theptapp.com/clientlogin</label>
                                        <div class="radio">
                                        <label><input type="radio"  id="agree" required='' name="agree"  value="1"<?php  if(isset($client_quetion[0]['agree'])){if($client_quetion[0]['agree']=='1'){ echo 'checked="checked"';}else{ } }?>>I Agree</label></div>
                                        <div class="radio"><label><input type="radio"  id="agree" required='' name="agree"<?php  if(isset($client_quetion[0]['agree'])){if($client_quetion[0]['agree']=='0'){ echo 'checked="checked"';}else{ } }?> value="0">I Do Not Agree</label></div>
                                    </div>
                                    <div class="form-group">
                                        <label>Full Name </label>
                                        <input type="text" class="form-control" id="full_name" required name="full_name" placeholder="Full Name" value="<?php if(isset($client_quetion[0]['full_name'])){ echo $client_quetion[0]['full_name'];}else{ }?>">
                                    </div>
                                    <div class="form-group">
                                        <label>Date</label>
                                        <input type="text"  class="form-control datepicker" id="datepicker" required name="date" placeholder="Date" value="<?php if(isset($client_quetion[0]['date'])){ echo $client_quetion[0]['date'];}else{ }?>">
                                    </div>
                                </div>
                            </div>
                          </div>
                          <div class="clearfix"></div>
                          <button type="button" id="back1" class="btn submit_btn">Back</button>
                          <input type="submit" id="update_quetion" class="btn submit_btn" type="button" value="Submit">
                     </form>
                    </div>
                    
                </div>
            </div>
            
        </div>
        
    </div>
    
    <!--Main container sec end-->
     <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script type="text/javascript">
     $(document).ready(function(){
          $('#datepicker').datepicker({
              autoclose:true,
              format: 'yyyy-mm-dd',
          });
          $('#back1').click(function(){
                  $.ajax({
                  url:"<?php echo base_url() ?>user/client_quetion_add",
                  type:"post",
                  //data:data1, 
                  success: function(response)
                  {                       
                     $("#one").addClass("active");
                     $("#two").removeClass("active");
                     $("#informaiton").html(response);
                     
                  }

              });
              
            });
          });
</script>
    
