  
<?php $this->load->view('baneer_section'); ?>
        	<div class="container">
                	<div class="profile_header">
                    	<div class="col-sm-2">
                        	<span class="user_profile_pic"><img class=" img-responsive" src="<?php echo base_url(); ?>profilepic/<?php echo $trainner_details[0]['user_pic'];?>" alt=""/></span>
                        </div>
                        <div class="col-sm-8">
                        	<div class="user_profile_info">
                               	<h3><?php echo $trainner_details[0]['name'].' '.$trainner_details[0]['surname']; ?></h3>
                                <?php 
                                $user_id = $this->session->userdata('user_id');
                                $current_date = date("Y-m-d H:i:s");
                                $user_type = $this->session->userdata('user_type');
                                ?>
                                <input type="hidden" name="user_id" id="user_id" value="<?php echo $user_id;?>">
                                <span class="user_degi">Student</span>
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur imperdiet volutpat orci, et consectetur nibh vulputate sed. Vestibulum mollis, nisl vel tristique congue, erat purus ullamcorper eros, non gravida risus elit sit amet justo. </p> 
                            </div>
                            <?php if($user_id != "" && $user_type != "trainner"){?>
                            <div class="add_profile"><input type="button" class="btn submit_profile" id="update" value="Hire Me"></div>
                            <?php }?>
                        </div>
                        <div class="col-sm-2">
                        	<ul class="social_icon_user">
                                	<li><a href="<?php echo $trainner_details[0]['fb_url'];?>"><span class="fa fa-facebook"></span></a></li>
                                    <li><a href="<?php echo $trainner_details[0]['twit_url'];?>"><span class="fa fa-twitter"></span></a></li>
                                    <li><a href="<?php echo $trainner_details[0]['link_url'];?>"><span class="fa fa-linkedin"></span></a></li>
                                    <input type="hidden" name="trainner_id" id="trainner_id" value="<?php echo $trainner_details[0]['id'];?>">
                                    <input type="hidden" name="added_date" id="added_date" value="<?php echo $current_date;?>">
                                    <input type="hidden" name="modify_date" id="modify_date" value="<?php echo $current_date;?>">
                            </ul>

                        </div>
                    	 
                    </div>
                
            </div>
        </div>
        
    </header>
    <!--Header sec end-->
    
    <!--Main container sec start-->
    <div class="main_container">
    
    	<div class="container">
        	<div class="row">
            	<div class="col-sm-12">

                  <div id="hire_result"><h4>You have hired trainner</h4></div>
                  <div id="hire_result1"><h4>You have already hired trainner</h4></div>
                
                	<div class="panel panel_app">
                      <div class="panel-heading">
                        <h3 class="panel-title">Biography </h3>
                      </div>
                      <div class="panel-body">
                       	<p><?php echo $trainner_details[0]['bio'];?></p>
                      </div>
                    </div>
                    
                    <div class="panel panel_app">
                      <div class="panel-heading">
                        <h3 class="panel-title">Certification  </h3>
                      </div>
                      <div class="panel-body">
                       	<p><?php echo $trainner_details[0]['certi'];?></p>
                      </div>
                    </div>
                    
                    <div class="panel panel_app">
                      <div class="panel-heading">
                        <h3 class="panel-title">Awards  </h3>
                      </div>
                      <div class="panel-body">
                       	<p><?php echo $trainner_details[0]['award'];?></p>
                      </div>
                    </div>
                    
                    <div class="panel panel_app">
                      <div class="panel-heading">
                        <h3 class="panel-title">Accomplishments   </h3>
                      </div>
                      <div class="panel-body">
                       	<p><?php echo $trainner_details[0]['accomplish'];?></p>
                      </div>
                    </div>
                    
                    <div class="panel panel_app">
                      <div class="panel-heading">
                        <h3 class="panel-title">Location   </h3>
                      </div>
                      <div class="panel-body">
                       	<p><?php echo $trainner_details[0]['loc'];?></p>
                      </div>
                    </div>
                    
                    <div class="panel panel_app">
                      <div class="panel-heading">
                        <h3 class="panel-title">Credentials    </h3>
                      </div>
                      <div class="panel-body">
                       	<p><?php echo $trainner_details[0]['credential'];?></p>
                      </div>
                    </div>
                    
                    <div class="panel panel_app">
                      <div class="panel-heading">
                        <h3 class="panel-title"> Interests & hobby    </h3>
                      </div>
                      <div class="panel-body">
                       	<p><?php echo $trainner_details[0]['hob'];?></p>
                      </div>
                    </div>
                    
                </div>
            </div>
            
        </div>
        
    </div>
    
    <!--Main container sec end-->
    
    <div class="clearfix"></div>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
     <script type="text/javascript">
     $(document).ready(function(){
       $(".add_profile").on('click','#update',function() {
            
        
            var trainner_id = $('#trainner_id').val();
            var user_id = $('#user_id').val();
            var added_date = $('#added_date').val();
            var modify_date = $('#modify_date').val();
           
                $.ajax({
                  type: "post",
                  url: "<?php echo base_url(); ?>user/hire_trainner",
                  data: "trainner_id="+trainner_id+"&user_id="+user_id+"&added_date="+added_date+"&modify_date="+modify_date,
                  
                  success: function(data){
                              
                                  if(data=='true'){
                                             
                                     $('#hire_result1').show();
                                     $('#hire_result').hide();
                                  }else{

                                      $('#hire_result').show();
                                      $('#hire_result1').hide();
                                  }
                             }
                            
                            
                  
                });
            
        });
    });
</script>
