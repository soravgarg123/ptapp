<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Admin_model extends CI_Model {

	function __construct(){
			    parent::__construct();
			   
	}
	
	function check_user_exist($email){
		        $email=$this->input->post('email');		                  
		        $this->db->select('*');
                $this->db->from('users');
				$this->db->where('email',$email);				 
				$query=$this->db->get();		
				if($query->num_rows()>0)
				{				
				    return true;
				}
				else
				{
				    return false;
				}
	}
	
	function login($email,$password){
	            $this->db->select('*');
                $this->db->from('users');
				$this->db->where('email',$email);
				$this->db->where('password',$password);
				$query=$this->db->get();
	            if($query->num_rows()>0){
	         	  foreach($query->result() as $rows){
	            	//add all data to session
	            $newdata = array('user_id' 	   => $rows->user_id,
	                             'email'       => $rows->email,
			                     'user_type'   => $rows->user_type,
		                         'logged_in'   => TRUE,
	                   );
				}
	            $this->session->set_userdata($newdata);
	            return true;            
			}			
			return false;
	}
	function get_trainers(){
	            $this->db->select('*');
                $this->db->from('trainner');
				$query=$this->db->get();
	            return $query->result_array();            
					
			
	}
	function trainers_delete($id){ 
	$this->db->where('user_id', $id);
	$this->db->delete('trainner');
	return true;
	}
	function users_delete($id){ 
	$this->db->where('user_id', $id);
	$this->db->delete('users');  
	return true;
	}
	function get_clients(){
		$query = $this->db->query(" SELECT `te`.`id` AS `trainee_id`,`te`.`trainner_id` AS `trainner_id`,`te`.`user_id` AS `user_id`,`te`.`publish_date` AS `publish_date`,`tr`.`name` AS `trainer_fname`,`tr`.`surname` AS `trainer_lname`,`te`.`name` AS `trainee_name`,`te`.`email` AS `trainee_email` FROM `trainee` AS `te` INNER JOIN `trainner` AS `tr` ON `tr`.`user_id` = `te`.`trainner_id` ORDER BY `te`.`id` DESC ");
		
		return $query->result_array();
	}
	function clients_delete($id){ 
	$this->db->where('id',$id);
	$this->db->delete('trainee'); 
	//echo $this->db->last_query(); die;
	return true;
	}
	function get_services(){
	            $this->db->select('*');
                $this->db->from('services');
				$query=$this->db->get();
	            return $query->result_array();            
					
			
	}
	function services_delete( $id){ 
	$this->db->where('id', $id);
	$this->db->delete('services'); 
	return true;
	}
	function add_service($data)
    {
        $id      = $data['id'];
        $service_title      = $data['service_title'];
        $description      = $data['description'];
        if(isset($data['image'])){
			$image = $data['image'];
			}else{
			$image = '';
		}
       
        
       
      
       if(!empty($id)){
		   if(empty($image)){
			 $data              = array(
										'service_title' => $service_title,
											'description' => $description,
										
										);
			}else{
				
				
			 $data              = array(
										'service_title' => $service_title,
											'description' => $description,
										'image' => $image
										);	
			}
			$this->db->where('id', $id);
			$this->db->update('services', $data);
		
		}else{
			 $data              = array(
											'service_title' => $service_title,
											'description' => $description,
											'image' => $image
											
										);
			 $this->db->insert('services', $data);
		}
    }
     function get_service($id)
    {
        
        $this->db->where('id', $id);
        $query = $this->db->get('services');
        return $query->result_array();
    }
    /*********Plans****************/
    function get_plans(){
	            $this->db->select('*');
                $this->db->from('plans');
				$query=$this->db->get();
	            return $query->result_array();            
					
			
	}
	function plans_delete( $id){ 
	$this->db->where('id', $id);
	$this->db->delete('plans'); 
	return true;
	}

	public function updatecommonImage($id,$filename){
		$this->db->where('id', $id);
		$this->db->update('common_images', array('image_name' => $filename));
	}

	public function updateclientLogin($id,$filename,$quotes){
		$this->db->where('id', $id);
		$this->db->update('common_images', array('image_name' => $filename,'quotes' => $quotes));
	}

	function add_plan($data)
    {
        $id      = $data['id'];
        $plan_title      = $data['plan_title'];
        $description      = $data['description'];
        $price      = $data['price'];
        $time      = $data['time'];
        if(isset($data['image'])){
			$image = $data['image'];
			}else{
			$image = '';
		}
       
        
       
      
       if(!empty($id)){
		   if(empty($image)){
			 $data              = array(
										'plan_title' => $plan_title,
										'price' => $price,
										'time' => $time,
										'description' => $description,
										
										);
			}else{
				
				
			 $data              = array(
										'plan_title' => $plan_title,
										'price' => $price,
										'time' => $time,
										'description' => $description,
										'image' => $image
										);	
			}
			$this->db->where('id', $id);
			$this->db->update('plans', $data);
		
		}else{
			 $data              = array(
											'plan_title' => $plan_title,
											'price' => $price,
											'time' => $time,
											'description' => $description,
											'image' => $image
											
										);
			 $this->db->insert('plans', $data);
		}
    }
     function get_plan($id)
    {
        
        $this->db->where('id', $id);
        $query = $this->db->get('plans');
        return $query->result_array();
    }
    /*
-----------------------------Code Written by Aditya ---------------------
    */
		public function get_people_say(){
		$this->db->select('tab1.*');
		$this->db->from('people_say as tab1');
		//$this->db->join('trainner as tab2', 'tab1.trainner_id = tab2.user_id');
		$data = $this->db->get();
		return $data->result_array();
	}

	public function add_people_say($form_data){ 
		if($this->db->insert('people_say', $form_data))
		{
			return true;
		}
		else
		{
			return false;
		}
	}

public function people_say_by_id($id){
		$this->db->select('tab1.*');
		$this->db->from('people_say as tab1');
		//$this->db->join('trainner as tab2', 'tab1.trainner_id = tab2.user_id');
		$this->db->where('tab1.id', $id);
		$data = $this->db->get();
		return $data->row_array();
	}
	public function edit_people_say($form_data, $id){
		$this->db->where('id', $id);
		if($this->db->update('people_say', $form_data))
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	public function delete_people_say($id){
		$this->db->where('id', $id);
		if($this->db->delete('people_say'))
		{
			return true;
		}
		else
		{
			return false;
		}
	}	
/*
---------------------------date : 24/11/2015 --------------------------------------
code 
*/
	public function get_why_us(){
		$this->db->select('*');
		$this->db->from('why_us');
		$data = $this->db->get();
		//echo $this->db->last_query(); exit();
		return $data->result_array();
	}

	public function insert_why_us($form_data){
		if($this->db->insert('why_us', $form_data))
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	public function why_us_by_id($id){
		$this->db->select('*');
		$this->db->from('why_us');
		$this->db->where('id', $id);
		$data = $this->db->get();
		return $data->row_array();
	}
	public function edit_why_us($id, $form_data){
		$this->db->where('id', $id);
		if($this->db->update('why_us', $form_data))
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	public function delete_why_us($id){
		$this->db->where('id', $id);
		if($this->db->delete('why_us'))
		{
			return true;
		}
		else
		{
			return false;
		}
	}

	/*
-----------Code Added Date : 01/12/2015 -----------------------------------
------------------create function for manage lates update start here -----------------------
	*/

	public function get_latest_update(){
		$this->db->select('*');
		$this->db->from('latest_update');
		$data = $this->db->get();
		return $data->row_array();
	}
	public function add_latest_update($form_data){
		if($this->db->insert('latest_update', $form_data))
		{
			return true;
		}
		else
		{
			return false;
		}
	}

	public function latest_update_id($id){
		$this->db->select('*');
		$this->db->from('latest_update');
		$this->db->where('id', $id);
		$data = $this->db->get();
		return $data->row_array();
	}
	public function edit_latest_update($id, $form_data){
		$this->db->where('id', $id);
		if($this->db->update('latest_update', $form_data))
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	public function delete_latest_update($id){
		$this->db->where('id', $id);
		if($this->db->delete('latest_update'))
		{
			return true;
		}
		else
		{
			return false;
		}
	}


/*
-----------Code Added Date : 01/12/2015 -----------------------------------
------------------create function for manage lates update end here -----------------------
	*/
/*-----------create functions for manage banner images start here -----------------------
*/
	public function get_banner_image(){
		$this->db->select('*');
		$this->db->from('banner_image');
		$data = $this->db->get();
		return $data->result_array();
	}

	public function get_product_banner_image(){
		$this->db->select('*');
		$this->db->from('product_banner_images');
		$data = $this->db->get();
		return $data->result_array();
	}




	public function get_story_image(){
		$this->db->select('*');
		$this->db->from('common_images');
		$data = $this->db->get();
		return $data->result_array();
	}

	public function get_story_imageById($id){
		$this->db->select('*');
		$this->db->from('common_images');
		$this->db->where('id',$id);
		$data = $this->db->get();
		return $data->result_array();
	}

	public function add_banner_image($form_data){
		if($this->db->insert('banner_image' , $form_data))
		{
			return true;
		}
		else
		{
			return false;
		}
	}

	public function add_product_banner_image($form_data){
		if($this->db->insert('product_banner_images' , $form_data))
		{
			return true;
		}
		else
		{
			return false;
		}
	}


	public function delete_banner_image($id){
		$this->db->where('id', $id);
		if($this->db->delete('banner_image'))
		{
			return true;
		}
		else
		{
			return false;
		}
	}

	public function delete_product_banner_image($id){
		$this->db->where('banner_id', $id);
		if($this->db->delete('product_banner_images'))
		{
			return true;
		}
		else
		{
			return false;
		}
	}



/*-----------create functions for manage banner images end here -----------------------
*/
/*
-------------create function for manage about us page start here ------------------
*/
	public function get_about_us(){
		$this->db->select('*');
		$this->db->from('about_us');
		$data = $this->db->get();
		return $data->row_array();
	}
	public function add_about_us($form_data){
		if($this->db->insert('about_us', $form_data))
		{
			return true;
		}
		else
		{
			return false;
		}
	}

	public function get_about_by_id($id){
		$this->db->select('*');
		$this->db->from('about_us');
		$this->db->where('id', $id);
		$data = $this->db->get();
		return $data->row_array();
	}
	public function edit_about_us($id, $form_data){
		$this->db->where('id', $id);
		if($this->db->update('about_us', $form_data))
		{
			return true;
		}
		else
		{
			return false;
		}
	}
/*
-------------create function for manage about us page end here ------------------
*/

/*
-------Date : 08-12-2015 create function for manage social links start here----------
*/

	public function get_links(){
		$this->db->select('*');
		$this->db->from('social_link');
		$data = $this->db->get();
		return $data->result_array();
	}
	public function add_social_link($form_data){
		if($this->db->insert('social_link', $form_data))
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	public function edit_social_link($id, $form_data){
		$this->db->where('id', $id);
		if($this->db->update('social_link', $form_data))
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	public function get_social_by_id($id){
		$this->db->select('*');
		$this->db->from('social_link');
		$this->db->where('id', $id);
		$data = $this->db->get();
		return $data->row_array();
	}

	public function delete_social_link($id){
		$this->db->where('id', $id);
		if($this->db->delete('social_link'))
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	public function get_subscribers(){
		$this->db->select('*');
		$this->db->from('subscribe');
		$data = $this->db->get();
		return $data->result_array();
	}
/*
-------Date : 08-12-2015 create function for manage social links end here----------
*/
/*
---------------------------End of Code Block ----------------------------
*/
	
}
