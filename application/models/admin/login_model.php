
<?php
Class Login_model extends CI_Model
{
    function __construct()
    {
        parent::__construct();
        $this->load->database();
        $a = end($this->uri->segments);
        
        
        $sess_data = array(
            
            'ab' => $a
        );
        
        $this->session->set_userdata($sess_data);
        $abc = $this->session->userdata('ab');
    }
    function login($username, $password)
    {
        $this->db->select('username, password,id');
        $this->db->from('admin_login');
        $this->db->where('email', $username);
        $this->db->where('password', $password);
        
        $query = $this->db->get();
        
        if ($query->num_rows() == 1) {
            $row = $query->row();
            
            $sess_data = array(
                
                'user_id' => $row->id,
                'username' => $row->username,
                'fullname' => $row->fullname,
                'mobile' => $row->mobile,
                'password' => $row->password
            );
            
            $this->session->set_userdata($sess_data);
            return true;
        } else {
            return false;
        }
    }
    function reset()
    {
        $user     = $this->input->post('username');
        $password = $this->input->post('newpassword');
        $data     = array(
            'password' => $password
        );
        $this->db->where('username', $user);
        $query = $this->db->update(ADMIN, $data);
        if ($query) {
            
            echo "success";
            
        }
    }
    function registration()
    {
        
        $mobile = $this->input->post('mobile');
        
        $username = $this->input->post('username');
        $password = $this->input->post('password');
        $country  = $this->input->post('country');
        $data     = array(
            'mobile' => $mobile,
            'username' => $username,
            'password' => $password,
            'country' => $country
        );
        $query    = $this->db->insert(REGISTER, $data);
        
    }
    
    function forget_password()
    {
        $username             = $this->input->post('email');
        $email                = $this->input->post('username');
        $_SESSION['USER1']    = $username;
        $_SESSION['USER']     = $email;
        $forget_password_link = "http://localhost/mradul_ci/index.php/Admin/reset_password";
        $data                 = array(
            'forget_password_link' => $forget_password_link
        );
        $this->db->where('email', $username);
        $query = $this->db->update(ADMIN, $data);
        
    }
    function update_password()
    {
        
        $email    = $_SESSION['USER'];
        $username = $_SESSION['USER1'];
        $data1    = $this->input->post('new_password');
        $data2    = $this->input->post('confirm_password');
        if ($data1 == $data2) {
            $data3 = array(
                'password' => $data1
            );
            $this->db->where('email', $email);
            $this->db->or_where('username', $username);
            $query = $this->db->update(ADMIN, $data3);
            if ($query) {
                redirect('Admin/login');
            }
        }
        
        else {
            $newdata = array(
                'a' => 'Password are not matched'
            );
            
            $a = $this->session->set_userdata($newdata);
        }
    }
    
    /*function forget_pass_cheak()
    {
    
    $email=$this->input->post('email');
    $this->db->where('forget_password_code',$email);
    $query=$this->db->get(ADMIN);
    if($query->num_rows()!='')
    {
    
    redirect('admin/reset');
    }
    else
    {
    $data1="Please show in your email and then type here msg";
    echo $data1;
    }
    }*/
    
    function add_channel($data)
    {
        $channel_id      = $data['channel_id'];
        $channel_name      = $data['channel_name'];
        $price      = $data['price'];
        $category_id      = $data['category_id'];
        $channel_frequency      = $data['channel_frequency'];
        if(isset($data['channel_image'])){
			$channel_image = $data['channel_image'];
			}else{
			$channel_image = '';
		}
       
        
       
      
       if(!empty($channel_id)){
		   if(empty($channel_image)){
			 $data              = array(
										'channel_name' => $channel_name,
										'price' => $price,
										'channel_frequency' => $channel_frequency,
										'category_id' => $category_id
										);
			}else{
				
				
			 $data              = array(
										'channel_name' => $channel_name,
										'price' => $price,
										'channel_frequency' => $channel_frequency,
										'category_id' => $category_id,
										'channel_image' => $channel_image
										);	
			}
			$this->db->where('channel_id', $channel_id);
			$this->db->update('manage_channel', $data);
		
		}else{
			 $data              = array(
											'channel_name' => $channel_name,
											'price' => $price,
											'channel_frequency' => $channel_frequency,
											'category_id' => $category_id,
											'channel_image' => $channel_image
										);
			 $this->db->insert('manage_channel', $data);
		}
    }
    function show_channel()
    {
        $query = $this->db->get('manage_channel');
        return $query->result_array();
    }
    
    function delete_channel1($id)
    {
        $this->db->delete('manage_channel', array(
            'channel_id' => $id
        ));
        return $this->db->affected_rows();
    }
    function channel_search($keyword)
    {
        $this->db->like('channel_name', $keyword);
        $this->db->or_like('channel_frequency', $keyword);
        $this->db->or_like('price', $keyword);
        $this->db->order_by('channel_name', 'asc');
        $query = $this->db->get('manage_channel');
        if (count($query) != 0) {
            return $query->result_array();
        } else {
            echo 'no';
        }
    }
     function get_channel($id)
    {
        
        $this->db->where('channel_id', $id);
        $query = $this->db->get('manage_channel');
        return $query->result_array();
    }
     function get_profile($id)
    {
        
        $this->db->where('id', $id);
        $query = $this->db->get('admin_login');
        return $query->result_array();
    }
    function profile($data)
    {
        $id      = $data['id'];
        $fullname      = $data['fullname'];
        $email      = $data['email'];
        $mobile      = $data['mobile'];
        $password      = $data['password'];
      if(!empty($password)){
		 
		  $data   = array(
			'fullname' => $fullname,
			'email' => $email,
			'mobile' => $mobile,
			'password' => $password
			
			);
		}else{
		 
		  $data   = array(
			'fullname' => $fullname,
			'email' => $email,
			'mobile' => $mobile	
			
			);
		}
		
       
      
       if(!empty($id)){
		   
			$this->db->where('id', $id);
			$this->db->update('admin_login', $data);
		
		}else{
		$id = 	 $this->db->insert('admin_login', $data);
		}
		  return $id;
    }
    
}
?>
