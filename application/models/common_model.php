<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Common_Model Extends CI_Model {

	public function add($tableName, $data)
		{
			$this->db->insert($tableName, $data);
			return $this->db->insert_id();
		}

	public function edit($tableName,$columnName,$data,$id)
		{
			$this ->db->where($columnName,$id);
			$this->db->update($tableName,$data);
			if($this->db->affected_rows() > 0){
				return true;
			}else{
				return false;
			}
		}

	public function getAllData($tableName,$columnName,$type)
		{
			$this->db->select('*');
			$this->db->from($tableName);
			$this->db->order_by($columnName, $type);
			$query = $this->db->get();
			return $query->result_array();
		}
	public function getAllDatawhere($tableName,$columnName,$type,$where)
		{
			$this->db->select('*');
			$this->db->from($tableName);
			$this->db->order_by($columnName, $type);
			$this->db->where($where);
			$query = $this->db->get();
			return $query->result_array();
		}

	public function deleteData($table,$column,$id)
		{
			$this->db->where($column, $id);
			$this->db->delete($table); 
			if($this->db->affected_rows() > 0){
				return true;
			}else{
				return false;
			}   
		}

	public function totalCount($table)
		{
			$query = $this->db->get($table);
			return $query->num_rows(); 
		}

	public function getSingleData($tableName,$columnName,$id,$oderColumn ='',$type ='')
		{
			$this->db->select('*');
			$this->db->from($tableName);
			$this->db->where($columnName, $id);
			if(!empty($oderColumn) && !empty($type))
			{
				$this->db->order_by($oderColumn, $type);
			}
			$query = $this->db->get();
			return $query->result_array();
		}

	public function getMultiple($tableName,$arr)
		{
			$this->db->select('*');
			$this->db->from($tableName);
			foreach ($arr as $key => $value) {
				$this->db->where($key, $value);
			}
			$query = $this->db->get();
			return $query->result_array();
		}

	public function normalJoin($table1,$column1,$table2,$column2)
		{
			$this->db->select('*');
			$this->db->from($table1);
			$this->db->join($table2,$table2.$column2 = $table1.$column1);
			$query = $this->db->get();
			return $query->result_array();
		}

	public function conditionalJoin($table1,$column1,$table2,$column2,$condition,$conditionColumn)
		{
			$this->db->select('*');
			$this->db->from($table1);
			$this->db->join($table2,$table2.$column2 = $table1.$column1);
			$this->db->where($table1.$conditionColumn, $condition);
			$query = $this->db->get();
			return $query->result_array();
		}
		public function commentJoin($where)
		{
			$this->db->select('*');
			$this->db->from('comment');
			$this->db->join('article','article.id = comment.article_id');
			$this->db->join('trainner','trainner.user_id = comment.user_id');
			$this->db->where($where);
			$query = $this->db->get();
			return $query->result_array();
		}
		public function likeJoin($where)
		{
			$this->db->select('*');
			$this->db->from('like');
			$this->db->join('article','article.id = like.article_id');
			$this->db->join('trainner','trainner.user_id = like.user_id');
			$this->db->where($where);
			$query = $this->db->get();
			return $query->result_array();
		}
		
}
?>