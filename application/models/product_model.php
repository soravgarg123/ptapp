<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Product_Model Extends CI_Model {

	public function get_all_product($table,$where,$select,$order_by,$limit,$offset)
		{
			$this->db->limit($limit, $offset);
			$this->db->select($select);

			$this->db->where($where);
			$this->db->order_by($order_by,"desc");
        
        		$query = $this->db->get($table);
      			//  print_r($query->result());die;
        		return $query->result();
		}

     public function get_all_product_key($table,$where,$select,$order_by,$limit,$offset,$key)
    {
           
            $this->db->limit($limit);
            $this->db->offset($offset);
		      	$this->db->select($select);
            $this->db->where($where);
            if($key!="")
            {
                  $keyar=explode(',',$key);
                  $this->db->where_in('product_category_path',$keyar);

            }
			
		      	$this->db->order_by($order_by,"desc");
            $query = $this->db->get($table);
           // echo  $this->db->last_query();
           return $query->result();



    }

     public function get_all_product_key_total($table,$where,$select,$order_by,$limit,$offset,$key)
    {
           
          
            $this->db->select($select);
            $this->db->where($where);
            if($key!="")
            {
                  $keyar=explode(',',$key);
                  $this->db->where_in('product_category_path',$keyar);

            }
            
            $this->db->order_by($order_by,"desc");
            $query = $this->db->get($table);
            return count($query->result());



    }

      function updates($table,$data,$where)
  {
    
    
    if($this->db->where($where)->update($table,$data))
    {
      return "updated";
    }
    else
    {
      return false;
    }
  }

  


    function add($table_name, $values)

  {
  
    if(!is_array($values))
    {
      echo 'please submit $values variable as array. Key of array should be the feild name of table and value of corresponding key should be also given.';
      return;
    }

    if( $this->db->insert($table_name, $values) )
    {
      return $this->db->insert_id();
    }
    else
    {
      //return false;
    }


  }

 function get_orders($table,$field='',$orderby='')
   {      

       $this->db->select($table.'.*, shiping_master.* ');
       $this->db->from($table);
       $this->db->join('shiping_master','shiping_master.ship_id='.$table.'.shiping_id');

       if($field!="")
       {
         if($orderby=="")
         {
              $orderby='asc';
         }
         $this->db->order_by($field,$orderby);
       }
        $query=$this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return array();
        }
    }



    function get_all_records($table)
   {      

 
       $this->db->from($table);
    
     
        $query=$this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return array();
        }
    }





	function make_query($quer)
	{
	    $query=$this->db->query($quer);
	   if ($query->num_rows() > 0) {
                return $query->result_array();
            } else {
                return array();
            }
	}

	function get_recordByid($table,$data)
   {
        $query = $this->db->get_where($table,$data);

        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return array();
        }
    }



	
		
}
?>
