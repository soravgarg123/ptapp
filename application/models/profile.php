  
	<!--Header sec start-->
    <header class="header_sect" id="header">
    	
        <div class="clearfix"></div>
       
        <div class="inner_head_sect">
        	<div class="container">
                	<div class="profile_header">
                    	<div class="col-sm-2">
                        	<span class="user_profile_pic"><img class=" img-responsive" src="<?php echo base_url(); ?>profilepic/<?php echo $user_details[0]['user_pic'];?>" alt=""/></span>
                        </div>                     
                        <div class="col-sm-8">
                        	<div class="user_profile_info">
                          <h3><?php echo $user_details[0]['trainner_name'].' '.$user_details[0]['trainner_surname'];?></h3>
                                <span class="user_degi">Student</span>
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur imperdiet volutpat orci, et consectetur nibh vulputate sed. Vestibulum mollis, nisl vel tristique congue, erat purus ullamcorper eros, non gravida risus elit sit amet justo. </p> 
                            </div>
                        </div>
                        <div class="col-sm-2">
                        	<ul class="social_icon_user">
                                	<li><a href="#"><span class="fa fa-facebook"></span></a></li>
                                    <li><a href="#"><span class="fa fa-twitter"></span></a></li>
                                    <li><a href="#"><span class="fa fa-linkedin"></span></a></li>
                            </ul>
                        </div>
                    	
                    </div>
                
            </div>
        </div>
        
    </header>
    <!--Header sec end-->
    
    <!--Main container sec start-->
    <div class="main_container">
    
    	<div class="container">
        	<div class="row">
            	<div class="col-sm-12">
                
                	<div class="panel panel_app">
                      <div class="panel-heading">
                        <h3 class="panel-title">Biography </h3>
                      </div>
                      <div class="panel-body">
                       	<p><?php echo $user_details[0]['bio'];?></p>
                      </div>
                    </div>
                    
                    <div class="panel panel_app">
                      <div class="panel-heading">
                        <h3 class="panel-title">Certification  </h3>
                      </div>
                      <div class="panel-body">
                       	<p><?php echo $user_details[0]['certi'];?></p>
                      </div>
                    </div>
                    
                    <div class="panel panel_app">
                      <div class="panel-heading">
                        <h3 class="panel-title">Awards  </h3>
                      </div>
                      <div class="panel-body">
                       	<p><?php echo $user_details[0]['award'];?></p>
                      </div>
                    </div>
                    
                    <div class="panel panel_app">
                      <div class="panel-heading">
                        <h3 class="panel-title">Accomplishments   </h3>
                      </div>
                      <div class="panel-body">
                       	<p><?php echo $user_details[0]['accomplish'];?></p>
                      </div>
                    </div>
                    
                    <div class="panel panel_app">
                      <div class="panel-heading">
                        <h3 class="panel-title">Location   </h3>
                      </div>
                      <div class="panel-body">
                       	<p><?php echo $user_details[0]['loc'];?></p>
                      </div>
                    </div>
                    
                    <div class="panel panel_app">
                      <div class="panel-heading">
                        <h3 class="panel-title">Credentials</h3>
                      </div>
                      <div class="panel-body">
                       	<p><?php echo $user_details[0]['credential'];?></p>
                      </div>
                    </div>
                    
                    <div class="panel panel_app">
                      <div class="panel-heading">
                        <h3 class="panel-title"> Interests & hobby    </h3>
                      </div>
                      <div class="panel-body">
                       	<p><?php echo $user_details[0]['hob'];?></p>
                        <p><?php echo $user_details[0]['intrest'];?></p>
                      </div>
                    </div>
                    
                </div>
            </div>
            
        </div>
        
    </div>
    
    <!--Main container sec end-->
    
    <div class="clearfix"></div>
