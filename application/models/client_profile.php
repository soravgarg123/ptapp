 
	<!--Header sec start-->
    <header class="header_sect" id="header">
        <div class="clearfix"></div>       
          <div class="inner_head_sect">
          	<div class="container">
                  	<div class="profile_header">
                      	  <div class="col-sm-2">
                          	<span class="user_profile_pic">
                            <?php 
                            $img = $client_details[0]['user_pic'];
                            if($img != ""){?>
                            <img class=" img-responsive" src="<?php echo base_url(); ?>trainee/<?php echo $client_details[0]['user_pic'];?>" alt=""/>
                            <?php }else{?>
                            <img class=" img-responsive" src="<?php echo base_url(); ?>trainee/strainer1.jpg" alt=""/>
                              <?php } ?>
                            </span>
                          </div>
                          <div class="col-sm-8">
                          	<div class="user_profile_info">
                              	<h3><?php echo $client_details[0]['name']; ?></h3>
                                  <span class="user_degi">Student</span>
                                  <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur imperdiet volutpat orci, et consectetur nibh vulputate sed. Vestibulum mollis, nisl vel tristique congue, erat purus ullamcorper eros, non gravida risus elit sit amet justo. </p> 
                            </div>
                          </div>
                      </div>
              </div>
          </div>
    </header>
    <!--Header sec end-->

      <!-- ///////////////////// start Code for show flash messages /////////////////!-->
  <div class="main_container">
    
      <div class="container">
          <div class="row">
              <div class="col-sm-12">
                <?php if($this->session->flashdata('success'))
                { ?>
                <div class="alert alert-success" id="success">
                <a href="#" class="close" data-dismiss="alert" aria-label="close" title="close">×</a>
                  <h4>Success !</h4> <?php echo $this->session->flashdata('success'); ?>
                  </div>
               <?php    
                }
                 ?>

                 <?php if($this->session->flashdata('failure'))
                  { ?>
                  <div class="alert alert-danger" id="failure">
                  <a href="#" class="close" data-dismiss="alert" aria-label="close" title="close">×</a>
                    <h4>Failure !</h4> <?php echo $this->session->flashdata('failure'); ?>
                    </div>
                 <?php    
                  }
                 ?>
                
            <div id='view_calendar'></div>                    
                </div>
            </div>
            
        </div>
        
    </div>
<!-- /////////////////////End Code for show flash messages///////////////////// !-->

    <!--Main container sec start-->
    <div class="main_container">
      <div class="container">
            <div class="row">
              <div class="col-sm-3">
                    <div class="trainee_tabs_sect">
                        <h3>Client Details</h3>
                        <!-- Nav tabs -->
                        <?php $user_type=$this->session->userdata('user_type');  ?>
                          <ul class="nav_tabs">
                            <li class="active"><a href="#informaiton" aria-controls="informaiton" role="tab" data-toggle="tab">Client Informaiton</a></li>
                            <?php
                             if($user_type == "trainee"){                           
                            ?>
                            <li><a href="#biography" aria-controls="biography" role="tab" data-toggle="tab">Add Nutrition</a></li>
                            <?php }?>
                             <li onclick="loadDoc()"><a href="#awards" aria-controls="awards" role="tab" data-toggle="tab">View Nutrition</a></li>
                            <?php
                             if($user_type == "trainee"){                           
                            ?>
                            <li><a href="#accomplishments" aria-controls="accomplishments" role="tab" data-toggle="tab">Add Workout</a></li>
                            <?php }?>
                            <!-- ajay 3 oct -->
                            <li onclick="client_plan()"><a href="#location" aria-controls="location" role="tab" data-toggle="tab">Training Plan</a></li>
                             <li ><a href="#meal" aria-controls="location" role="tab" data-toggle="tab">Meal Plan</a></li>
                             <!-- ajay 3 oct end-->
                             <li onclick="show_image()"><a href="#credentials" aria-controls="credentials" role="tab" data-toggle="tab">Add After/Before Images</a></li>
                            
                            <!-- <li><a href="#hobby" aria-controls="hobby" role="tab" data-toggle="tab"> Interests & hobby</a></li>
                            <li><a href="#interests" aria-controls="interests" role="tab" data-toggle="tab">interests</a></li> -->
                          </ul>
                    </div>
                </div>
                <div class="col-sm-9">
                  <div class="trainee_tab_content">
                        <!-- Tab panes -->
                          <div class="tab-content">
                            <div role="tabpanel" class="tab-pane active" id="informaiton">
                              <h3 class="trai_title_sect">Client Informaiton</h3>
                                <table class="table table-bordered prifile-input-field">
                                       <tr>
                                           <th><h3 class="panel-title">Age </h3></th>
                                           <td><?php echo $client_details[0]['age'];?></td>
                                       </tr>
                                       <tr>
                                           <th><h3 class="panel-title">Height</h3></th>
                                           <td><?php echo $client_details[0]['height'];?></td>
                                       </tr>
                                       <tr>
                                           <th><h3 class="panel-title">Width</h3></th>
                                           <td><?php echo $client_details[0]['weight'];?></td>
                                       </tr>
                                       <tr>
                                           <th><h3 class="panel-title">Bmr</h3></th>
                                           <td><?php echo $client_details[0]['bmr'];?></td>
                                       </tr>
                                       <tr>
                                           <th><h3 class="panel-title">Skinfolds</h3></th>
                                           <td><?php echo $client_details[0]['skinfolds'];?></td>
                                       </tr>
                                       <tr>
                                           <th><h3 class="panel-title">Bicep</h3></th>
                                           <td><?php echo $client_details[0]['bicep'];?></td>
                                       </tr>
                                       <tr>
                                           <th><h3 class="panel-title"> Tricep</h3></th>
                                           <td><?php echo $client_details[0]['tricep'];?></td>
                                       </tr>
                                       <tr>
                                           <th><h3 class="panel-title"> Abs</h3></th>
                                           <td><?php echo $client_details[0]['abs'];?></td>
                                       </tr>
                                       <tr>
                                           <th><h3 class="panel-title"> Subscapular</h3></th>
                                           <td><?php echo $client_details[0]['subscapular'];?></td>
                                       </tr>
                                       <tr>
                                           <th><h3 class="panel-title"> Chin</h3></th>
                                           <td><?php echo $client_details[0]['chin'];?></td>
                                       </tr>
                                       <tr>
                                           <th><h3 class="panel-title"> Cheek</h3></th>
                                           <td><?php echo $client_details[0]['cheek'];?></td>
                                       </tr>
                                       <tr>
                                           <th><h3 class="panel-title"> Pectoral</h3></th>
                                           <td><?php echo $client_details[0]['pectoral'];?></td>
                                       </tr>
                                       <tr>
                                           <th><h3 class="panel-title"> Suprailliac</h3></th>
                                           <td><?php echo $client_details[0]['Suprailliac'];?></td>
                                       </tr>
                                       <tr>
                                           <th><h3 class="panel-title"> Mid-axillary</h3></th>
                                           <td><?php echo $client_details[0]['mid-axillary'];?></td>
                                       </tr>
                                       <tr>
                                           <th><h3 class="panel-title">Umbilical</h3></th>
                                           <td><?php echo $client_details[0]['umbilical'];?></td>
                                       </tr>
                                       <tr>
                                           <th><h3 class="panel-title">Knee</h3></th>
                                           <td><?php echo $client_details[0]['knee'];?></td>
                                       </tr>
                                       <tr>
                                           <th><h3 class="panel-title">Medial-calf</h3></th>
                                           <td><?php echo $client_details[0]['medial-calf'];?></td>
                                       </tr>
                                       <tr>
                                           <th><h3 class="panel-title">Quadriceps</h3></th>
                                           <td><?php echo $client_details[0]['quadriceps'];?></td>
                                       </tr>
                                       <tr>
                                           <th><h3 class="panel-title">Hamstrings</h3></th>
                                           <td><?php echo $client_details[0]['hamstrings'];?></td>
                                       </tr>
                                       <tr>
                                           <th><h3 class="panel-title">Goals</h3></th>
                                           <td><?php echo $client_details[0]['goals'];?></td>
                                       </tr>
                                       <tr>
                                           <th><h3 class="panel-title">Notes</h3></th>
                                           <td><?php echo $client_details[0]['notes'];?></td>
                                       </tr>
                                </table>
                            </div>
                            <div role="tabpanel" class="tab-pane" id="biography">
                              <form method="post" id="nutrition_dairy">
                              <div id="nutrition">Your details save successfully</div>
                              <h3 class="trai_title_sect">Add Nutrition</h3>
                                <?php $current_date_time = date("Y-m-d h:i:sa");?>
                                <input type="hidden" name="date" id="date" value="<?php echo $current_date_time;?>"> 
                                <input type="hidden" name="user_id" id="user_id" value="<?php echo $client_details[0]['user_id'];?>">
                                <input type="hidden" name="name" id="name" value="<?php echo $client_details[0]['name'];?>">  
                                <div class="form_wrapper">
                                    <div class="form-group">
                                        <label>Title</label>
                                        <input type="text" autofocus placeholder="Title" class="form-control"  id="title" name="title" autocomplete="off">
                                    </div> 
                                    <div class="form-group">
                                        <label>Category</label>
                                         <select class="form-control" autocomplete="off" name="category" required="">
                                             <option value="">Select Category</option>
                                             <option value="breakfast">Breakfast</option>
                                             <option value="lunch">Lunch</option>
                                             <option value="dinner">Dinner</option>
                                         </select>
                                    </div>
                                    <div class="form-group">
                                        <label>Calory</label>
                                        <input type="text" autofocus  placeholder="Calory" class="form-control"  id="calory" name="calory" autocomplete="off">
                                    </div> 
                                    <div class="form-group">
                                        <label>Description</label>
                                        <textarea class="form-control" name="description" id="description"></textarea>
                                    </div>
                                  </div> 
                                  <button id="submit" class="btn submit_btn" type="button">Submit</button>
                                </form>
                            </div>
                            <div role="tabpanel" class="tab-pane" id="awards">
                              <h3 class="trai_title_sect">View Nutrition</h3>
                              <form id="search_date" method="post">
                                <table class="table prifile-input-field">
                                       <tr>
                                       <link rel="stylesheet" href="<?php echo base_url(); ?>css/jquery-ui.css">
                                       <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7/jquery.min.js"></script>
                                       <script>
                                         $(document).ready(function() {
                                         $( "#datepicker" ).datepicker({dateFormat: 'yy-mm-dd'});
                                          });
                                       </script>
                                            <td><input type="text" id="datepicker" name="datepicker" class="form-control" placeholder="Select Date"></td>
                                            <td><button id="search" class="btn submit_btn" type="button">Search</button></td>
                                       </tr>
                                </table>
                                </form>
                                <table class="table table-bordered prifile-input-field" id="demo"></table>
                            </div>
                            <div role="tabpanel" class="tab-pane" id="accomplishments">
                                <form method="post" id="workout-dairy">
                              <div id="workout">Your details save successfully</div>
                              <h3 class="trai_title_sect">Add Workout</h3>
                                <?php $current_date_time = date("Y-m-d h:i:sa");?>
                                <input type="hidden" name="date" id="date" value="<?php echo $current_date_time;?>"> 
                                <input type="hidden" name="user_id" id="user_id" value="<?php echo $client_details[0]['user_id'];?>">
                                <input type="hidden" name="name" id="name" value="<?php echo $client_details[0]['name'];?>">  
                                <div class="form_wrapper">
                                    <div class="form-group">
                                        <label>Weight</label>
                                        <input type="text" autofocus placeholder="Weight" class="form-control"  id="weight" name="weight" autocomplete="off">
                                    </div> 
                                    <div class="form-group">
                                        <label>Sleep(hrs)</label>
                                         <input type="text" autofocus placeholder="Sleep" class="form-control"  id="sleep" name="sleep" autocomplete="off">
                                    </div>
                                    <div class="form-group">
                                        <label>Calories</label>
                                        <input type="text" autofocus  placeholder="Calories" class="form-control"  id="calories" name="calories" autocomplete="off">
                                    </div> 
                                    <div class="form-group">
                                        <label>Notes</label>
                                        <textarea class="form-control" placeholder="Notes" name="notes" id="notes"></textarea>
                                    </div>
                                  </div> 
                                  <button id="workout-submit" class="btn submit_btn" type="button">Submit</button>
                                </form>
                            </div>
                           <!--  ajay 3 oct -->
                            <div role="tabpanel" class="tab-pane" id="location">
                              <h3 class="trai_title_sect">Training Plan</h3>
                                 <table class="table table-bordered prifile-input-field" id="client_plan_result"></table>
                            </div>
                            <!--  ajay 3 oct end-->
                            <div role="tabpanel" class="tab-pane" id="credentials">
                              <h3 class="trai_title_sect">Add After/Before Images</h3>
                              <form method="post" id="add_image">
                              <div id="images">Image Added Successfully</div>
                                <div class="form_wrapper">
                                    <div class="form-group">
                                   
                                         <div class="photo_upload_sect">
                                                  <input type="file" value="" id="user_pic" name="user_pic">   
                                                  <input type="hidden" name="user_id" id="user_id" value="<?php echo $client_details[0]['user_id'];?>">      
                                                    <span class="fa fa-cloud-upload"></span>
                                                    <h5>Upload your Photo</h5>
                                         </div> 
                                         <div id="upload_images">
                                        

                                         </div>
                                    </div>
                                </div>
                                <button id="image" class="btn submit_btn" type="button">add</button>
                                </form>
                            </div>
                            <div role="tabpanel" class="tab-pane" id="hobby">
                              <h3 class="trai_title_sect">Hobby</h3>
                                <div class="form_wrapper">
                                    <div class="form-group">
                                        <textarea class="form-control" cols="5" role="4"> </textarea>
                                    </div>
                                </div>
                            </div>
                            <div role="tabpanel" class="tab-pane" id="interests">
                              <h3 class="trai_title_sect">Interests</h3>
                                <div class="form_wrapper">
                                    <div class="form-group">
                                        <input type="text" class="form-control" value="show meal plan"/>
                                    </div>
                                </div>
                            </div>
 <!-- ///////////////////////////code written by Aditya //////////////////////////// !-->                        
                           <!-- !-->


                            <div role="tabpanel" class="tab-pane" id="meal">
                              <h3 class="trai_title_sect">Meal Plan</h3>
                              <?php if($this->session->userdata('user_type') == "trainner") {?>
                              <input type="button" data-toggle="modal" data-target="#myModal" style="float:right;" class="btn-primary" id="add_meal_plans" value="Add Meal Plans"> 
                              <?php } ?>
                                 <div class="form_wrapper">
                                    <div class="form-group">

                                        <table id="example" class="table table-striped table-bordered">
                                        <thead>
                                          <tr id="">
                                            <th>Row</th>
                                            <th>Sun</th>
                                            <th>Mon</th>
                                            <th>Tue</th>
                                            <th>Wed</th>
                                            <th>Thu</th>
                                            <th>Fri</th>
                                            <th>Sat</th>
                                          </tr>
                                        </thead>
                                        <tbody>
                                        <?php
                                        if(!empty($meal)) { ?>
                                          <tr id="row1">
                                            <td>Row1</td>
                                            <td colspan="7"></td>
                                          </tr> 
                                          <tr id="row2">
                                            <td>Row2</td>
                                            <td colspan="7"></td>
                                          </tr>
                                          <tr id="row3">
                                            <td>Row3</td>
                                            <td colspan="7"></td>
                                          </tr>
                                          <tr id="row4">
                                            <td>Row4</td>
                                            <td colspan="7"></td>
                                          </tr>
                                          <tr id="row5">
                                            <td>Row5</td>
                                            <td colspan="7"></td>
                                          </tr>
                                          <tr id="row6">
                                            <td>Row6</td>
                                            <td colspan="7"></td>
                                          </tr>
                                          <tr id="row7">
                                            <td>Row7</td>
                                            <td colspan="7"></td>
                                          </tr>
                                          <tr id="row8">
                                            <td>Row8</td>
                                            <td colspan="7"></td>
                                          </tr>
                                          <?php }else { ?>
                                          <tr>
                                            <td class="bg-danger" colspan="8">No Record Found</td>
                                          </tr>
                                            <?php } ?>
                                        </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                           <!-- !-->

<!-- ///////////////////start of Add meal plan modal ///////////////////////// !-->
<?php if($this->session->userdata('user_type') == "trainner"){  ?>
      <!-- Book Appoinment Modal Start -->
        <div class="modal fade" id="meal_Modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
          <div class="modal-dialog" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Add Meal</h4>
              </div>
              <div class="modal-body">
                <form method="post" action="" >

                <div class="col-md-12">                        
                      <div class="col-md-4">Name</div>
                      <div class="col-md-8">
                         <input type="text" name="name" id="name" class="form-control"/>
                        <span class="error"></span>
                      </div>
                  </div></br></br>

                  <div class="col-md-12">                        
                      <div class="col-md-4"> Client</div>
                      <div class="col-md-8">
                      <?php 
                      $data = array(null =>  'Select Trainee');
                      if(isset($trainee))
                      {
                        foreach($trainee as $a)
                        {
                          $data[$a['user_id']] = $a['name'];
                        }
                        echo form_dropdown('trainee', $data, '', 'class="form-control" required');
                      }
                      ?>
                      </div>
                  </div></br></br>
                  <div class="col-md-12">                        
                      <div class="col-md-4">Category</div>
                      <div class="col-md-8">
                      <?php
                        unset($data);
                        $data=  array(null => 'Select Category');
                        if(isset($category))
                        {
                          foreach($category as $a){
                            $data[$a['id']] = $a['name'];
                          }
                          echo form_dropdown('category', $data, '', 'class="form-control" required');
                        }
                      ?>  
                      </div> 
                  </div></br></br>
                  <div class="col-md-12">                        
                      <div class="col-md-4">Start Date</div>
                      <div class="col-md-8">
                        <input type="text"  id="start_date" required="required" name="start_date" class="form-control" />
                      </div>
                  </div></br></br>
                  <div class="col-md-12">                        
                      <div class="col-md-4">End Date</div>
                      <div class="col-md-8">
                        <input type="text"  id="end_date" name="end_date" class="form-control" />
                      </div>
                  </div></br></br>

                    <div class="col-md-12">                        
                      <div class="col-md-4">Row</div>
                      <div class="col-md-8">
                         <select  id="row"  name="row" class="form-control" />
                         <option value="0">Select Row</option>
                         <option value="row1">Row1</option>
                         <option value="row2">Row2</option>
                         <option value="row3">Row3</option>
                         <option value="row4">Row4</option>
                         <option value="row5">Row5</option>
                         <option value="row6">Row6</option>
                         <option value="row7">Row7</option>
                         <option value="row8">Row8</option>
                         </select> 
                        <span class="error"></span>
                      </div>
                  </div></br></br>
               </div></br>
              <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="submit" id="submit-btn" class="btn btn-primary">Submit</button>
              </div>
              </form>
            </div>
          </div>
        </div>
<?php  } ?>
<!-- ///////////////////End of Add meal plan modal/////////////////////////////////////////// !-->
 
<!-- //////////////////////////////end of code//////////////////////////////////  !-->
                          </div>
                          
                          <div class="clearfix"></div>
                        <!-- <a class="btn submit_btn" href="#">Update</a> -->
                    </div>
                    
                </div>
            </div>
            
        </div>
        
    </div>
    
    <!--Main container sec end-->
    
   
    <div class="clearfix"></div>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script type="text/javascript">
    $(document).ready(function(){
      
         $(".tab-pane").on('click','#submit',function() {
         
            var data1 = new FormData($('#nutrition_dairy')[0]);
           $.ajax({
             url:"<?php echo base_url() ?>user/add_nutrition",
             type:"post",
             data: data1,
             contentType: false,
             processData: false,
             success: function(response)
             {
               $('#nutrition').show();
               $('#nutrition_dairy')[0].reset();
               setTimeout(function() { $("#nutrition").fadeOut(1500); }, 5000)
             }
         });
     
         });
      
    });
</script>
<script>
function loadDoc() {
  var user_id = document.getElementById("user_id").value;
  var name = document.getElementById("name").value;
  var xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function() {
    if (xhttp.readyState == 4 && xhttp.status == 200) {
       document.getElementById("demo").innerHTML = xhttp.responseText;

    }
  }
  xhttp.open("GET", "<?php echo base_url();?>user/search_nutrition_details?user_id="+user_id+"&name="+name, true);
  xhttp.send();
}
</script>
<script type="text/javascript">
    $(document).ready(function(){
      
         $(".tab-pane").on('click','#search',function() {
         
          var date = $('#datepicker').val();
          var name = $('#name').val();
         
          
           $.ajax({
             url:"<?php echo base_url() ?>user/search_date_nutrition_details?date="+date+"&name="+name,
             type:"post",
             data: "date="+date,
             contentType: false,
             processData: false,
             success: function(data)
             {

             
              $('#demo').html(data);
              
             }
         });
     
         });
      
    });
</script>
<script type="text/javascript">
    $(document).ready(function(){
      
         $(".tab-pane").on('click','#workout-submit',function() {
         
            var data1 = new FormData($('#workout-dairy')[0]);
           $.ajax({
             url:"<?php echo base_url() ?>user/add_workout",
             type:"post",
             data: data1,
             contentType: false,
             processData: false,
             success: function(response)
             {
               $('#workout').show();
               $('#workout-dairy')[0].reset();
               setTimeout(function() { $("#workout").fadeOut(1500); }, 5000)
             }
         });
     
         });
      
       /* ------------------------Code Written by Aditya ---------------------
  ------------------------script start for meal plan--------------------------
  */
  $('#add_meal_plans').click(function(){
    $('#meal_Modal').modal('show');
    $('#start_date').datepicker({daysOfWeekDisabled: [0,2,3,4,5,6], format : 'd M yyyy'});
    

     /*$('#start_date').datepicker({
        onSelect: function(dateStr) {alert();
          var d = $.datepicker.parseDate('mm/dd/yy', dateStr);
          d.setDate(d.getDate() + 3); // Add three days
          $('#end_date').datepicker('setDate', d);
        }
      });*/
        $('#start_date').blur(function(){
          var date = new Date($('#start_date').val());
          date.setDate(date.getDate()+7);
          var str = date.toString();
          var a = new Array();
          var a = str.split(" ", 4);
         $('#end_date').val(a[2]+' '+a[1]+ ' '+a[3]);
        });
    
  });
  /*
-------------------------script end for meal plan-------------------------------
  */
/* ------------------------start  script for flash messages-------------------
*/
  setTimeout(function(){
    $('#success').hide();
  }, 3000);

    setTimeout(function(){
    $('#failure').hide();
  }, 3000);
   /*---------------end script for flash messages---------------------*/
   /*
--------------------------start code for meal plan----------------------------------- 
   */
  // alert('<?php echo json_encode("hi"); ?>');
   var meal = '<?php echo json_encode($meal); ?>';
   var obj = JSON.parse(meal);

   $.each(obj, function(key, value){
    if(value.row == 'row1')
    { 
      //$('#row1 td:not(:first)').append();
      $('#row1 td:not(:first)').append(value.name+'-' + value.cname+'|').addClass('bg-primary');
    }
    else if(value.row == 'row2')
    {
      $('#row2 td:not(:first)').append(value.name+'-' + value.cname +'|').addClass('bg-success');
    } 
    else if(value.row == 'row3')
    {
      $('#row3 td:not(:first)').append(value.name+'-' + value.cname +'|').addClass('bg-info');
    }
    else if(value.row == 'row4')
    {
      $('#row4 td:not(:first)').append(value.name+'-' + value.cname +'|').addClass('bg-warning');
    }
    else if(value.row == 'row5')
    {
      $('#row5 td:not(:first)').append(value.name+'-' + value.cname +'|').addClass('bg-danger');
    }
    else if(value.row == 'row6')
    {
      $('#row6 td:not(:first)').append(value.name+'-' + value.cname +'|').addClass('bg-custom1');
    }
    else if(value.row == 'row7')
    {
      $('#row7 td:not(:first)').append(value.name+'-' + value.cname +'|').addClass('bg-custom2');
    }
    else if(value.row == 'row8')
    {
      $('#row8 td:not(:first)').append(value.name+'-' + value.cname +'|').addClass('bg-custom3');
    } 
   });
   /*
--------------------------end code for meal plan----------------------------------- 
   */
/*---------------------------  end of code---------------------------------------- 
   */ 

    });
</script>
<!-- ajay 3 oct -->
<script>
function client_plan() {

  var user_id = document.getElementById("user_id").value;
  var name = document.getElementById("name").value;
  var xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function() {
    if (xhttp.readyState == 4 && xhttp.status == 200) {
       document.getElementById("client_plan_result").innerHTML = xhttp.responseText;

    }
  }
  xhttp.open("GET", "<?php echo base_url();?>user/search_client_plan?user_id="+user_id+"&name="+name, true);
  xhttp.send();
}
</script>
<!-- ajay 3 oct end-->
<script type="text/javascript">
    $(document).ready(function(){
      
         $(".tab-pane").on('click','#image',function() {
            var data1 = new FormData($('#add_image')[0]);
           $.ajax({
             url:"<?php echo base_url();?>user/add_image",
             type:"post",
             data: data1,
             contentType: false,
             processData: false,
             success: function(data)
             {
                $("#upload_images").html(data);
                $('#images').show();
                $('#workout-dairy')[0].reset();
                setTimeout(function() { $("#images").fadeOut(1500); }, 5000)
             }
         });
     
         });
      
    });
</script>
<script>
function show_image() {

  var user_id = document.getElementById("user_id").value;
  var xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function() {
    if (xhttp.readyState == 4 && xhttp.status == 200) {
       document.getElementById("upload_images").innerHTML = xhttp.responseText;

    }
  }
  xhttp.open("GET", "<?php echo base_url();?>user/show_image?user_id="+user_id, true);
  xhttp.send();
}
</script>
