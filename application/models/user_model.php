<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class User_model extends CI_Model {//

	function __construct(){
			    parent::__construct();
			    $this->load->library('session');
			     $this->load->library('email');
	}
	function signup(){
				$user_type = $this->input->post('user_type');
				$current_date = date("Y-m-d H:i:s");
				$status = 0;
			    $data=array('user_type' => $this->input->post('user_type'),
			                'email' => $this->input->post('email'),	 
						    'password' => md5($this->input->post('password')),
						    'status' => $status,
						    'publish_date' => $current_date,
						    'start_date' => $current_date,
						    'modify_date' => $current_date,
						    'payment_status' => 2
						  );
	            $this->db->insert('users',$data);
	            $last_insert_id = $this->db->insert_id();
                 if($user_type == "trainee"){
		        $data1=array('user_id' => $last_insert_id,
			  	              'name' => $this->input->post('name'),
			  			      // 'surname' => $this->input->post('surname'),
			  			      'user_pic'=>'default.png',
						      'email' => $this->input->post('email'),
						      'country' => $this->input->post('country'),
						      'password' => md5($this->input->post('password')),
						      'status' => $status,				  
						      'publish_date' => $current_date,
						      'modify_date' => $current_date,
						   );
		        $this->db->insert('trainee',$data1);

	          }else if($user_type == "trainner"){
                $data2=array('user_id' => $last_insert_id,
                   	            'user_type' => 'trainner',
			  	                'name' => $this->input->post('name'),
			  	                'occupation' => $this->input->post('occupation'),
			  	                'user_pic'=>'default.png',
			  	                'location' => $this->input->post('location'),
			  	                'qualifications' => $this->input->post('qualifications'),
			  			        'surname' => $this->input->post('surname'),
						        'email' => $this->input->post('email'),
						        'country' => $this->input->post('country'),
						        'password' => md5($this->input->post('password')),
						        'status' => $status,				  
						        'publish_date' => $current_date,
						        'modify_date' => $current_date,
						        'trainer_payment_status' => 2
						      );
		        $this->db->insert('trainner',$data2);
		        return $last_insert_id;
	          }
	}
	function check_user_exist($email){
		        $email=$this->input->post('email');		                  
		        $this->db->select('*');
                $this->db->from('users');
				$this->db->where('email',$email);				 
				$query=$this->db->get();		
				if($query->num_rows()>0)
				{				
				    return true;
				}
				else
				{
				    return false;
				}
	}
	function get_country(){
				$this->db->order_by('country_name', 'asc'); 
				$query = $this->db->get('countries'); 
				return $query->result_array();
	}
	function get_services(){
				$this->db->order_by('id', 'desc'); 
				$query = $this->db->get('services'); 
				return $query->result_array();
	}
	function get_service(){
				$this->db->order_by('id', 'desc'); 
				$query = $this->db->get('services'); 
				return $query->result_array();
	}
	function service($id){
				$this->db->select('*');
                $this->db->from('services');
				$this->db->where('id',$id);				    
				$query=$this->db->get();
				return $query->result_array();
	}
	function get_plans(){
				$this->db->order_by('id', 'asc'); 
				$query = $this->db->get('plans'); 
				return $query->result_array();
	}
	function login($email,$password){
			$this->db->select('*');
	        $this->db->from('users');
			$this->db->where('email',$email);
			$this->db->where('password',$password);
			$query = $this->db->get();
			return $query->result_array();
	} 

	function add_profile_trainner(){
		        $user_id = $this->session->userdata('user_id');
				$this->db->select('*');
                $this->db->from('trainner');
				$this->db->where('user_id',$user_id);				    
				$query=$this->db->get();
				return $query->result_array();
	}
	function add_profile_trainee(){
		        $user_id = $this->session->userdata('user_id');
				$this->db->select('*');
                $this->db->from('trainee');
				$this->db->where('user_id',$user_id);				    
				$query=$this->db->get();
				return $query->result_array();
	}
	function edit_profile_trainee($user_id){
		        //$user_id = $this->session->userdata('user_id');
		       
				$this->db->select('*');
                $this->db->from('trainee');
				$this->db->where('user_id',$user_id);				    
				$query=$this->db->get();
				return $query->result_array();
	}
	function user_information_trainner($data){
		$file =$_FILES;
		$img ='';
  			if(!empty($file)){
		        if($_FILES['user_pic']['name'] != ""){
		               $img=$_FILES['user_pic']['name'];
		        }else{

		        	   $img=$_POST['user_pic'];
		        }		             
                $up=$_FILES['user_pic']['tmp_name'];  
                if(is_uploaded_file($up))
                  {
                    move_uploaded_file($up, "trainner/" .$img);
                  } 
               }
                $newdata = array('name' =>$data['name'] ,'surname' =>$data['surname'] ,'password' =>md5($data['new_password']),'user_pic' =>$img  ,'fb_url' =>$data['fb_url'],'twit_url' =>$data['twit_url'] ,'link_url' =>$data['link_url'],'youtube' =>$data['youtube'],'bio' =>$data['bio'] ,'certi' =>$data['certi'],'award' =>$data['award'] ,'accomplish' =>$data['accomplish'],'loc' =>$data['loc'] ,'credential' =>$data['credential'],'hob' =>$data['hob'] ,'intrest' =>$data['intrest'],'type' =>$data['user_type']);
		        $user_id = $this->input->post('user_id');                
	            $this->db->where('user_id', $user_id);
                $this->db->update('trainner', $newdata);     
	}
	function check_password_trainner($data){
	            $user_id = $this->session->userdata('user_id');
		        $current_password = $this->input->post('current_password');
		        $this->db->select('*');
                $this->db->from('trainner');
				$this->db->where('user_id',$user_id);
				$this->db->where('password',$current_password);	 
				$query=$this->db->get();
				if($query->num_rows()>0)
				  {
					return true;
				  }else{	
					return false;
						}
	}
	function user_information_trainee($data){
		        $img=$_FILES['user_pic']['name'];		             
                $up=$_FILES['user_pic']['tmp_name'];  
                if(is_uploaded_file($up))
                 {
                   move_uploaded_file($up, "trainee/" .$img);
                 }   
                $newdata = array('password' =>md5($data['new_password']),'user_pic' =>$img ,'rank_level' =>$data['rank_level'],'skill' =>$data['skill'] ,'fb_url' =>$data['fb_url'],'twit_url' =>$data['twit_url'] ,'link_url' =>$data['link_url']);
		        $user_id = $this->input->post('user_id');                
	            $this->db->where('user_id', $user_id);
                $this->db->update('trainee', $newdata);     
	}
	function check_password_trainee($data){
	            $user_id = $this->session->userdata('user_id');
		        $current_password = md5($this->input->post('current_password'));
		        $this->db->select('*');
                $this->db->from('trainee');
				$this->db->where('user_id',$user_id);
				$this->db->where('password',$current_password);	 
				$query=$this->db->get();
				if($query->num_rows()>0)
					{
					   return true;
					}else{
					   return false;
						 }
	}
	function trainner_details(){
		        $user_id = $this->session->userdata('user_id');
				$this->db->select('*');
                $this->db->from('trainner');		    				    
				$query=$this->db->get();
				return $query->result_array();
	}
	function search_trainner_details($search){
		        $user_id = $this->session->userdata('user_id');			    
                $query = $this->db->query("SELECT * from  trainner  where name Like '$search%'");				  
				return $query->result_array();
	}
	function trainee_details(){
		        $user_id = $this->session->userdata('user_id');
				$this->db->select('*');
                $this->db->from('trainee');		    				    
				$query=$this->db->get();
				return $query->result_array();
	}
	function search_trainee_details($search){
		        $user_id = $this->session->userdata('user_id');			    
                $query = $this->db->query("SELECT * from  trainee  where  name Like '$search%'");				  
				return $query->result_array();
	}
	function trainner_profile($trainner_id){
                $this->db->select('*');
                $this->db->from('trainner');
                $this->db->where('id',$trainner_id);	
				$query=$this->db->get();
				return $query->result_array();				
	}
	function hire_trainner($data){
                $trainner_id = $data["trainner_id"];
                $trainee_id = $data["trainee_id"];                   
                $this->db->select('*');
                $this->db->from('hire_trainers');
                $this->db->where('id',$trainner_id);
                $this->db->where('id',$trainee_id);	
				$query=$this->db->get();
				if($query->num_rows()>0)
				{	   
				  echo "true";
				}else{	    
				  $this->db->insert('hire_trainers',$data);
					 }
	}
	function trainee_upload_image(){
			    $trainee_id = $this->session->userdata('user_id');
                $image=$_FILES['photoimg']['name'];
                $current_date = date("Y-m-d H:i:s");
                $up=$_FILES['photoimg']['tmp_name']; 
                if(is_uploaded_file($up))
                     {
                        move_uploaded_file($up, "trainee_gallery/" .$image);
                     }
                $newdata = array('image_name' =>$image,'image_type' =>'0' ,'id' =>'0','id' =>$trainee_id ,'status' =>'0','added_date' =>$current_date ,'modify_date' =>$current_date);
                     $result = $this->db->insert('photos',$newdata);
                if($result){ 
                         echo "true";
                            }
	}
	function add_trainee1($data){ 
                $back = $this->input->post('back');
			    if($back == ''){ 
	            $current_date = date("Y-m-d H:i:s");
	            $status = 0;
	            $data1 = array('user_type'=>'trainee','email'=>$this->input->post('email'),'password'=>md5($this->input->post('password')),'status'=>$status,'publish_date'=>$current_date,'modify_date'=>$current_date,
	            	);
				$query=$this->db->insert('users', $data1);
				$last_insert_id = $this->db->insert_id();
	            $this->session->set_userdata('last_trainee_id',array('user_id'=>$last_insert_id));
	            $user_type = 'trainee';
	            if($_FILES['user_pic']['name'] != ""){
		               $img=$_FILES['user_pic']['name'];
		        }else{

		        	   $img=$_POST['user_pic'];
		        }		             
                $up=$_FILES['user_pic']['tmp_name'];  
                if(is_uploaded_file($up))
                  {
                    move_uploaded_file($up, "trainee/" .$img);
                  }
	            $data2=array('trainner_id' => $this->input->post('trainner_id'),
	            	                'user_id' => $last_insert_id,
	            	                'user_pic' => $img,
	            	                'user_type' => $user_type,
				  	                'name' => $this->input->post('name'),
				  			        'email' => $this->input->post('email'),
							        'password' => md5($this->input->post('password')),
							        'age' => $this->input->post('age'),
							        'height' => $this->input->post('height'),
							        'width' => $this->input->post('width'),
							        'rmr' => $this->input->post('rmr'),
							        'bp' => $this->input->post('bp'),
							        'rhr' => $this->input->post('rhr'),
							        'lbm' => $this->input->post('lbm'),
							        'ce' => $this->input->post('ce'),
							        'dmi' => $this->input->post('dmi'),
							        'rwi' => $this->input->post('rwi'),
							        'bmr' => $this->input->post('bmr'),
							        'goals' => $this->input->post('goals'),
							        'notes' => $this->input->post('notes'),
							        'status' => $status,		  
							        'publish_date' => $current_date,
							        'modify_date' => $current_date,
							      );
	            $query=$this->db->insert('trainee', $data2);
	            //$this->email->from('your@example.com', 'Your Name');
				//$this->email->to('ajay.coderevolts@gmail.com');
				//$this->email->cc('ajay.coderevolts@gmail.com');
				//$this->email->bcc('ajay.coderevolts@gmail.com');
				//$this->email->subject('Email Test');
				//$this->email->message('Testing the email class.');
				//$this->email->send();  

                }else{
	            $current_date = date("Y-m-d H:i:s");
	            $status = 0;
	            $user_details=$this->session->userdata('last_trainee_id');
	            $user_id=$user_details['user_id'];
	            $data1 = array('user_type'=>'trainee','email'=>$this->input->post('email'),'password'=>$this->input->post('password'),'status'=>$status,'publish_date'=>$current_date,'modify_date'=>$current_date,
	            	);
	            $this->db->where('user_id', $user_id);
				$query=$this->db->update('users', $data1);
	            $user_type = 'trainee';
				$data2=array('trainner_id' => $this->input->post('trainner_id'),
					                'user_id' => $user_id,
	            	                'user_type' => $user_type,
				  	                'name' => $this->input->post('name'),
				  			        'email' => $this->input->post('email'),
							        'password' => $this->input->post('password'),
							        'age' => $this->input->post('age'),
							        'height' => $this->input->post('height'),
							        'width' => $this->input->post('width'),
							        'rmr' => $this->input->post('rmr'),
							        'bp' => $this->input->post('bp'),
							        'rhr' => $this->input->post('rhr'),
							        'lbm' => $this->input->post('lbm'),
							        'ce' => $this->input->post('ce'),
							        'dmi' => $this->input->post('dmi'),
							        'rwi' => $this->input->post('rwi'),
							        'bmr' => $this->input->post('bmr'),
							        'goals' => $this->input->post('goals'),
							        'notes' => $this->input->post('notes'),
							        'abs' => $data['abs'],
							        'subscapular' => $data['subscapular'],
							        'chin' => $data['chin'],
							        'chin' => $data['chin'],
							        'pectoral' => $data['pectoral'],
							        'Suprailliac' => $data['Suprailliac'],
							        'mid-axillary' => $data['mid-axillary'],
							        'umbilical' => $data['umbilical'],
							        'knee' => $data['knee'],
							        'medial-calf' => $data['medial-calf'],
							        'quadriceps' => $data['quadriceps'],
							        'hamstrings' => $data['hamstrings'],
							        'goals' => $data['goals'],
							        'notes' => $data['notes'],
							        'skinfolds' => $data['skinfolds'],
							        'bicep' => $data['bicep'],
							        'tricep' => $data['tricep'],
							        'status' => $status,		  
							        'publish_date' => $current_date,
							        'modify_date' => $current_date,
							      );
							      
				$this->db->where('user_id', $user_id);
				$query=$this->db->update('trainee', $data2);
				$user_details=$this->session->userdata('last_trainee_id');
	            $user_id=$user_details['user_id'];
	            $this->db->select('*');
		        $this->db->from('trainee');
		        $this->db->where('user_id',$user_id);
		        $query = $this->db->get();
		        return $query->row_array();
               }       
	}
	function edit_trainee1($data){ 
                $back = $this->input->post('back');
			    if($back == ''){ 
	            $current_date = date("Y-m-d H:i:s");
	            $status = 0;
	            $user_type=$this->session->userdata('user_type');
	            if($user_type == "trainner"){
                    $user_id = $this->input->post('trainee_id');
                   
	            }else{

	            	$user_id=$this->session->userdata('user_id');
	            }
	            
	            $data1 = array('user_type'=>'trainee','email'=>$this->input->post('email'),'password'=>$this->input->post('password'),'status'=>$status,'publish_date'=>$current_date,'modify_date'=>$current_date,
	            	);
				$this->db->where('user_id', $user_id);
				$query=$this->db->update('users', $data1);
	            $user_type = 'trainee';
	            $img='';
	            if(!empty($_FILES)){
	            if($_FILES['user_pic']['name'] != ""){
		               $img=$_FILES['user_pic']['name'];
		        }else{

		        	   $img=$_POST['user_pic'];
		        }		             
                $up=$_FILES['user_pic']['tmp_name'];  
                if(is_uploaded_file($up))
                  {
                    move_uploaded_file($up, "trainee/" .$img);
                  }
              }
	            $data2=array('trainner_id' => $this->input->post('trainner_id'),
	            	                'user_id' => $user_id,
	            	                'user_pic' => $img,
	            	                'user_type' => $user_type,
				  	                'name' => $this->input->post('name'),
				  			        'email' => $this->input->post('email'),
							        'password' => $this->input->post('password'),
							        'age' => $this->input->post('age'),
							        'height' => $this->input->post('height'),
							        'width' => $this->input->post('width'),
							        'rmr' => $this->input->post('rmr'),
							        'bp' => $this->input->post('bp'),
							        'rhr' => $this->input->post('rhr'),
							        'lbm' => $this->input->post('lbm'),
							        'ce' => $this->input->post('ce'),
							        'dmi' => $this->input->post('dmi'),
							        'rwi' => $this->input->post('rwi'),
							        'bmr' => $this->input->post('bmr'),
							        'goals' => $this->input->post('goals'),
							        'notes' => $this->input->post('notes'),
							        'status' => $status,		  
							        'publish_date' => $current_date,
							        'modify_date' => $current_date,
							      );

	            $this->db->where('user_id', $user_id);
				$query=$this->db->update('trainee', $data2);
                 
                $this->db->select('*');
                $this->db->from('trainee');
                $this->db->where('user_id',$user_id);	
				$query=$this->db->get();
				$result = $query->result_array();
              
				$trainner_id = $result[0]['trainner_id'];
				$trainee_pic = $result[0]['user_pic'];
				$trainee_email = $result[0]['email'];
				
				$this->db->select('*');
                $this->db->from('trainner');
                $this->db->where('user_id',$trainner_id);	
				$query=$this->db->get();
				$result1 = $query->result_array();
				$trainner_email = $result1[0]['email'];
                $status = 0;
                $current_date = date("Y-m-d H:i:s");
		        $value = $result[0]['name']." updated profile details";
		        $type = "Edit Profile";
		        $data1 = array('sender_id' => $user_id,'receiver_id' => $trainner_id,'parent_id'=>$user_id,'message' => $value,'type' => $type,'trainee_pic' => $trainee_pic,'status' => $status,'added_date' => $current_date);
	            $query=$this->db->insert('notification', $data1);

	            $this->email->from($trainee_email);
				//$this->email->to($trainner_email);
				//$this->email->cc('ajay@mobiweb.com');
				// $this->email->bcc('ajay.coderevolts@gmail.com');
				//$this->email->subject('Notification');
				//$this->email->message($value);
				//$this->email->send(); 

				$this->db->select('*');
		        $this->db->from('trainee');
		        $this->db->where('user_id',$user_id);
		        $query = $this->db->get();
		        return $query->row_array();

                }else{
	            $current_date = date("Y-m-d H:i:s");
	            $status = 0;
	            $user_id=$this->session->userdata('user_id');
	            $data1 = array('user_type'=>'trainee','email'=>$this->input->post('email'),'password'=>$this->input->post('password'),'status'=>$status,'publish_date'=>$current_date,'modify_date'=>$current_date,
	            	);
	            $this->db->where('user_id', $user_id);
				$query=$this->db->update('users', $data1);
	            $user_type = 'trainee';
				$data2=array('trainner_id' => $this->input->post('trainner_id'),
					                'user_id' => $user_id,
	            	                'user_type' => $user_type,
				  	                'name' => $this->input->post('name'),
				  			        'email' => $this->input->post('email'),
							        'password' => $this->input->post('password'),
							        'age' => $this->input->post('age'),
							        'height' => $this->input->post('height'),
							        'width' => $this->input->post('width'),
							        'rmr' => $this->input->post('rmr'),
							        'bp' => $this->input->post('bp'),
							        'rhr' => $this->input->post('rhr'),
							        'lbm' => $this->input->post('lbm'),
							        'ce' => $this->input->post('ce'),
							        'dmi' => $this->input->post('dmi'),
							        'rwi' => $this->input->post('rwi'),
							        'bmr' => $this->input->post('bmr'),
							        'skinfolds' => $this->input->post('skinfolds'),
							        'bicep' => $this->input->post('bicep'),
							        'tricep' => $this->input->post('tricep'),
							        'abs' => $data['abs'],
							        'subscapular' => $data['subscapular'],
							        'chin' => $data['chin'],
							        'chin' => $data['chin'],
							        'pectoral' => $data['pectoral'],
							        'Suprailliac' => $data['Suprailliac'],
							        'mid-axillary' => $data['mid-axillary'],
							        'umbilical' => $data['umbilical'],
							        'knee' => $data['knee'],
							        'medial-calf' => $data['medial-calf'],
							        'quadriceps' => $data['quadriceps'],
							        'hamstrings' => $data['hamstrings'],
							        'goals' => $data['goals'],
							        'notes' => $data['notes'],
							        'status' => $status,		  
							        'publish_date' => $current_date,
							        'modify_date' => $current_date,
							      );
				$this->db->where('user_id', $user_id);
				$query=$this->db->update('trainee', $data2);
				$user_id=$this->session->userdata('user_id');

	            $this->db->select('*');
		        $this->db->from('trainee');
		        $this->db->where('user_id',$user_id);
		        $query = $this->db->get();
		        return $query->row_array();
               }       
	}
	function add_trainee2($data){
	           $user_details=$this->session->userdata('last_trainee_id');
	           $user_id=$user_details['user_id'];
	           $this->db->where('user_id', $user_id);
	           $query=$this->db->update('trainee', $data);   
	}
	function edit_trainee2($data){
                 
    $data1 = array('skinfolds'=>$data['skinfolds'],'bicep'=>$data['bicep'],'tricep'=>$data['tricep'],'abs'=>$data['abs'],'subscapular'=>$data['subscapular'],'chin'=>$data['chin'],'cheek'=>$data['cheek'],'pectoral'=>$data['pectoral'],'Suprailliac'=>$data['Suprailliac'],'mid-axillary'=>$data['mid-axillary'],'arm'=>$data['arm'],'arm_right'=>$data['arm_right'],'chest'=>$data['chest'],'chest_right'=>$data['chest_right'],'waist'=>$data['waist'],'waist_right'=>$data['waist_right'],'hips'=>$data['hips'],'hips_right'=>$data['hips_right'],'thigh'=>$data['thigh'],'thigh_right'=>$data['thigh_right'],'calf'=>$data['calf'],'calf_right'=>$data['calf_right']);
		     
		       $user_type=$this->session->userdata('user_type');
		       if($user_type == "trainner"){
                    $user_id = $this->input->post('trainee_id');
                   
	            }else{

	            	$user_id=$this->session->userdata('user_id');
	            }
	           // $user_id=$this->session->userdata('user_id');
	           $this->db->where('user_id', $user_id);
	           $query=$this->db->update('trainee', $data1);

	          

	           $this->db->select('*');
		       $this->db->from('trainee');
		       $this->db->where('user_id',$user_id);
		       $query = $this->db->get();
		       return $query->row_array();   
	}
	function add_trainee3($data){
	           $user_details=$this->session->userdata('last_trainee_id');
	           $user_id=$user_details['user_id'];
	           $this->db->where('user_id', $user_id);
	           $query=$this->db->update('trainee', $data);   
	}
	function edit_trainee3($data){

     $data1 = array('umbilical'=>$data['umbilical'],'knee'=>$data['knee'],'medial-calf'=>$data['medial-calf'],'quadriceps'=>$data['quadriceps'],'hamstrings'=>$data['hamstrings']);
		
	           $user_type=$this->session->userdata('user_type');
		       if($user_type == "trainner"){
                    $user_id = $this->input->post('trainee_id');
                   
	            }else{

	            	$user_id=$this->session->userdata('user_id');
	            }
	           $this->db->where('user_id', $user_id);
	           $query=$this->db->update('trainee', $data1);  

              $this->db->select('*');
		       $this->db->from('trainee');
		       $this->db->where('user_id',$user_id);
		       $query = $this->db->get();
		       return $query->row_array();
	}
	function edit_trainee4($data,$trainee_id){
	           $user_id=$this->session->userdata('user_id');
	           $this->db->where('user_id', $trainee_id);
	           $query=$this->db->update('trainee', $data); 
              $this->db->select('*');
		       $this->db->from('trainee');
		       $this->db->where('user_id',$user_id);
		       $query = $this->db->get();
		       return $query->row_array();

	}
	function trainee_profile_back(){ 
		    $user_details=$this->session->userdata('last_trainee_id');
            $user_id=$user_details['user_id'];
            $this->db->select('*');
	        $this->db->from('trainee');
	        $this->db->where('user_id',$user_id);
	        $query = $this->db->get();
	        return $query->row_array();       
	}
	function edit_trainee_profile_back($user_id=""){ 
		    $user_type=$this->session->userdata('user_type');
			       if($user_type == "trainner"){
	                    $user_id = $user_id;
	                   
		            }else{

		            	$user_id=$this->session->userdata('user_id');
		            }
            $this->db->select('*');
	        $this->db->from('trainee');
	        $this->db->where('user_id',$user_id);
	        $query = $this->db->get();
	        echo $this->db->last_query();
	        // print_r($query->row_array());       
	}
	function client_list(){ 
                $user_id=$this->session->userdata('user_id');
		$this->db->select('*');
	        $this->db->from('trainee');
	         $this->db->where('trainner_id',$user_id);
	        $query = $this->db->get();
	        return $query->result_array();    	      
	}
	function search_client_details($search){
		        $user_id = $this->session->userdata('user_id');	
		      	    
                $query = $this->db->query("SELECT * from  trainee  where  trainner_id ='$user_id' and name Like '$search%'");				  
				return $query->result_array();
	}
	function client_profile($client_id){
                $this->db->select('*');
                $this->db->from('trainee');
                $this->db->where('user_id',$client_id);	
				$query=$this->db->get();
				return $query->result_array();
	}
	function client_workout($client_id){
                $this->db->select('*');
                $this->db->from('workout_details');
                $this->db->where('user_id',$client_id);	
				$query=$this->db->get();
				return $query->result_array();
	}
	function add_nutrition($data){
	
	                       
				$selected_date = $data["datepicker"];
	                        $newDate = date("Y-m-d", strtotime($selected_date));
				$trainee_id = $data['user_id'];
				$this->db->select('*');
				$this->db->from('trainee');
				$this->db->where('user_id',$trainee_id);	
				$query=$this->db->get();
				$result = $query->result_array();
				$trainner_id = $result[0]['trainner_id'];
				$trainee_pic = $result[0]['user_pic'];
				$trainee_email = $result[0]['email'];
				
				$this->db->select('*');
				$this->db->from('trainner');
				$this->db->where('user_id',$trainner_id);	
				$query=$this->db->get();
				$result1 = $query->result_array();
				//$trainner_email = $result1['email'];
				$status = 0;
				$current_date = date("Y-m-d H:i:s");
				$value = $data['name']." added nutrition";
				$type = "add nutrition";
				
				 $data2 = array('user_id'=>$data["user_id"],'name'=>$data["name"],'title'=>$data["title"],'category'=>$data["category"],'calory'=>$data["calory"],'description'=>$data["description"],'date'=>$data["date"],'selected_date'=>$newDate);
				 
			        $query=$this->db->insert('nutrition_details', $data2);
			        $last_insert_id = $this->db->insert_id();
			        $data1 = array('sender_id' => $data['user_id'],'receiver_id' => $trainner_id,'parent_id' => $last_insert_id,'message' => $value,'type' => $type,'trainee_pic' => $trainee_pic,'status' => $status,'added_date' => $current_date);
			        $query=$this->db->insert('notification', $data1);
                              return true;
			        //$this->email->from($trainee_email);
				//$this->email->to($trainner_email);
				//$this->email->cc('ajay@mobiweb.com');
				// $this->email->bcc('ajay.coderevolts@gmail.com');
				//$this->email->subject('Notification');
				//$this->email->message($value);
				//$this->email->send();  
			    	
	}
	function search_nutrition_details($user_id){

				$cdate = date("Y-m-d");
				$week =  date('W', strtotime($cdate));
				$year =  date('Y', strtotime($cdate));      
				$firstdayofweek = date("Y-m-d", strtotime("{$year}-W{$week}-0"));
				$lastdayofweek = date("Y-m-d", strtotime("{$year}-W{$week}-6"));
				
				$this->db->select('*');
				$this->db->from('nutrition_details');
				$this->db->where('user_id',$user_id);
				$this->db->where("selected_date between '$firstdayofweek' AND '$lastdayofweek'");
				$this->db->order_by("selected_date", "desc"); 	
				$query=$this->db->get();
				return $query->result_array();
	}
	function search_date_nutrition_details($date){
			   $query = $this->db->query("SELECT * FROM `nutrition_details` WHERE `date` ='$date'");
			   return $query->result_array();
	}
	function add_workout($data){

			    $query=$this->db->insert('workout_details', $data);	
			    	
	}
	function add_client_plan($data){

			    $query=$this->db->insert('client_plan', $data);	
			    	
	}
	function select_client($trainner_id){

			    $this->db->select('*');
			    $this->db->where('trainner_id',$trainner_id);
                $this->db->from('trainee');
				$query=$this->db->get();
				return $query->result_array();	
			    	
	}

	function client_quetion($user_id){

			    $this->db->select('*');
			    $this->db->where('trainee_id',$user_id);
                $this->db->from('quetion');
				$query=$this->db->get();
				return $query->result_array();	
			    	
	}

	function client_quetion_delete($id){
			$this->db->where('id', $id);
	        $this->db->delete('quetion');
	        if($this->db->affected_rows() > 0){
	        	return true;
	        }else{
	        	return false;
	        }
	          
	}

	function client_quetion_edit($id){

			    $this->db->select('*');
			    $this->db->where('id',$id);
                $this->db->from('quetion');
				$query=$this->db->get();
				return $query->result_array();	
			    	
	}
	public function update_client_qution($data){
	       $id= $data['id'];
           $this->db->where('id', $id);
	       $query=$this->db->update('quetion', $data);

           $this->db->select('*');
		   $this->db->from('quetion');
		   $this->db->where('id',$id);
		   $query = $this->db->get();
		   return $query->result_array();


	}

	function search_client_plan($user_id){


		        $cdate = date("Y-m-d");
				$week =  date('W', strtotime($cdate));
				$year =  date('Y', strtotime($cdate));      
				$firstdayofweek = date("Y-m-d", strtotime("{$year}-W{$week}-0"));
				$lastdayofweek = date("Y-m-d", strtotime("{$year}-W{$week}-6"));

		       
                $this->db->select('*');
                $this->db->from('client_plan');
                $this->db->where('user_id',$user_id);
                $this->db->where("selected_date between '$firstdayofweek' AND '$lastdayofweek'");	
                $this->db->order_by("selected_date", "desc"); 
				$query=$this->db->get();
				return $query->result_array();
	}
	function add_image($data){
                
                $user_id = $this->session->userdata('user_id');
                $trainee_id = $data['user_id'];
	            if($_FILES['user_pic']['name'] != ""){
		               $img=$_FILES['user_pic']['name'];
		        }else{

		        	   $img=$_POST['user_pic'];
		        }		             
                $up=$_FILES['user_pic']['tmp_name'];  
                if(is_uploaded_file($up))
                  {
                    move_uploaded_file($up, "add-after-before-image/" .$img);
                  } 
                     
                $newdata = array('trainner_id' =>$user_id,'trainee_id' =>$trainee_id,'image'=>$img);
                $this->db->where('user_id', $trainee_id);
                $q = $this->db->get('trainee');
                $data = $q->result_array();


                $this->db->select('*');
                $this->db->from('trainee');
                $this->db->where('user_id',$trainee_id);	
				$query=$this->db->get();
				$result = $query->result_array();
				$trainner_id = $result[0]['trainner_id'];
				$trainee_pic = $result[0]['user_pic'];
				$trainee_email = $result[0]['email'];
				
				$this->db->select('*');
                $this->db->from('trainner');
                $this->db->where('user_id',$trainner_id);
                $query=$this->db->get();
		 		$result1= $query->result_array();				
				$trainner_email = $result1[0]['email'];				
				
                $status = 0;
                $current_date = date("Y-m-d H:i:s");
		        $value = $data[0]['name']." added image";
		        $type = "add image";
		        
	            $query=$this->db->insert('add-after-before-image', $newdata); 
	             $last_insert_id = $this->db->insert_id();			        
	            $data1 = array('sender_id' => $trainee_id,'receiver_id' => $trainner_id,'parent_id' => $last_insert_id,'message' => $value,'type' => $type,'trainee_pic' => $trainee_pic,'status' => $status,'added_date' => $current_date);
	            $query=$this->db->insert('notification', $data1);

	            $this->email->from($trainee_email);
				//$this->email->to($trainner_email);
				//$this->email->cc('ajay@mobiweb.com');
				// $this->email->bcc('ajay.coderevolts@gmail.com');
				//$this->email->subject('Notification');
				//$this->email->message($value);
				//$this->email->send(); 

	            $last_insert_id = $this->db->insert_id();

	            $this->db->select('*');
                $this->db->from('add-after-before-image');
                $this->db->where('id',$last_insert_id);
				$query=$this->db->get();
				return $query->result_array();
      }
      function show_image(){

      	        $data = $_REQUEST['user_id'];
                $this->db->select('*');
                $this->db->from('add-after-before-image');
                $this->db->where('trainee_id',$data);
				$query=$this->db->get();
				return $query->result_array();  
	   }
	   function notification_time($data){

			    $query=$this->db->insert('nutrition_time', $data);	
			    	
	  }
	  public function nutrition_edit($id){
		$this->db->select('*');
		$this->db->from('nutrition_details');
		$this->db->where('id',$id);
		$query = $this->db->get();
		return $query->result_array();
	}
	public function update_nutrition_details($data){
		   $id= $data['id'];
           $this->db->where('id', $id);
	       $query=$this->db->update('nutrition_details', $data);

           $this->db->select('*');
		   $this->db->from('nutrition_details');
		   $this->db->where('id',$id);
		   $query = $this->db->get();
		   return $query->result_array();

	      
	}
	public function update_client_plan($data){
	       $id= $data['id'];
           $this->db->where('id', $id);
	       $query=$this->db->update('client_plan', $data);

           $this->db->select('*');
		   $this->db->from('client_plan');
		   $this->db->where('id',$id);
		   $query = $this->db->get();
		   return $query->result_array();


	}
	public function nutrition_delete($id){

			$this->db->where('id', $id);
	        $this->db->delete('nutrition_details'); 

	       
	       
	}
	public function training_plan_delete($id){

			$this->db->where('id', $id);
	        $this->db->delete('client_plan'); 

	       
	       
	}
	public function training_plan_edit($id){
		$this->db->select('*');
		$this->db->from('client_plan');
		$this->db->where('id',$id);
		$query = $this->db->get();
		return $query->result_array();
	}
	   
       public function nutrition_details_by_week($data){
       
                $start_date = $data['start_date'];               
                $end_date = $data['end_date']; 
                $user_id = $data['user_id'];                 
               
		$this->db->select('*');
		$this->db->from('nutrition_details');
		$this->db->where('user_id',$user_id);
		$this->db->where("selected_date between '$start_date' AND '$end_date'");
		$this->db->order_by("selected_date", "desc"); 	
		$query=$this->db->get();
		return $query->result_array();	
	       
	       
	}
	   public function trainning_plan_by_week($data){
       
                $plan_start_date = $data['plan_start_date'];
                $plan_end_date = $data['plan_end_date']; 
                $plan_user_id = $data['plan_user_id'];  
                               
               
		$this->db->select('*');
		$this->db->from('client_plan');
		$this->db->where('user_id',$plan_user_id);
		$this->db->where("selected_date between '$plan_start_date' AND '$plan_end_date'");
		$this->db->order_by("selected_date", "desc"); 	
		$query=$this->db->get();
		return $query->result_array();	
	       
	       
	}


	/*
-----------------------------------------------------------------------------
code written by Aditya Dubey start here
Date : 3 Nov 2015 
*/
	public function insert_appointment($form_data){
		if($this->db->insert('appointment', $form_data))
		{
			$this->db->select('tab1.email as trainee_email, tab1.name as trainee_name,
				tab2.email as trainner_email, tab2.name as trainner_name');
			$this->db->from('trainee as tab1, trainner as tab2');
			$this->db->where('tab1.user_id', $this->input->post('trainee'));
			$this->db->where('tab2.user_id', $this->session->userdata('user_id'));
			$data = $this->db->get();
			//echo $this->db->last_query(); exit();
			return $data->row_array();
		}
		else
		{
			return false;
		}
	}

	public function view_appointment(){
		$this->db->select('tab1.*, tab2.name as trainee_name');
		$this->db->from('appointment as tab1');
		$this->db->join('trainee as tab2', 'tab1.trainee_id = tab2.user_id');
		$this->db->where('tab1.active', 1);
		if($this->session->userdata('user_type') == "trainner")
		{
			$this->db->where('tab1.trainer_id', $this->session->userdata('user_id'));
		}
		else
		{
			$this->db->where('tab1.trainee_id', $this->session->userdata('user_id'));
		}
		
		$data = $this->db->get();
		//echo $this->db->last_query(); exit();
		return $data->result_array();
	}

	public function getBookSlotsData($app_id){
		$this->db->select('tab1.*, tab2.name as trainee_name');
		$this->db->from('appointment as tab1');
		$this->db->join('trainee as tab2', 'tab1.trainee_id = tab2.user_id');
		$this->db->where('tab1.id', $app_id);

		$data = $this->db->get();
		//echo $app_id; exit();
		return $data->row_array();
	}

	public function delete_book_app($app_id){
		$this->db->where('id', $app_id);
		if($this->db->delete('appointment'))
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	
	public function insert_payment($form_data){
		if($this->db->insert('payment', $form_data))
		{
			$this->db->select('trainner.email, trainner.name, plans.*');
			$this->db->from('trainner, plans');
			$this->db->where('trainner.user_id', $this->session->userdata('user_id'));
			$this->db->where('plans.id', $form_data['plan_id']);
			$data = $this->db->get();
			//echo $this->db->last_query(); exit();
			return $data->row_array();
		}
		else
		{
			return false;
		}
	}

	public function get_trainee(){
		$this->db->select('user_id, name');
		$this->db->from('trainee');
		$this->db->where('trainner_id', $this->session->userdata('user_id'));
		$data = $this->db->get();
		return $data->result_array();
	}
	public function get_trainee_new(){
		$this->db->select('user_id, name');
		$this->db->from('trainee');
		$this->db->where('user_id', $this->session->userdata('user_id'));
		$data = $this->db->get();
		return $data->result_array();
	}

	public function get_category(){
		$this->db->select('id, name');
		$this->db->from('category');
		$data = $this->db->get();
		return $data->result_array();
	}
	public function insert_meal_plan($form_data){
		if($this->db->insert('meal_plans', $form_data))
		{
			return true;
		}
		else
		{
			return false;
		}
	}

	public function get_meal_plans($client_id){
		$this->db->select('meal_plans.*, category.name as cname');
		$this->db->from('meal_plans');
		$this->db->join('category', 'meal_plans.category_id = category.id');
		$this->db->where('trainee_id', $client_id);
		$data = $this->db->get();
		return $data->result_array();
	}
	public function get_people_say(){
		$this->db->select('tab1.*');
		$this->db->from('people_say as tab1');
		$data = $this->db->get();
		return $data->result_array();
	}
	public function get_client_trainner($user_id){
		$this->db->select('*');
		$this->db->from('trainee');
		$this->db->where('user_id', $user_id);
		$data = $this->db->get();
		$return = $data->result_array();
		$trainner_id = $return[0]['trainner_id'];
		
		$this->db->select('*');
		$this->db->from('trainner');
		$this->db->where('user_id', $trainner_id);
		$data = $this->db->get();
		return $data->result_array();
		
	}
	

	/*
-----------------------------------------------------------------------------
code end here
Date : 3 Nov 2015 
*/
/*
---------------------Code written by Aditya Dubey -----------------------------
date : 21/11/2015
----------------------------- start here --------------------------------------- 
	*/
	public function get_setting(){
		if($this->session->userdata('user_type') == "trainner")
		{
			$col_name = "trainner_id";
			$table_name = "trainner_notification_setting";
		}
		else
		{
			$col_name = "trainee_id";
			$table_name = "trainee_notification_setting";
		}
		$this->db->select('*');
		$this->db->from($table_name);
		$this->db->where($col_name, $this->session->userdata('user_id'));
		$data = $this->db->get();
		return $data->row_array();
	}

	public function insert_setting($form_data, $table_name, $col_name, $q){
		if($q == "insert")
		{
			if($this->db->insert($table_name, $form_data))
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		else
		{
			$this->db->where($col_name, $this->session->userdata('user_id'));
			if($this->db->update($table_name, $form_data))
			{
				return true;
			}
			else
			{
				return false;
			}
		}
	}

	public function get_trainner_name(){
		$sql = " select concat(name, ' ',surname) as trainner_name, email
				from trainner 
				where user_id = '".$this->session->userdata('user_id')."' ";
		$data = $this->db->query($sql);
		//echo $this->db->last_query(); exit();
		return $data->row_array();
	}
	public function get_trainee_name($trainee_id){
		$sql = " select email
				from trainee 
				where user_id = '$trainee_id' ";
		$data = $this->db->query($sql);
		//echo $this->db->last_query(); exit();
		return $data->row_array();
	}

	public function get_why_us(){
		$this->db->select('*');
		$this->db->from('why_us');
		$data = $this->db->get();
		//echo $this->db->last_query(); exit();
		return $data->result_array();
	}
	/*public function insert_mail($mail_data, $trainner_email, $trainee_email){
		if($this->db->insert('notification', $mail_data))
		{
			return true;
		}
		else
		{
			return false;
		}
	}*/

/*
------------------START HERE date : 26/11/2015 --------------------------------------------- 
*/
	public function get_latest_dairy(){
		//$today = date('Y-m-d');

		$this->db->select('tab1.*, tab2.name as trainee_name, tab2.user_pic as trainee_pic');
		$this->db->from('appointment as tab1');
		$this->db->join('trainee as tab2', 'tab1.trainee_id = tab2.user_id');
		$this->db->where('tab1.trainer_id', $this->session->userdata('user_id'));
		//$this->db->having('count(tab1.id) <=', 3);
		$this->db->where('tab1.created_date >=', date('Y-m-d'));
		$this->db->order_by('tab1.id', 'ASC');
		$this->db->limit(3);
		$data = $this->db->get();
		//echo $this->db->last_query(); exit();
		return $data->result_array();
	}
/*
------------------END HERE date : 26/11/2015 --------------------------------------------- 
*/	
	/*
---------------------Code written by Aditya Dubey -----------------------------
date : 21/11/2015
----------------------------- end here --------------------------------------- 
	*/
	
	/*
----------------code added date 1/12/2015--------------------------------
	*/
	public function get_latest_update(){
		$this->db->select('*');
		$this->db->from('latest_update');
		$data =  $this->db->get();
		return $data->row_array();
	}
	public function get_banner(){
		$this->db->select('*');
		$this->db->from('banner_image');
		$data = $this->db->get();
		return $data->result_array();
	}

	public function get_product_banner(){
		$this->db->select('*');
		$this->db->from('product_banner_images');
		$this->db->where('status',1);
		$data = $this->db->get();
		return $data->result_array();
	}


	/*
---------Code Added Date 03/12/2015 -------------------------------------
----------------start function for edit meal plan -----------------------
	*/
	public function edit_meal_plan($plan_id, $form_data){
		$this->db->where('id', $plan_id);
		if($this->db->update('meal_plans', $form_data))
		{	
			return true;
		}
		else
		{
			return false;
		}
	}
	public function delete_meal_plan($id){
		$this->db->where('id', $id);
		if($this->db->delete('meal_plans'))
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	public function get_meal_by_id($client_id, $plan_id){
		$this->db->select('*');
		$this->db->from('meal_plans');
		$this->db->where('id', $plan_id);
		$this->db->where('trainee_id', $client_id);
		$data = $this->db->get();
		return $data->row_array();
	}
	/*
---------Code Added Date 03/12/2015 -------------------------------------
----------------end of function for edit meal plan -----------------------
	*/

/*
--------------------------------Date : 08-12-2015 ---------------------------
----------------start function for social link url ------------------------
*/
	public function get_social_link(){
		$this->db->select('*');
		$this->db->from('social_link');
		$this->db->group_by('name');
		$data = $this->db->get();
		
		return $data->result_array();
	}

	/* Start Text Chat Code By Sorav Garg */

	public function getPTDetails($cid)
	{
		$query = $this->db->query("SELECT `trainner_id` from  trainee   WHERE user_id = ".$cid);				  
		return $query->result_array();
	}

	public function getUserProfile($id){
		$this->db->select('*');
		$this->db->from('trainee');
		$this->db->where('user_id', $id);
		$query = $this->db->get();
		return $query->result_array();
	}

	public function getPTProfile($id){
		$this->db->select('*');
		$this->db->from('trainner');
		$this->db->where('user_id', $id);
		$query = $this->db->get();
		return $query->result_array();
	}

	public function insertChatMsg($data)
	{
		$this->db->insert('chating',$data);
		return $this->db->insert_id();
	}

	public function getChatingMsgs($own_id,$id){
		$query = $this->db->query("SELECT * from  chating   WHERE ( chat_sender_id = $own_id OR chat_reciever_id = $own_id ) AND ( chat_sender_id = $id OR chat_reciever_id = $id ) ");				  
		return $query->result_array();
	}

	/* End Text Chat Code By Sorav Garg */

	/*-----------code written by Aditya Dubey start here----------------
	-------------create buisness logic for profit and loss ----------
	*/

	public function getProfitLossData($id){
		$query = $this->db->query(" SELECT * FROM `profit_loss` AS `pl` INNER JOIN `profit_loss_detail` AS `pld` ON `pld`.`profit_id` = `pl`.`pl_id` WHERE `pld`.`profit_id` =  ".$id);
		return $query->result_array();
	}

	
	/*-----------code written by Aditya Dubey  end here----------------
	-------------create buisness logic for profit and loss ----------
	*/

	public function add_subscribe($form_data){
		if($this->db->insert('subscribe', $form_data))
		{
			return true;
		}
		else
		{
			return false;
		}
	}
}
