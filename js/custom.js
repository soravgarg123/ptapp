$(document).ready(function() {
  //js for page loading animation start
	 $(".animsition").animsition({
	  
		inClass               :   'fade-in',
		outClass              :   'fade-out',
		inDuration            :    1500,
		outDuration           :    800,
		linkElement           :   '.animsition-link',
		// e.g. linkElement   :   'a:not([target="_blank"]):not([href^=#])'
		loading               :    true,
		loadingParentElement  :   'body', //animsition wrapper element
		loadingClass          :   'animsition-loading',
		unSupportCss          : [ 'animation-duration',
								  '-webkit-animation-duration',
								  '-o-animation-duration'
								],
		//"unSupportCss" option allows you to disable the "animsition" in case the css property in the array is not supported by your browser.
		//The default setting is to disable the "animsition" in a browser that does not support "animation-duration".
		
		overlay               :   false,
		
		overlayClass          :   'animsition-overlay-slide',
		overlayParentElement  :   'body'
	  });

  //js for page loading animation end 
  
  //js for calling nice scorll start
   $("html").niceScroll({});
 //js for calling nice scorll end
      
	   $("#home_slider").owlCarousel({
	  items: 1,
      navigation : true,
      slideSpeed : 300,
      paginationSpeed : 400,
      singleItem : true,
	
      });
 	  
 
	  $("#brand_carousl").owlCarousel({
	  items: 6,
      navigation : true,
      slideSpeed : 300,
      paginationSpeed : 400,
      singleItem : false,
	  navigationText: ["<i class='fa fa-angle-left'></i>","<i class='fa fa-angle-right'></i>"]
      });
	  
	  $("#testimon_carousl").owlCarousel({
	  items: 1,
      navigation : true,
      slideSpeed : 300,
      paginationSpeed : 400,
      singleItem : false,
	  navigationText: ["<i class='fa fa-angle-left'></i>","<i class='fa fa-angle-right'></i>"]
      });
  
});



   

